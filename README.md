# Welcome to the STRÏMM codebase
![STRÏMM](logo/main_strimm_logo_with_text.png)

## What is STRÏMM?

STRÏMM (__S__ynchronised __T__race __R__ecording __I__n __I__mageJ and __M__icro__M__anager, pronounced like "stream") is a software application designed to perform synchronous acquisition of data from multiple, high throughput data sources into one program allowing users to view incoming data in real time. Data is then saved in easy to use formats for further offline analysis.

Under the hood STRÏMM is a Kotlin application (100% interoperable with Java) with the open source softwares of [ImageJ 2.0](https://imagej.net/) and [MicroManager 2.0](https://micro-manager.org/wiki/Version_2.0) being utilised within STRÏMM. The use of these programs and other areas such as the GUI and other features is underpinned by an actor-based framework called [Akka](https://akka.io/). This framework is responsible for handling all incoming data and running experiments. It does this through an actor based concept where actors are assigned roles and given messages to carry out specific tasks. It is lightweight, and has many features built in like thread safety and scalability.

## The codebase
The codebase is split into two main areas: STRIMM_IJ and STRIMM_JNI

#### STRIMM_IJ
This is the main section of the codebase. Code relating to running experiments, handling data, the user interface etc is housed here

#### STRIMM_JNI
With JNI standing for Java Native Interface. This is code written to interface between Java and C/C++. More specifically this is for communication with a National Instruments Data Acquisition Board (NIDAQ).

### Additional Directories (note - these are mostly relevant only when developing STRÏMM)
#### WorkingDirectory
This is intended to be the directory where STRÏMM will run. It will also be where any input and output files will be placed. Input files will be things such as experimental configurations and MicroManager configurations and drivers. Output files will be things such as experimental acquistion files.

#### MMCore
This directory contains a compiled version of MicroManager v2.0.0 beta 20180711.

#### Package
This directory contains scripts to package STRÏMM into an executable.

## Personnel
STRÏMM is a collaborative project between the University of St Andrews, University of Sheffield, University of Glasgow, and [Cairn Research Ltd](https://www.cairn-research.co.uk/)
### Developers
[Jacob Francis](https://pulverlab.wp.st-andrews.ac.uk/people/) (University of St Andrews)  
[Elliot Steele](https://ashleycadby.staff.shef.ac.uk/authors/elliot/) (University of Sheffield)  
[Allen Kelly](https://www.gla.ac.uk/researchinstitutes/icams/staff/allenkelly/) (University of Glasgow)

### Contributors
#### Cairn Research Ltd
Jeremy Graham  
Ed Finucane
#### University of St Andrews
Dr. Stefan Pulver  
James Macleod  
Alexander Konovalov
#### University of Sheffield
Prof. Ash Cadby
