
#include "CairnNI6008.h"
//#include "stdfx.h"
#include <string>
#include "ModuleInterface.h"
#include <sstream>

#ifdef WIN32
   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>
   #define snprintf _snprintf 
#endif
int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);


#define SHUT_MODE_LABEL			"Shutter Digital Out Bit"
#define SHUT_MODE_UNUSED		"Unused"
#define SHUT_MODE_INVERT		"TTL Invert"
#define SHUT_MODE_LOW			"TTL Low"
#define SHUT_MODE_HIGH			"TTL High"
#define DOSM_UNUSED				0
#define DOSM_INVERT				1
#define DOSM_LOW				2
#define DOSM_HIGH				3
#define DOUT_LABEL				"Direct Digital Out"
#define AOUT_LABEL              "Direct Analog Out"
#define NUM_SHUTTER_CHANNELS	4
using namespace std;
const char* g_OptoByte = "CairnNI6008";
const char* digitalPort0 = "Dev1/port0/line0:3";
const char* digitalPort1 = "Dev1/port1/line0:3";

// TODO: linux entry code

// windows DLL entry code
#ifdef WIN32
BOOL APIENTRY DllMain( HANDLE /*hModule*/, 
                      DWORD  ul_reason_for_call, 
                      LPVOID /*lpReserved*/
                      )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
   case DLL_THREAD_DETACH:
		break;
   case DLL_PROCESS_DETACH:
		break;
   }
   return TRUE;
}
#endif

MODULE_API void InitializeModuleData()
{
	RegisterDevice(g_OptoByte, MM::ShutterDevice, "Cairn NI output port");
   //AddAvailableDeviceName(g_OptoByte, "Cairn NI output port");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0) return 0;
	if (strcmp(deviceName, g_OptoByte) == 0) return new CCairnNI6221();
	return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}


///////////////////////////////////////////////////////////////////////////////
// osShutter implementation
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CCairnNI6221::GetName(char* name) const
{
   CDeviceUtils::CopyLimitedString(name, g_OptoByte);
}

int CCairnNI6221::ErrorHandler(int error)
{
	char    errBuff[2048]={'\0'};
	if( DAQmxFailed(error) )
			DAQmxGetExtendedErrorInfo(errBuff,2048);
		if( DOtaskHandle0 ) {
			/*********************************************/
			// DAQmx Stop Code
			/*********************************************/
			DAQmxStopTask(DOtaskHandle0);
			DAQmxClearTask(DOtaskHandle0);
			DOtaskHandle0 = 0;
		}
		if( DOtaskHandle1 ) {
			/*********************************************/
			// DAQmx Stop Code
			/*********************************************/
			DAQmxStopTask(DOtaskHandle1);
			DAQmxClearTask(DOtaskHandle1);
			DOtaskHandle1 = 0;
		}
		if( AOtaskHandle ) {
			/*********************************************/
			// DAQmx Stop Code
			/*********************************************/
			DAQmxStopTask(AOtaskHandle);
			DAQmxClearTask(AOtaskHandle);
			AOtaskHandle = 0;
		}
		if( DAQmxFailed(error) )
			printf("DAQmx Error: %s\n",errBuff);
	return error;
}

int CCairnNI6221::init_optoDO (  )
{
	for (int i=0; i < DOUT_COUNT_USED_LINE_0; i++) 
	{
		data_line_0[i] = 0;
	}
	for (int i=0; i < DOUT_COUNT_USED_LINE_1; i++) 
	{
		data_line_1[i] = 0;
	}

	AOData[0] = 0.0;
	AOData[1] = 0.0;

	DOtaskHandle0 = 0;
	DOtaskHandle1 = 0;
	AOtaskHandle = 0;

	int32 ret = DAQmxCreateTask("",&DOtaskHandle0);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxCreateTask("",&DOtaskHandle1);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ret = DAQmxCreateTask("",&AOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	/*********************************************/
	// DAQmx Create Digital output Channels
	/*********************************************/
	ret = DAQmxCreateDOChan(DOtaskHandle0,digitalPort0,"",DAQmx_Val_ChanForAllLines);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxCreateDOChan(DOtaskHandle1,digitalPort1,"",DAQmx_Val_ChanForAllLines);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	
	ret = DAQmxCreateAOVoltageChan(AOtaskHandle,"Dev1/ao0","",0.0,5.0,DAQmx_Val_Volts,"");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ret = DAQmxCreateAOVoltageChan(AOtaskHandle,"Dev1/ao1","",0.0,5.0,DAQmx_Val_Volts,"");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	//char        errBuff[2048]={'\0'};
	ret = DAQmxStartTask(DOtaskHandle0);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxStartTask(DOtaskHandle1);
	//DAQmxGetExtendedErrorInfo(errBuff,2048);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxStartTask(AOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);


		binValue = 0;
	return 0;
}

int CCairnNI6221::Shutdown()
{
	SetOpen(false);
	DAQmxStopTask(DOtaskHandle0);
	DAQmxClearTask(DOtaskHandle0);
	DOtaskHandle0 = 0;
	DAQmxStopTask(DOtaskHandle1);
	DAQmxClearTask(DOtaskHandle1);
	DOtaskHandle1 = 0;
	DAQmxStopTask(AOtaskHandle);
	DAQmxClearTask(AOtaskHandle);
	AOtaskHandle = 0;
	
	initialized_ = false; 

	return DEVICE_OK;
}

int CCairnNI6221::Initialize()
{
	if (initialized_) return DEVICE_OK;
	if ( init_optoDO() ) return DEVICE_ERR;

	int ret = CreateProperty(MM::g_Keyword_Name, g_OptoByte, MM::String, true);
	if (DEVICE_OK != ret) return ret;

	ret = CreateProperty(MM::g_Keyword_Description, "Cairn NI port driver", MM::String, true);
	if (DEVICE_OK != ret) return ret;

	CPropertyAction* pAct = new CPropertyAction (this, &CCairnNI6221::OnShutter);
	ret = CreateProperty( "Shutter" , "0", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( "Shutter" , "0" ); // Closed
	AddAllowedValue( "Shutter" , "1" ); // Open


		CPropertyActionEx *pExAct;

	int i;


	for ( i=0; i < DOUT_COUNT_USED_LINE_1; i++) 
	{
		pExAct = new CPropertyActionEx (this, &CCairnNI6221::OnLine_0 , i );
		ostringstream os;
		os << DOUT_LABEL << "_P0." << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Integer, false, pExAct); 
		if (ret != DEVICE_OK) return ret; 
		AddAllowedValue( os.str().c_str() , "0" ); 
		AddAllowedValue( os.str().c_str() , "1" ); 
	}
	for ( i=0; i < DOUT_COUNT_USED_LINE_1; i++) 
	{
		pExAct = new CPropertyActionEx (this, &CCairnNI6221::OnLine_1 , i );
		ostringstream os;
		os << DOUT_LABEL << "_P1." << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Integer, false, pExAct); 
		if (ret != DEVICE_OK) return ret; 
		AddAllowedValue( os.str().c_str() , "0" ); 
		AddAllowedValue( os.str().c_str() , "1" ); 
	}

	for ( i=0 ; i < DAC_COUNT_USED; i++)
	{
		pExAct = new CPropertyActionEx (this, &CCairnNI6221::OnAnaOut , i );
		ostringstream os;
		os << AOUT_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Float, false, pExAct);
		if (ret != DEVICE_OK) return ret;
		SetPropertyLimits( os.str().c_str() , 0.00 , 5.00);
	}

	//pAct = new CPropertyAction (this, &CCairnNI6221::OnValue);
	//ret = CreateProperty( "Value" , "0", MM::Integer, false, pAct); 
	//if (ret != DEVICE_OK) return ret; 

	ret = UpdateStatus();
	if (ret != DEVICE_OK) return ret;
	initialized_ = true;
	return DEVICE_OK;
}


bool CCairnNI6221::Busy()
{
	return false;
}


int CCairnNI6221::SetOpen (bool open)
{
	if ( open )
	{
		shuttered_ = false;
		DOSetData(data_line_0, data_line_1);
	}else{
		//uInt8 DigitalShutter0[DOUT_COUNT_USED_LINE_0];
		uInt8 DigitalShutter1[DOUT_COUNT_USED_LINE_1];
		int i;
		//for ( i = 0 ; i < DOUT_COUNT_USED_LINE_0 ; i++ )
		//{

		//		if ( i < NUM_SHUTTER_CHANNELS)
		//		{
		//			DigitalShutter0[i] = 0;
		//		} else {
		//			DigitalShutter0[i] = data_line_0[i];
		//		}
		//}
		for ( i = 0 ; i < DOUT_COUNT_USED_LINE_1 ; i++ )
		{

				if ( i < NUM_SHUTTER_CHANNELS)
				{
					DigitalShutter1[i] = 0;
				} else {
					DigitalShutter1[i] = data_line_1[i];
				}
		}
		DOSetData( data_line_0, DigitalShutter1);
		shuttered_ = true;
	}
	return DEVICE_OK;
}

int CCairnNI6221::GetOpen(bool& open)
{
	if ( shuttered_ )
		open = false;
	else
		open = true;
	return DEVICE_OK;
}

int CCairnNI6221::DOSetData(uInt8 *myDataLine0, uInt8 *myDataLine1)
{
	int32 ret =DAQmxWriteDigitalLines(DOtaskHandle0,1,1,10.0,DAQmx_Val_GroupByChannel,myDataLine0,NULL,NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	
	ret =      DAQmxWriteDigitalLines(DOtaskHandle1,1,1,10.0,DAQmx_Val_GroupByChannel,myDataLine1,NULL,NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
	//////std::string aString;
	//////int anint;
	//////for (int i = 0 ; i <8 ; i++)
	//////{
	//////	anint = (int)myData[i];

	//////			  char *myBuff;
	//////			  //string strRetVal;

	//////			  // Create a new char array
	//////			  myBuff = new char[100];

	//////			  // Set it to empty
	//////			  memset(myBuff,'\0',100);

	//////			  // Convert to string
	//////			  itoa(anint,myBuff,10);

	//////			  // Copy the buffer into the string object
	//////			  aString = myBuff;
 ////// 
	//////			  // Delete the buffer
	//////			  delete[] myBuff;

	//////			  MessageBoxA(0,aString.c_str(),"test1",0);
	//////}
	
}



//int CCairnNI6221::OnValue(MM::PropertyBase* pProp, MM::ActionType eAct)
//{
//	long pos;
//	if (eAct == MM::BeforeGet)
//	{
//		pos = binValue;
//		pProp->Set(pos);
//	}
//	else if (eAct == MM::AfterSet)
//	{
//		pProp->Get(pos);
//		binValue = pos;
//		int i;
//		for ( i=0 ; i<8 ; i++ )
//		{
//			data[i] = pos & 1;
//			pos = pos >> 1;
//		}
//		if (!shuttered_)
//			DOSetData( data);
//   }
//   return DEVICE_OK;
//}
// copy "$(OutDir)mmgr_dal_CairnNI6501.dll" "D:\Program Files\Micro-Manager-1.4\mmgr_dal_CairnNI6501.dll"
int CCairnNI6221::OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if ( shuttered_ )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			return SetOpen( false );
		else
			return SetOpen( true );
   }
   return DEVICE_OK;
}

int CCairnNI6221::OnLine_0(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		if ( data_line_0[channel]  )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			data_line_0[channel] = 1;
		else
			data_line_0[channel] = 0;
		if (!shuttered_)
		{
		  DOSetData( data_line_0, data_line_1);
		} else {
			uInt8  tempData[DOUT_COUNT_USED_LINE_1];
			for (int i = 0 ; i < DOUT_COUNT_USED_LINE_1 ; i++)
			{
				if ( i < NUM_SHUTTER_CHANNELS)
				{
					tempData[i] = 0;
				} else {
					tempData[i] = data_line_1[i];
				}
			}
			DOSetData(  data_line_0, tempData);
		}


   }
   return DEVICE_OK;
}

int CCairnNI6221::OnLine_1(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		if ( data_line_1[channel]  )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			data_line_1[channel] = 1;
		else
			data_line_1[channel] = 0;
		if (!shuttered_)
		{
		  DOSetData( data_line_0, data_line_1);
		} else {
			uInt8  tempData[DOUT_COUNT_USED_LINE_1];
			for (int i = 0 ; i < DOUT_COUNT_USED_LINE_1 ; i++)
			{
				if ( i < NUM_SHUTTER_CHANNELS)
				{
					tempData[i] = 0;
				} else {
					tempData[i] = data_line_0[i];
				}
			}
			DOSetData( tempData, data_line_1 );
		}


   }
   return DEVICE_OK;
}

int CCairnNI6221::OnAnaOut(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		float pVal;
		pVal = (float)AOData[channel];// * percentMult;
		pProp->Set(pVal);
	}
	else if (eAct == MM::AfterSet)
	{
		float64 val;
		pProp->Get(val);
		val = val;// / percentMult;
		AOData[channel] = val;
		AOSetData(AOData);
	}
	return DEVICE_OK;
}

int CCairnNI6221::AOSetData(float64 *myAOData)
{
	int32 ret = DAQmxWriteAnalogF64(AOtaskHandle,1,1,5.0,DAQmx_Val_GroupByChannel,myAOData,NULL,NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}