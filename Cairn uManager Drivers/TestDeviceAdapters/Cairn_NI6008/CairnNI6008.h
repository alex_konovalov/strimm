#ifndef _CAIRNNI6221_H_
#define _CAIRNNI6221_H_

#include "DeviceBase.h"
#include "ImgBuffer.h"
#include "DeviceThreads.h"
//#include "FTD2XX.h"
#include "NIDAQmx.h"
#include <string>
#include <map>

#define ERR_UNKNOWN_MODE         102
#define ERR_UNKNOWN_POSITION     103


#define DOUT_COUNT_MAX_LINE_1		4
#define DOUT_COUNT_MAX_LINE_0		8
#define DOUT_COUNT_USED_LINE_1		4
#define DOUT_COUNT_USED_LINE_0		4
#define DAC_COUNT_MAX				2
#define DAC_COUNT_USED				2
//Cairn Optoscan Device
class CCairnNI6221 : public CShutterBase<CCairnNI6221>
{
public:
	CCairnNI6221() : bits_(0),initialized_(false),shuttered_(true){
		//EnableDelay(); // signals that the dealy setting will be used
	}
	~CCairnNI6221() {}
	int Initialize();
	int Shutdown();
	int init_optoDO ( void );
	void GetName (char* pszName) const;
	bool Busy();
	// Shutter API
	int SetOpen (bool open);
	int GetOpen(bool& open);
	int Fire(double /*deltaT*/) {return DEVICE_UNSUPPORTED_COMMAND;}

	int OnLine_0(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
	int OnLine_1(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
	int OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct);
	//int OnValue(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnAnaOut(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);

private:
	uInt8  data_line_0[DOUT_COUNT_USED_LINE_0];
	uInt8  data_line_1[DOUT_COUNT_USED_LINE_1];

	float64 AOData[DAC_COUNT_USED];
	unsigned char bits_;
	long binValue;
	//FT_HANDLE hnd_;
	bool initialized_;
	bool shuttered_;
	void SetBits( unsigned char bits );
	int ErrorHandler(int error);
	TaskHandle DOtaskHandle0,DOtaskHandle1, AOtaskHandle;
	int DOSetData(uInt8 *myDataLine0, uInt8 *myDataLine1);
	int AOSetData(float64 *myAOData);

	int DACUsed[DAC_COUNT_MAX];


};

#endif //_OPTOBYTE_H_
