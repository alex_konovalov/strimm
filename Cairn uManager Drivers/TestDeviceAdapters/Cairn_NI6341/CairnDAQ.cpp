#include "CairnDAQ.h"
//#include "stdfx.h"
#include <string>
#include <math.h>
#include "ModuleInterface.h"
#include "Error.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
//#pragma comment(lib, "NIDAQmx.lib")
#include "NIDAQmx.h"
//#include "osLibrary.h"
#include "os_maths.h"

#ifdef WIN32
   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>
   #define snprintf _snprintf 
#endif

int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);

#define AIN_0_LABEL				"Direct Analog In 0"
#define AIN_1_LABEL				"Direct Analog In 0"
#define AIN_2_LABEL				"Direct Analog In 0"
#define AIN_3_LABEL				"Direct Analog In 0"
#define AIN_4_LABEL				"Direct Analog In 0"
#define AIN_5_LABEL				"Direct Analog In 0"
#define AIN_6_LABEL				"Direct Analog In 0"
#define AIN_7_LABEL				"Direct Analog In 0"

#define DIN_LABEL				"Direct Digital In"

#define DOUT_LABEL				"Direct Digital Out"
#define AOUT_LABEL              "Direct Analog Out"
#define OS_OP_CHAN_LABEL		"Optoscan Output Slit Channel"
#define OS_IP_CHAN_LABEL		"Optoscan Input Slit Channel"
#define OS_WV_CHAN_LABEL		"Optoscan Wavelength Channel"
#define ANotLabel				"Unused"
#define AO0OutLabel				"Analog Out 0"
#define AO1OutLabel				"Analog Out 1"
#define AO2OutLabel				"Analog Out 2"
#define AO3OutLabel				"Analog Out 3"
#define AO_CHAN_0				1
#define AO_CHAN_1				2
#define AO_CHAN_2				3
#define AO_CHAN_3				4


#define SHUT_MODE_LABEL			"Shutter Digital Out Bit"
#define SHUT_MODE_UNUSED		"Unused"
#define SHUT_MODE_INVERT		"TTL Invert"
#define SHUT_MODE_LOW			"TTL Low"
#define SHUT_MODE_HIGH			"TTL High"
#define DOSM_UNUSED				0
#define DOSM_INVERT				1
#define DOSM_LOW				2
#define DOSM_HIGH				3

#define DEFAULTS_LABEL			"All Settings Defaults"
#define DEFAULTS_NONE			"None"
#define DEFAULTS_OPTOSCAN		"Optoscan Defaults"
#define DEFAULTS_NONE_VAL		0
#define DEFAULTS_OPTOSCAN_VAL	1

#define AOVoltsLabel			"Analog Output Voltage"
#define AOVolts1				"5.0 Volts"
#define AOVolts2				"10.0 Volts"


using namespace std;
//static TaskHandle  AOtaskHandle = 0, DOtaskHandle = 0;

//#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

const char* g_Desc = "Cairn IO Controller";
const char* g_CairnDAQ = "CairnIO";
const char* digitalPort = "Dev1/port0/line0:7";
const char* digitalInPort = "Dev1/port0/line8:15";


// TODO: linux entry code

// windows DLL entry code
#ifdef WIN32
BOOL APIENTRY DllMain( HANDLE /*hModule*/, DWORD  reason, LPVOID /*lpReserved*/  )
{
	switch (reason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
   case DLL_THREAD_DETACH:
		break;
   case DLL_PROCESS_DETACH:
		break;
   }
   return TRUE;
}
#endif

MODULE_API void InitializeModuleData()
{
	//RegisterDevice(g_CairnDAQ, MM::UnknownType, g_Desc);
   AddAvailableDeviceName(g_CairnDAQ, g_Desc );
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0) return 0;
	if (strcmp(deviceName, g_CairnDAQ) == 0) return new CCairnDAQ();
	return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}


///////////////////////////////////////////////////////////////////////////////
// osShutter implementation
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~

CCairnDAQ::CCairnDAQ() :
initialized_(false),
shuttered_(true)
//gratingLines(1800),
//inVal (0),
//outVal (0),
//bandWidth (0),
//wavelength(300),
//waveVolts(0)
{
	
	waveChan = 0;
	inChan = 0;
	outChan = 0;
	lastDefaults = 0;
	DACUsed[0] = 0;
	DACUsed[1] = 0;
	DACUsed[2] = 0;
	DACUsed[3] = 0;
	int i;
	
	for ( i=0 ; i<DOUT_COUNT_MAX ; i++ ) DOShutMode[i] = DOSM_UNUSED;

	InitializeDefaultErrorMessages();
	   // Name
	int ret = CreateProperty( "ADIO Name ", g_CairnDAQ, MM::String, true);
	//if (DEVICE_OK != ret) return ret;

	ret = CreateProperty( "ADIO Description" , g_Desc , MM::String, true);
	//if (DEVICE_OK != ret) return ret;
      // Port
    CPropertyAction* pAct = new CPropertyAction (this, &CCairnDAQ::OnGrating);
    CreateProperty("Optoscan Grating Lines", "", MM::Integer, false, pAct,true);
	AddAllowedValue( "Optoscan Grating Lines" , "1200" ); 
	AddAllowedValue( "Optoscan Grating Lines" , "1800" ); 
	AddAllowedValue( "Optoscan Grating Lines" , "2000" ); 
	SetProperty("Optoscan Grating Lines", "1200");

}
CCairnDAQ::~CCairnDAQ()
{
   Shutdown();
}
void CCairnDAQ::GetName(char* name) const
{
   CDeviceUtils::CopyLimitedString(name, g_CairnDAQ); 
}
int CCairnDAQ::initDAQ (  )
{
	taskHandle=0;
	AOtaskHandle = 0;
	DOtaskHandle = 0;
	DItaskHandle = 0;
	binValue  = 0;
	percentMult = 20.0;

	data[0] = 0;
	data[1] = 0;
	data[2] = 0;
	data[3] = 0;
	data[4] = 0;
	data[5] = 0;
	data[6] = 0;
	data[7] = 0;

	inputData[0] =0;
	inputData[1] =0;
	inputData[2] =0;
	inputData[3] =0;
	inputData[4] =0;
	inputData[5] =0;
	inputData[6] =0;
	inputData[7] =0;

	AOdata[0] = 0.0;
	AOdata[1] = 0.0;
	AOdata[2] = 0.0;
	AOdata[3] = 0.0;



	int32 ret = DAQmxCreateTask("",&DOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ret = DAQmxCreateTask("",&AOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	
	ret = DAQmxCreateTask("",&DItaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxCreateTask("",&AItaskHandle);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	/*********************************************/
	// DAQmx Create Digital output Channels
	/*********************************************/
	ret = DAQmxCreateDOChan(DOtaskHandle,digitalPort,"",DAQmx_Val_ChanForAllLines);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	/*********************************************/
	// DAQmx Create Analogue Voltage Output Channels
	/*********************************************/
	ret = DAQmxCreateAOVoltageChan(AOtaskHandle,"Dev1/ao0","",0.0,5.0,DAQmx_Val_Volts,"");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ret = DAQmxCreateAOVoltageChan(AOtaskHandle,"Dev1/ao1","",0.0,5.0,DAQmx_Val_Volts,"");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ret = DAQmxCreateAOVoltageChan(AOtaskHandle,"Dev1/ao2","",0.0,5.0,DAQmx_Val_Volts,"");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ret = DAQmxCreateAOVoltageChan(AOtaskHandle,"Dev1/ao3","",0.0,5.0,DAQmx_Val_Volts,"");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	///*********************************************/
	//// DAQmx Create Digital Input Channels
	///*********************************************/
	ret = DAQmxCreateDIChan(DItaskHandle,digitalInPort,"",DAQmx_Val_ChanForAllLines);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	///*********************************************/
	//// DAQmx Create Analogue Voltage Input Channels
	///*********************************************/
	//ret = DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai0","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai1","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai2","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai3","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai4","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai5","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai6","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai7","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	///*********************************************/
	//// DAQmx Set Sample rate and type
	///*********************************************/
	//ret = DAQmxCfgSampClkTiming(AItaskHandle,"",10000.0,DAQmx_Val_Rising,DAQmx_Val_ContSamps,1000);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	//ret = DAQmxRegisterEveryNSamplesEvent(AItaskHandle,DAQmx_Val_Acquired_Into_Buffer,1000,0,EveryNCallback,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	/*********************************************/
	// DAQmx Start Code
	/*********************************************/
	ret = DAQmxStartTask(DOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxStartTask(AOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxStartTask(DItaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxStartTask(AItaskHandle);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	
	////*********************************************/
	//// DAQmx Write Code
	////*********************************************/
	//ret = DAQmxWriteDigitalLines(DOtaskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,data,NULL,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	//ret = DAQmxWriteAnalogF64(AOtaskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,AOdata,NULL,NULL);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	
	//double version = os_Library_Version;  
	
	return 0;
}
int CCairnDAQ::Shutdown()
{

	DAQmxStopTask(DOtaskHandle);
	DAQmxClearTask(DOtaskHandle);
	DOtaskHandle = 0;
	DAQmxStopTask(AOtaskHandle);
	DAQmxClearTask(AOtaskHandle);
	AOtaskHandle = 0;
	DAQmxStopTask(DItaskHandle);
	DAQmxClearTask(DItaskHandle);
	DItaskHandle = 0;
	
	initialized_ = false; 

	return DEVICE_OK;
}
int CCairnDAQ::ErrorHandler(int error)
{
	char    errBuff[2048]={'\0'};
	if( DAQmxFailed(error) )
			DAQmxGetExtendedErrorInfo(errBuff,2048);
		if( AOtaskHandle ) {
			/*********************************************/
			// DAQmx Stop Code
			/*********************************************/
			DAQmxStopTask(AOtaskHandle);
			DAQmxClearTask(AOtaskHandle);
			AOtaskHandle = 0;
		}
		if( DOtaskHandle ) {
			/*********************************************/
			// DAQmx Stop Code
			/*********************************************/
			DAQmxStopTask(DOtaskHandle);
			DAQmxClearTask(DOtaskHandle);
			DOtaskHandle = 0;
		}
		if( DAQmxFailed(error) )
			printf("DAQmx Error: %s\n",errBuff);
	return error;
}
int CCairnDAQ::Initialize()
{
	if (initialized_) return DEVICE_OK;
	//gratingLines = 1800.0;
	inVolts = 0.0;
	outVolts = 0.0;
	bandWidth = 0.0;
	wavelength = 300.0;
	waveVolts = 0.0;
	int ret = initDAQ();
	if (DEVICE_OK != ret) return ret;
	double topWave, botWave;
	double topBand, botBand;
	//gratingLines = 1800;
	if (gratingLines <= 1201)
	{
		topWave = 800.0;
		botWave = 300.0;
		topBand = 30.0;
		botBand = 0.0;
		gratingOffset = 0.0;
	}
	else if (gratingLines <= 1801)
	{
		topWave = 610.0;
		botWave = 300.0;
		topBand = 23.0;
		botBand = 0.0;
		gratingOffset = -1.25;
	}
	else 
	{
		topWave = 580.0;
		botWave = 300.0;
		topBand = 19.0;
		botBand = 0.0;
		gratingOffset = -1.8;
	}

	CPropertyAction* pAct = new CPropertyAction (this, &CCairnDAQ::OnShutter);
	ret = CreateProperty( "Shutter Override" , "0", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( "Shutter Override" , "0" ); // Closed
	AddAllowedValue( "Shutter Override" , "1" ); // Open

	CPropertyActionEx *pExAct;

	int i;
	for ( i=0; i < DOUT_COUNT_USED; i++) 
	{
		pExAct = new CPropertyActionEx(this, &CCairnDAQ::OnDOShutMode , i );
		ostringstream os;
		os << SHUT_MODE_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str(), "0.0", MM::String, false, pExAct);
		if (ret != DEVICE_OK) return ret;
		AddAllowedValue( os.str().c_str() , SHUT_MODE_UNUSED , DOSM_UNUSED );
		AddAllowedValue( os.str().c_str() , SHUT_MODE_INVERT , DOSM_INVERT ); 
		AddAllowedValue( os.str().c_str() , SHUT_MODE_LOW , DOSM_LOW ); 
		AddAllowedValue( os.str().c_str() , SHUT_MODE_HIGH , DOSM_HIGH ); 
	}

	for ( i=0; i < DOUT_COUNT_USED; i++) 
	{
		pExAct = new CPropertyActionEx (this, &CCairnDAQ::OnBit , i );
		ostringstream os;
		os << DOUT_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Integer, false, pExAct); 
		if (ret != DEVICE_OK) return ret; 
		AddAllowedValue( os.str().c_str() , "0" ); 
		AddAllowedValue( os.str().c_str() , "1" ); 
	}

	for ( i=0 ; i < DAC_COUNT_USED; i++)
	{
		pExAct = new CPropertyActionEx (this, &CCairnDAQ::OnAnaOut , i );
		ostringstream os;
		os << AOUT_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Float, false, pExAct);
		if (ret != DEVICE_OK) return ret;
		SetPropertyLimits( os.str().c_str() , 0.0 , 100.0);
	}
	
	for ( i=0 ; i < DIN_COUNT_USED; i++)
	{
		pExAct = new CPropertyActionEx (this, &CCairnDAQ::OnDigIn , i );
		ostringstream os;
		os << DIN_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Float, true, pExAct);
		if (ret != DEVICE_OK) return ret;
	}

	pAct = new CPropertyAction (this, &CCairnDAQ::OnValue);
	ret = CreateProperty( "Direct Digital Output Value" , "0", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 

	pAct = new CPropertyAction (this, &CCairnDAQ::OnWavelength);
	ret = CreateProperty( "Optoscan Wavelength", "0.0", MM::Integer, false, pAct);
	if (ret != DEVICE_OK) return ret;
	SetPropertyLimits("Optoscan Wavelength",botWave,topWave);

	pAct = new CPropertyAction (this, &CCairnDAQ::OnBandwidth);
	ret = CreateProperty( "Optoscan Bandwidth", "0", MM::Integer, false, pAct);
	if (ret != DEVICE_OK) return ret;
	SetPropertyLimits("Optoscan Bandwidth",botBand,topBand);

	pAct = new CPropertyAction (this, &CCairnDAQ::OnWaveVolts);
	ret = CreateProperty( "Report Optoscan Wavelength Volts", "0", MM::Float, true, pAct);
	if (ret != DEVICE_OK) return ret;

	pAct = new CPropertyAction (this, &CCairnDAQ::OnInVolts);
	ret = CreateProperty( "Report Optoscan Input Slit Volts", "0", MM::Float, true, pAct);
	if (ret != DEVICE_OK) return ret;

	pAct = new CPropertyAction (this, &CCairnDAQ::OnOutVolts);
	ret = CreateProperty( "Report Optoscan Output Slit Volts", "0", MM::Float, true, pAct);
	if (ret != DEVICE_OK) return ret;




	pAct = new CPropertyAction (this, &CCairnDAQ::OnWaveChan);
	ret = CreateProperty( OS_WV_CHAN_LABEL , "0", MM::String, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( OS_WV_CHAN_LABEL , ANotLabel ); 
	AddAllowedValue( OS_WV_CHAN_LABEL , AO0OutLabel ); 
	AddAllowedValue( OS_WV_CHAN_LABEL , AO1OutLabel ); 
	AddAllowedValue( OS_WV_CHAN_LABEL , AO2OutLabel ); 
	AddAllowedValue( OS_WV_CHAN_LABEL , AO3OutLabel ); 

	pAct = new CPropertyAction (this, &CCairnDAQ::OnInChan);
	ret = CreateProperty( OS_IP_CHAN_LABEL , "0", MM::String, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( OS_IP_CHAN_LABEL , ANotLabel ); 
	AddAllowedValue( OS_IP_CHAN_LABEL , AO0OutLabel ); 
	AddAllowedValue( OS_IP_CHAN_LABEL , AO1OutLabel ); 
	AddAllowedValue( OS_IP_CHAN_LABEL , AO2OutLabel ); 
	AddAllowedValue( OS_IP_CHAN_LABEL , AO3OutLabel ); 

	pAct = new CPropertyAction (this, &CCairnDAQ::OnOutChan);
	ret = CreateProperty( OS_OP_CHAN_LABEL , "0", MM::String, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( OS_OP_CHAN_LABEL , ANotLabel ); 
	AddAllowedValue( OS_OP_CHAN_LABEL , AO0OutLabel ); 
	AddAllowedValue( OS_OP_CHAN_LABEL , AO1OutLabel ); 
	AddAllowedValue( OS_OP_CHAN_LABEL , AO2OutLabel ); 
	AddAllowedValue( OS_OP_CHAN_LABEL , AO3OutLabel ); 

	pAct = new CPropertyAction (this, &CCairnDAQ::OnDefaults);
	ret = CreateProperty( DEFAULTS_LABEL , "0", MM::String, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( DEFAULTS_LABEL , DEFAULTS_NONE  );
	AddAllowedValue( DEFAULTS_LABEL , DEFAULTS_OPTOSCAN  );

	ret = UpdateStatus();
	if (ret != DEVICE_OK) return ret;
	SetVoltages();
	initialized_ = true;
	return DEVICE_OK;
}
bool CCairnDAQ::Busy()
{
	//bool32 AOTaskDone;
	//bool32 DOTaskDone;

	//int32 ret = DAQmxIsTaskDone (DOtaskHandle, &DOTaskDone);
	////if (ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//ret = DAQmxIsTaskDone (AOtaskHandle, &AOTaskDone);
	////if (ret != DEVICE_OK) return (int)ErrorHandler(ret);

	//if (DOTaskDone && AOTaskDone)
	//{
	//	return false;
	//}
	//else
	//{
	//	return true;
	//}
	return false;
}
int CCairnDAQ::DOSetData(uInt8 *myData)
{
	int32 ret =DAQmxWriteDigitalLines(DOtaskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,myData,NULL,NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}
int CCairnDAQ::AOSetData(float64 *myAOData)
{
	int32 ret = DAQmxWriteAnalogF64(AOtaskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,myAOData,NULL,NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}
int CCairnDAQ::SetVoltages()
{
	int DACchan;
	if ( inChan )
	{
		inVolts = bandWidth;
		DACchan = inChan - 1; 
		os_In_Slit_Bandwidth_To_Width ( gratingLines , wavelength , &inVolts );
		os_In_Slit_Width_To_Voltage ( &inVolts );
		AOdata[DACchan] = inVolts;
	}

	if ( outChan )
	{
		outVolts = bandWidth;
		DACchan = outChan - 1; 
		os_Out_Slit_Bandwidth_To_Width ( gratingLines , wavelength , &outVolts );
		os_Out_Slit_Width_To_Voltage ( &outVolts );
		AOdata[DACchan] = outVolts;
	}
		
	if ( waveChan )
	{
		waveVolts = wavelength;
		DACchan = waveChan - 1; 
		os_Wavelength_To_Voltage (gratingOffset, gratingLines , &waveVolts );
		AOdata[DACchan] = waveVolts;
	}
	AOSetData(AOdata);
	return 0; 
}
int CCairnDAQ::DIGetData(uInt8 *myRead)
{
	int32		read,bytesPerSamp;
	int32 ret = DAQmxReadDigitalLines(DItaskHandle,1,10.0,DAQmx_Val_GroupByChannel,myRead,8,&read,&bytesPerSamp,NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}
int CCairnDAQ::SetOpen (bool open)
{
	if ( open )
	{
		shuttered_ = false;
		DOSetData(data);
	}else{
		uInt8 DigitalShutter[DOUT_COUNT_MAX];
		int i;
		for ( i = 0 ; i < DOUT_COUNT_MAX ; i++ )
		{
			switch ( DOShutMode[i] )
			{
				case DOSM_INVERT:
						if ( data[i] )
							DigitalShutter[i] = 0;
						else
							DigitalShutter[i] = 1;
					break;
				case DOSM_HIGH:
						DigitalShutter[i] = 1;
					break;
				case DOSM_LOW:
						DigitalShutter[i] = 0;
					break;
				default:	
					DigitalShutter[i] = data[i];
			}
		}
		DOSetData( DigitalShutter);
		shuttered_ = true;
	}
	return DEVICE_OK;
}
int CCairnDAQ::GetOpen(bool& open)
{
	if ( shuttered_ )
		open = false;
	else
		open = true;
	return DEVICE_OK;
}
int CCairnDAQ::OnValue(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	long pos;
	if (eAct == MM::BeforeGet)
	{
		pos = binValue;
		pProp->Set(pos);
	}
	else if (eAct == MM::AfterSet)
	{
		pProp->Get(pos);
		binValue = pos;
		int i;
		for ( i=0 ; i<8 ; i++ )
		{
			data[i] = pos & 1;
			pos = pos >> 1;
		}
		DOSetData( data);
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if ( shuttered_ )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			return SetOpen( false );
		else
			return SetOpen( true );
   }
   return DEVICE_OK;
}



int CCairnDAQ::OnBit(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		if ( data[channel]  )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			data[channel] = 1;
		else
			data[channel] = 0;
		DOSetData( data);
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnWavelength(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		double pVal;
		pVal = wavelength;
		pProp->Set(pVal);
	}
	else if (eAct == MM::AfterSet)
	{
		double val;
		pProp->Get(val);
		wavelength = val;
		SetVoltages();
	}
	return DEVICE_OK;
}
int CCairnDAQ::OnBandwidth(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		double pVal;
		pVal = bandWidth;

		pProp->Set(pVal);
		//SetVoltages();
	}
	else if (eAct == MM::AfterSet)
	{
		double val;
		pProp->Get(val);
		bandWidth = val;
		SetVoltages();
	}
	return DEVICE_OK;
}
int CCairnDAQ::OnAnaOut(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		float pVal;
		pVal = (float)AOdata[channel] * percentMult;
		pProp->Set(pVal);
	}
	else if (eAct == MM::AfterSet)
	{
		float64 val;
		pProp->Get(val);
		val = val / percentMult;
		AOdata[channel] = val;
		AOSetData(AOdata);
	}
	return DEVICE_OK;
}
int CCairnDAQ::OnDigIn(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		long pVal;
		DIGetData(inputData);
		pVal = inputData[channel];
		pProp->Set(pVal);
	}
	return DEVICE_OK;
}
int CCairnDAQ::OnGrating(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	
   if (eAct == MM::BeforeGet)
   {
	    long pVal;
		pVal = gratingLines;
		pProp->Set(pVal);

   }
   else if (eAct == MM::AfterSet)
   {
	  long val;
      if (initialized_)
      {
         // revert
         pProp->Set(gratingLines);
         return ERR_PORT_CHANGE_FORBIDDEN;
      }

      pProp->Get(val);
	  gratingLines = val;
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnWaveVolts(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	
   if (eAct == MM::BeforeGet)
   {
	    double pVal;
		pVal = waveVolts;
		pProp->Set(pVal);

   }
   else if (eAct == MM::AfterSet)
   {
	 //  double pVal;
		//pVal = waveVolts;
		//pProp->Set(pVal);

   }
   return DEVICE_OK;
}
int CCairnDAQ::OnWaveChan(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
	    string pVal;
		pVal = ANotLabel;
		switch ( waveChan )
		{
			case AO_CHAN_0:
				pVal = AO0OutLabel;
				break;
			case AO_CHAN_1:
				pVal = AO1OutLabel;
				break;
			case AO_CHAN_2:
				pVal = AO2OutLabel;
				break;
			case AO_CHAN_3:
				pVal = AO3OutLabel;
				break;
		}
		pProp->Set ( pVal.c_str ( ) );
   }
   else if (eAct == MM::AfterSet)
   {
		string val;
		pProp->Get(val);
		if ( waveChan )	DACUsed[waveChan-1] = 0;
		waveChan = 0;
		if ( 0 == val.compare( AO0OutLabel ) ) waveChan = AO_CHAN_0;
		if ( 0 == val.compare( AO1OutLabel ) ) waveChan = AO_CHAN_1;
		if ( 0 == val.compare( AO2OutLabel ) ) waveChan = AO_CHAN_2;
		if ( 0 == val.compare( AO3OutLabel ) ) waveChan = AO_CHAN_3;
		if ( waveChan )
		{
			if  ( DACUsed[waveChan-1] ) 
			{
				waveChan = 0;
				return DEVICE_CAN_NOT_SET_PROPERTY;
			}else{
				DACUsed[waveChan-1] = 1;
			}
		}
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnInChan(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	
   if (eAct == MM::BeforeGet)
   {
	    string pVal;
		pVal = ANotLabel;
		switch ( inChan )
		{
			case AO_CHAN_0:
				pVal = AO0OutLabel;
				break;
			case AO_CHAN_1:
				pVal = AO1OutLabel;
				break;
			case AO_CHAN_2:
				pVal = AO2OutLabel;
				break;
			case AO_CHAN_3:
				pVal = AO3OutLabel;
				break;
		}
		pProp->Set ( pVal.c_str ( ) );
   }
   else if (eAct == MM::AfterSet)
   {
		string val;
		pProp->Get(val);
		if ( inChan )	DACUsed[inChan-1] = 0;
		inChan = 0;
		if ( 0 == val.compare( AO0OutLabel ) ) inChan = AO_CHAN_0;
		if ( 0 == val.compare( AO1OutLabel ) ) inChan = AO_CHAN_1;
		if ( 0 == val.compare( AO2OutLabel ) ) inChan = AO_CHAN_2;
		if ( 0 == val.compare( AO3OutLabel ) ) inChan = AO_CHAN_3;
		if ( inChan )
		{
			if  ( DACUsed[inChan-1] ) 
			{
				inChan = 0;
				return DEVICE_CAN_NOT_SET_PROPERTY;
			}else{
				DACUsed[inChan-1] = 1;
			}
		}
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnOutChan(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	
   if (eAct == MM::BeforeGet)
   {
	    string pVal;
		pVal = ANotLabel;
		switch ( outChan )
		{
			case AO_CHAN_0:
				pVal = AO0OutLabel;
				break;
			case AO_CHAN_1:
				pVal = AO1OutLabel;
				break;
			case AO_CHAN_2:
				pVal = AO2OutLabel;
				break;
			case AO_CHAN_3:
				pVal = AO3OutLabel;
				break;
		}
		pProp->Set ( pVal.c_str ( ) );
   }
   else if (eAct == MM::AfterSet)
   {
		string val;
		pProp->Get(val);
		if ( outChan )	DACUsed[outChan-1] = 0;
		outChan = 0;
		if ( 0 == val.compare( AO0OutLabel ) ) outChan = AO_CHAN_0;
		if ( 0 == val.compare( AO1OutLabel ) ) outChan = AO_CHAN_1;
		if ( 0 == val.compare( AO2OutLabel ) ) outChan = AO_CHAN_2;
		if ( 0 == val.compare( AO3OutLabel ) ) outChan = AO_CHAN_3;
		if ( outChan )
		{
			if  ( DACUsed[outChan-1] ) 
			{
				outChan = 0;
				return DEVICE_CAN_NOT_SET_PROPERTY;
			}else{
				DACUsed[outChan-1] = 1;
			}
		}
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnDOShutMode(MM::PropertyBase* pProp, MM::ActionType eAct, long channel )
{
   if (eAct == MM::BeforeGet)
   {
	    string pVal;
		pVal = SHUT_MODE_UNUSED;
		switch ( DOShutMode[channel] )
		{
			case DOSM_INVERT:
				pVal = SHUT_MODE_INVERT;
				break;
			case DOSM_LOW:
				pVal = SHUT_MODE_LOW;
				break;
			case DOSM_HIGH:
				pVal = SHUT_MODE_HIGH;
				break;
		}
		pProp->Set ( pVal.c_str ( ) );
   }
   else if (eAct == MM::AfterSet)
   {
		string val;
		pProp->Get(val);
		DOShutMode[channel] = 0;
		if ( 0 == val.compare( SHUT_MODE_INVERT ) ) DOShutMode[channel] = DOSM_INVERT;
		if ( 0 == val.compare( SHUT_MODE_LOW ) ) DOShutMode[channel] = DOSM_LOW;
		if ( 0 == val.compare( SHUT_MODE_HIGH ) ) DOShutMode[channel] = DOSM_HIGH;
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnDefaults(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
		string pVal;
		switch ( lastDefaults )
		{
			case DEFAULTS_OPTOSCAN_VAL:
				pVal = DEFAULTS_OPTOSCAN;
				break;
			default:
				pVal = DEFAULTS_NONE;
				break;
		}
		pProp->Set ( pVal.c_str ( ) );
   }
   else if (eAct == MM::AfterSet)
   {
		string val;
		pProp->Get(val);
		lastDefaults = 0;
		if ( 0 == val.compare( DEFAULTS_OPTOSCAN ) ) 
		{
			lastDefaults = DEFAULTS_OPTOSCAN_VAL;
			inChan = AO_CHAN_0;
			outChan = AO_CHAN_2;
			waveChan = AO_CHAN_1;
			DOShutMode[0] = DOSM_HIGH;
		}
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnInVolts(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	
   if (eAct == MM::BeforeGet)
   {
	    float pVal;
		pVal = inVolts;
		pProp->Set(pVal);

   }
   else if (eAct == MM::AfterSet)
   {
	 //  float pVal;
		//pVal = inVolts;
		//pProp->Set(pVal);

   }
   return DEVICE_OK;
}
int CCairnDAQ::OnOutVolts(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	
   if (eAct == MM::BeforeGet)
   {
	    float pVal;
		pVal = outVolts;
		pProp->Set(pVal);

   }
   else if (eAct == MM::AfterSet)
   {
	 //  float pVal;
		//pVal = outVolts;
		//pProp->Set(pVal);

   }
   return DEVICE_OK;
}
//int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData)
//{
//	int32       error=0;
//	char        errBuff[2048]={'\0'};
//	static int  totalRead=0;
//	int32       read=0;
//	float64     data[1000];
//
//	/*********************************************/
//	// DAQmx Read Code
//	/*********************************************/
//	int32 ret = DAQmxReadAnalogF64(taskHandle,1000,10.0,DAQmx_Val_GroupByScanNumber,data,1000,&read,NULL);
//	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
//}