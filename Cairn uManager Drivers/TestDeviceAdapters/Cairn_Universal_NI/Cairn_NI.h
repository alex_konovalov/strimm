#ifndef _CAIRNNI_H_
#define _CAIRNNI_H_

#include "DeviceBase.h"
#include "ImgBuffer.h"
#include "DeviceThreads.h"
#include "NIDAQmx.h"

#include <string>
#include <map>

#define ERR_UNKNOWN_MODE 102
#define ERR_UNKNOWN_POSITION 103

//TODO: Change to appropriate values
#define DOUT_COUNT_MAX_LINE_1	4
#define DOUT_COUNT_MAX_LINE_0	8
#define DOUT_COUNT_USED_LINE_1	4
#define DOUT_COUNT_USED_LINE_0	8
#define DAC_OUT_COUNT_MAX		2
#define DAC_OUT_COUNT_USED		2

class CCairnNI : public CShutterBase<CCairnNI>
{
public:
	CCairnNI();
	~CCairnNI();

	int Initialize();
	int Shutdown();
	int init_optoDO(void);
	void GetName(char* pszName) const;
	bool Busy();
	std::vector<std::string> GetDevices();
	int SetOpen(bool open);
	int GetOpen(bool& open);
	int Fire(double) { return DEVICE_UNSUPPORTED_COMMAND; }

	int OnLine_0(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
	int OnLine_1(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
	int OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnAnaOut(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
	int OnDevice(MM::PropertyBase* pProp, MM::ActionType eAct);
	std::string GetNextEntry(std::string line, size_t& index);

private:
	uInt8 dout_count_used_line_0_;
	uInt8 dout_count_used_line_1_;
	uInt8 dac_out_count_used_;
	std::string device_type_;
	std::vector<uInt8> data_line_0;
	std::vector<uInt8> data_line_1;
	std::vector<float64> AOData;
	std::string device_;
	

	unsigned char bits_;
	long binValue;
	long deviceNumber;

	bool initialized_;
	bool shuttered_;
	int ErrorHandler(int error);
	TaskHandle DOTaskHandle0, DOTaskHandle1, AOTaskHandle;
	int DO0_SetData(uInt8 *myDataLine0);
	int DO1_SetData(uInt8 *myDataLine1);
	int AOSetData(float64 *myAOData);

	int DACUsed[DAC_OUT_COUNT_MAX];
};

#endif // !_CAIRNNI_H_




