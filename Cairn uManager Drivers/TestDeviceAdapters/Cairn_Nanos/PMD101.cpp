///////////////////////////////////////////////////////////////////////////////
// FILE:          PMD101.cpp
// PROJECT:       Micro-Manager
// SUBSYSTEM:     DeviceAdapters
//-----------------------------------------------------------------------------
// DESCRIPTION:   PMD101 Controller Driver
//
// AUTHOR:        Nenad Amodaj, nenad@amodaj.com, 08/28/2006
//                Steffen Rau, s.rau@pi.ws, 10/03/2008
//                Jan Felix Evers, jan-felix.evers@cos.uni-heidelberg.de, 24/03/2014

// COPYRIGHT:     University of California, San Francisco, 2006
//                Physik Instrumente (PI) GmbH & Co. KG, 2008
//                University Heideblberg, 2014
// LICENSE:       This file is distributed under the BSD license.
//                License text is included with the source distribution.
//
//                This file is distributed in the hope that it will be useful,
//                but WITHOUT ANY WARRANTY; without even the implied warranty
//                of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//                IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//                CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//                INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.
//

#ifdef WIN32
   #include <windows.h>
//   #define snprintf _snprintf (obsolete with 2015 compilers - in fact causes error)
#endif

#include "PMD101.h"
#include <string>
#include <math.h>
#include "../../MMDevice/ModuleInterface.h"
#include <sstream>

const char* g_PMD101_ZStageDeviceName = "PM PiezoLegs Stage";
const char* g_PMD101_ZStageAxisName = "Axis";
const char* g_PMD101_ZStageAxisMinLimitUm = "Min_Limit_um";
const char* g_PMD101_ZStageAxisMaxLimitUm = "Max_Limit_um";

const char* g_PropertyChannel = "IOChannel";
const char* g_PropertyHasNI = "NI_installed";

const char* g_Yes = "Yes";
const char* g_No = "No";

const char* g_AxisType = "Rotation stage";

using namespace std;

///////////////////////////////////////////////////////////////////////////////
// Exported MMDevice API
///////////////////////////////////////////////////////////////////////////////
MODULE_API void InitializeModuleData()
{
   RegisterDevice(g_PMD101_ZStageDeviceName, MM::StageDevice, "PMD101 Z-stage");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
   if (deviceName == 0)
      return 0;

   if (strcmp(deviceName, g_PMD101_ZStageDeviceName) == 0)
   {
      PMD101_ZStage* s = new PMD101_ZStage();
      return s;
   }

   return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}

// General utility function:
int ClearPort(MM::Device& device, MM::Core& core, std::string port)
{
   // Clear contents of serial port 
   const unsigned int bufSize = 255;
   unsigned char clear[bufSize];                      
   unsigned long read = bufSize;
   int ret;                                                                   
   while (read == bufSize)                                                   
   {                                                                     
      ret = core.ReadFromSerial(&device, port.c_str(), clear, bufSize, read);
      if (ret != DEVICE_OK)                               
         return ret;                                               
   }
   return DEVICE_OK;                                                           
} 
 

///////////////////////////////////////////////////////////////////////////////
// PMD101_ZStage

PMD101_ZStage::PMD101_ZStage() :
   port_("Undefined"),
   encoderCountsPerUnit_(8192),
   initialized_(false),
   answerTimeoutMs_(1000),
   axisMinLimitUm_(-15000.0),
   axisMaxLimitUm_(0.0),
   //ni_aochannel_("Dev1/ao1"), 
   //task_(0),
   minV_(-10.0),
   maxV_(10.0),
   backlash_(50.0),
   isRotation_(false),
   unitLength_(10.0),
   hasNi_(false)
{
   InitializeDefaultErrorMessages();

   // create pre-initialization properties
   // ------------------------------------

   // Name
   CreateProperty(MM::g_Keyword_Name, g_PMD101_ZStageDeviceName, MM::String, true);

   // Description
   CreateProperty(MM::g_Keyword_Description, "Piezo Motion PMD101 Controller", MM::String, true);

   // Port
   CPropertyAction* pAct = new CPropertyAction (this, &PMD101_ZStage::OnPort);
   CreateProperty(MM::g_Keyword_Port, "Undefined", MM::String, false, pAct, true);

   // axis limit in um
   pAct = new CPropertyAction (this, &PMD101_ZStage::OnAxisMinLimit);
   CreateProperty(g_PMD101_ZStageAxisMinLimitUm, "-15000.0", MM::Float, false, pAct, true);
   
   pAct = new CPropertyAction (this, &PMD101_ZStage::OnAxisMaxLimit);
   CreateProperty(g_PMD101_ZStageAxisMaxLimitUm, "0.0", MM::Float, false, pAct, true);

   

   //Unit length
   pAct = new CPropertyAction (this, &PMD101_ZStage::OnUnitLength);
   CreateProperty("Encoder Step Size uM", "10.0", MM::Float, false, pAct, true);

   // // EncoderCountsPerUnit
   //pAct = new CPropertyAction (this, &PMD101_ZStage::OnEncoderCountsPerUnit);
   //CreateProperty("EncoderCountsPerUnit", "8192", MM::Integer, false, pAct,true);
   //stepSizeUm_ = unitLength_/1000;

   pAct = new CPropertyAction (this, &PMD101_ZStage::OnIsRotation);
   int nRet = CreateProperty(g_AxisType, g_No, MM::String, false, pAct, true);
   assert(nRet == DEVICE_OK);
   AddAllowedValue(g_AxisType, g_Yes);
   AddAllowedValue(g_AxisType, g_No);

 /*  pAct = new CPropertyAction (this, &PMD101_ZStage::OnNIChannel);
    nRet = CreateProperty(g_PropertyChannel, "devname", MM::String, false, pAct, true);
   assert(nRet == DEVICE_OK);

   pAct = new CPropertyAction (this, &PMD101_ZStage::OnHasNI);
   nRet = CreateProperty(g_PropertyHasNI, g_No, MM::String, false, pAct, true);
   assert(nRet == DEVICE_OK);
   AddAllowedValue(g_PropertyHasNI, g_No);
   AddAllowedValue(g_PropertyHasNI, g_Yes);*/
   
}

PMD101_ZStage::~PMD101_ZStage()
{
   Shutdown();
}

void PMD101_ZStage::GetName(char* Name) const
{
   CDeviceUtils::CopyLimitedString(Name, g_PMD101_ZStageDeviceName);
}

int PMD101_ZStage::Initialize()
{
	// test ismoving
	checkIsMoving_ = true;
	Busy();
   // switch on servo, otherwise "MOV" will fail
  ostringstream command;
   /*command << "Y1=2";
   int ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
   if (ret != DEVICE_OK)
      return ret;*/

   

  
   CDeviceUtils::SleepMs(10);
   int ret = GetPositionSteps(curSteps_);
   if (ret != DEVICE_OK)
      return ret;

   // axis limits
   CPropertyAction* pAct = new CPropertyAction (this, &PMD101_ZStage::OnPosition);
   CreateProperty(MM::g_Keyword_Position, "0.0", MM::Float, false, pAct);
   SetPropertyLimits(MM::g_Keyword_Position, axisMinLimitUm_, axisMaxLimitUm_);

   ret = UpdateStatus();
   if (ret != DEVICE_OK)
      return ret;

      if (hasNi_)
   {
	   pAct = new CPropertyAction (this, &PMD101_ZStage::OnOffset);

	   int nRet = CreateProperty("NI Offset", "-2", MM::Float, false, pAct);
	   assert(nRet == DEVICE_OK);
	   nRet = SetPropertyLimits("NI Offset",minV_,maxV_);
	   assert(nRet == DEVICE_OK);


	   pAct = new CPropertyAction (this, &PMD101_ZStage::OnReferencePoint);

	   nRet = CreateProperty("NI Reference Point", "-2", MM::Float, false, pAct);
	   assert(nRet == DEVICE_OK);
	   nRet = SetPropertyLimits("NI Reference Point",axisMinLimitUm_,axisMaxLimitUm_);
	   assert(nRet == DEVICE_OK);

	   pAct = new CPropertyAction (this, &PMD101_ZStage::OnSlope);
	   nRet = CreateProperty("NI Slope", "0.02", MM::Float, false, pAct);
	   assert(nRet == DEVICE_OK);
	   double range = (maxV_-minV_)/100.;
	   nRet = SetPropertyLimits("NI Slope",-range,range);
	   assert(nRet == DEVICE_OK);



	   //long niRet = DAQmxCreateTask("", &task_);
	   //if (niRet != DAQmxSuccess)
		//   return (int)niRet;

	   //niRet = DAQmxCreateAOVoltageChan(task_, ni_aochannel_.c_str(), "", minV_, maxV_ ,DAQmx_Val_Volts,NULL);
	   //if (niRet != DAQmxSuccess)
		//   return (int)niRet;

		//   niRet = DAQmxStartTask(task_);


   }


   initialized_ = true;
   return DEVICE_OK;
}

int PMD101_ZStage::Shutdown()
{
	if (initialized_)
	{
		initialized_ = false;
	}

	//if(hasNi_)
	//{
	//	DAQmxStopTask(task_);
	//	DAQmxClearTask(task_);
	//}

   return DEVICE_OK;
}

bool PMD101_ZStage::Busy()
{
   if (!checkIsMoving_)
      return false;

   ostringstream command;
   command << "*";

   // send command
   int ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
   if (ret != DEVICE_OK)
      return ret;


   // block/wait for acknowledge, or until we time out;
   string answer;
   ret = GetSerialAnswer(port_.c_str(), "\r", answer);
   if (ret != DEVICE_OK)
   {
      // "#5" failed, maybe controller does not support this
      // clear error with two "ERR?"
      /*GetError();
      GetError();*/
      checkIsMoving_ = false;
      return false;
   }

   long isMoving;
   if (!GetValue(answer, isMoving))
      return false;

   return (isMoving != 0);
}

int PMD101_ZStage::SetPositionSteps(long steps)
{
	//long currentSteps;
	//GetPositionSteps(currentSteps);

	//if(isRotation_) // rotate the shortest way!)
	//{
	//	long countsPerRotation = 360.0 *  (double)encoderCountsPerUnit_ / unitLength_;
	//	long mValCurrent = currentSteps % countsPerRotation;
	//	long mValCommandSteps = steps % countsPerRotation;
	//	unsigned long lVal = abs(currentSteps / countsPerRotation +.5);

	//	if(lVal > 10){ // reset encoder if encoder has counted several rotations 
	//		currentSteps = mValCurrent;

	//		ostringstream command;
	//		command << "S";

	//		// send command
	//		int ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	//		if (ret != DEVICE_OK)
	//			return ret;

	//		command << "O" << mValCurrent;

	//		// send command
	//		ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	//		if (ret != DEVICE_OK)
	//			return ret;

	//		command << "T" << mValCurrent;

	//		// send command
	//		ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	//		if (ret != DEVICE_OK)
	//			return ret;

	//	}

	//	long posDiff  = mValCurrent - mValCommandSteps;
	//	if(abs(posDiff) >   countsPerRotation / 2)
	//	{
	//		steps = currentSteps - posDiff + countsPerRotation;
	//	}
	//	else
	//		steps = currentSteps + posDiff;
	//	
	//}

	//if(currentSteps > steps){
	//	ostringstream command;
	//	command << "T" << steps - (backlash_/stepSizeUm_);

	//	// send command
	//	int ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	//	if (ret != DEVICE_OK)
	//		return ret;

	//	CDeviceUtils::SleepMs(10);
	//}

	ostringstream command;
   command << "T" << steps;

   // send command
   int ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
   if (ret != DEVICE_OK)
      return ret;

  // if(hasNi_)
  // {
//	  UpdateVoltage();
  // }
  
   
   CDeviceUtils::SleepMs(10);
   // block/wait for acknowledge, or until we time out;
   //return GetError();
   return DEVICE_OK;

  
}

//int PMD101_ZStage::UpdateVoltage()
//{
	//float64 data[1];

	//double currentPosition;
	//GetPositionUm(currentPosition);
	//data[0] = (float64)(referencePoint_-currentPosition) * slope_ + offset_ ;

	//if(data[0] > maxV_)
	//	data[0] = maxV_;
	//if(data[0] < minV_)
	//	data[0] = minV_;


	//long niRet = DAQmxWriteAnalogF64(task_, 1, 1, 10.0, DAQmx_Val_GroupByChannel, data, NULL, NULL);
	//if (niRet != DAQmxSuccess)
	//{
	//	ostringstream os;
	//	os << "Error setting voltage on the NI card: v=" << data[0] << " V, error=" << niRet;
	//	LogMessage(os.str());
	//	return (int)niRet;
	//}
//}


int PMD101_ZStage::GetPositionSteps(long& steps)
{
	ostringstream command;
   command << "e" ;

   // send command
   int ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
   if (ret != DEVICE_OK)
      return ret;

   // block/wait for acknowledge, or until we time out;
   string answer;
   ret = GetSerialAnswer(port_.c_str(), "\r", answer);
   if (ret != DEVICE_OK)
      return ret;

   if(!ExtractValue(answer,"e"))
	    return ERR_UNRECOGNIZED_ANSWER;

   if (!GetValue(answer, steps))
      return ERR_UNRECOGNIZED_ANSWER;
   //steps = steps /100;
   return DEVICE_OK;

}
  
int PMD101_ZStage::SetPositionUm(double pos)
{
	long steps = pos / stepSizeUm_;
	//long steps = (pos * 1000) / 10;
   return SetPositionSteps(steps);

}

int PMD101_ZStage::GetError()
{
   int ret = SendSerialCommand(port_.c_str(), "ERR?", "\n");
   if (ret != DEVICE_OK)
      return ret;
   string answer;
   ret = GetSerialAnswer(port_.c_str(), "\n", answer);
   if (ret != DEVICE_OK)
      return ret;

   int errNo = atoi(answer.c_str());
   if (errNo == 0)
	   return DEVICE_OK;

   return ERR_OFFSET + errNo;   
}

int PMD101_ZStage::GetPositionUm(double& pos)
{
	long steps;
	int ret = GetPositionSteps(steps);
	if(ret != DEVICE_OK)
		return ret;

	pos = steps * stepSizeUm_;
	return DEVICE_OK;
  
}


int PMD101_ZStage::SetOrigin()
{
	int indexTargetPosition =0;
	ostringstream command;
	command << "S" ;

	// send command
	int ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;
	command.str("");
	command << "Y2=126" ;

	
	// send command
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;



	int apos = 100000;
	command.str("");
	command << "-" << apos ;
	// send command
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;

	CDeviceUtils::SleepMs(500);

	command.str("");
	command << "C1" ;

	// send command
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;
	CDeviceUtils::SleepMs(5000);

	command.str("");

	command << "Y73=" << indexTargetPosition ; //Y72 is the command to denote nominal position at index  

	// send command

	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;


	command.str("");
	command << "Y4=3000000" ;
	// send command
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;
	// Start the index search 
	// N1 reference search to "left" with limit #0
	// N2 reference search to "right" with limit #1
	// N3 reference search to "left", for encoder index
	// N4 reference search to "right", for encoder index
	// N5 first do N1, than N4
	// N6 first do N2, than N3
	// N7 reference search to "left" till physical BLOCK
	// N8 reference search to "right" till physical BLOCK
	// N9 first do N7, than N2
	// N10 first do N8, than N1

	command.str("");
	command << "N8" ;


	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;


	// wait for reference drive to finish
	int i=0;
	for( i=0;i<40;i++)
	{
		command.str("");
		command << "n" ; //poll status
		 ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
		if (ret != DEVICE_OK)
			return ret;
		
		// block/wait for acknowledge, or until we time out;
		string answer;
		ret = GetSerialAnswer(port_.c_str(), "\r", answer);
		if (ret != DEVICE_OK)
			return ret;

		double status;
		if (!GetValue(answer, status))
			return ERR_UNRECOGNIZED_ANSWER;

		if(status == 0)
			break;

		CDeviceUtils::SleepMs(2000);

	}

	if(i==40)
		return ERROR_TIMEOUT;
	
	/////////////
	///////// Calibrate Zero
	////////////
	

	command.str("");
	command << "-" <<  -10000 ;
	// send command
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;
	//Set bottom as zero
	command.str("");
	command << "O0" ;
	// send command
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;

	command.str("");
	command << "Y4=0" ;
	// send command
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;

	

	/////////////
	///////// probe for encoder directionality
	////////////
	command.str("");
	command << "E" ; //poll status
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;
	// block/wait for acknowledge, or until we time out;
	string answer;
	ret = GetSerialAnswer(port_.c_str(), "\r", answer);
	if (ret != DEVICE_OK)
		return ret;	
	


	command.str("");
	command << "T" << indexTargetPosition ;

	// send command
	ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
	if (ret != DEVICE_OK)
		return ret;


	return DEVICE_OK;
}

int PMD101_ZStage::GetLimits(double &min, double &max)
{
	ostringstream command;
	command << "Y3?" ;
	 int ret = SendSerialCommand(port_.c_str(), command.str().c_str(), "\r");
   if (ret != DEVICE_OK)
      return ret;


	min = axisMinLimitUm_;
	max = axisMaxLimitUm_;
   return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// Action handlers
///////////////////////////////////////////////////////////////////////////////

int PMD101_ZStage::OnPort(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
      pProp->Set(port_.c_str());
   }
   else if (eAct == MM::AfterSet)
   {
      if (initialized_)
      {
         // revert
         pProp->Set(port_.c_str());
         return ERR_PORT_CHANGE_FORBIDDEN;
      }

      pProp->Get(port_);
   }

   return DEVICE_OK;
}


//int PMD101_ZStage::OnEncoderCountsPerUnit(MM::PropertyBase* pProp, MM::ActionType eAct)
//{
//	if (eAct == MM::BeforeGet)
//	{
//
//		pProp->Set(encoderCountsPerUnit_);
//
//	}
//	else if (eAct == MM::AfterSet)
//	{
//		double ec_pUnit;
//		pProp->Get(encoderCountsPerUnit_);
//		stepSizeUm_ = unitLength_ / encoderCountsPerUnit_;
//
//	}
//
//   return DEVICE_OK;
//}

int PMD101_ZStage::OnUnitLength(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
	  
		   pProp->Set(unitLength_);
	   
   }
   else if (eAct == MM::AfterSet)
   {
	 
      pProp->Get(unitLength_);
	  stepSizeUm_ = unitLength_ / 1000;
	 
   }

   return DEVICE_OK;
}

int PMD101_ZStage::OnIsRotation(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
      pProp->Set(isRotation_ ? g_Yes : g_No);
   }
   else if (eAct == MM::AfterSet)
   {
      string val;
      pProp->Get(val);
      if (val.compare(g_Yes) == 0)
         isRotation_ = true;
      else
         isRotation_ = false;
   }

   return DEVICE_OK;
}

int PMD101_ZStage::OnAxisMinLimit(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
      pProp->Set(axisMinLimitUm_);

   }
   else if (eAct == MM::AfterSet)
   {
	 //  long minSteps = 

      pProp->Get(axisMinLimitUm_);
   }

   return DEVICE_OK;
}

int PMD101_ZStage::OnAxisMaxLimit(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
      pProp->Set(axisMaxLimitUm_);
   }
   else if (eAct == MM::AfterSet)
   {
      pProp->Get(axisMaxLimitUm_);
   }

   return DEVICE_OK;
}

int PMD101_ZStage::OnPosition(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
      double pos;
      int ret = GetPositionUm(pos);
      if (ret != DEVICE_OK)
         return ret;

      pProp->Set(pos);
   }
   else if (eAct == MM::AfterSet)
   {
      double pos;
      pProp->Get(pos);
      int ret = SetPositionUm(pos);
      if (ret != DEVICE_OK)
         return ret;

   }

   return DEVICE_OK;
}

//int PMD101_ZStage::OnHasNI(MM::PropertyBase* pProp, MM::ActionType eAct)
//{
//   if (eAct == MM::BeforeGet)
//   {
//      pProp->Set(hasNi_ ? g_Yes : g_No);
//   }
//   else if (eAct == MM::AfterSet)
//   {
//      string val;
//      pProp->Get(val);
//      if (val.compare(g_Yes) == 0)
//         hasNi_ = true;
//      else
//         hasNi_ = false;
//   }
//
//   return DEVICE_OK;
//}

//int PMD101_ZStage::OnNIChannel(MM::PropertyBase* pProp, MM::ActionType eAct)
//{
//	
//		//cout << ni_channel_.c_str();
//   if (eAct == MM::BeforeGet)
//   {
//      pProp->Set(ni_aochannel_.c_str());
//	  
//   }
//   else if (eAct == MM::AfterSet)
//   {
//      pProp->Get(ni_aochannel_);
//   }
//
//   return DEVICE_OK;
//}

int PMD101_ZStage::OnSlope(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	 if (eAct == MM::BeforeGet)
   {
      pProp->Set(slope_);
	  
   }
   else if (eAct == MM::AfterSet)
   {
      pProp->Get(slope_);
   }

//	 int ret = UpdateVoltage();
	 //if(ret != DEVICE_OK)
	//	 return ret;

   return DEVICE_OK;
}
  
int PMD101_ZStage::OnOffset(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	 if (eAct == MM::BeforeGet)
   {
      pProp->Set(offset_);
	  
   }
   else if (eAct == MM::AfterSet)
   {
      pProp->Get(offset_);
   }
  // int ret = UpdateVoltage();
  // if(ret != DEVICE_OK)
	//   return ret;

   return DEVICE_OK;
}

int PMD101_ZStage::OnReferencePoint(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	 if (eAct == MM::BeforeGet)
   {
      pProp->Set(referencePoint_);
	  
   }
   else if (eAct == MM::AfterSet)
   {
      pProp->Get(referencePoint_);
   }

	// int ret = UpdateVoltage();
	// if(ret != DEVICE_OK)
	//	 return ret;

   return DEVICE_OK;
}


bool PMD101_ZStage::GetValue(string& sMessage, double& dval)
{
   if (!ExtractValue(sMessage))
      return false;
   
   char *pend;
   const char* szMessage = sMessage.c_str();
   double dValue = strtod(szMessage, &pend);
   
   // return true only if scan was stopped by spaces, linefeed or the terminating NUL and if the
   // string was not empty to start with
   if (pend != szMessage)
   {
      while( *pend!='\0' && (*pend==' '||*pend=='\n')) pend++;
      if (*pend=='\0')
      {
         dval = dValue;
         return true;
      }
   }
   return false;
}

bool PMD101_ZStage::GetValue(string& sMessage, long& lval)
{
   if (!ExtractValue(sMessage))
      return false;

   char *pend;
   const char* szMessage = sMessage.c_str();
   long lValue = strtol(szMessage, &pend, 0);
   
   // return true only if scan was stopped by spaces, linefeed or the terminating NUL and if the
   // string was not empty to start with
   if (pend != szMessage)
   {
      while( *pend!='\0' && (*pend==' '||*pend=='\n')) pend++;
      if (*pend=='\0')
      {
         lval = lValue;
         return true;
      }
   }
   return false;
}

bool PMD101_ZStage::ExtractValue(std::string& sMessage,const char* leadingPattern)
{
	 size_t p = sMessage.find_last_of(leadingPattern);
	 if ( p != std::string::npos )
       sMessage.erase(0,p+1);

	  // trim whitspaces from right ...
   p = sMessage.find_last_not_of(" \t\r\n");
   if (p != std::string::npos)
       sMessage.erase(++p);
   
   // ... and left
   p = sMessage.find_first_not_of(" \n\t\r");
   if (p == std::string::npos)
      return false;
   
   sMessage.erase(0,p);
   return true;
}

bool PMD101_ZStage::ExtractValue(std::string& sMessage)
{
   // value is after last '=', if any '=' is found
   size_t p = sMessage.find_last_of('=');
   if ( p != std::string::npos )
       sMessage.erase(0,p+1);
   
   // trim whitspaces from right ...
   p = sMessage.find_last_not_of(" \t\r\n");
   if (p != std::string::npos)
       sMessage.erase(++p);
   
   // ... and left
   p = sMessage.find_first_not_of(" \n\t\r");
   if (p == std::string::npos)
      return false;
   
   sMessage.erase(0,p);
   return true;
}
