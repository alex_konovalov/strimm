///////////////////////////////////////////////////////////////////////////////
// FILE:          PI_GCS.h
// PROJECT:       Micro-Manager
// SUBSYSTEM:     DeviceAdapters
//-----------------------------------------------------------------------------
// DESCRIPTION:   PI GCS Controller Driver
//
// AUTHOR:        Nenad Amodaj, nenad@amodaj.com, 08/28/2006
//                Steffen Rau, s.rau@pi.ws, 28/03/2008
// COPYRIGHT:     University of California, San Francisco, 2006
//                Physik Instrumente (PI) GmbH & Co. KG, 2008
// LICENSE:       This file is distributed under the BSD license.
//                License text is included with the source distribution.
//
//                This file is distributed in the hope that it will be useful,
//                but WITHOUT ANY WARRANTY; without even the implied warranty
//                of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//                IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//                CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//                INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.
// CVS:           $Id: Nikon.cpp 574 2007-11-07 21:00:45Z nenad $
//

#ifndef _PMD101_H_
#define _PMD101_H_

#include "../../MMDevice/MMDevice.h"
#include "../../MMDevice/DeviceBase.h"
//#include "NIDAQmx.h"
#include <string>
#include <map>

//////////////////////////////////////////////////////////////////////////////
// Error codes
//
#define ERR_PORT_CHANGE_FORBIDDEN    10004
#define ERR_UNRECOGNIZED_ANSWER      10009
#define ERR_OFFSET 10100

class PMD101_ZStage : public CStageBase<PMD101_ZStage>
{
public:
   PMD101_ZStage();
   ~PMD101_ZStage();
  
   // Device API
   // ----------
   int Initialize();
   int Shutdown();
  
   void GetName(char* pszName) const;
   bool Busy();

   // Stage API
   // ---------
  int SetPositionUm(double pos);
  int GetPositionUm(double& pos);
  int SetPositionSteps(long steps);
  int GetPositionSteps(long& steps);
  int SetOrigin();
  int GetLimits(double& min, double& max);

  int IsStageSequenceable(bool& isSequenceable) const {isSequenceable = false; return DEVICE_OK;}
  bool IsContinuousFocusDrive() const {return false;}

   // action interface
   // ----------------
   int OnPort(MM::PropertyBase* pProp, MM::ActionType eAct);
   /*int OnEncoderCountsPerUnit(MM::PropertyBase* pProp, MM::ActionType eAct);*/
   int OnUnitLength(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnIsRotation(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnAxisName(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnAxisMinLimit(MM::PropertyBase* pProp, MM::ActionType eAct);  
   int OnAxisMaxLimit(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnPosition(MM::PropertyBase* pProp, MM::ActionType eAct);

   /*int OnHasNI(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnNIChannel(MM::PropertyBase* pProp, MM::ActionType eAct);*/
   int OnSlope(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnOffset(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnReferencePoint(MM::PropertyBase* pProp, MM::ActionType eAct);
  
   //int OnControllerNegLimit(MM::PropertyBase* pProp, MM::ActionType eAct); //Y3 set by = query by ?
   //int OnControllerPosLimit(MM::PropertyBase* pProp, MM::ActionType eAct); //Y4
   //int OnControllerTargetCountWindow(MM::PropertyBase* pProp, MM::ActionType eAct); //Y5
   //int OnControllerMinSpeed(MM::PropertyBase* pProp, MM::ActionType eAct); //Y7
   //int OnControllerMaxSpeed(MM::PropertyBase* pProp, MM::ActionType eAct); //Y8
   //int OnControllerMaxStartAcceleration(MM::PropertyBase* pProp, MM::ActionType eAct); //Y9 startRampe
   //int OnControllerMaxStopAcceleration(MM::PropertyBase* pProp, MM::ActionType eAct); //Y10 BremsRampe
   //int OnControllerTargetMode(MM::PropertyBase* pProp, MM::ActionType eAct); //Y12 ReglerTyp
   //int OnControllerAxisID(MM::PropertyBase* pProp, MM::ActionType eAct); //Y40 Axis ID
   //int OnControllerTargetReached(MM::PropertyBase* pProp, MM::ActionType eAct); //Y55 target reached window
   //int OnControllerEncoderScale(MM::PropertyBase* pProp, MM::ActionType eAct); //Y62 encoder scale
   //int OnControllerEncoderFilter(MM::PropertyBase* pProp, MM::ActionType eAct); //Y65 encoder filter
   //int OnControllerEncoderHysteresis(MM::PropertyBase* pProp, MM::ActionType eAct); //Y67 encoder hysteresis

//C1
//    Y9



  


private:
   int ExecuteCommand(const std::string& cmd, std::string& response);
   bool GetValue(std::string& sMessage, double& dval);
   bool GetValue(std::string& sMessage, long& lval);
   bool ExtractValue(std::string& sMessage);
   bool ExtractValue(std::string& sMessage,const char* leadingPattern);
   //int UpdateVoltage();

   int GetError();

   std::string port_;
   std::string ni_aochannel_;

   double minV_;
   double maxV_;

   bool hasNi_;
   bool checkIsMoving_;
   double stepSizeUm_;
   double backlash_;
   bool initialized_;
   long curSteps_;
   double referencePoint_;
   double answerTimeoutMs_;
   double axisMinLimitUm_;
   double axisMaxLimitUm_;

   double slope_;
   double offset_;

   double unitLength_;
   bool isRotation_;

   long encoderCountsPerUnit_;



   //TaskHandle task_;
};

#endif //_PI_GCS_H_
