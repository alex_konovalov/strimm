#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define     RAD_SCALE               57.295778667  // 360 / ( 2 * pi )
#define     TWO_COS_D               1.9696155
#define     MIRROR_DISTANCE         54.4
#define     D                       10.0
#define     SLIT_SCALE              628.2800000000



// Here lines_mm is grating lines per mm and value is wavelength in volts out.
int os_Wavelength_To_Voltage (double mech_offset, double lines_mm , double * value )
{
    double tmp = *value;
    double lines_nm = lines_mm / 1000000;
    tmp = tmp / TWO_COS_D;
    tmp = tmp * lines_nm;
    tmp = asin ( tmp );
    tmp = tmp * RAD_SCALE;
    tmp = tmp / 4;
    tmp = tmp - 2.5;
    *value = tmp + mech_offset;
    return 0;
}

int os_In_Slit_Bandwidth_To_Width ( double lines_mm , double wavelength , double * value )
{
    double tmp = *value;
    double lines_nm = lines_mm / 1000000;
    double alpha = lines_nm * wavelength;
    alpha = alpha / TWO_COS_D;
    alpha = asin ( alpha );
    alpha = alpha * RAD_SCALE;
    alpha = alpha - D;
    tmp = tmp * lines_nm * MIRROR_DISTANCE;
    tmp = tmp / ( cos ( alpha / RAD_SCALE ) );
    tmp = tmp * 1000;
    *value = tmp;
    return 0;
}

int os_In_Slit_Width_To_Voltage ( double * value )
{
    double tmp = *value;
    tmp = tmp / SLIT_SCALE;
    *value = tmp;
    return 0;
}

int os_Out_Slit_Bandwidth_To_Width ( double lines_mm , double wavelength , double * value )
{
    double tmp = *value;
    double lines_nm = lines_mm / 1000000;
    double alpha = lines_nm * wavelength;
    alpha = alpha / TWO_COS_D;
    alpha = asin ( alpha );
    alpha = alpha * RAD_SCALE;
    double beta = alpha + D;
    tmp = tmp * lines_nm * MIRROR_DISTANCE;
    tmp = tmp / ( cos ( beta / RAD_SCALE ) );
    tmp = tmp * 1000;
    *value = tmp;
    return 0;
}

int os_Out_Slit_Width_To_Voltage ( double * value )
{
    double tmp = *value;
    tmp = tmp / SLIT_SCALE;
    *value = tmp;
    return 0;
}


