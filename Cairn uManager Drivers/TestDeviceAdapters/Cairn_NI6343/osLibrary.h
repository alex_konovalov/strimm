/*
	File -			osLibrary.h 
	Description -	Header file for using osLibrary.dll to drive the Optoscan.
	Copyright -		2002-2007 Cairn Research Ltd.
	Contact -		software@cairn-research.co.uk
*/

#include <stdio.h>
#include <windows.h>

// Standard library response to no error condition
#define os_OK  0

// Useful global variables
char szOSVersion[50];				// Contains library version after call to OSOpenLibrary
double dOSVersion;					// Contains library version after call to OSOpenLibrary
char szOSQueryIO[1024];				// Contains current interface list after call to OSOpenLibrary

// Useful structure to hold details about the monochromator.
// See notes at the end on osLibrary.ini about the quality
// of this information.
typedef struct {
  char szOSLines[10];				// String showing the lines per nm of the grating
  char szOSMinWave[25];				// Minimum supported wavelength of the grating in nm
  char szOSMaxWave[25];				// Maximum supported wavelength of the grating in nm
  char szOSMinInSlit[25];			// Minimum supported wavelength of the input slit in nm
  char szOSMaxInSlit[25];			// Maximum supported wavelength of the input slit in nm
  char szOSMinOutSlit[25];			// Minimum supported wavelength of the output slit in nm
  char szOSMaxOutSlit[25];			// Maximum supported wavelength of the output slit in nm
  BOOL bOSInSlitDriven;				// Is the input slit drive (as opposed to manual)
  BOOL bOSOutSlitDriven;			// Is the output slit drive (as opposed to manual)
  DWORD dwOSLines;					// Number of lines per mm on the grating
  double dOSMinWave;				
  double dOSMaxWave;
  double dOSMinInSlit;
  double dOSMaxInSlit;
  double dOSMinOutSlit;
  double dOSMaxOutSlit;
} sMonochromator;




// These are the prototype definitions of the calls into osLibrary.dll , ...
typedef double		(CALLBACK* lpos_Library_Version) ( );
typedef LPSTR		(CALLBACK* lpos_Library_Version_Str) ( );
typedef LPSTR		(CALLBACK* lpos_Char_Error_String) ( DWORD ECode );
typedef DWORD		(CALLBACK* lpos_Query_IO) ( DWORD Reserved , LPSTR Data , DWORD Size );
typedef DWORD		(CALLBACK* lpos_Close_IO) ( );
typedef DWORD		(CALLBACK* lpos_Open_IO) ( DWORD Reserved , LPSTR Data );
typedef DWORD		(CALLBACK* lpos_Wavelength_To_Voltage) ( double *Wavelength );
typedef DWORD		(CALLBACK* lpos_Wavelength_To_RGB) ( double Wavelength, PBYTE R, PBYTE G, PBYTE B );
typedef DWORD		(CALLBACK* lpos_Min_Wavelength) ( double *Wavelength );
typedef DWORD		(CALLBACK* lpos_Max_Wavelength) ( double *Wavelength );
typedef DWORD		(CALLBACK* lpos_Min_InSlit) ( double *Wavelength );
typedef DWORD		(CALLBACK* lpos_Max_InSlit) ( double *Wavelength );
typedef DWORD		(CALLBACK* lpos_Min_OutSlit) ( double *Wavelength );
typedef DWORD		(CALLBACK* lpos_Max_OutSlit) ( double *Wavelength );
typedef DWORD		(CALLBACK* lpos_Valid_Params) ( double Wavelength, double InSlit, double OutSlit );
typedef DWORD		(CALLBACK* lpos_Set_Params) ( double Wavelength, double InSlit, double OutSlit );
typedef DWORD		(CALLBACK* lpos_Shut_In_Slit) ( BOOL Value );
typedef DWORD		(CALLBACK* lpos_Shut_Out_Slit) ( BOOL Value );
typedef DWORD		(CALLBACK* lpos_Shut_Grating) ( BOOL Value );
typedef DWORD		(CALLBACK* lpos_Shut_Both_Slits) ( BOOL Value );
typedef DWORD		(CALLBACK* lpos_In_Slit_Bandwidth_To_Width) ( double Wavelength, double *Slit );
typedef DWORD		(CALLBACK* lpos_Out_Slit_Bandwidth_To_Width) ( double Wavelength, double *Slit );
typedef DWORD		(CALLBACK* lpos_In_Slit_Width_To_Voltage) ( double *Slit );
typedef DWORD		(CALLBACK* lpos_Out_Slit_Width_To_Voltage) ( double *Slit );
typedef DWORD		(CALLBACK* lpos_In_Slit_Width_To_Bandwidth) ( double Wavelength, double *Slit );
typedef DWORD		(CALLBACK* lpos_Out_Slit_Width_To_Bandwidth) ( double Wavelength, double *Slit );
typedef DWORD		(CALLBACK* lpos_Switch_Control) ( BOOL Value );
typedef DWORD		(CALLBACK* lpos_Get_Ready) ( BOOL *Wavelength, BOOL *InSlit, BOOL *OutSlit );
typedef DWORD		(CALLBACK* lpos_Num_Driver_Errors) ( );
typedef LPSTR		(CALLBACK* lpos_Last_Driver_Error) ( );
typedef DWORD		(CALLBACK* lpos_Grating_Lines) ( DWORD *Lines );
typedef DWORD		(CALLBACK* lpos_Set_Grating_Lines) ( DWORD Lines );
typedef DWORD		(CALLBACK* lpos_Slits_Driven) ( BOOL *InSlitsDriven , BOOL *OutSlitsDriven );
typedef DWORD		(CALLBACK* lpos_Get_Handle) ( DWORD *Handle );
typedef DWORD		(CALLBACK* lpos_Set_Handle) ( DWORD Handle );


// ... and these are the variables that the function addresses will be loaded into.
lpos_Library_Version				os_Library_Version =				NULL;
lpos_Library_Version_Str			os_Library_Version_Str =			NULL;
lpos_Char_Error_String  			os_Char_Error_String =				NULL;
lpos_Query_IO						os_Query_IO =						NULL;
lpos_Open_IO						os_Open_IO =						NULL;
lpos_Close_IO						os_Close_IO =						NULL;
lpos_Wavelength_To_Voltage			os_Wavelength_To_Voltage =			NULL;
lpos_Wavelength_To_RGB				os_Wavelength_To_RGB =				NULL;
lpos_Min_Wavelength					os_Min_Wavelength =					NULL;
lpos_Max_Wavelength					os_Max_Wavelength =					NULL;
lpos_Min_InSlit 					os_Min_InSlit =						NULL;
lpos_Max_InSlit						os_Max_InSlit =						NULL;
lpos_Min_OutSlit 					os_Min_OutSlit =					NULL;
lpos_Max_OutSlit					os_Max_OutSlit =					NULL;
lpos_Valid_Params					os_Valid_Params =					NULL;
lpos_Set_Params						os_Set_Params =						NULL;
lpos_Shut_In_Slit					os_Shut_In_Slit =					NULL;
lpos_Shut_Out_Slit					os_Shut_Out_Slit =					NULL;
lpos_Shut_Grating					os_Shut_Grating =					NULL;
lpos_Shut_Both_Slits				os_Shut_Both_Slits =				NULL;
lpos_In_Slit_Bandwidth_To_Width		os_In_Slit_Bandwidth_To_Width =		NULL;
lpos_Out_Slit_Bandwidth_To_Width	os_Out_Slit_Bandwidth_To_Width =	NULL;
lpos_In_Slit_Width_To_Voltage		os_In_Slit_Width_To_Voltage =		NULL;
lpos_Out_Slit_Width_To_Voltage		os_Out_Slit_Width_To_Voltage =		NULL;
lpos_In_Slit_Width_To_Bandwidth		os_In_Slit_Width_To_Bandwidth =		NULL;
lpos_Out_Slit_Width_To_Bandwidth	os_Out_Slit_Width_To_Bandwidth =	NULL;
lpos_Num_Driver_Errors				os_Num_Driver_Errors =				NULL;
lpos_Last_Driver_Error				os_Last_Driver_Error =				NULL;
lpos_Grating_Lines					os_Grating_Lines =					NULL;
lpos_Set_Grating_Lines				os_Set_Grating_Lines =				NULL;
lpos_Slits_Driven					os_Slits_Driven =					NULL;
lpos_Get_Handle						os_Get_Handle =						NULL;
lpos_Set_Handle						os_Set_Handle =						NULL;


// General Windoze GetLastError error message display function
void WinError ( void )
{
	DWORD Err = GetLastError();
	if ( Err == 0 ) return;
	LPVOID lpMsgBuf;
	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		Err,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL );
	MessageBox( 0 , (LPCTSTR)lpMsgBuf, "Windows Error", MB_OK | MB_ICONINFORMATION );
	LocalFree( lpMsgBuf );
}


// General function that can be use to check for an
// error condition on the result of any osLibrary
// function call.
BOOL ErrorCheck( DWORD code )
{
	if (code == os_OK) return FALSE;
	LPSTR errorstr = os_Char_Error_String(code);
	MessageBox( 0 , errorstr , "osLibrary Error" , MB_OK | MB_ICONERROR );
	return TRUE;
}


void NullAll ( void ) 
{  
	os_Library_Version =				NULL;
	os_Library_Version_Str =			NULL;
  	os_Char_Error_String =				NULL;
	os_Query_IO =						NULL;
	os_Open_IO =						NULL;
	os_Close_IO =						NULL;
	os_Wavelength_To_Voltage =			NULL;
    os_Wavelength_To_RGB =				NULL;
	os_Min_Wavelength =					NULL;
	os_Max_Wavelength =					NULL;
	os_Min_InSlit =						NULL;
	os_Max_InSlit =						NULL;
	os_Min_OutSlit =					NULL;
	os_Max_OutSlit =					NULL;
	os_Valid_Params =					NULL;
	os_Set_Params =						NULL;
	os_Shut_In_Slit =					NULL;
	os_Shut_Out_Slit =					NULL;
	os_Shut_Grating =					NULL;
	os_Shut_Both_Slits =				NULL;
	os_In_Slit_Bandwidth_To_Width =		NULL;
	os_Out_Slit_Bandwidth_To_Width =	NULL;
	os_In_Slit_Width_To_Voltage =		NULL;
	os_Out_Slit_Width_To_Voltage =		NULL;
	os_In_Slit_Width_To_Bandwidth =		NULL;
	os_Out_Slit_Width_To_Bandwidth =	NULL;
    os_Num_Driver_Errors =				NULL;
	os_Last_Driver_Error =				NULL;
	os_Grating_Lines =					NULL;
	os_Set_Grating_Lines =				NULL;
	os_Slits_Driven =					NULL;
    os_Slits_Driven	=					NULL;
    os_Get_Handle =						NULL;
    os_Set_Handle =						NULL;
	szOSVersion[0] =					'\0';
	dOSVersion =						0.0;
	szOSQueryIO[0] =					'\0';
}


// Once an interface has been opened this function can be
// used to populate an sMonochromator structure with the
// details of the Optoscan attached to the interface.
// See notes at the end on osLibrary.ini about the quality
// of this information.
BOOL UpdateMonochromatorDetails ( sMonochromator *Monochromator )
{

	if ( ErrorCheck( os_Grating_Lines( &Monochromator->dwOSLines ) ) ) goto CALL_ERROR;
	sprintf( Monochromator->szOSLines , "%u lines/nm"  , Monochromator->dwOSLines );

	if ( ErrorCheck( os_Min_Wavelength( &Monochromator->dOSMinWave ) ) ) goto CALL_ERROR;
	sprintf( Monochromator->szOSMinWave , "%.1f nm"  , Monochromator->dOSMinWave );

	if (ErrorCheck(os_Max_Wavelength( &Monochromator->dOSMaxWave ))) goto CALL_ERROR;
	sprintf( Monochromator->szOSMaxWave , "%.1f nm"  , Monochromator->dOSMaxWave );

	if ( ErrorCheck( os_Min_InSlit( &Monochromator->dOSMinInSlit ) ) ) goto CALL_ERROR;
	sprintf( Monochromator->szOSMinInSlit , "%.1f nm"  , Monochromator->dOSMinInSlit );

	if (ErrorCheck(os_Max_InSlit( &Monochromator->dOSMaxInSlit ))) goto CALL_ERROR;
	sprintf( Monochromator->szOSMaxInSlit , "%.1f nm"  , Monochromator->dOSMaxInSlit );
	
	if ( ErrorCheck( os_Min_OutSlit( &Monochromator->dOSMinOutSlit ) ) ) goto CALL_ERROR;
	sprintf( Monochromator->szOSMinOutSlit , "%.1f nm"  , Monochromator->dOSMinOutSlit );

	if (ErrorCheck(os_Max_OutSlit( &Monochromator->dOSMaxOutSlit ))) goto CALL_ERROR;
	sprintf( Monochromator->szOSMaxOutSlit , "%.1f nm"  , Monochromator->dOSMaxOutSlit );
	
    if ( ErrorCheck( os_Slits_Driven( &Monochromator->bOSInSlitDriven , &Monochromator->bOSOutSlitDriven ) ) ) goto CALL_ERROR;

	return TRUE;

	CALL_ERROR:

	return FALSE;

}

// Global varable to store the handle of osLibrary.dll whilst it is in use;
HMODULE hLib = NULL;

// This function closes the osLibrary dll
void CloseOSLibrary( void ) 
{
	FreeLibrary(hLib);
	hLib = NULL;
	NullAll();
}

// This function dynamically loads the library dll
// and imports all the functions exported by the dll.
BOOL OpenOSLibrary( void ) 
{
	
	if (hLib != NULL) return true;

	NullAll( );

	hLib=LoadLibrary("osLibrary.dll");
    if(hLib==NULL) return false;

    // Returns double version identifier for the library
	os_Library_Version = (lpos_Library_Version)GetProcAddress( (HMODULE)hLib , "os_Library_Version" );
    if (os_Library_Version == NULL) goto FAIL;

    // Returns char * version identifier for the library
    os_Library_Version_Str = (lpos_Library_Version_Str)GetProcAddress( (HMODULE)hLib , "os_Library_Version_Str" );
    if (os_Library_Version_Str == NULL) goto FAIL;

    // Given an osLibrary error return code will return a char * error string
	os_Char_Error_String = (lpos_Char_Error_String)GetProcAddress( (HMODULE)hLib , "os_Char_Error_String" );
    if (os_Library_Version_Str == NULL) goto FAIL;

    // Given a buffer will return a tab (#9) delimited char * string list of available interfaces
    os_Query_IO = (lpos_Query_IO)GetProcAddress( (HMODULE)hLib , "os_Query_IO" );
    if (os_Query_IO == NULL) goto FAIL;

    // Given a char * string of the required interface will open it and return an error code if unsuccessful
    os_Open_IO = (lpos_Open_IO)GetProcAddress( (HMODULE)hLib , "os_Open_IO" );
    if (os_Open_IO == NULL) goto FAIL;

    os_Close_IO = (lpos_Close_IO)GetProcAddress( (HMODULE)hLib , "os_Close_IO" );
    if (os_Close_IO == NULL) goto FAIL;

    os_Wavelength_To_Voltage = (lpos_Wavelength_To_Voltage)GetProcAddress( (HMODULE)hLib , "os_Wavelength_To_Voltage" );
    if (os_Wavelength_To_Voltage == NULL) goto FAIL;

    os_Wavelength_To_RGB = (lpos_Wavelength_To_RGB)GetProcAddress( (HMODULE)hLib , "os_Wavelength_To_RGB" );
    if (os_Wavelength_To_RGB == NULL) goto FAIL;

    os_Min_Wavelength = (lpos_Min_Wavelength)GetProcAddress( (HMODULE)hLib , "os_Min_Wavelength" );
    if (os_Min_Wavelength == NULL) goto FAIL;
	
    os_Max_Wavelength = (lpos_Max_Wavelength)GetProcAddress( (HMODULE)hLib , "os_Max_Wavelength" );
    if (os_Max_Wavelength == NULL) goto FAIL;
	
    os_Min_InSlit = (lpos_Min_InSlit)GetProcAddress( (HMODULE)hLib , "os_Min_InSlit" );
    if (os_Min_InSlit == NULL) goto FAIL;
	
    os_Max_InSlit = (lpos_Max_InSlit)GetProcAddress( (HMODULE)hLib , "os_Max_InSlit" );
    if (os_Max_InSlit == NULL) goto FAIL;
	
    os_Min_OutSlit = (lpos_Min_OutSlit)GetProcAddress( (HMODULE)hLib , "os_Min_OutSlit" );
    if (os_Min_OutSlit == NULL) goto FAIL;
	
    os_Max_OutSlit = (lpos_Max_OutSlit)GetProcAddress( (HMODULE)hLib , "os_Max_OutSlit" );
    if (os_Max_OutSlit == NULL) goto FAIL;
	
    os_Valid_Params = (lpos_Valid_Params)GetProcAddress( (HMODULE)hLib , "os_Valid_Params" );
    if (os_Valid_Params == NULL) goto FAIL;
	
    os_Set_Params = (lpos_Set_Params)GetProcAddress( (HMODULE)hLib , "os_Set_Params" );
    if (os_Set_Params == NULL) goto FAIL;

    os_Shut_In_Slit = (lpos_Shut_In_Slit)GetProcAddress( (HMODULE)hLib , "os_Shut_In_Slit" );
    if (os_Shut_In_Slit == NULL) goto FAIL;

    os_Shut_Out_Slit = (lpos_Shut_Out_Slit)GetProcAddress( (HMODULE)hLib , "os_Shut_Out_Slit" );
    if (os_Shut_Out_Slit == NULL) goto FAIL;

    os_Shut_Both_Slits = (lpos_Shut_Both_Slits)GetProcAddress( (HMODULE)hLib , "os_Shut_Both_Slits" );
    if (os_Shut_Both_Slits == NULL) goto FAIL;

    os_Shut_Grating = (lpos_Shut_Grating)GetProcAddress( (HMODULE)hLib , "os_Shut_Grating" );
    if (os_Shut_Grating == NULL) goto FAIL;

    os_In_Slit_Bandwidth_To_Width = (lpos_In_Slit_Bandwidth_To_Width)GetProcAddress( (HMODULE)hLib , "os_In_Slit_Bandwidth_To_Width" );
    if (os_In_Slit_Bandwidth_To_Width == NULL) goto FAIL;
    
	os_Out_Slit_Bandwidth_To_Width = (lpos_Out_Slit_Bandwidth_To_Width)GetProcAddress( (HMODULE)hLib , "os_Out_Slit_Bandwidth_To_Width" );
    if (os_Out_Slit_Bandwidth_To_Width == NULL) goto FAIL;

    os_In_Slit_Width_To_Voltage = (lpos_In_Slit_Width_To_Voltage)GetProcAddress( (HMODULE)hLib , "os_In_Slit_Width_To_Voltage" );
    if (os_In_Slit_Width_To_Voltage == NULL) goto FAIL;
    
	os_Out_Slit_Width_To_Voltage = (lpos_Out_Slit_Width_To_Voltage)GetProcAddress( (HMODULE)hLib , "os_Out_Slit_Width_To_Voltage" );
    if (os_Out_Slit_Width_To_Voltage == NULL) goto FAIL;

    os_In_Slit_Width_To_Bandwidth = (lpos_In_Slit_Width_To_Bandwidth)GetProcAddress( (HMODULE)hLib , "os_In_Slit_Width_To_Bandwidth" );
    if (os_In_Slit_Width_To_Bandwidth == NULL) goto FAIL;
	
	os_Out_Slit_Width_To_Bandwidth = (lpos_Out_Slit_Width_To_Bandwidth)GetProcAddress( (HMODULE)hLib , "os_Out_Slit_Width_To_Bandwidth" );
    if (os_Out_Slit_Width_To_Bandwidth == NULL) goto FAIL;

	os_Num_Driver_Errors = (lpos_Num_Driver_Errors)GetProcAddress( (HMODULE)hLib , "os_Num_Driver_Errors" );
    if (os_Num_Driver_Errors == NULL) goto FAIL;
    
	os_Last_Driver_Error = (lpos_Last_Driver_Error)GetProcAddress( (HMODULE)hLib , "os_Last_Driver_Error" );
    if (os_Last_Driver_Error == NULL) goto FAIL;
	
	os_Grating_Lines = (lpos_Grating_Lines)GetProcAddress( (HMODULE)hLib , "os_Grating_Lines" );
    if (os_Grating_Lines == NULL) goto FAIL;

	os_Set_Grating_Lines = (lpos_Set_Grating_Lines)GetProcAddress( (HMODULE)hLib , "os_Set_Grating_Lines" );
    if (os_Set_Grating_Lines == NULL) goto FAIL;

	os_Slits_Driven= (lpos_Slits_Driven)GetProcAddress( (HMODULE)hLib , "os_Slits_Driven" );
    if (os_Slits_Driven == NULL) goto FAIL;

	os_Get_Handle= (lpos_Get_Handle)GetProcAddress( (HMODULE)hLib , "os_Get_Handle" );
    if (os_Get_Handle == NULL) goto FAIL;

	os_Set_Handle= (lpos_Set_Handle)GetProcAddress( (HMODULE)hLib , "os_Set_Handle" );
    if (os_Set_Handle == NULL) goto FAIL;


	// N.B. if optoscans are being hot swapped (e.g. USB version) then
	// os_Query_IO can be called at any time to update the list.
	szOSQueryIO[0] = '\0';
	if ( ErrorCheck( os_Query_IO( 0 , szOSQueryIO , 1024 ) ) ) return false;

	goto ALL_LOADED;
	
	FAIL:
	
	WinError ();
	CloseOSLibrary();
	
	MessageBox( 0 , "Unable to initiate all osLibrary links." , "osLibrary Error" , MB_OK | MB_ICONERROR );
	return FALSE;

	ALL_LOADED:

	szOSVersion[0] = '\0';
	dOSVersion = 0.0;

	dOSVersion = os_Library_Version ();

	strcat( szOSVersion , "Optoscan Library Version " );
	strcat( szOSVersion , os_Library_Version_Str() );

	return TRUE;
	
	
}


//Possible error codes returned by osLibrary.dll
#define  os_UNKNOWN_ERROR                    0xFFFFFFFF;
#define  os_UNSUPPORTED_IO_ID                0x00001100;
#define  os_INVALID_CARD_NAME                0x00001101;
#define  os_IO_NOT_INITIATED                 0x00001102;
#define  os_IO_CARD_SUPPORT_LIB_NOT_FOUND    0x00001103;
#define  os_INVALID_BUFFER                   0x00001104;
#define  os_QUERY_IO_ERROR                   0x00001105;
#define  os_UNABLE_TO_INIT                   0x00001106;
#define  os_FILE_NOT_FOUND                   0x00001107;
#define  os_DEFAULT_LOAD_FAIL                0x00001108;
#define  os_INTERFACE_IN_USE                 0x00001109;

#define  os_NO_DA_FOUND                      0x00001200;
#define  os_DA_DATA_FLOW_ERROR               0x00001201;
#define  os_DA_RANGE_ERROR                   0x00001202;
#define  os_DA_CONFIG_ERROR                  0x00001203;
#define  os_DA_OUTPUT_ERROR                  0x00001204;

#define  os_NO_DIN_FOUND                     0x00001300;
#define  os_DIN_RESOLUTION_ERROR             0x00001301;
#define  os_DIN_DATA_FLOW_ERROR              0x00001302;
#define  os_DIN_NOTE_PROC_ERROR              0x00001303;
#define  os_DIN_CONFIG_ERROR                 0x00001304;
#define  os_DIN_START_ERROR                  0x00001305;
#define  os_DIN_INPUT_ERROR                  0x00001306;

#define  os_NO_DOUT_FOUND                    0x00001400;
#define  os_DOUT_RESOLUTION_ERROR            0x00001401;
#define  os_DOUT_DATA_FLOW_ERROR             0x00001402;
#define  os_DOUT_CONFIG_ERROR                0x00001403;
#define  os_DOUT_OUTPUT_ERROR                0x00001404;

#define  os_INVALID_DAC_WAVELENGTH           0x00001500;
#define  os_WAVELENGTH_TO_VOLT_ERROR         0x00001501;
#define  os_INVALID_SLIT_WIDTH               0x00001502;
#define  os_SLIT_BW_TO_WIDTH_ERROR           0x00001503;
#define  os_SLIT_WIDTH_TO_VOLT_ERROR         0x00001504;
#define  os_INVALID_GRATING_LINES            0x00001505;
#define  os_SLIT_WIDTH_TO_BW_ERROR           0x00001506;
#define  os_INVALID_HANDLE                   0x00001507;


/*
			Notes on the use of osLibrary.ini
			*********************************




*/
