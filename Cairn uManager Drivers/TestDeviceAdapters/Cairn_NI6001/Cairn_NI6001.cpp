#include "Cairn_NI6001.h"

#include "ModuleInterface.h"
#include <sstream>

#ifdef WIN32
	#define WIN_32_LEAN_AND_MEAN
	#include <windows.h>
	#define snprintf _snprintf
#endif // WIN32

int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);

#define SHUT_MODE_LABEL			"Shutter Digital Out Bit"
#define SHUT_MODE_UNUSED		"Unused"
#define SHUT_MODE_TTL_INVERT	"TTL Invert"
#define SHUT_MODE_TTL_LOW		"TTL Low"
#define SHIT_MODE_TTL_HIGH		"TTL High"
#define DOSM_UNUSED				0
#define DOSM_INVERT				1
#define DOSM_LOW				2
#define DOSM_HIGH				3
#define DOUT_LABEL				"Direct Digital Out"
#define AOUT_LABEL				"Direct Analogue Out"
#define NUM_SHUTTER_CHANNELS	4

const char* g_OptoByte = "CairnNI6001";
const std::string digitalPort0 = "/port0/line0:7";
const std::string digitalPort1 = "/port1/line0:3";
const std::string analoguePort0 = "/ao0";
const std::string analoguePort1 = "/ao1";
const char* g_PropertyDevice = "Available Devices";
#ifdef WIN32
BOOL APIENTRY DllMain(HANDLE, DWORD ul_reason_for_call, LPVOID)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
	default:
		break;
	}
	return TRUE;
}
#endif // WIN32

MODULE_API void InitializeModuleData()
{
	RegisterDevice(g_OptoByte, MM::ShutterDevice, "Cairn NI output port");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0) return 0;
	if (strcmp(deviceName, g_OptoByte) == 0)return new CCairnNI6001();
	return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
	delete pDevice;
}

CCairnNI6001::CCairnNI6001()
	: bits_(0), initialized_(false), shuttered_(true)
{
	CPropertyAction* pAct = new CPropertyAction(this, &CCairnNI6001::OnDevice);
	//CreateProperty("Device Number", "1", MM::Integer, false, pAct, true);
	CreateStringProperty(g_PropertyDevice, "devname",  false, pAct, true);
	deviceNumber = 1;

	std::vector<std::string> devices = GetDevices();
	if (devices.size() == 0)
	{
		AddAllowedValue(g_PropertyDevice, "No valid devices found");
	}
	else
	{
		for (std::vector<std::string>::iterator i = devices.begin(); i != devices.end(); ++i) {
			AddAllowedValue(g_PropertyDevice, (*i).c_str());
		}
	}

}


CCairnNI6001::~CCairnNI6001()
{
}


// Provide a list of available devices
std::vector<std::string> CCairnNI6001::GetDevices()
{
	std::vector<std::string> result;
	char devNames[4096];
	DAQmxGetSysDevNames(devNames, 4096);
	// Names are comma-separated.
	std::string allNames = devNames;
	size_t index = allNames.find(", ");
	if (index == std::string::npos)
	{
		// Zero or one devices.
		if (allNames.size() != 0)
		{
			result.push_back(allNames);
		}
	}
	else
	{
		result.push_back(allNames.substr(0, index));
		do {
			result.push_back(GetNextEntry(allNames, index));
		} while (index != std::string::npos);
	}
	return result;
}

// Helper function: extract the next substring from a comma-separated
// list of values, and update startIndex as we go. Should be started
// with an index of std::string::npos if you want to start from the
// beginning of the string, and will end when the index is
// std::string::npos again.
std::string CCairnNI6001::GetNextEntry(std::string line, size_t& startIndex)
{
	size_t nextIndex = line.find(", ", startIndex + 1);
	std::string result;
	if (startIndex == std::string::npos &&
		nextIndex == std::string::npos)
	{
		// No commas found in the entire string.
		result = line;
	}
	else
	{
		size_t tmp = startIndex;
		if (startIndex == std::string::npos)
		{
			// Start from the beginning instead.
			tmp = 0;
		}
		else if (startIndex > 0)
		{
			// In the middle of the string, so add 2 to skip the
			// ", " part we expect to have here.
			tmp += 2;
		}
		result = line.substr(tmp, nextIndex - tmp);
		startIndex = nextIndex;
	}
	return result;
}
void CCairnNI6001::GetName(char* name) const
{
	CDeviceUtils::CopyLimitedString(name, g_OptoByte);
}

int CCairnNI6001::ErrorHandler(int error)
{
	char errBuff[2048] = { '\0' };
	if (DAQmxFailed(error))
	{
		DAQmxGetExtendedErrorInfo(errBuff, 2048);
		if (DOTaskHandle0) {
			DAQmxStopTask(DOTaskHandle0);
			DAQmxClearTask(DOTaskHandle0);
			DOTaskHandle0 = 0;
		}
		if (DOTaskHandle1) {
			DAQmxStopTask(DOTaskHandle1);
			DAQmxClearTask(DOTaskHandle1);
			DOTaskHandle1 = 0;
		}
		if (AOTaskHandle) {
			DAQmxStopTask(AOTaskHandle);
			DAQmxClearTask(AOTaskHandle);
			AOTaskHandle = 0;
		}
		printf("DAQmx Error: %s\n", errBuff);
	}
	return error;
}

int CCairnNI6001::init_optoDO()
{
	for (int i = 0; i < DOUT_COUNT_MAX_LINE_0; i++)
	{
		data_line_0[i] = 0;
	}

	for (int i = 0; i < DOUT_COUNT_MAX_LINE_1; i++)
	{
		data_line_1[i] = 0;
	}


	AOData[0] = 0.0;
	AOData[1] = 0.0;

	DOTaskHandle0 = 0;
	DOTaskHandle1 = 0;
	AOTaskHandle = 0;


	int32 ret = DAQmxCreateTask("", &DOTaskHandle0);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxCreateTask("", &DOTaskHandle1);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);


	ret = DAQmxCreateTask("", &AOTaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	std::stringstream ss;
	ss << device_.c_str() << digitalPort0;

	ret = DAQmxCreateDOChan(DOTaskHandle0, ss.str().c_str(), "", DAQmx_Val_ChanPerLine);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ss.str("");
	ss << device_.c_str() << digitalPort1;

	ret = DAQmxCreateDOChan(DOTaskHandle1, ss.str().c_str(), "", DAQmx_Val_ChanPerLine);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ss.str("");
	ss << device_.c_str() << analoguePort0;

	ret = DAQmxCreateAOVoltageChan(AOTaskHandle, ss.str().c_str(), "", 0.0, 5.0, DAQmx_Val_Volts, "");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ss.str("");
	ss << device_.c_str() << analoguePort1;
	//ss << "Dev" << deviceNumber << analoguePort1;

	ret = DAQmxCreateAOVoltageChan(AOTaskHandle, ss.str().c_str(), "", 0.0, 5.0, DAQmx_Val_Volts, "");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ret = DAQmxStartTask(DOTaskHandle0);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxStartTask(DOTaskHandle1);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxStartTask(AOTaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	binValue = 0;

	return 0;
}


int CCairnNI6001::Shutdown()
{
	SetOpen(false);
	
	DAQmxStopTask(DOTaskHandle0);
	DAQmxClearTask(DOTaskHandle0);
	DOTaskHandle0 = 0;

	DAQmxStopTask(DOTaskHandle1);
	DAQmxClearTask(DOTaskHandle1);
	DOTaskHandle1 = 0;

	DAQmxStopTask(AOTaskHandle);
	DAQmxClearTask(AOTaskHandle);
	AOTaskHandle = 0;

	initialized_ = false;

	return DEVICE_OK;
}


int CCairnNI6001::Initialize()
{
	if (initialized_) return DEVICE_OK;
	if (init_optoDO()) return DEVICE_ERR;

	int ret = CreateProperty(MM::g_Keyword_Name, g_OptoByte, MM::String, true);
	if (DEVICE_OK != ret) return ret;

	ret = CreateProperty(MM::g_Keyword_Description, "Cairn NI port driver", MM::String, true);
	if (DEVICE_OK != ret) return ret;

	CPropertyAction* pAct = new CPropertyAction(this, &CCairnNI6001::OnShutter);
	ret = CreateProperty("Shutter", "0", MM::Integer, false, pAct);
	if (DEVICE_OK != ret) return ret;
	AddAllowedValue("Shutter", "0");
	AddAllowedValue("Shutter", "1");

	CPropertyActionEx *pExAct;

	int i;

	for (i = 0; i < DOUT_COUNT_USED_LINE_0; i++)
	{
		pExAct = new CPropertyActionEx(this, &CCairnNI6001::OnLine_0, i);
		std::ostringstream os;
		os << DOUT_LABEL << "_P0." << i;
		ret = CreateProperty(os.str().c_str(), "0", MM::Integer, false, pExAct);
		if (DEVICE_OK != ret) return ret;
		AddAllowedValue(os.str().c_str(), "0");
		AddAllowedValue(os.str().c_str(), "1");
	}
	for (i = 0; i < DOUT_COUNT_USED_LINE_1; i++)
	{
		pExAct = new CPropertyActionEx(this, &CCairnNI6001::OnLine_1, i);
		std::ostringstream os;
		os << DOUT_LABEL << "_P1." << i;
		ret = CreateProperty(os.str().c_str(), "0", MM::Integer, false, pExAct);
		if (DEVICE_OK != ret) return ret;
		AddAllowedValue(os.str().c_str(), "0");
		AddAllowedValue(os.str().c_str(), "1");
	}
	for (i = 0; i < DAC_OUT_COUNT_USED; i++)
	{
		pExAct = new CPropertyActionEx(this, &CCairnNI6001::OnAnaOut, i);
		std::ostringstream os;
		os << AOUT_LABEL << " " << i;
		ret = CreateProperty(os.str().c_str(), "0", MM::Float, false, pExAct);
		if (ret != DEVICE_OK) return ret;
		SetPropertyLimits(os.str().c_str(), 0.00, 5.00);
	}

	ret = UpdateStatus();
	if (ret != DEVICE_OK) return ret;
	initialized_ = true;
	return DEVICE_OK;
}


bool CCairnNI6001::Busy()
{
	return false;
}

int CCairnNI6001::SetOpen(bool open)
{
	if (open)
	{
		shuttered_ = false;
		DO1_SetData(data_line_1);
	}
	else
	{
		uInt8 DigitalShutter1[DOUT_COUNT_USED_LINE_1];
		for (int i = 0; i < DOUT_COUNT_USED_LINE_1; i++)
		{
			if (i < NUM_SHUTTER_CHANNELS)
			{
				DigitalShutter1[i] = 0;
			}
			else
			{
				DigitalShutter1[i] = data_line_1[i];
			}
		}
		DO1_SetData(DigitalShutter1);
		shuttered_ = true;
	}
	return DEVICE_OK;
}


int CCairnNI6001::GetOpen(bool& open)
{
	open = !shuttered_;
	return DEVICE_OK;
}


int CCairnNI6001::DO0_SetData(uInt8 *myDataLine0)
{
	int32 ret = DAQmxWriteDigitalLines(DOTaskHandle0, 1, 1, 10.0, DAQmx_Val_GroupByChannel, myDataLine0, NULL, NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);


	return DEVICE_OK;
}
int CCairnNI6001::DO1_SetData( uInt8 *myDataLine1)
{

	int32 ret = DAQmxWriteDigitalLines(DOTaskHandle1, 1, 1, 10.0, DAQmx_Val_GroupByChannel, myDataLine1, NULL, NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	return DEVICE_OK;
}
int CCairnNI6001::OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if (shuttered_)
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if (pos)
			return SetOpen(false);
		else
			return SetOpen(true);
	}
	return DEVICE_OK;
}

//int CCairnNI6001::OnDevice(MM::PropertyBase* pProp, MM::ActionType eAct)
//{
//	if (eAct == MM::BeforeGet)
//	{
//		pProp->Set((long)deviceNumber);
//	}
//	else if (eAct == MM::AfterSet)
//	{
//		pProp->Get(deviceNumber);
//	}
//	return DEVICE_OK;
//}

int CCairnNI6001::OnLine_0(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		if (data_line_0[channel])
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if (pos)
			data_line_0[channel] = 1;
		else
			data_line_0[channel] = 0;

			DO0_SetData(data_line_0);
	}
	return DEVICE_OK;
}


int CCairnNI6001::OnLine_1(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		if (data_line_1[channel])
			pProp->Set(1L);
		else
			pProp->Set(0L);

	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if (pos)
			data_line_1[channel] = 1;
		else
			data_line_1[channel] = 0;
		if (!shuttered_)
		{
			DO1_SetData(data_line_1);
		}
		else
		{
			uInt8 tempData[DOUT_COUNT_USED_LINE_0];
			for (int i = 0; i < DOUT_COUNT_USED_LINE_0; i++)
			{
				if (i < NUM_SHUTTER_CHANNELS)
				{
					tempData[i] = 0;
				}
				else
				{
					tempData[i] = data_line_1[i];
				}
			}
			DO1_SetData(tempData);
		}
	}
	return DEVICE_OK;
}

int CCairnNI6001::OnAnaOut(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		float pVal;
		pVal = (float)AOData[channel];
		pProp->Set(pVal);
	}
	else if (eAct == MM::AfterSet)
	{
		float64 val;
		pProp->Get(val);
		AOData[channel] = val;
		AOSetData(AOData);
	}
	return DEVICE_OK;
}

int CCairnNI6001::AOSetData(float64 *myAOData)
{
	int32 ret = DAQmxWriteAnalogF64(AOTaskHandle, 1, 1, 5.0, DAQmx_Val_GroupByChannel, myAOData, NULL, NULL);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}
int CCairnNI6001::OnDevice(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set(device_.c_str());
	}
	else if (eAct == MM::AfterSet)
	{
		pProp->Get(device_);
		
	}

	return DEVICE_OK;
}