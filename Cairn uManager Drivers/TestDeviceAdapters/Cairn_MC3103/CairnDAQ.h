

#ifndef _CAIRNDAQ_H_
#define _CAIRNDAQ_H_

#include "DeviceBase.h"
#include "ImgBuffer.h"
#include "DeviceThreads.h"
#include "cbw.h"
#include <string>
#include <map>

//////////////////////////////////////////////////////////////////////////////
// Error codes
//
//#define ERR_UNKNOWN_MODE         102
//#define ERR_UNKNOWN_POSITION     103
#define ERR_UNKNOWN_MODE			 10001
#define ERR_UNKNOWN_POSITION         10002
#define ERR_INVALID_SPEED            10003
#define ERR_PORT_CHANGE_FORBIDDEN    10004
#define ERR_SET_POSITION_FAILED      10005
#define ERR_INVALID_STEP_SIZE        10006
#define ERR_LOW_LEVEL_MODE_FAILED    10007
#define ERR_INVALID_MODE             10008

#define DAC_COUNT_MAX_5V			0
#define DAC_COUNT_USED_5V			0
#define DAC_COUNT_MAX_10V			8
#define DAC_COUNT_USED_10V			8

#define DOUT_COUNT_MAX				8
#define DOUT_COUNT_USED				8
#define DIN_COUNT_MAX				0
#define DIN_COUNT_USED				0


class CCairnDAQ : public CShutterBase<CCairnDAQ>  
{
public:
   CCairnDAQ() ;
   ~CCairnDAQ() ;
  
   // MMDevice API
   // ------------
   int Initialize();
   int Shutdown();
   int initDAQ();

   void GetName (char* pszName) const;
   bool Busy();

   // Shutter API
   int SetOpen (bool open);
   int GetOpen(bool& open);
   int Fire(double /*deltaT*/) {return DEVICE_UNSUPPORTED_COMMAND;}

   int OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnValue(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnWavelength(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnBandwidth(MM::PropertyBase* pProp, MM::ActionType eAct);
  
   int OnGrating(MM::PropertyBase* pProp, MM::ActionType eAct);

   int OnWaveVolts(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnWaveChan(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnInChan(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnOutChan(MM::PropertyBase* pProp, MM::ActionType eAct);

   int OnDOShutMode(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
   int OnBit(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
   int OnAnaOut5V(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
   int OnAnaOut10V(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
   //int OnDigIn(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
   int OnDefaults(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnInVolts(MM::PropertyBase* pProp, MM::ActionType eAct);
   int OnOutVolts(MM::PropertyBase* pProp, MM::ActionType eAct);
private:
    unsigned char bits_;
	long binValue;
 //   uInt8  data[DOUT_COUNT_MAX];
	//uInt8  inputData[DIN_COUNT_MAX];
 //   float64  AOdata[DAC_COUNT_MAX];

	bool initialized_;
	bool shuttered_;
	void SetBits( unsigned char bits );

	int boardNum;
	long count, rate;
	int range;
	
	int DAOptions;
	int LowChan, HighChan;
	int portNum, direction;
	WORD DOValues;
	unsigned short *DAValues;    /* Win32 pointer to D/A buffer */
	
	HGLOBAL DAMemHandle;
	
	float    revLevel;// = (float)CURRENTREVNUM;
	//TaskHandle  AOtaskHandle , DOtaskHandle, AItaskHandle, DItaskHandle ;
	//TaskHandle  taskHandle;
	int DOSetData(WORD *myData);
	int AOSetData();
	//int DIGetData(uInt8 *myRead);
	//int SetVoltages();
	int ErrorHandler(int error);
	double wavelength, waveVolts;
	long waveChan,inChan,outChan;
	double bandWidth;
	double inVolts, outVolts;
	bool hasRunOnce;
	std::string gratingLines;
	double grLines;
	double gratingOffset;
	float percentMult;
	float percentMult10V;
	int DACTotal;
	int DACUsed[DAC_COUNT_MAX_5V + DAC_COUNT_MAX_10V];
	int DOShutMode[DOUT_COUNT_MAX];
	long lastDefaults;
};








//////////////////////////////////////////////////////////////////////////////
// DemoShutter class
// Simulation of shutter device
//////////////////////////////////////////////////////////////////////////////
class DemoDA : public CSignalIOBase<DemoDA>
{
public:
   DemoDA ();
   ~DemoDA ();

   int Shutdown() {return DEVICE_OK;}
   void GetName(char* name) const {strcpy(name,"Demo DA");}
   int SetGateOpen(bool open); 
   int GetGateOpen(bool& open);
   int SetSignal(double volts);
   int GetSignal(double& volts);
   int GetLimits(double& minVolts, double& maxVolts) {minVolts=0.0; maxVolts= 10.0; return DEVICE_OK;}
   bool Busy() {return false;}
   int Initialize() {return DEVICE_OK;}


private:
   double volt_;
   double gatedVolts_;
   bool open_;
};





#endif //_DEMOCAMERA_H_
