
#include "OptoDO.h"
//#include "stdfx.h"
#include <string>
#include <math.h>
#include "ModuleInterface.h"
#include <sstream>

#ifdef WIN32
   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>
   #define snprintf _snprintf 
#endif
int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);


#define SHUT_MODE_LABEL			"Shutter Digital Out Bit"
#define SHUT_MODE_UNUSED		"Unused"
#define SHUT_MODE_INVERT		"TTL Invert"
#define SHUT_MODE_LOW			"TTL Low"
#define SHUT_MODE_HIGH			"TTL High"
#define DOSM_UNUSED				0
#define DOSM_INVERT				1
#define DOSM_LOW				2
#define DOSM_HIGH				3
#define DOUT_LABEL				"Direct Digital Out 1"
//#define DOUT_LABEL_2				"Direct Digital Out 2"

using namespace std;
const char* g_OptoByte = "CairnNI6501";
const char* digitalPort = "Dev1/port1/line0:7";
//const char* digitalPort1 = "Dev1/port2/line0:7";
// TODO: linux entry code

// windows DLL entry code
#ifdef WIN32
BOOL APIENTRY DllMain( HANDLE /*hModule*/, 
                      DWORD  ul_reason_for_call, 
                      LPVOID /*lpReserved*/
                      )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
   case DLL_THREAD_DETACH:
		break;
   case DLL_PROCESS_DETACH:
		break;
   }
   return TRUE;
}
#endif

MODULE_API void InitializeModuleData()
{
   //AddAvailableDeviceName(g_OptoByte, "Cairn NI output port");
   RegisterDevice(g_OptoByte, MM::ShutterDevice, "Cairn NI output port");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0) return 0;
	if (strcmp(deviceName, g_OptoByte) == 0) return new COptoDO();
	return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}


///////////////////////////////////////////////////////////////////////////////
// osShutter implementation
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
void COptoDO::GetName(char* name) const
{
   CDeviceUtils::CopyLimitedString(name, g_OptoByte);
}

int COptoDO::ErrorHandler(int error)
{
	char    errBuff[2048]={'\0'};
	if( DAQmxFailed(error) )
			DAQmxGetExtendedErrorInfo(errBuff,2048);
		if( DOtaskHandle ) {
			/*********************************************/
			// DAQmx Stop Code
			/*********************************************/
			DAQmxStopTask(DOtaskHandle);
			DAQmxClearTask(DOtaskHandle);
			DOtaskHandle = 0;
			/*DAQmxStopTask(DOtaskHandle1);
			DAQmxClearTask(DOtaskHandle1);
			DOtaskHandle1 = 0;*/
		}
		if( DAQmxFailed(error) )
			printf("DAQmx Error: %s\n",errBuff);
	return error;
}

int COptoDO::init_optoDO (  )
{

	data[0] = 0;
	data[1] = 0;
	data[2] = 0;
	data[3] = 0;
	data[4] = 0;
	data[5] = 0;
	data[6] = 0;
	data[7] = 0;

	DOtaskHandle = 0;

	int32 ret = DAQmxCreateTask("",&DOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	/*int32 ret = DAQmxCreateTask("",&DOtaskHandle1);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);*/

	/*********************************************/
	// DAQmx Create Digital output Channels
	/*********************************************/
	ret = DAQmxCreateDOChan(DOtaskHandle,digitalPort,"",DAQmx_Val_ChanForAllLines);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	/*ret = DAQmxCreateDOChan(DOtaskHandle1,digitalPort1,"",DAQmx_Val_ChanForAllLines);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);*/


////	FT_STATUS stat;
////	FT_HANDLE hnd = 0;
////	FT_DEVICE dev;
////	DWORD cnt,id;
////	char ser[16];
////	char desc[64]; 
////	stat = FT_ListDevices( &cnt , NULL , FT_LIST_NUMBER_ONLY );
////	DWORD i;
////	for ( i=0 ; i<cnt ; i++ )
////	{
////		stat = FT_Open( i , &hnd );
////		stat = FT_GetDeviceInfo( hnd , &dev , &id , ser , desc , NULL ); 
////		if ( id == 0x156B0005 ) break;
////		FT_Close(hnd); 
////		hnd = 0;
////	}
////	if ( 0 == hnd ) 
////		return DEVICE_ERR;
//////		Don't think no Optobytes present should be an error?
//////		return DEVICE_OK;
////	hnd_ = hnd;
////	
////	stat = FT_SetBitMode( hnd , 0xFF , 0x00 );
////	stat = FT_SetBitMode( hnd , 0xFF , 0x01 );
//////	bits_ = 0xFF;
//////	FT_Write( hnd_ , &bits_ , 1 , &i );
//////	Sleep(100);
//////	bits_ = 0;
//////	FT_Write( hnd_ , &bits_ , 1 , &i );
//////	Sleep(100);
//////	bits_ = 0xFF;
//////	FT_Write( hnd_ , &bits_ , 1 , &i );
//////	Sleep(100);
////	bits_ = 0;
////	FT_Write( hnd_ , &bits_ , 1 , &i );
//////	Sleep(100);
		binValue = 0;
	return 0;
}

int COptoDO::Shutdown()
{
	SetOpen(false);
	DAQmxStopTask(DOtaskHandle);
	DAQmxClearTask(DOtaskHandle);
	DOtaskHandle = 0;

	
	initialized_ = false; 

	return DEVICE_OK;
}

int COptoDO::Initialize()
{
	if (initialized_) return DEVICE_OK;
	if ( init_optoDO() ) return DEVICE_ERR;

	int ret = CreateProperty(MM::g_Keyword_Name, g_OptoByte, MM::String, true);
	if (DEVICE_OK != ret) return ret;

	ret = CreateProperty(MM::g_Keyword_Description, "Cairn NI port driver", MM::String, true);
	if (DEVICE_OK != ret) return ret;

	CPropertyAction* pAct = new CPropertyAction (this, &COptoDO::OnShutter);
	ret = CreateProperty( "Shutter" , "0", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( "Shutter" , "0" ); // Closed
	AddAllowedValue( "Shutter" , "1" ); // Open


		CPropertyActionEx *pExAct;

	int i;
	//for ( i=0; i < DOUT_COUNT_USED; i++) 
	//{
	//	pExAct = new CPropertyActionEx(this, &COptoDO::OnDOShutMode , i );
	//	ostringstream os;
	//	os << SHUT_MODE_LABEL << " " << i;
	//	ret = CreateProperty( os.str().c_str(), "0.0", MM::String, false, pExAct);
	//	if (ret != DEVICE_OK) return ret;
	//	AddAllowedValue( os.str().c_str() , SHUT_MODE_UNUSED , DOSM_UNUSED );
	//	AddAllowedValue( os.str().c_str() , SHUT_MODE_INVERT , DOSM_INVERT ); 
	//	AddAllowedValue( os.str().c_str() , SHUT_MODE_LOW , DOSM_LOW ); 
	//	AddAllowedValue( os.str().c_str() , SHUT_MODE_HIGH , DOSM_HIGH ); 
	//}

	for ( i=0; i < DOUT_COUNT_USED; i++) 
	{
		pExAct = new CPropertyActionEx (this, &COptoDO::OnBit , i );
		ostringstream os;
		os << DOUT_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Integer, false, pExAct); 
		if (ret != DEVICE_OK) return ret; 
		AddAllowedValue( os.str().c_str() , "0" ); 
		AddAllowedValue( os.str().c_str() , "1" ); 
	}

	/*for ( i=0; i < DOUT_COUNT_USED; i++) 
	{
		pExAct = new CPropertyActionEx (this, &COptoDO::OnBit , i );
		ostringstream os;
		os << DOUT_LABEL_2 << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Integer, false, pExAct); 
		if (ret != DEVICE_OK) return ret; 
		AddAllowedValue( os.str().c_str() , "0" ); 
		AddAllowedValue( os.str().c_str() , "1" ); 
	}*/

	pAct = new CPropertyAction (this, &COptoDO::OnValue);
	ret = CreateProperty( "Value" , "0", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 

	ret = UpdateStatus();
	if (ret != DEVICE_OK) return ret;
	initialized_ = true;
	return DEVICE_OK;
}


bool COptoDO::Busy()
{
	return false;
}


////void COptoDO::SetBits( unsigned char bits )
////{
////	if ( shuttered_ ) return;
////	DWORD wrtn;
////	FT_Write( hnd_ , &bits , 1 , &wrtn );
////	return;
////}

//int COptoDO::SetOpen (bool open)
//{
//	if ( open )
//	{
//		shuttered_ = false;
//		SetBits ( bits_ );
//	}else{
//		SetBits ( 0 );
//		shuttered_ = true;
//	}
//	return DEVICE_OK;
//}
int COptoDO::SetOpen (bool open)
{
	if ( open )
	{
		shuttered_ = false;
		DOSetData(data);
	}else{
		uInt8 DigitalShutter[DOUT_COUNT_MAX];
		//uInt8 DigitalShutter1[DOUT_COUNT_MAX];
		int i;
		for ( i = 0 ; i < DOUT_COUNT_MAX ; i++ )
		{
			//switch ( DOShutMode[i] )
			//{
			//	case DOSM_INVERT:
			//			if ( data[i] )
			//				DigitalShutter[i] = 0;
			//			else
			//				DigitalShutter[i] = 1;
			//		break;
			//	case DOSM_HIGH:
			//			DigitalShutter[i] = 1;
			//		break;
			//	case DOSM_LOW:
			if (i < 8 )
			{
				DigitalShutter[i] = 0;
			} else {
				DigitalShutter[i] = data[i];
			}
						//DigitalShutter1[i] = 0;
			//		break;
			//	default:	
			//		DigitalShutter[i] = data[i];
			//}
		}
		DOSetData( DigitalShutter);
		shuttered_ = true;
	}
	return DEVICE_OK;
}

int COptoDO::GetOpen(bool& open)
{
	if ( shuttered_ )
		open = false;
	else
		open = true;
	return DEVICE_OK;
}

int COptoDO::DOSetData(uInt8 *myData)
{

	int32 ret =DAQmxWriteDigitalLines(DOtaskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,myData,NULL,NULL);
	//int32 ret =DAQmxWriteDigitalLines(DOtaskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,myData1,NULL,NULL);
	//////std::string aString;
	//////int anint;
	//////for (int i = 0 ; i <8 ; i++)
	//////{
	//////	anint = (int)myData[i];

	//////			  char *myBuff;
	//////			  //string strRetVal;

	//////			  // Create a new char array
	//////			  myBuff = new char[100];

	//////			  // Set it to empty
	//////			  memset(myBuff,'\0',100);

	//////			  // Convert to string
	//////			  itoa(anint,myBuff,10);

	//////			  // Copy the buffer into the string object
	//////			  aString = myBuff;
 ////// 
	//////			  // Delete the buffer
	//////			  delete[] myBuff;

	//////			  MessageBoxA(0,aString.c_str(),"test1",0);
	//////}
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}



int COptoDO::OnValue(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	long pos;
	if (eAct == MM::BeforeGet)
	{
		pos = binValue;
		pProp->Set(pos);
	}
	else if (eAct == MM::AfterSet)
	{
		pProp->Get(pos);
		binValue = pos;
		int i;
		for ( i=0 ; i<8 ; i++ )
		{
			data[i] = pos & 1;
			pos = pos >> 1;
		}
		if (!shuttered_) {
			DOSetData( data);
		} else {
			uInt8 DigitalShutter[DOUT_COUNT_MAX];

			int i;
			for ( i = 0 ; i < DOUT_COUNT_MAX ; i++ )
			{
				if (i < 4 )
				{
					DigitalShutter[i] = 0;
				} else {
					DigitalShutter[i] = data[i];
				}

			}
			DOSetData( data);
		}
   }
   return DEVICE_OK;
}
// copy "$(OutDir)mmgr_dal_CairnNI6501.dll" "D:\Program Files\Micro-Manager-1.4\mmgr_dal_CairnNI6501.dll"
int COptoDO::OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if ( shuttered_ )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			return SetOpen( false );
		else
			return SetOpen( true );
   }
   return DEVICE_OK;
}

int COptoDO::OnBit(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		if ( data[channel]  )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			data[channel] = 1;
		else
			data[channel] = 0;
		if (!shuttered_) {
		  DOSetData( data);
		} else {
			uInt8 DigitalShutter[DOUT_COUNT_MAX];

			int i;
			for ( i = 0 ; i < DOUT_COUNT_MAX ; i++ )
			{
				if (i < 4 )
				{
					DigitalShutter[i] = 0;
				} else {
					DigitalShutter[i] = data[i];
				}

			}
			DOSetData( DigitalShutter);
		}
   }
   return DEVICE_OK;
}

