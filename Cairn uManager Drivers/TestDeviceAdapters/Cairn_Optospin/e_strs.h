/*
 	Description:	Optospin library error code header
 	Author: 		Andrew Hill <a.hill@cairn-research.co.uk>
 	Copyright:		2010 - 2011 Andrew Hill

    This file is part of optospin.dll and liboptospin.so

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

CALL void CNV add_err_str ( const char * str , t_err_lvl lvl , int user );
CALL void CNV clr_err_strs ( t_err_lvl lvl );
void _init_errors ( void );
void _term_errors ( void );


