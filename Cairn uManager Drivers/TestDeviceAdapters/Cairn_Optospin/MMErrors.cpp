
#include <windows.h>
#include "MMErrors.h"
#include <sstream>
#include <fstream>


using namespace std;

//unsigned char lmaj;
//unsigned char lmin;
//bool lib_inited = false;

unsigned long base_time;
double freq_;

void debg_callback( t_rtr_hnd handle , int *user , char *msg  )
{
	//ofstream myfile( "log.txt" , ios::app );
	//if (myfile.is_open())
	//{
	//	myfile << *user;
	//	myfile << " - ";
	//	myfile << (unsigned long long)handle;
	//	myfile << " - ";
	//	myfile << "Lib - ";
	//	myfile << msg;
	//	myfile << "\n";
	//	myfile.close();
	//}
	//else 
	//	MessageBox( 0 , "Unable to open log.txt" , "Cairn Optospin Error", 0 );
	//(*user)++;
	//return;
}

void usb_callback( t_rtr_hnd handle , int *user , char *msg  )
{

	//double res;
	//unsigned __int64 val;
 //   QueryPerformanceCounter( (LARGE_INTEGER *)&val );
 //   res = (val - base_time ) * freq_;
	//res = res * 1000;
	//ofstream myfile( "log.txt" , ios::app );
	//if (myfile.is_open())
	//{
	//	myfile << *user;
	//	myfile << " - ";
	//	myfile << res;
	//	myfile << " - ";
	//	myfile << (unsigned long long)handle;
	//	myfile << " - ";
	//	myfile << "USB - ";
	//	myfile << msg;
	//	myfile << "\n";
	//	myfile.close();
	//}
	//else 
	//	MessageBox( 0 , "Unable to open log.txt" , "Cairn Optospin Error", 0 );
	//(*user)++;
	//return;
}

bool ErrorCheck ( int rValue , bool ShowMessage )
{
	ostringstream tos;
	char * err_str;
	bool ret = false;
	if ( RTR_OK == rValue ) return false;
	t_err_lvl lvl;
	rtr_get_error_str( &err_str , &lvl );
	while ( lvl != ERR_LVL_NONE )
	{
		switch  ( lvl )
		{
			case ERR_LVL_ABORT:		tos << "ABORT - "; ret = true; break;
			case ERR_LVL_HALT:		tos << "HALT - "; ret = true; break;
			case ERR_LVL_INFORM:    tos << "INFORM - "; break;
			case ERR_LVL_SKIP:		tos << "SKIP - "; ret = true; break;
			case ERR_LVL_NOTE:		tos << "NOTE - "; break;
			default:				tos << "UNKNOWN - "; ret = true; break;
		}
		tos << err_str;
		tos << "\n";

		rtr_get_error_str( &err_str , &lvl );
	}
	tos << "Error code value: ";
	tos << rValue;
	tos << "\n";
	if ( ShowMessage )
		MessageBoxA( 0 , tos.str().c_str() , "Cairn Optospin Error", 0 );
	int i = 0;
	// debg_callback( NULL , &i , (char * )tos.str().c_str() ) ;
	return ret;
}