/*
 	Description:	Generic platform code header
 	Author: 		Andrew Hill <a.hill@cairn-research.co.uk>
 	Copyright:		2008-2012 Andrew Hill

    This file is part of various Andrew Hill & Cairn Reearch programs.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef PLATFORM_H
#define PLATFORM_H


/* This *should* setup for the whole build. */
#ifdef _WIN32
  #define WINDOWS
  #ifndef _WIN64
    #define BITS32
  #endif
#endif

#ifdef _WIN64
   #define WINDOWS
   #define BITS64
#endif

#ifdef __linux__
  #define BITS32
  #define LINUX
#endif

//Avaiable defines, YMMV:
// USE_SOCKETS
// USE_DYNAMIC_LINKING - Needs -ldl dynamic linking on Linux
// USE_THREADS
// USE_MUTEX
// USE_TIMERS - Needs -lrt realtime posix library on linux
// USE_IPC

#ifndef DO_ALWAYS_INLINE
    // hmmmm. Seems inline not supported on X64 gcc yet?
    #ifdef GCC
        #define DO_ALWAYS_INLINE __attribute__((__always_inline__))
    #endif
    #ifdef _MSC_VER
       #define DO_ALWAYS_INLINE __forceinline
    #endif
    #ifndef DO_ALWAYS_INLINE
        #define	DO_ALWAYS_INLINE
    #endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef GCC
    #include <stdbool.h>
	#define _INLINE_	inline
#endif

#include <ctype.h>

#ifdef WINDOWS
	#ifdef _MSC_VER
	    #pragma warning( disable : 4996 34 )  // Disable some M$ embrace, extend, extinguish guff.
		#define _CRT_SECURE_NO_DEPRECATE      // Disable more M$ embrace, extend, extinguish guff.
		#define _CRT_SECURE_NO_WARNING        // Disable more M$ embrace, extend, extinguish guff.
		#define _INLINE_ wtf???
	#endif
	#ifdef _WINDLL
		#define BUILD_DLL
	#endif
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
    #include <winbase.h>
    #ifdef USE_SOCKETS
        #include <winsock2.h>
    #endif
#endif

#ifdef LINUX
    #include <sched.h>
	#include <strings.h>
    #include <limits.h>
    #include <unistd.h>
    #include <errno.h>
    #ifdef USE_THREADS
        #include <pthread.h>
    #endif
    #ifdef USE_DYNAMIC_LINKING
        #include <dlfcn.h>
    #endif
    #ifdef USE_TIMERS
        #include <signal.h>
        #include <time.h>
        #include <bits/siginfo.h>
        #include <sys/time.h>
    #endif
    #ifdef USE_SOCKETS
        #include <sys/select.h>
        #include <arpa/inet.h>
        #include <sys/socket.h>
        #include <netinet/in.h>
        #include <netdb.h>
    #endif
    #ifdef USE_IPC
        #include <sys/msg.h>
    #endif
#endif

typedef unsigned char byte;

#ifdef WINDOWS
    #define lib_ext ".dll"
	#ifdef BUILD_DLL
		#define LIB_CALL __declspec(dllexport)
    #else
        #define LIB_CALL __declspec(dllimport)
    #endif
	#define LIB_IMPORT __declspec(dllimport)
    #define LIB_ENTRY BOOL WINAPI
    #define INLINE inline
    #define platform "Windows"
    #define path_delim "\\"
	#define _PLAT_CDECL _cdecl
    #define _decall _PLAT_CDECL
    void sched_yield ( void );
    typedef unsigned int system_error_code;
    void system_error_message ( system_error_code err );
#endif

#ifdef LINUX
    #define core_default_path "/usr/lib/"
    #define lib_ext ".so"
    #define LIB_CALL
    #define LIB_IMPORT
	#define _PLAT_CDECL
	#define _cdecl
    #define STDCALL __attribute__((__stdcall__))
    #define SOCKET_ERROR -1
    #define INVALID_SOCKET -1
    #define platform "Linux"
    #define path_delim "/"
    #define _decall
    #define MAX_PATH pathconf("/", _PC_PATH_MAX)
#endif

#ifdef USE_TIMERS
    typedef int tmr_handle;
    typedef void (_cdecl* tmr_callback) ( int * user , int count , tmr_handle hnd );
    #ifndef TIMER_COUNT
        #define TIMER_COUNT 5
    #endif
    #define INVALID_TIMER_HANDLE 0
#endif

#ifdef USE_SOCKETS
    #ifdef WINDOWS
        typedef int socklen_t;
        typedef SOCKET de_socket;
        #define SHUT_RDWR 2
    #endif
    #ifdef LINUX
        #define closesocket close
        #define SD_BOTH SHUT_RDWR
        typedef int de_socket;
    #endif
#endif

#ifdef WINDOWS  //This is used by DYNAMIC_LINKING and TIMERS so just do it.
    typedef HANDLE lib_handle;
    #define INVALID_LIB_HANDLE INVALID_HANDLE_VALUE
#endif

#ifdef USE_DYNAMIC_LINKING
    #ifdef WINDOWS
    #endif
    #ifdef LINUX
        typedef void * lib_handle;
        #define INVALID_LIB_HANDLE NULL
    #endif
#endif

#ifdef USE_IPC
//    #define thread_param void *
    #ifdef WINDOWS
        #define INVALID_QUEUE_HANDLE INVALID_HANDLE
        typedef HANDLE queue_handle;
        typedef key_t queue_key;
//        typedef LPTHREAD_START_ROUTINE thread_call;
//        #define INVALID_THREAD_HANDLE INVALID_HANDLE_VALUE
//        #define thread_return DWORD WINAPI
//        #define thread_return_type DWORD
//        #define BLANK_THREAD_RETURN 0
    #endif
    #ifdef LINUX
        #define INVALID_QUEUE_HANDLE -1
        #define QUEUE_KEY_GEN_PATH "/usr/lib/dateng/libdecore.so.1.0"
        typedef int queue_handle;
        typedef key_t queue_key;
//        typedef void * thread_call;
//        #define INVALID_THREAD_HANDLE 0
//        #define thread_return void *
//        #define thread_return_type void *
//        #define BLANK_THREAD_RETURN NULL
    #endif
#endif


#ifdef USE_THREADS
    #define thread_param void *
    #ifdef WINDOWS
        typedef HANDLE thread_handle;
        typedef LPTHREAD_START_ROUTINE thread_call;
        #define INVALID_THREAD_HANDLE INVALID_HANDLE_VALUE
        #define thread_return DWORD WINAPI
        #define thread_return_type DWORD
        #define BLANK_THREAD_RETURN 0
    #endif
    #ifdef LINUX
        typedef pthread_t thread_handle;
        typedef void * thread_call;
        #define INVALID_THREAD_HANDLE 0
        #define thread_return void *
        #define thread_return_type void *
        #define BLANK_THREAD_RETURN NULL
    #endif
#endif

#ifdef USE_MUTEX
    #ifdef WINDOWS
        typedef HANDLE mutex_handle;
        #define INVALID_MUTEX_HANDLE INVALID_HANDLE_VALUE
    #endif
    #ifdef LINUX
        typedef void * mutex_handle;
        #define INVALID_MUTEX_HANDLE NULL
    #endif
#endif

#define ERPLT_NOT_FOUND             -1 // Requested resource not found
#define ERPLT_INVAL_PARAM           -2 // An invalid parameter was supplied
#define ERPLT_OUT_OF_RES            -3 // Out of requested resources
#define ERPLT_SYS_CALL              -4 // A system call returned an error


#define line_delim "\n\r"
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)
#define bcopy(b1,b2,len) (memmove((b2), (b1), (len)), (void) 0)
typedef  double datetime;
typedef datetime * pdatetime;

#ifdef WINDOWS
    FILE * popen ( const char* command, const char* flags );
    int pclose ( FILE* fd );
#endif

/* Need a seperate utils space. */
char * to_lower ( char* s );
#ifdef USE_DYNAMIC_LINKING
    lib_handle load_library ( char * filename );
    void * hook_library ( lib_handle handle , char * name );
    void free_library ( lib_handle handle );
#endif
#ifdef USE_SOCKETS
    de_socket connect_socket ( char *address , int port );
#endif
#ifdef USE_THREADS
    int thread_create ( thread_handle *handle , thread_call call , thread_param user );
    void thread_free ( thread_handle *thread );
#endif
#ifdef USE_IPC
queue_handle open_queue( queue_key keyval, bool exclusive );
queue_key gen_queue_key( int id );
int close_queue ( queue_handle queue );
#endif
#ifdef USE_MUTEX
    mutex_handle mutex_create ( void );
    bool mutex_acquire ( mutex_handle mtx );
    bool mutex_release ( mutex_handle mtx );
    void mutex_free ( mutex_handle *mtx );
#endif

void show_message ( char * msg , char *capt );

void ms_wait ( int msec );
int file_exist ( const char * filename );
char * current_path ( void );

void reset_time_stamp ( void );
double time_stamp_ms ( int reset );

int platform_init ( void );
void platform_term ( void );


#ifdef USE_TIMERS
//    int platform_init_timers ( void );
    void platform_free_timer ( tmr_handle hnd );
    tmr_handle platform_new_timer ( int msec , tmr_callback cllbck , int user );
    void platform_term_timers ( void );
#endif


#endif /* PLATFORM_H */


