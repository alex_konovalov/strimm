
#include "platform.h"                               // Platform specific code
#include "optospin.h"
#include "math.h"

//typedef void (_cdecl* debg_callback) ( t_rtr_hnd handle , int *user , char * msg  );
t_debg_callback _debug_hook = NULL;
int _debug_int = 0;
char * dbg_msg = NULL;

t_usb_callback _usb_hook = NULL;
int _usb_int = 0;
char * usb_msg = NULL;

int _debug_callback_installed ( void )
{
    return ( NULL != _debug_hook );
}

int _usb_callback_installed ( void )
{
    return ( NULL != _usb_hook );
}

void _do_usb ( t_rtr_hnd handle , char * msg  )
{
	if ( NULL != _usb_hook )
    {
	    int nw;
        double now = time_stamp_ms ( 0 );
        if ( ! usb_msg ) usb_msg = (char *) malloc ( 1024 );
        usb_msg[0] = 0;
        nw = (int) floor ( ( now /** 1000*/ ) + 0.5 );
        sprintf( usb_msg , "%d - %s" , nw , msg );
        _usb_hook ( handle , &_usb_int , usb_msg );
    }
    return;
}

void _do_debug ( t_rtr_hnd handle , char * msg  )
{
	if ( NULL != _debug_hook )
    {
	    int nw;
        double now = time_stamp_ms ( 0 );
        if ( ! dbg_msg ) dbg_msg = (char *) malloc ( 1024 );
        dbg_msg[0] = 0;
        nw = (int) floor ( ( now /** 1000*/ ) + 0.5 );
        sprintf( dbg_msg , "%d - %s" , nw , msg );
        _debug_hook ( handle , &_debug_int , dbg_msg );
    }
    return;
}

CALL int CNV rtr_do_clear_all_hooks ( void )
{
    _usb_hook = NULL;
    _debug_hook = NULL;
    return RTR_OK;
}

CALL int CNV rtr_do_clear_usb_hook ( void )
{
    _usb_hook = NULL;
    _do_debug( NULL , "USB callback hook uninstalled." );
    return RTR_OK;
}

CALL int CNV rtr_do_clear_debug_hook ( void )
{
    _do_debug( NULL , "Uninstalling Debug callback hook." );
    _debug_hook = NULL;
    return RTR_OK;
}

CALL int CNV rtr_set_usb_hook ( t_usb_callback callback )
{
    callback ( NULL , &_usb_int , "USB hooked." );
    _usb_hook = callback;
    _do_debug( NULL , "USB callback hook installed." );
    return RTR_OK;
}

CALL int CNV rtr_set_debug_hook ( t_debg_callback callback )
{
    _debug_hook = callback;
    _do_debug( NULL , "Debug callback hook installed." );
    return RTR_OK;
}


