/*
 	Description:	Optospin library error code
 	Author: 		Andrew Hill <a.hill@cairn-research.co.uk>
 	Copyright:		2010 - 2011 Andrew Hill

    This file is part of optospin.dll and liboptospin.so

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "platform.h"
#include "optospin.h"
#include "debugs.h"
#include "e_strs.h"
#include "errors.h"
#include <assert.h>

void _internal_error ( int code , char * message , t_err_lvl lvl )
{
    char * errstr = (char *) malloc ( strlen ( message ) + 128 );
    errstr[0] = 0;
    strcat ( errstr , message );
    switch ( code )
    {
        case RTR_OK:
          strcat ( errstr , ", no error." );
          break;
        case RTR_ERR_IO:
          strcat ( errstr , ", general I/O error." );
          break;
        case RTR_ERR_TIMEOUT:
          strcat ( errstr , ", timeout error." );
          break;
        case RTR_ERR_INCOMPLETE:
          strcat ( errstr , ", operation incomplete." );
          break;
        case RTR_ERR_NOT_FOUND:
          strcat ( errstr , ", resource not found." );
          break;
        case RTR_ERR_INVALID_HND:
          strcat ( errstr , ", invalid handle." );
          break;
        case RTR_ERR_INVALID_PARAM:
          strcat ( errstr , ", invalid parameter." );
          break;
        case RTR_ERR_UNSUPPORTED:
          strcat ( errstr , ", not supported." );
          break;
        case RTR_ERR_ACK:
          strcat ( errstr , ", operation not acknowledged." );
          break;
        case RTR_ERR_DUMMY_FLUMOXED:
          strcat ( errstr , ", op not supported by dummy rotor." );
          break;
        case RTR_ERR_ROTOR:
          strcat ( errstr , ", USB rotor flagged." );
          break;
        case RTR_ERR_BUFFER_OVERFLOW:
          strcat ( errstr , ", buffer overflow caught." );
          break;
        case RTR_ERR_NOT_INIT:
          strcat ( errstr , ", library uninitiated." );
          break;
		case RTR_ERR_ROTOR_STEPPING:
          strcat ( errstr , ", rotor step time timeout." );
          break;
		case RTR_ERR_ROTOR_WAITING:
          strcat ( errstr , ", rotor still reports resting." );
          break;
		case RTR_ERR_MESSAGE:
          strcat ( errstr , ", library message." );
          break;
        default:
          strcat ( errstr , ", unknown internal ecode." );
          break;
    }
    add_err_str ( errstr , lvl , 0 );
    _do_debug( NULL , errstr );
    return;
}

