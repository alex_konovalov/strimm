

#ifdef WIN32
   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>
   //#define snprintf _snprintf 
#endif

#include "MMOptospinR.h"
#include "MMDevice.h"
#include "DeviceBase.h"
#include <cstdio>
#include <string>
#include <list>
#include <vector>
#include <math.h>
#include "ModuleInterface.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include "platform.h"
#include "MMErrors.h"


char buf[1024];

using namespace std;

#define DEVICE_ALL_GONE_PEAR_SHAPED DEVICE_ERR
#define ERROR_CANNOT_FIND_ARRAY_POS DEVICE_ERR

const char* g_RotorDeviceName = "Optospin Controller";
const char* g_RotorSerial = "Optospin serial number";
const char* g_WheelDeviceName = "Filter wheel ";
const char* g_WheelDeviceName1 = "Filter wheel 1";
const char* g_WheelDeviceName2 = "Filter wheel 2";
const char* g_WheelDeviceName3 = "Filter wheel 3";
const char* g_WheelDeviceName4 = "Filter wheel 4";


const char* m_IndependentMode = "Independent mode";
const char* m_CombinedMode = "Combined mode";
const char* m_InertialMode = "Inertial mode";

t_rtr_mode rot_mode_flags;
COptoSpinHub * mainHub;
unsigned char lmaj;
unsigned char lmin;
bool lib_inited = false;

bool bbuild =false;
#define CHECK_LIB_INIT if(!lib_inited) \
						{ \
							ofstream myfile( "optospin_log.txt" , ios::ate ); \
							myfile.close(); \
							lib_inited = true; \
						    /*rtr_set_debug_hook ( debg_callback );  \
							rtr_set_usb_hook ( usb_callback );*/ \
						}

// windows DLL entry code
#ifdef WIN32
   BOOL APIENTRY DllMain( HANDLE /*hModule*/, 
                          DWORD  ul_reason_for_call, 
                          LPVOID /*lpReserved*/
		   			 )
   {
   	switch (ul_reason_for_call)
   	{
  		case DLL_THREAD_ATTACH:
   		case DLL_THREAD_DETACH:
   			break;
   		case DLL_PROCESS_ATTACH:
			rtr_do_lib_init(&lmaj, &lmin);
			break;
		case DLL_PROCESS_DETACH:
			rtr_do_lib_term ( );
   			break;
   	}
       return TRUE;
   }
#endif


 MODULE_API void InitializeModuleData()
{
	RegisterDevice(g_RotorDeviceName, MM::HubDevice, "Optospin Hub");
   //AddAvailableDeviceName(g_RotorDeviceName, "Optospin Hub");
   //AddAvailableDeviceName(g_WheelDeviceName1, "Filter wheel 1");
   //AddAvailableDeviceName(g_WheelDeviceName2, "Filter wheel 2");
   //AddAvailableDeviceName(g_WheelDeviceName3, "Filter wheel 3");
   //AddAvailableDeviceName(g_WheelDeviceName4, "Filter wheel 4");
 }

 MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0)
		return 0;

	if (strcmp(deviceName, g_RotorDeviceName) == 0)
   {
      // create hub
      return new COptoSpinHub();
   }
   else if (strcmp(deviceName, g_WheelDeviceName1) == 0)
   {
      // create filter wheel
      return new COptoSpinWheel(0);
   } 
   else if (strcmp(deviceName, g_WheelDeviceName2) == 0)
   {
      // create filter wheel
      return new COptoSpinWheel(1);
   }
   else if (strcmp(deviceName, g_WheelDeviceName3) == 0)
   {
      // create filter wheel
      return new COptoSpinWheel(2);
   }
	else if (strcmp(deviceName, g_WheelDeviceName4) == 0)
   {
      // create filter wheel
      return new COptoSpinWheel(3);
   }

   return 0;
 }

 MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}


int getWheelModes(t_whl_idx whl_cnt, t_rtr_hnd rhnd, std::vector<wheel_group_modes> *wheelModes)
 {

	 //std::vector<wheel_group_modes> wheelModes;
	wheelModes->clear();
	for (int i = 0; i < whl_cnt ; i++)
	{
		if ((i+2) % 2 == 0)
		{
			wheel_group_modes whl_m;
			whl_m.mode = 0;
			whl_m.mode_name = m_IndependentMode;
			whl_m.num_of_wheels = 1;
			whl_m.rtr_mode = RTR_MODE_NORMAL;
			wheelModes->push_back(whl_m);
		}
		else
		{
			double val = ((double) i / 2) - 0.5;
			wheelModes->at((int)val).num_of_wheels = 2;
		}
	}
	//int res = rtr_set_mode ( rhnd , 0 );
	//if ( res == RTR_ERR_INSUFFICIENT_HARDWARE )
	//{

	//}else{
	//	if ( ErrorCheck( res , 0 ) ) 
	//	{
	//		return res;
	//	}
	//}
	int res = rtr_get_mode ( rhnd , &rot_mode_flags );
		if ( ErrorCheck ( res , 0 ) ) 
			return res;


	rot_mode_flags = rot_mode_flags + RTR_MODE_NORMAL;
	if ( rot_mode_flags & RTR_MODE_COMBINED_A )
	{
		rot_mode_flags = rot_mode_flags + RTR_MODE_COMBINED_A;
		wheelModes->at(0).rtr_mode = wheelModes->at(0).rtr_mode + RTR_MODE_COMBINED_A;
		wheelModes->at(0).mode = 1;
		wheelModes->at(0).mode_name.assign( m_CombinedMode);
	} else if ( rot_mode_flags & RTR_MODE_INERTIAL_A ) {
		rot_mode_flags = rot_mode_flags + RTR_MODE_INERTIAL_A;
		wheelModes->at(0).rtr_mode = wheelModes->at(0).rtr_mode + RTR_MODE_INERTIAL_A;
		wheelModes->at(0).mode = 2;
		wheelModes->at(0).mode_name.assign( m_InertialMode );
	} 
	if ( rot_mode_flags & RTR_MODE_COMBINED_B )
	{
		rot_mode_flags = rot_mode_flags + RTR_MODE_COMBINED_B;
		wheelModes->at(1).rtr_mode = wheelModes->at(1).rtr_mode + RTR_MODE_COMBINED_B;
		wheelModes->at(1).mode = 1;
		wheelModes->at(1).mode_name.assign( m_CombinedMode);
	} else if ( rot_mode_flags & RTR_MODE_INERTIAL_B ) {
		rot_mode_flags = rot_mode_flags + RTR_MODE_INERTIAL_B;
		wheelModes->at(1).rtr_mode = wheelModes->at(1).rtr_mode + RTR_MODE_INERTIAL_B;
		wheelModes->at(1).mode = 2;
		wheelModes->at(1).mode_name.assign( m_InertialMode );
	}

	return DEVICE_OK;
 }

 COptoSpinHub::COptoSpinHub() 
 {
	initialized_ = false;
	library_open_ = false;
	CHECK_LIB_INIT
	fposn = NULL;
	InitializeDefaultErrorMessages();
	HubHandle = INVALID_RTR_HANDLE;	
	// Name
	CreateProperty(MM::g_Keyword_Name, "Cairn Optospin", MM::String, true);
	// Description
	CreateProperty(MM::g_Keyword_Description, "Cairn Optospin Rotor", MM::String, true);
	t_whl_idx rtr_cnt;
	
	int ret = rtr_get_device_count ( &rtr_cnt );
	if ( RTR_OK != ret )
	{
		if ( ErrorCheck ( ret , false ) ) 
			return;
	}
	char *ser;
	char *desc;
	t_whl_idx whl_count;
	t_rtr_hnd rhnd = NULL;

	CPropertyAction* pAct ;
	pAct = new CPropertyAction (this, &COptoSpinHub::OnUnit);
	CreateProperty(g_RotorSerial, "", MM::String, false, pAct, true);
	string tempSet;
	for ( unsigned int i = 1 ; i<=rtr_cnt ; i++)
	{
        ret = rtr_get_device_info( i , &ser , &desc);
		if ( RTR_OK != ret )
		{
			if ( ErrorCheck ( ret , false ) ) 
				return;
		}
	    
	    ostringstream os;
		os << ser;
		availablerotors_.push_back(os.str());
		AddAllowedValue( g_RotorSerial , os.str().c_str() );
		if (i = 1)
			tempSet.assign(os.str());
		os.str("");
    }
	SetProperty(g_RotorSerial, tempSet.c_str());
 }

COptoSpinHub::~COptoSpinHub()
{
	Shutdown();
	if ( fposn ) 
		free ( fposn );
}

 int COptoSpinHub::Initialize()
 {
	 if (initialized_)
		 return DEVICE_OK;

	int res = CreateProperty( "serial number" , mySerial.c_str() , MM::String, true);
	if (DEVICE_OK != res) return res;
	
	CPropertyAction* pAct ;
	ostringstream os;
	spin_speed = 10.00;
	pAct = new CPropertyAction (this, &COptoSpinHub::OnSpin);		
	os << "Filter Wheel" << " Spin";
	int ret = CreateProperty( os.str().c_str() , "", MM::String, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue(  os.str().c_str() , "False" ); 
	AddAllowedValue(  os.str().c_str() , "True" ); 
	os.str("");

	pAct = new CPropertyAction (this, &COptoSpinHub::OnHertz );
	os;
	os << "Spin Speed";
	ret = CreateProperty( os.str().c_str() , "0", MM::Float, false, pAct);
	if (ret != DEVICE_OK) return ret;
	SetPropertyLimits( os.str().c_str() , 1.00 , 150.00);

	pAct = new CPropertyAction (this, &COptoSpinHub::OnAllPositions);
	res = CreateProperty( "Positions" , "" , MM::String, false, pAct);
	if (DEVICE_OK != res) return res;

	initialized_ = true;
	return DEVICE_OK;
 }

 int COptoSpinHub::Shutdown()
 {
	//_drop_serial_from_list ( (char *)mySerial.c_str() );	
	if ( HubHandle != INVALID_RTR_HANDLE )
	{
		rtr_do_device_close(HubHandle);
		library_open_ = false;
		HubHandle = INVALID_RTR_HANDLE;
	};
	 return 0;
 }

 void COptoSpinHub::GetName(char* name) const
{
	CDeviceUtils::CopyLimitedString( name , g_RotorDeviceName );
}

 bool COptoSpinHub::Busy() 
 {
	t_bool _rdy;
	int err = rtr_get_ready_to_acqu ( HubHandle , &_rdy );
	if ( err != RTR_OK)
	{
		ErrorCheck ( err , true );
		return true;
	}
	
	if ( _rdy )
		return false;
	else
		return true;
 }

 int COptoSpinHub::DetectInstalledDevices()
 {

	 InitializeModuleData();
	 ostringstream oss;
	 bbuild = true;
	 int count = 0;
	 for ( int i = 0; i < wheelGroupModes.size(); i++)
	 {
		 if ( strcmp ( m_IndependentMode , wheelGroupModes[i].mode_name.c_str() ) == 0 )
		 {
			 for ( int j = 0; j < wheelGroupModes[i].num_of_wheels ; j++)
			 {	
				 count++;
				 //tempVal = (i * 2) + j;
				 oss << g_WheelDeviceName << count;
				 MM::Device* pDev = ::CreateDevice(oss.str().c_str());
				if (pDev) 
				{
					AddInstalledDevice(pDev);
				}
				oss.str("");
			 }
		 }
		 if ( strcmp ( m_CombinedMode , wheelGroupModes[i].mode_name.c_str() ) == 0 )
		 {
			count++;
			oss << g_WheelDeviceName << count;
			MM::Device* pDev = ::CreateDevice(oss.str().c_str());
			if (pDev) 
			{
				AddInstalledDevice(pDev);
			}
			oss.str("");
			count++;
		 }
		 if ( strcmp ( m_InertialMode , wheelGroupModes[i].mode_name.c_str() ) == 0 )
		 {
			count++;
			oss << g_WheelDeviceName << count;
			MM::Device* pDev = ::CreateDevice(oss.str().c_str());
			if (pDev) 
			{
				AddInstalledDevice(pDev);
			}
			oss.str("");
			count++;
		 }

	}
	 
	 return 0;
 }


 int COptoSpinHub::OnUnit(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set( mySerial.c_str() );
	}
	else if (eAct == MM::AfterSet)
	{
		t_whl_idx whl_count;
		t_rtr_hnd rhnd = NULL;
		string str;
		pProp->Get( str );
		mySerial = str;
		if (! library_open_)
		{
			int ret = rtr_get_device_by_serial( (char *)mySerial.c_str() , &rhnd , t_bool_false  );
			if ( ErrorCheck ( ret , false ) ) return ret;
			ret = rtr_get_wheel_count( rhnd , &whl_count);
			if ( ErrorCheck ( ret , false ) ) return ret;
			myWheelCount = whl_count;
			ret = getWheelModes(whl_count, rhnd, &wheelGroupModes);
			if ( ErrorCheck ( ret , false ) ) return ret;
		
			ret = rtr_get_max_wheel_count ( rhnd , &max_wheel_count );
			if ( ErrorCheck ( ret , false ) ) return ret;

			if ( !fposn )
				fposn = (t_whl_pos *)malloc ( sizeof ( t_whl_pos ) * max_wheel_count );

			for ( ret = 0 ; ret < max_wheel_count ; ret++ )
			{
				fposn[ret] = 0;
			};			
			HubHandle = rhnd;
			library_open_ = true;
		}


	}
   return DEVICE_OK; 
 }
 int COptoSpinHub::OnSpin(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if ( spinning_ )
			pProp->Set( "True" );
		else
			pProp->Set( "False" );
	}
	else if (eAct == MM::AfterSet)
	{
		int err;
		unsigned int i;
		string str;
		pProp->Get( str );
		if ( ( strcmp ( "True" , str.c_str() ) == 0 ) && ( ! spinning_ ) )
		{
			rtr_do_clr_err_strs(ERR_LVL_ALL);
			err = rtr_do_spin_wheels ( HubHandle );
			if ( err != RTR_OK )
			{
				if (ErrorCheck(err, true))
				{
					pProp->Set("False"); 
					return DEVICE_ALL_GONE_PEAR_SHAPED;
				}
			}
			COptoSpinWheel * tmp;
			for ( i=0 ; i< myAttachedWheels.size() ; i++ )
			{
				tmp = myAttachedWheels[i];
				tmp->is_spinning_ = true;
			}		
			spinning_ = true;
			return DEVICE_OK;
		}
		if ( ( strcmp ( "False" , str.c_str() ) == 0 ) && ( spinning_ ) )
		{
			err = rtr_do_stop_wheels ( HubHandle );
			if ( err != RTR_OK )
			{
				pProp->Set("False"); 
				return DEVICE_ALL_GONE_PEAR_SHAPED;
			}
			COptoSpinWheel * tmp;
			for ( i=0 ; i<myAttachedWheels.size() ; i++ )
			{
				tmp = myAttachedWheels[i];
				tmp->is_spinning_ = false;
			}		
			spinning_ = false;
			return DEVICE_OK;
		}
	}
   return DEVICE_OK; 
 }

  int COptoSpinHub::OnHertz(MM::PropertyBase* pProp, MM::ActionType eAct)
{

	if (eAct == MM::BeforeGet)
	{
		float pVal;
		pVal = spin_speed;
		pProp->Set(pVal);
	}

	else if (eAct == MM::AfterSet)
	{
		double sVal;
		pProp->Get(sVal);
		spin_speed = sVal;
		int err= rtr_set_speed ( HubHandle , sVal );

		if ( ErrorCheck(err,true) )
		{
			return DEVICE_CAN_NOT_SET_PROPERTY;
		}
		 //myWheels[channel]->myPosition = pos;
   }
   return DEVICE_OK;

  }
void _write_fposn_to_ascii_buffer ( int max , t_whl_pos * fpos )
{
	buf[0] = 0;
	char* tmp = buf;
	int i;
	for ( i = 0 ; i < max ; i++ )
	{
		strcat(tmp,itoa(fpos[i],&buf[1000],10));
		if ( i != max - 1 ) strcat(tmp,"-");
	}
	return;
}

 int COptoSpinHub::OnAllPositions(MM::PropertyBase* pProp, MM::ActionType eAct)
 {
	if (eAct == MM::BeforeGet)
	{

		//int ret = rtr_get_wheel_positions(HubHandle , fposn);
		//if (ret != DEVICE_OK) return ret;
		//_write_fposn_to_ascii_buffer ( max_wheel_count , fposn );
		ostringstream os;
		for ( int i = 0; i < myAttachedWheels.size(); i++)
		{
			os << myAttachedWheels[i]->myPosition;
			if ( i < myAttachedWheels.size() - 1 )
				os << "-";

		}
		pProp->Set(os.str().c_str());
	}
	else if (eAct == MM::AfterSet)
	{
		string val;
		pProp->Get(val);
		buf[0]=0;
		char* t1 = buf;
		char* t2;
		strcat( t1 , val.c_str() );
		strcat( t1 , "-" );
		int idx = 0;
		
		// Prolly needs more debugging
		while ( *t1 )
		{
			t2 = t1;
			while ( *t2 )
			{
				if ( *t2 == 45 )
				{
					*t2 = 0;
				} else {
				t2++;
				}
			}
			
			if ( *t1 ) 
			{
				fposn[idx] = atoi(t1);
				if ( idx < myAttachedWheels.size() )
					myAttachedWheels[idx]->myPosition = fposn[idx];
				idx++;
			}
			t2++;
			t1 = t2;
		}
		//wait for ready to move here
		t_bool ready;
		int err = rtr_do_wait_ready_to_move ( HubHandle ,  &ready );
		if ( t_bool_false == ready ) return DEVICE_CAN_NOT_SET_PROPERTY;

		int rtn = rtr_set_wheel_positions ( HubHandle , fposn );
		if ( ErrorCheck ( rtn , 0 ) ) return DEVICE_ALL_GONE_PEAR_SHAPED;
		_write_fposn_to_ascii_buffer ( max_wheel_count , fposn );
		pProp->Set((const char*)&buf[0]);

	}
	return DEVICE_OK; 
 }
//int COptoSpinHub::OnMode(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
//{
//   if (eAct == MM::BeforeGet)
//   {
//	  
//      pProp->Set(wheelModes[channel].mode_nm.c_str());
//   }
//   else if (eAct == MM::AfterSet)
//   {
//      if (initialized)
//      {
//         // revert
//         pProp->Set(wheelModes[channel].mode_nm.c_str());
//         return ERR_MODE_CHANGE_FORBIDDEN;
//      }
//
//	  pProp->Get(wheelModes[channel].mode_nm);
//
//	  //rot_mode_flags = RTR_MODE_NORMAL;
//	  if ( strcmp ( m_IndependentMode , wheelModes[channel].mode_nm.c_str() ) == 0 )
//	  {
//		  if ( 0 == channel)
//			rot_mode_flags = rot_mode_flags & 0x000000C0;
//		  else
//			rot_mode_flags = rot_mode_flags & 0x00000030;
//
//		  wheelModes[channel].mode_nm.assign(m_IndependentMode); 
//		  wheelModes[channel].mode = 0;
//		 
//	  }
//	  if ( strcmp ( m_CombinedMode , wheelModes[channel].mode_nm.c_str() ) == 0 ) 
//	  {
//		  if ( 0 == channel)
//		  {
//			rot_mode_flags = rot_mode_flags & 0x000000C0;
//			rot_mode_flags = rot_mode_flags + RTR_MODE_COMBINED_A;
//		  }
//		  else
//		  {
//			rot_mode_flags = rot_mode_flags & 0x00000030;
//			rot_mode_flags = rot_mode_flags + RTR_MODE_COMBINED_B;
//		  }
//
//		  wheelModes[channel].mode_nm.assign(m_CombinedMode); 
//		  wheelModes[channel].mode = 1;
//	  }
//	  if ( strcmp ( m_InertialMode , wheelModes[channel].mode_nm.c_str() ) == 0 )
//	  {
//		  if ( 0 == channel)
//		  {
//			rot_mode_flags = rot_mode_flags & 0x000000C0;
//			rot_mode_flags = rot_mode_flags + RTR_MODE_INERTIAL_A;
//		  }
//		  else
//		  {
//			rot_mode_flags = rot_mode_flags & 0x00000030;
//			rot_mode_flags = rot_mode_flags + RTR_MODE_INERTIAL_B;
//		  }
//
//		  wheelModes[channel].mode_nm.assign(m_InertialMode); 
//		  wheelModes[channel].mode = 2;
//	  }
//	  int res = rtr_set_mode ( myHandle , rot_mode_flags );
//	  if (res != DEVICE_OK) return res;
//   }
//   return DEVICE_OK;
//}
// int  COptoSpinHub::OnTest(MM::PropertyBase* pProp, MM::ActionType eAct)
//	 {
//	if (eAct == MM::BeforeGet)
//	{
//		pProp->Set( "Yay" );
//	}
//	else if (eAct == MM::AfterSet)
//	{
//
//
//	}
//   return DEVICE_OK; 
// }

COptoSpinWheel::COptoSpinWheel(int index) 
{
	InitializeDefaultErrorMessages();
	initialized_ = false;
	is_spinning_ = false;
	would_spin_ = false;
	changedTime_= 0.0;
	myIndex = index + 1;
	ostringstream os;
	os << g_WheelDeviceName << index+1;
	myName.assign(os.str().c_str());

	// Name
	CreateProperty(MM::g_Keyword_Name, myName.c_str(), MM::String, true);
	// Description
	CreateProperty(MM::g_Keyword_Description, "Optospin Filter Wheel", MM::String, true);
	// Wheel Mode
	CPropertyAction* pAct ;
	pAct = new CPropertyAction (this, &COptoSpinWheel::OnMode);
	CreateProperty("Wheel Mode", "", MM::String, false, pAct, true);
    CreateHubIDProperty();

	EnableDelay(); // signals that the dealy setting will be used
}

COptoSpinWheel::~COptoSpinWheel()
{
	Shutdown();

	// if our parent COptoSpinHub is orpahned and we are it's last wheel
    // then we should nuke the COptoSpinHub and _drop_rotor_from_list
}

int getMyArrayPos(std::vector<std::string> name, int index)
{
	if (index == 1) return 0;
	int countPos = 0;
	for (int i = 0; i < name.size(); i++)
	{
		if ( strcmp ( m_IndependentMode , name[i].c_str() ) == 0)
		{
			if (index -1 == i)
				return countPos;

			countPos++;
		}
		if ( strcmp ( m_CombinedMode , name[i].c_str() ) == 0)
		{
			if (index -1 == i)
				return countPos;

			countPos++;
			countPos++;
		}
		if ( strcmp ( m_InertialMode , name[i].c_str() ) == 0)
		{
			if (index -1 == i)
				return countPos;

			countPos++;
			countPos++;
		}

	}
	return ERROR_CANNOT_FIND_ARRAY_POS;
}
int COptoSpinWheel::Initialize()
{
	if (initialized_) return DEVICE_OK;

	COptoSpinHub* optospinHub = static_cast<COptoSpinHub*>(GetParentHub());
	optospinHub->myAttachedWheels.push_back(this);
	int res = CreateProperty( "Mode" , myModeName.c_str() , MM::String, true);
	optospinHub->modeNames.push_back(myModeName);
	haveArrayPos = false;
	WheelHandle = optospinHub->HubHandle;
	changedTime_ = GetCurrentMMTime();

	if ( WheelHandle == INVALID_RTR_HANDLE )
	{
		MessageBoxW(0,L"Aaaaaarrrrggghhh",L"WTF!!!",0);
		return DEVICE_ALL_GONE_PEAR_SHAPED;
	}

	if (DEVICE_OK != res) return res;
	t_whl_pos filt_cnt = 0;
	res = rtr_get_wheel_position_max ( WheelHandle , myIndex , &filt_cnt );
	if ( RTR_ERR_INVALID_HND == res )
	{
		if ( ErrorCheck( res , true ) )
		{
			MessageBoxW(0, L"Optospin wheel was given an invalid rotor handle !!!!!." , L"Cairn Optospin." , 0);
			return DEVICE_NOT_CONNECTED;
		}
	}
	if ( RTR_OK != res )
	{
		if ( ErrorCheck( res , true ) )
		{
			return DEVICE_ALL_GONE_PEAR_SHAPED;
		}
	}
	if ( 0 == filt_cnt )
	{
		return DEVICE_ALL_GONE_PEAR_SHAPED;
	}
	myPosCount = filt_cnt;
	
	t_whl_pos imat;
	res = rtr_get_wheel_position ( WheelHandle , myIndex , &imat );
	if ( RTR_OK != res )
	{
		if ( ErrorCheck( res , true ) )
		{
			return DEVICE_ALL_GONE_PEAR_SHAPED;
		}
	}
	if ( 0 == imat )
	{
		return DEVICE_ALL_GONE_PEAR_SHAPED;
	}
	myPosition = imat;

	const int bufSize = 1024;
	char buf[bufSize];
	for ( long i=0; i< myPosCount; i++)
	{
		snprintf ( buf, bufSize , "Filter-%d" , i+1 );
		SetPositionLabel ( i , buf ); 
	}

	// State
	// -----
	CPropertyAction* pAct = new CPropertyAction (this, &COptoSpinWheel::OnState);
	int ret = CreateProperty(MM::g_Keyword_State, "1", MM::Integer, false, pAct);
	if (ret != DEVICE_OK)
		return ret;

	// Label
	// -----
	pAct = new CPropertyAction (this, &CStateBase::OnLabel);
	ret = CreateProperty(MM::g_Keyword_Label, "", MM::String, false, pAct);
	if (ret != DEVICE_OK)
		return ret;

	ostringstream os;
	if ( strcmp ( m_IndependentMode , myModeName.c_str() ) == 0)
	{
		pAct = new CPropertyAction (this, &COptoSpinWheel::OnSpins);		
		os << " Spins";
		int ret = CreateProperty( os.str().c_str() , "", MM::String, false, pAct); 
		if (ret != DEVICE_OK) return ret; 
		AddAllowedValue(  os.str().c_str() , "False" ); 
		AddAllowedValue(  os.str().c_str() , "True" ); 
		os.str("");
	}

	pAct = new CPropertyAction (this, &COptoSpinWheel::OnLocked);
	os << " Position Locked";
	ret = CreateProperty( os.str().c_str() , "", MM::String, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( os.str().c_str() , "False" ); // Closed
	AddAllowedValue( os.str().c_str(), "True" ); // Open
	os.str("");

	initialized_ = true;
	return DEVICE_OK;
}

int COptoSpinWheel::Shutdown()
{
	WheelHandle = INVALID_RTR_HANDLE;	
	return 0;
}
void COptoSpinWheel::GetName(char* name)const
{
	CDeviceUtils::CopyLimitedString( name , myName.c_str() );
}
bool COptoSpinWheel::Busy()
{
	t_bool _rdy;
	int err = rtr_get_ready_to_acqu ( WheelHandle , &_rdy );
	if ( err != RTR_OK)
	{
		ErrorCheck ( err , true );
		return true;
	}
	
	if ( _rdy ) {
	   MM::MMTime interval = GetCurrentMMTime() - changedTime_;
	   MM::MMTime delay(GetDelayMs()*1000.0);
	   if (interval < delay)
		  return true;
	   else
		  return false;
		//return false;

	}else {
		return true;	
	}

	MM::MMTime interval = GetCurrentMMTime() - changedTime_;
   MM::MMTime delay(GetDelayMs()*1000.0);
   if (interval < delay)
      return true;
   else
      return false;
}

int COptoSpinWheel::OnMode(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{  
		int idx;
		if (myIndex < 3 )
			idx = 1;
		else
			idx = 2;

		myMode = wheelGroupModes[idx-1].mode;
		myModeName = wheelGroupModes[idx-1].mode_name;
		pProp->Set(myModeName.c_str());
	}
	else if (eAct == MM::AfterSet)
	{
		string str;
		pProp->Get( str );
		myModeName = str;
	}
	return DEVICE_OK;
}

int COptoSpinWheel::OnState(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	ostringstream val;
	//if(!haveArrayPos)
	//{
	//	COptoSpinHub* optospinHub = static_cast<COptoSpinHub*>(GetParentHub());
	//	myArrayPos = getMyArrayPos(optospinHub->modeNames,myIndex);
	//	haveArrayPos = true;
	//}
	// Set timer for the Busy signal
    changedTime_ = GetCurrentMMTime();
	if (eAct == MM::BeforeGet)
	{
		val << myPosition-1;
		//long val = myWheels[channel]->myPosition;
		pProp->Set(val.str().c_str());
		val.str("");
	}
	else if (eAct == MM::AfterSet)
	{
//		char asciiStr[5] = {48,48,48,48,0};
		t_whl_pos posns[4] = {0,0,0,0};
		int cnt = 0;
		string sVal;
		pProp->Get(sVal);
		char * pos_str;
		pos_str = (char *)sVal.c_str();

		int pos = atoi ( sVal.c_str() ) + 1; 
		
		if ( is_spinning_ )	return DEVICE_CAN_NOT_SET_PROPERTY;
		if ( pos > myPosCount) return DEVICE_CAN_NOT_SET_PROPERTY;
		if ( pos < 0 )  return DEVICE_CAN_NOT_SET_PROPERTY;

		posns[myIndex-1] = pos;
//		asciiStr[myIndex-1] = pos_str[0];	

		
		myPosition = pos;
//		myPosition = asciiStr[myIndex-1] - 48;

//		rtr_do_clr_err_strs(ERR_LVL_ALL);

		//wait for ready to move here
		t_bool ready;
		int err = rtr_do_wait_ready_to_move ( WheelHandle ,  &ready );
		if ( t_bool_false == ready ) return DEVICE_CAN_NOT_SET_PROPERTY;

		err = rtr_set_wheel_positions ( WheelHandle , posns ); 
//		int err = rtr_set_ascii_wheel_positions ( myHandle , asciiStr , 4 , 48 ); // 49 = ascii "1"

		//int err = rtr_set_wheel_position(myHandle, myWheels[channel]->myIndex , gotoPos);
		if ( ErrorCheck(err,true) )
		{
			return DEVICE_CAN_NOT_SET_PROPERTY;
		}
		 //myWheels[channel]->myPosition = pos;
   }
   return DEVICE_OK;
}
int COptoSpinWheel::OnLocked(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if ( is_spinning_ ) pProp->Set( "True" ); else pProp->Set( "False" );
	}
	else if (eAct == MM::AfterSet)
	{
		if ( is_spinning_ ) pProp->Set( "True" ); else pProp->Set( "False" );
/*		string spins;
		pProp->Get(spins);

		if ( (strcmp( spins.c_str() , "True") ) )
		{
			is_spinning = true;
		}
		if ( (strcmp ( spins.c_str() , "False")) )
		{
			is_spinning = false;
		}
*/
	}
	return DEVICE_OK;
}
int COptoSpinWheel::OnSpins(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
			if (would_spin_ == 0 ) 
				pProp->Set( "False" ); 
			else 
				pProp->Set( "True" );
			return DEVICE_OK;	
	};
	if (eAct == MM::AfterSet)
	{
		if ( is_spinning_ )
		{
			if ( would_spin_ == 0 ) 
				pProp->Set( "False" ); 
			else 
				pProp->Set( "True" );
			return DEVICE_OK;	
		}
		int err;
		string spins;
		pProp->Get(spins);
		if ( (strcmp(spins.c_str() , "True") == 0 ) && (would_spin_ == 0 ) )
		{
			err = rtr_set_spin_wheel ( WheelHandle , myIndex , 1 );
			if ( err != RTR_OK )
			{
				pProp->Set("False"); 
				return DEVICE_ALL_GONE_PEAR_SHAPED;
			}
			would_spin_ = 1;
		}
		if ( (strcmp ( spins.c_str() , "False") == 0 ) && ( would_spin_ != 0 ) )
		{
			err = rtr_set_spin_wheel ( WheelHandle , myIndex , 0 );
			if ( err != RTR_OK )
			{
				pProp->Set("True"); 
				return DEVICE_ALL_GONE_PEAR_SHAPED;
			}
			would_spin_ = 0;
		}
	}
	return DEVICE_OK;
}