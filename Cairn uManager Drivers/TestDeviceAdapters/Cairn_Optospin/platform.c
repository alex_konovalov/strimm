/*
 	Description:	Generic platform freeing code
 	Author: 		Andrew Hill <a.hill@cairn-research.co.uk>
 	Copyright:		2008-2011 Andrew Hill

    This file is part of various Andrew Hill & Cairn Reearch programs.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "platform.h"
#include <math.h>

#ifdef WINDOWS
    #include <windows.h>
    #include <winbase.h>
	#include <WinUser.h>
	#include <errno.h>
    #ifdef USE_SOCKETS
        #include <winsock2.h>
    #endif
#endif

#ifdef LINUX
    #include <sched.h>
    #include <errno.h>
    #include <dlfcn.h>
    #include <limits.h>
    #include <unistd.h>
    #include <sys/time.h>
    #include <time.h>
    #include <sys/select.h>
    #ifdef USE_SOCKETS
        #include <sys/socket.h>
        #include <netinet/in.h>
        #include <netdb.h>
    #endif
    #ifdef USE_THREADS
        #include <pthread.h>
    #endif
    #ifdef USE_TIMERS
        #include <assert.h>
    #endif
#endif

#ifdef USE_TIMERS
    #define NO_TIMER 0
    #ifdef WINDOWS
        lib_handle winmm_hnd=0;
        typedef MMRESULT (WINAPI *t_StartTimer) ( UINT Delay,UINT Res,LPTIMECALLBACK Proc,DWORD_PTR User,UINT Event);
        typedef MMRESULT (WINAPI *t_StopTimer) ( UINT id );
        t_StartTimer winStartTimer;
        t_StopTimer winStopTimer;
        typedef MMRESULT _tmr_handle;
        #define USE_SIG 0
    #endif
    #ifdef LINUX
        typedef timer_t _tmr_handle;
        #define USE_SIG SIGRTMIN
    #endif
#endif

char * str = NULL;
int str_size = 0;

#ifdef WINDOWS
    __int64 _pc_freq, _pc_start;
#endif

#ifdef LINUX
    double tick_start;
#endif

char * to_lower ( char* s )
{
    char *ret = s;
    if(s) for( ;*s; ++s)
        *s = tolower((unsigned char)*s) & 0xFF;
    return ret;
}

#ifdef WINDOWS
FILE *popen ( const char* command, const char* flags)
{
    return _popen(command,flags);
}

int pclose ( FILE* fd)
{
    return _pclose(fd);
}

void system_error_message ( system_error_code err )
{
	void * lpMsgBuf;
	lpMsgBuf = NULL;
	if ( err == 0 ) return;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		err,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) lpMsgBuf,
		0,
		NULL );
	MessageBox( 0 , (LPCTSTR)lpMsgBuf, "System Error", MB_OK | MB_ICONINFORMATION );
	LocalFree( lpMsgBuf );
    return;
}
#endif

#ifdef USE_IPC
queue_handle open_queue ( queue_key keyval , bool exclusive )
{
    queue_handle qid;
    #ifdef LINUX
    int msgflg = IPC_CREAT | 0660 ;
    if ( exclusive ) msgflg = msgflg | IPC_EXCL;
    qid = msgget( keyval, msgflg );
    if ( qid == -1) return INVALID_QUEUE_HANDLE;
    #endif
    #ifdef WINDOWS
    adwsfwaqerf
    #endif
    return qid;
}

int close_queue ( queue_handle queue )
{
    int ret = 0;
    if ( INVALID_QUEUE_HANDLE == queue ) return ret;
    #ifdef LINUX
    struct msqid_ds *buf = NULL;
    ret = msgctl( queue , IPC_RMID , buf );

    #endif
    #ifdef WINDOWS
    adwsfwaqerf
    #endif
    return ret;
}

queue_key gen_queue_key ( int id )
{
    queue_key key;

    #ifdef LINUX
    key = ftok( QUEUE_KEY_GEN_PATH , id );
    #endif
    #ifdef WINDOWS
    adwsfwaqerf
    #endif
    return key;
}
#endif

#ifdef USE_SOCKETS
de_socket connect_socket ( char *address , int port )
{
    struct sockaddr_in addr;
    int i;
    de_socket skt;
    skt = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if ( INVALID_SOCKET == skt )
    {
        return INVALID_SOCKET;
    }
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(address);
    addr.sin_port = htons(port);
    i = connect( skt , ( const struct sockaddr * )&addr , sizeof(addr) );
    if ( i == SOCKET_ERROR )
    {
        closesocket( skt );
        skt = INVALID_SOCKET;
    }
    return skt;
}
#endif

#ifdef USE_DYNAMIC_LINKING
void * hook_library ( lib_handle handle , char * name )
{
    #ifdef WINDOWS
	return (void *)GetProcAddress( handle , name );
    #endif
    #ifdef LINUX
    // requires -ldl dynamic linking library
    return dlsym ( handle , name );
    #endif
};


void free_library ( lib_handle handle )
{
    #ifdef WINDOWS
    FreeLibrary ( handle );
    #endif
    #ifdef LINUX
    dlclose( handle );
    #endif
};

lib_handle load_library ( char * filename )
{
    #ifdef WINDOWS
    return LoadLibrary ( filename );
    #endif
    #ifdef LINUX
    return dlopen ( filename , RTLD_NOW ); //RTLD_LAZY );
    #endif
};
#endif

#ifdef LINUX
struct timeval  tv;
struct timezone tz;
struct tm      *tm;
#endif

void reset_time_stamp ()
{
    #ifdef WINDOWS
    QueryPerformanceFrequency((LARGE_INTEGER*)&_pc_freq);
    QueryPerformanceCounter((LARGE_INTEGER*)&_pc_start);
    #endif
    #ifdef LINUX
	gettimeofday(&tv, &tz);
	tm = localtime(&tv.tv_sec);
	tick_start = tm->tm_hour * 3600 * 1000;
	tick_start = tick_start + tm->tm_min * 60 * 1000;
	tick_start = tick_start + tm->tm_sec * 1000;
	tick_start = 1 + tick_start + tv.tv_usec / 1000;
    #endif
    return;
}

double time_stamp_ms ( int reset )
{
    double res;
    #ifdef WINDOWS
    __int64 end;
    QueryPerformanceCounter((LARGE_INTEGER*)&end);
    res = end - _pc_start;
    res = res * 1000;
    res = res / _pc_freq;
    if ( reset ) _pc_start = end;

//    unsigned __int64 val;
//    QueryPerformanceCounter( (LARGE_INTEGER *)&val );
//    res = (val - base_time ) * freq_;
	//res = res * 1000;

    #endif
    #ifdef LINUX
    double tmp;
	gettimeofday(&tv, &tz);
	tm = localtime(&tv.tv_sec);
	tmp = tm->tm_hour * 3600 * 1000;
	tmp = tmp + tm->tm_min * 60 * 1000;
	tmp = tmp + tm->tm_sec * 1000;
	tmp = tmp + tv.tv_usec / 1000;
    tmp = tmp - tick_start;
    tmp = tmp / 1000;
    res = tmp;
    #endif
    return res;
}

void ms_wait ( int msec )
{
    #ifdef WINDOWS
    Sleep( msec );
    #endif
    #ifdef LINUX
    struct timeval timeout;
    timeout.tv_sec = msec/1000;
    timeout.tv_usec= (msec-timeout.tv_sec*1000)*1000;
    select (0, NULL, NULL, NULL, &timeout);
    #endif
    return;
}


char * current_path ( void )
{
    #ifdef WINDOWS
    int i;
    #endif
	if ( str_size < MAX_PATH+1 )
    {
        free ( str );
        str = ( char * ) malloc ( MAX_PATH+1 );
        str_size = MAX_PATH+1;
    }
    #ifdef WINDOWS
    i = GetCurrentDirectory ( MAX_PATH , str );
    if ( i < 1 ) return NULL;
    #endif
    #ifdef LINUX
    char * res = getcwd ( str , str_size );
    if ( NULL == res ) return NULL;
    #endif
    return str;
}

int file_exist(const char * filename)
{
    #ifdef WINDOWS
    FILE * file;
    file = fopen ( filename, "r");
    if ( file )
    {
        fclose(file);
        return 1;
    }
    #endif
    #ifdef LINUX
    FILE * file;
    file = fopen(filename, "r");
    if ( file )
    {
        fclose(file);
        return 1;
    }
    #endif
    return 0;
}

#ifdef USE_THREADS
void thread_free ( thread_handle *thread )
{
    #ifdef WINDOWS
    WaitForSingleObject ( *thread , 500 );
    CloseHandle ( *thread );
    #endif
    #ifdef LINUX
    pthread_join( *thread , NULL);
    #endif
    (*thread) = INVALID_THREAD_HANDLE;
    return;
}

int thread_create ( thread_handle *handle , thread_call call , thread_param user )
{
    thread_handle res;
    *handle = INVALID_THREAD_HANDLE;
    #ifdef WINDOWS
    res = CreateThread ( NULL , 0 , call , user , 0 , NULL );
    #endif
    #ifdef LINUX
    //requires pthread library
	pthread_create ( &res, NULL, call , user );
	#endif
    if ( INVALID_THREAD_HANDLE == res )
        return -1;
	*handle = res;
	return 0;
}
#endif

#ifdef WINDOWS
void sched_yield ( void ) { Sleep ( 0 ); return; }
#endif

#ifdef USE_MUTEX
mutex_handle mutex_create ( void )
{
    mutex_handle mtx;
    #ifdef WINDOWS
    mtx = CreateMutex ( NULL , FALSE , NULL );
    #endif
    #ifdef LINUX
    pthread_mutex_t *mutex;
    mutex = (pthread_mutex_t *)malloc ( sizeof ( pthread_mutex_t ) );
    int i = pthread_mutex_init( mutex , NULL );
    if ( i )
    {
        free ( mutex );
        return INVALID_MUTEX_HANDLE;
    }
    mtx = (void *) mutex;
    #endif
    return mtx;
}

void mutex_free ( mutex_handle *mtx )
{
    #ifdef WINDOWS
    CloseHandle ( *mtx );
    #endif
    #ifdef LINUX
    pthread_mutex_destroy(*mtx);
    #endif
    *mtx = INVALID_MUTEX_HANDLE;
    return;
}

bool mutex_acquire ( mutex_handle mtx )
{
    #ifdef WINDOWS
    /*  Note: in Windows a thread can repeatedly call WaitForSingleObject
        on a single Mutex *without* blocking. */
    int i = WaitForSingleObject ( mtx , INFINITE );
    if ( WAIT_OBJECT_0 != i ) return false;
    #endif
    #ifdef LINUX
    int i = pthread_mutex_lock ( mtx );
    if ( i ) return false;
    #endif
    return true;
}

bool mutex_release ( mutex_handle mtx )
{
    #ifdef WINDOWS
    ReleaseMutex ( mtx );
    #endif
    #ifdef LINUX
    int i = pthread_mutex_unlock ( mtx );
    if ( i ) return false;
    #endif
    return true;
}
#endif

#ifdef USE_TIMERS
struct _timer_details
{
    _tmr_handle hnd;
    int user;
    long count;
    tmr_callback callback;
};

typedef struct _timer_details timer_details;

timer_details timers[TIMER_COUNT];
int timers_inited = 0;

#ifdef LINUX
void _go_timer( int signo , siginfo_t* extra , void* cruft )
#endif
#ifdef WINDOWS
void CALLBACK _go_timer ( UINT timer , UINT uMsg , DWORD_PTR signo , DWORD dw1 , DWORD dw2 )
#endif
{
	int tmr = signo - USE_SIG;
	timers[tmr].count++;
	timers[tmr].callback( &timers[tmr].user , timers[tmr].count , tmr );
	return;
}


int platform_init_timers ( void )
{
    #ifdef LINUX
    assert( TIMER_COUNT < SIGRTMAX-SIGRTMIN );
    #endif
    int i;
    if ( timers_inited ) return 0;
    for ( i=0 ; i<TIMER_COUNT ; i++ )
    {
        timers[i].hnd = NO_TIMER;
        timers[i].callback = NULL;
    }
    #ifdef WINDOWS
    if (!winmm_hnd)
    {
        winmm_hnd = LoadLibrary ( "WinMM.dll" );
        if ( INVALID_LIB_HANDLE == winmm_hnd ) return ERPLT_NOT_FOUND;
        winStopTimer = (t_StopTimer)GetProcAddress ( winmm_hnd , "timeKillEvent" );
        winStartTimer = (t_StartTimer)GetProcAddress ( winmm_hnd , "timeSetEvent" );
    }

    #endif
    timers_inited = 1;
    return 0;
}

void platform_free_timer ( tmr_handle hnd )
{
    if ( timers[hnd].hnd == 0 ) return;
    #ifdef LINUX
    //requires -lrt the posix realtime extension library
    timer_delete( timers[hnd].hnd );
//  sigemptyset(&sigmask);
//  sigsuspend(&sigmask);
    #endif
    #ifdef WINDOWS
    winStopTimer( timers[hnd].hnd );
    #endif
    timers[hnd].hnd = 0;
    return;
}

tmr_handle platform_new_timer ( int msec , tmr_callback cllbck , int user )
{
    if ( NULL == cllbck ) return ERPLT_INVAL_PARAM;
    int i,idx;
    if ( !timers_inited ) platform_init_timers();
    idx = -1;
    for ( i=0 ; i<TIMER_COUNT ; i++ )
    {
        if ( timers[i].hnd == NO_TIMER )
        {
            idx = i;
            break;
        }
    }
    if ( idx == -1 )
    {
        return ERPLT_OUT_OF_RES;
    }
    #ifdef LINUX
    struct sigaction act;
    struct sigevent evp;
    struct itimerspec ts;
    sigemptyset(&act.sa_mask);
    act.sa_flags=SA_SIGINFO;
    act.sa_sigaction=_go_timer;
    if ( sigaction ( USE_SIG + idx , &act , NULL ) < 0 ) return ERPLT_SYS_CALL;
    evp.sigev_notify = SIGEV_SIGNAL;
    evp.sigev_signo =  USE_SIG + idx;
    evp.sigev_value.sival_ptr=(void*) &timers[idx].hnd;
    //requires -lrt the posix realtime extension library
    i = timer_create( CLOCK_REALTIME, &evp, &timers[idx].hnd );
    if ( i )
    {
        platform_free_timer ( idx );
        return ERPLT_SYS_CALL;
    }
    ts.it_interval.tv_sec=0;
    while ( msec > 999 )
    {
        ts.it_interval.tv_sec++;
        msec = msec - 1000;
    }
    ts.it_interval.tv_nsec = msec * 1000000;
    ts.it_value.tv_nsec=1000;
    ts.it_value.tv_sec=0;
    //requires -lrt the posix realtime extension library
    i = timer_settime ( timers[idx].hnd , 0 , &ts , NULL );
    if ( i )
    {
        platform_free_timer ( idx );
        return ERPLT_SYS_CALL;
    }
    #endif
    #ifdef WINDOWS
    i = winStartTimer( msec , 1 , _go_timer , idx , TIME_PERIODIC );
    if ( i == 0 ) return ERPLT_SYS_CALL;
    timers[idx].hnd = i;
    #endif
    timers[idx].callback = cllbck;
    timers[idx].count = 0;
    timers[idx].user = user;
    return idx;
}

void platform_term_timers ( void )
{
    int i;
    for ( i = 0 ; i < TIMER_COUNT ; i++ )
    {
        if ( timers[i].hnd != NO_TIMER )
        {
            platform_free_timer ( i );
        }
    }
    return;
    #ifdef WINDOWS
    if ( 0 != winmm_hnd )
    {
        FreeLibrary( winmm_hnd );
        winmm_hnd = 0;
    }
    #endif
    return;
}
#endif


#
#if defined(WIN32) || defined(WINDOWS)
#
void show_message ( char * msg , char * capt )
{
    MessageBox( 0 , msg , capt , MB_OK );
}
#else
void show_message ( char * msg , char * capt )
{
    char cmd[1024];
    sprintf ( cmd , "xmessage -center \"%s\" %s", msg , capt );
    if ( fork ( ) == 0 )
    {
        close ( 1 );
        close ( 2 );
        int ret = system ( cmd );
        exit ( ret );
    }
}
#endif


//void show_message ( char * msg , char * capt )
//{
//    #ifdef WINDOWS
//    MessageBox(0,msg,capt,MB_OK);
//    #endif
//    #ifdef LINUX
//    sdfasdfasdfadsf
//    #endif
//    return;
//}

// General Windoze GetLastError error message display function
//void WinError ( void )
//{
//	DWORD Err = GetLastError();
//	if ( Err == 0 ) return;
//	LPVOID lpMsgBuf;
//	FormatMessage(
//		FORMAT_MESSAGE_ALLOCATE_BUFFER |
//		FORMAT_MESSAGE_FROM_SYSTEM |
//		FORMAT_MESSAGE_IGNORE_INSERTS,
//		NULL,
//		Err,
//		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
//		(LPTSTR) &lpMsgBuf,
//		0,
//		NULL );
//	MessageBox( 0 , (LPCTSTR)lpMsgBuf, "Windows Error", MB_OK | MB_ICONINFORMATION );
//	LocalFree( lpMsgBuf );
//}


//typedef struct timestamp * ptimestamp;
//struct  timestamp
//{
//    unsigned long time;      // Number of milliseconds since midnight
//    unsigned long date;      // One plus number of days since 1/1/0001
//};

//void datetime_to_timestamp ( datetime value , ptimestamp stamp )
//{
//  double dbl = frac ( value ) ;
//  dbl = abs ( dbl );
//  dbl = dbl * MSecsPerDay ;
//  stamp->time = round ( dbl );
//  stamp->date = DateDelta + trunc ( value );
//}

//void divmod ( int dividend , int divisor , int * result , int * remainder )
//{
//    double res = dividend / divisor;
//    *result = floor ( res );
//    *remainder = dividend % divisor;
//    return result;
//}

//datetime encode_time ( int hour , int min , int sec , int msec )
//{
//    double res = 0.0;
//    if ( ( hour<HoursPerDay ) && ( min<MinsPerHour ) && ( sec<SecsPerMin ) && ( msec<MSecsPerSec ) )
//    {
//        res = hour*MinsPerHour * SecsPerMin * MSecsPerSec;
//        res = res  + min * SecsPerMin * MSecsPerSec;
//        res = res  + sec * MSecsPerSec;
//        res = res  + msec;
//        res = res  / MSecsPerDay ;
//    }
//    return res;
//}


//void decode_time( datetime value , int *hour , int *min , int *sec , int *msec )
//{
//    int min_count , msec_count;
//    struct timestamp stamp;
//    datetime_to_timestamp ( value , &stamp );
//    int time = stamp.time;
//    divmod( time , SecsPerMin * MSecsPerSec , min_count , msec_count );
//    divmod( min_count , MinsPerHour , hour , min);
//    divmod( msec_count , MSecsPerSec , sec , msec);
//}

int platform_init ( void )
{
    #ifdef USE_TIMERS
        platform_init_timers ( );
    #endif
    reset_time_stamp ( );
    #ifdef USE_SOCKETS
        #ifdef WINDOWS
            WSADATA Windoze;
            int res = WSAStartup(0x0202,&Windoze);
            if ( res ) return -1;
        #endif
        #ifdef LINUX
        #endif
    #endif
    if ( str ) free ( str );
    if ( str_size < 1024 )
    {
        str_size = 1024;
        str = (char *)malloc ( str_size );
    }
    return 0;
}

void platform_term ( void )
{
    #ifdef WINDOWS
        #ifdef USE_SOCKET
            WSACleanup ( );
        #endif
    #endif
    #ifdef USE_TIMERS
        platform_term_timers ( );
    #endif
    str_size = 0;
    free ( str );
    str = NULL;
}

