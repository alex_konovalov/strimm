#ifndef _MMOPTOSPIN_H_
#define _MMOPTOSPIN_H_

#include "MMDevice.h"
#include "DeviceBase.h"
#include "ImgBuffer.h"
#include <string>
#include <map>
#include <list>
#include "optospin.h"

struct wheel_group_modes {
	int num_of_wheels;
	int mode;
	unsigned int rtr_mode;
	std::string mode_name;
};
std::vector<wheel_group_modes> wheelGroupModes;
class COptoSpinHub;
//class COptoSpinController;
class COptoSpinWheel : public CStateDeviceBase<COptoSpinWheel>
{
public:
	COptoSpinWheel(int index);
	~COptoSpinWheel();
	bool is_spinning_;
	int myPosition;
	int Initialize();
	int Shutdown();
	void GetName(char* pszName) const;
	bool Busy();
	unsigned long GetNumberOfPositions()const {return myPosCount;}
	int OnMode(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnState(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnLocked(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnSpins(MM::PropertyBase* pProp, MM::ActionType eAct);
	
	
private:
	
	std::string mySerial;
	std::string myModeName;
	t_rtr_hnd WheelHandle;
	long numPos_;
    bool busy_;
    bool initialized_;
	
	bool would_spin_;
	t_whl_pos myPosCount;
    MM::MMTime changedTime_;
	int myMode;
	int myIndex;
	int myArrayPos;
	std::string myName;
	bool haveArrayPos;
	
	
};

//class COptoSpinController : public HubBase<COptoSpinController>
//{
//
//}
class COptoSpinHub : public HubBase<COptoSpinHub>
{
public:
	COptoSpinHub();
	~COptoSpinHub();

	t_rtr_hnd HubHandle;

	int Initialize();
	int Shutdown() ;
	void GetName(char* pName) const; 
	bool Busy()  ;
	std::vector<std::string> modeNames;
	std::vector<COptoSpinWheel*> myAttachedWheels;
	 // HUB api
	int DetectInstalledDevices();
	int OnUnit(MM::PropertyBase* pProp, MM::ActionType eAct);
	//int OnTest(MM::PropertyBase* pProp, MM::ActionType eAct);
	//int OnMode(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
	//int OnAsciiString(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnSpin(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnAllPositions(MM::PropertyBase* pProp, MM::ActionType eAct);
	//int OnTest(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnHertz(MM::PropertyBase* pProp, MM::ActionType eAct);
	//int OnMicroSeconds(MM::PropertyBase* pProp, MM::ActionType eAct);
private:
	t_whl_pos *fposn;
	t_whl_idx max_wheel_count;
	void GetPeripheralInventory();
	bool spinning_;
	bool busy_;
    bool initialized_;
	bool library_open_;
	std::string mySerial;
	int myWheelCount;
	std::string all_positions_;
	float spin_speed;
	std::vector<std::string> availablerotors_;
    std::vector<std::string> peripherals_;
};
#endif