package uk.co.strimm.streams

import akka.NotUsed
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.stream.*
import akka.stream.javadsl.*
import akka.stream.javadsl.Flow
import akka.stream.javadsl.Sink
import akka.stream.javadsl.Source
import net.imagej.overlay.EllipseOverlay
import uk.co.strimm.*
import uk.co.strimm.MicroManager.MMCameraDevice
import uk.co.strimm.actors.CameraDataStoreActor
import uk.co.strimm.actors.TraceDataStoreActor
import uk.co.strimm.actors.messages.complete.CompleteCameraDataStoring
import uk.co.strimm.actors.messages.complete.CompleteCameraStreaming
import uk.co.strimm.actors.messages.complete.CompleteStreamingTraceROI
import uk.co.strimm.actors.messages.complete.CompleteTraceDataStoring
import uk.co.strimm.actors.messages.fail.FailCameraDataStoring
import uk.co.strimm.actors.messages.fail.FailCameraStreaming
import uk.co.strimm.actors.messages.fail.FailStreamingTraceROI
import uk.co.strimm.actors.messages.fail.FailTraceDataStoring
import uk.co.strimm.experiment.Flow as StrimmFlow
import uk.co.strimm.experiment.Source as StrimmSource
import uk.co.strimm.experiment.Sink as StrimmSink
import uk.co.strimm.gui.GUIMain
import java.time.Duration
import java.util.*
import java.util.logging.Level
import uk.co.strimm.STRIMMImage
import uk.co.strimm.actors.messages.start.*
import uk.co.strimm.actors.messages.stop.TerminateActor
import uk.co.strimm.actors.messages.tell.TellAnalogueDataStream
import uk.co.strimm.actors.messages.tell.TellDeviceSamplingRate
import uk.co.strimm.actors.messages.tell.TellSetNumDataPoints
import uk.co.strimm.experiment.*
import uk.co.strimm.gui.CameraWindowPlugin
import uk.co.strimm.gui.TraceWindowPlugin
import uk.co.strimm.plugins.DockableWindowPlugin
import uk.co.strimm.services.AcquisitionMethodService
import kotlin.collections.ArrayList

class ExperimentStream(val expConfig: ExperimentConfiguration, loadCameraConfig : Boolean){
    var experimentImageSources = arrayListOf<ExperimentImageSource>()
    var experimentTraceSources = arrayListOf<ExperimentTraceSource>()
    var experimentImageImageFlows = arrayListOf<ExperimentImageImageFlow>()
    var experimentImageTraceFlows = arrayListOf<ExperimentImageTraceFlow>()
    var experimentTraceImageFlows = arrayListOf<ExperimentTraceImageFlow>()
    var experimentTraceTraceFlows = arrayListOf<ExperimentTraceTraceFlow>()
    var experimentImageSinks = arrayListOf<ExperimentImageSink>()
    var experimentTraceSinks = arrayListOf<ExperimentTraceSink>()
    var cameraActors = hashMapOf<ActorRef, MMCameraDevice>()
    var traceActors = hashMapOf<ActorRef, String>() //ActorRef, DeviceName//TODO use a TraceDevice class when it exists
    val newTraceROIActors = hashMapOf<ActorRef, String>()
    var cameraDataStoreActors = hashMapOf<ActorRef, String>()
    var traceDataStoreActors = hashMapOf<ActorRef, String>()
    var numConnectionsSpecified = 0
    var numConnectionsMade = 0
    var experimentName = expConfig.experimentConfigurationName

    var cameraDevices = arrayListOf<CameraDeviceInfo>()

    var stream : RunnableGraph<NotUsed>? = null
    var durationMs = 0

    init {
        loadMMCameras(loadCameraConfig)
    }

    fun loadMMCameras(loadCameraConfig : Boolean){
        if(loadCameraConfig) {
            //Load the device configuration first
            GUIMain.mmService.loadConfigurationFile(expConfig.MMDeviceConfigFile)
        }

        //Make a note of the camera devices that will be used
        populateListOfCameraDevices(expConfig)

        for(cameraDevice in cameraDevices){
            initialiseCamera(cameraDevice)
        }
    }

    private fun createStreamGraph(expConfig : ExperimentConfiguration) : Graph<ClosedShape, NotUsed> {
        try {
            return (GraphDSL.create { builder ->
                configureTimer()

                durationMs = expConfig.experimentDurationMs

                //Build the graph objects from the experiment config
                populateLists(expConfig, builder)

                //Find out what is going where
                experimentImageSources.forEach { x -> x.outs = getOutlets(x, expConfig) }
                experimentTraceSources.forEach { x -> x.outs = getOutlets(x, expConfig) }

                experimentImageImageFlows.forEach { x -> x.ins = getInlets(x, expConfig) }
                experimentImageImageFlows.forEach { x -> x.outs = getOutlets(x, expConfig) }

                experimentImageTraceFlows.forEach { x -> x.ins = getInlets(x, expConfig) }
                experimentImageTraceFlows.forEach { x -> x.outs = getOutlets(x, expConfig) }

                experimentTraceImageFlows.forEach { x -> x.ins = getInlets(x, expConfig) }
                experimentTraceImageFlows.forEach { x -> x.outs = getOutlets(x, expConfig) }

                experimentTraceTraceFlows.forEach { x -> x.ins = getInlets(x, expConfig) }
                experimentTraceTraceFlows.forEach { x -> x.outs = getOutlets(x, expConfig) }

                //Ins for sinks aren't current used in the logic in buildGraph() (they're covered by the outs in flows)
                // but its still useful to have them
                experimentImageSinks.forEach { x -> x.ins = getInlets(x, expConfig) }
                experimentTraceSinks.forEach { x -> x.ins = getInlets(x, expConfig) }

                //Create broadcast objects if more than one outlet from node
                setBroadcastObjectForSources(builder)
                setBroadcastObjectForFlows(builder)
                setMergeObjectForSinks(builder)
                setMergeObjectForFlows(builder)

                //Remember, the graph has to be closed (everything connected) before runtime
                buildGraph(builder)

                //Print out some details to log
                logInfo()

                GUIMain.loggerService.log(Level.INFO, "Finished building graph")
                checkNumberOfConnections()

                //TODO should we also validate what connections have gone to where (in addition to checking the number)?
                ClosedShape.getInstance()
            })
        }
        catch(ex: Exception){
            throw ex
        }
    }

    fun configureTimer(){
        GUIMain.timerService.REMOVE_ME()
//        GUIMain.loggerService.log(Level.INFO, "Configuring timer")
//        val timer = GUIMain.timerService.createTimer(GUIMain.timerService.getAvailableTimers()!![0], expConfig.hardwareDevices.preinitProperties.deviceName)!!
//        val devProp = timer.timer.getProperty("Device Name")!! as StringTimerProperty
//        devProp.value = devProp.getAllowedValues()[0]
//
//        when (timer.timer.initialise()) {
//            TimerResult.Success, TimerResult.Warning -> {
//                for(traceSource in experimentTraceSources){
//                    if(traceSource.channelName != ""){
//                        val channel = GUIMain.experimentService.getChannelForSource(traceSource.channelName)
//                        when(channel!!.type){
//                            ExperimentConstants.ConfigurationProperties.ANALOGUE_IN_TYPE -> {
//                                val aiChannelDesc = AIChannelDesc(ChannelName(channel.channelName),channel.clockDiv,channel.voltageMax,channel.voltageMin)
//                                val ads = AnalogueDataStream(timer.timer, aiChannelDesc)
//                                GUIMain.timerService.analogueDataStreams.add(ads)
//                            }
//                            ExperimentConstants.ConfigurationProperties.DIGITAL_OUT_TYPE -> {
//                                timer.timer.addDigitalOutput(ChannelName(channel.channelName),channel.clockDiv,ByteArray(1000) {i -> (i % 100 == 0).toByte() })
//                            }
//                            ExperimentConstants.ConfigurationProperties.ANALOGUE_OUT_TYPE -> {
//                                //TODO hardcoded
//                                timer.timer.addAnalogueOutput(timer.timer.getAvailableChannels(ChannelType.AnalogueOut)[0].apply { println(name) },
//                                        1, DoubleArray(1000) { i -> 100*sin(i.toDouble()) })
//                            }
//                            ExperimentConstants.ConfigurationProperties.DIGITAL_IN_TYPE -> {
//                                //TODO
//                            }
//                        }
//                    }
//                }
//            }
//            TimerResult.Error -> { println(timer.timer.getLastError()) }
//        }
    }

    fun createStream(expConfig : ExperimentConfiguration) : RunnableGraph<NotUsed>?{
        try{
            setNumberOfSpecifiedConnections(expConfig)
            val graph = createStreamGraph(expConfig)
            val streamGraph = RunnableGraph.fromGraph(graph)
            stream = streamGraph
            return stream
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error building stream graph. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }

        return null
    }

    /**
     * Tell all camera devices to start acquisition (either preview or live), then begin the stream that has been
     * created
     * @param liveAcquisition Flag if preview mode or live acquisition mode should be used
     */
    fun runStream(liveAcquisition : Boolean){
        if(liveAcquisition) {
            cameraDevices.forEach { x -> x.device.startLiveAcquisition(x.intervalMillis) }
            startStoringData()
        }
        else{
            cameraDevices.forEach { x -> x.device.startLivePreview(x.intervalMillis) }
        }

//        if (GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.start() == TimerResult.Error)
//            println(GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.getLastError())
        stream?.run(GUIMain.actorService.materializer)
    }

    /**
     * Log information about the number of connections specified and the number of connections actually made
     */
    private fun checkNumberOfConnections(){
        if(numConnectionsMade == numConnectionsSpecified){
            GUIMain.loggerService.log(Level.INFO, "Number of connections specified is equal to number of connections made (this is good)." +
                    " Number of specified connections is $numConnectionsSpecified, number of connections made is $numConnectionsMade")
        }
        else{
            GUIMain.loggerService.log(Level.SEVERE, "Number of connections specified not equal to number of connections made. The running of the stream graph will likely fail." +
                    " Number of specified connections is $numConnectionsSpecified, number of connections made is $numConnectionsMade")
        }
    }

    /**
     * Call this method to get the number of connections that should be made for the stream graph. This is based on the
     * experiment config object. numConnectionsSpecified is then compared to numConnectionsMade to check the validity of
     * the created stream graph
     * @param expConfig The experiment configuration object created from the json specification
     */
    private fun setNumberOfSpecifiedConnections(expConfig: ExperimentConfiguration){
        numConnectionsSpecified += expConfig.flowConfig.flows.map { x -> x.inputNames.size }.sum()
        numConnectionsSpecified += expConfig.sinkConfig.sinks.map { x -> x.inputNames.size }.sum()
    }

    /**
     * Method to log a load of basic info about the experiment stream being created
     */
    private fun logInfo(){
        val sb = StringBuilder()
        sb.append("Experiment stream info:\n")
        sb.append("Number of image sources: ${experimentImageSources.size}\n")
        sb.append("Number of trace sources: ${experimentTraceSources.size}\n")
        sb.append("Number of image to image flows: ${experimentImageImageFlows.size}\n")
        sb.append("Number of image to trace flows ${experimentImageTraceFlows.size}\n")
        sb.append("Number of trace to image flows ${experimentTraceImageFlows.size}\n")
        sb.append("Number of trace to trace flows ${experimentTraceTraceFlows.size}\n")
        sb.append("Number of image sinks ${experimentImageSinks.size}\n")
        sb.append("Number of trace sinks: ${experimentTraceSinks.size}\n")

        sb.append("Image sources details:\n")
        experimentImageSources.forEach { x ->
            sb.append("Source name: ${x.imgSourceName}, ")
            sb.append("number of source outlets: ${x.outs.size}\n")
        }
        experimentTraceSources.forEach { x ->
            sb.append("Source name: ${x.traceSourceName}, ")
            sb.append("number of source outlets: ${x.outs.size}\n")
        }
        experimentImageImageFlows.forEach { x ->
            sb.append("Flow name: ${x.imgImgFlowName}, ")
            sb.append("number of flow inlets: ${x.ins.size}, ")
            sb.append("number of flow outlets: ${x.outs.size}, ")
            sb.append("number of fwdOps: ${x.fwdOps.size}\n")
        }
        experimentImageTraceFlows.forEach { x ->
            sb.append("Flow name: ${x.imgTraceFlowName}, ")
            sb.append("number of flow inlets: ${x.ins.size}, ")
            sb.append("number of flow outlets: ${x.outs.size}, ")
            sb.append("number of fwdOps: ${x.fwdOps.size}\n")
        }
        experimentTraceImageFlows.forEach { x ->
            sb.append("Flow name: ${x.traceImgFlowName}, ")
            sb.append("number of flow inlets: ${x.ins.size}, ")
            sb.append("number of flow outlets: ${x.outs.size}, ")
            sb.append("number of fwdOps: ${x.fwdOps.size}\n")
        }
        experimentTraceTraceFlows.forEach { x ->
            sb.append("Flow name: ${x.traceTraceFlowName}, ")
            sb.append("number of flow inlets: ${x.ins.size}, ")
            sb.append("number of flow outlets: ${x.outs.size}, ")
            sb.append("number of fwdOps: ${x.fwdOps.size}\n")
        }
        experimentImageSinks.forEach { x ->
            sb.append("Sink name: ${x.imageSinkName}, ")
            sb.append("number of sink inlets: ${x.ins.size}\n")
        }
        experimentTraceSinks.forEach { x ->
            sb.append("Sink name: ${x.traceSinkName}, ")
            sb.append("number of sink inlets: ${x.ins.size}\n")
        }

        GUIMain.loggerService.log(Level.INFO,sb.toString())
    }

    //region Graph building
    /**
     * build the graph based on sources, flows and sinks as per the Akka API specifications
     * @param builder The graph building object
     */
    private fun buildGraph(builder : GraphDSL.Builder<NotUsed>){
        buildSourceGraphParts(builder)
        buildFlowGraphParts(builder)
    }

    /**
     * This method calls all source graph building methods
     * @param builder The graph building object
     */
    private fun buildSourceGraphParts(builder : GraphDSL.Builder<NotUsed>){
        GUIMain.loggerService.log(Level.INFO, "Building source parts")
        buildImageSourceParts(builder)
        buildTraceSourceParts(builder)
    }

    /**
     * This method calls all flow graph building methods
     * @param builder The graph building object
     */
    private fun buildFlowGraphParts(builder : GraphDSL.Builder<NotUsed>){
        GUIMain.loggerService.log(Level.INFO, "Building flow parts")
        buildImageImageFlowParts(builder)
        buildImageTraceFlowParts(builder)
        buildTraceImageFlowParts(builder)
        buildTraceTraceFlowParts(builder)
    }

    /**
     * Specify the graph connections for image sources. Note that the partial connections (forward ops) are stored
     * with the outlet node (flow or sink) and not with the source
     * @param builder The graph building object
     */
    private fun buildImageSourceParts(builder : GraphDSL.Builder<NotUsed>){
        try {
            for (imageSource in experimentImageSources) {
                if (imageSource.bcastObject != null) {
                    for (outlet in imageSource.outs) { //Go through each outlet of the source
                        when (outlet) {
                            is ExperimentImageImageFlow -> { //Join to a flow (created a ForwardOps object)
                                if (outlet.mergeObject != null) {
                                    val mergeFwdOp = ExperimentImageFwdOp(imageSource.imgSourceName, outlet.imgImgFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} (broadcast) viaFanIn ${outlet.imgImgFlowName} (merge)")
                                    mergeFwdOp.fwdOp = builder.from(imageSource.bcastObject).viaFanIn(outlet.mergeObject)
                                    outlet.mergeFwdOp = mergeFwdOp
                                    numConnectionsMade++
                                } else {
                                    val fwdOp = ExperimentImageFwdOp(imageSource.imgSourceName, outlet.imgImgFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} (broadcast) via ${outlet.imgImgFlowName}")
                                    fwdOp.fwdOp = builder.from(imageSource.bcastObject).via(outlet.flow)
                                    outlet.fwdOps.add(fwdOp)
                                    numConnectionsMade++
                                }
                            }
                            is ExperimentImageTraceFlow -> {
                                if (outlet.mergeObject != null) {
                                    val mergeFwdOp = ExperimentImageFwdOp(imageSource.imgSourceName, outlet.imgTraceFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} (broadcast) viaFanIn ${outlet.imgTraceFlowName} (merge)")
                                    mergeFwdOp.fwdOp = builder.from(imageSource.bcastObject).viaFanIn(outlet.mergeObject)
                                    outlet.mergeFwdOp = mergeFwdOp
                                    numConnectionsMade++
                                } else {
                                    val fwdOp = ExperimentTraceFwdOp(imageSource.imgSourceName, outlet.imgTraceFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} (broadcast) via ${outlet.imgTraceFlowName}")
                                    fwdOp.fwdOp = builder.from(imageSource.bcastObject).via(outlet.flow)
                                    outlet.fwdOps.add(fwdOp)
                                    numConnectionsMade++
                                }
                            }
                            is ExperimentTraceImageFlow ->{
                                throw Exception("image source ${imageSource.imgSourceName} is connected to a trace-to-image flow. The flow input type must be the same as the source output type")
                            }
                            is ExperimentTraceTraceFlow ->{
                                throw Exception("image source ${imageSource.imgSourceName} is connected to a trace-to-trace flow. The flow input type must be the same as the source output type")
                            }
                            is ExperimentImageSink -> { //Join to a sink
                                builder.from(imageSource.bcastObject).to(outlet.sink)
                                if (outlet.mergeObject != null) {
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} (broadcast) toFanIn ${outlet.imageSinkName} (merge)")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(imageSource.bcastObject).via(bufferFlow).toFanIn(outlet.mergeObject)
                                    }
                                    else{
                                        builder.from(imageSource.bcastObject).toFanIn(outlet.mergeObject)
                                    }

                                    numConnectionsMade++
                                } else {
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} (broadcast) to ${outlet.imageSinkName}")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))

                                        builder.from(imageSource.bcastObject).via(bufferFlow).to(outlet.sink)
                                    }
                                    else{
                                        builder.from(imageSource.bcastObject).to(outlet.sink)
                                    }

                                    numConnectionsMade++
                                }
                            }
                        }
                    }
                } else {
                    for (outlet in imageSource.outs) { //Go through each outlet of the source
                        when (outlet) {
                            is ExperimentImageImageFlow -> { //Join to a flow (created a ForwardOps object)
                                if(outlet.mergeObject != null){
                                    val fwdOp = ExperimentImageFwdOp(imageSource.imgSourceName, outlet.imgImgFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} viaFanIn ${outlet.imgImgFlowName} (merge)")
                                    fwdOp.fwdOp = builder.from(imageSource.source).viaFanIn(outlet.mergeObject)
                                    outlet.mergeFwdOp = fwdOp
                                    numConnectionsMade++
                                }
                                else {
                                    val fwdOp = ExperimentImageFwdOp(imageSource.imgSourceName, outlet.imgImgFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} via ${outlet.imgImgFlowName}")
                                    fwdOp.fwdOp = builder.from(imageSource.source).via(outlet.flow)
                                    outlet.fwdOps.add(fwdOp)
                                    numConnectionsMade++
                                }
                            }
                            is ExperimentImageTraceFlow -> {
                                if (outlet.mergeObject != null) {
                                    val mergeFwdOp = ExperimentImageFwdOp(imageSource.imgSourceName, outlet.imgTraceFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} viaFanIn ${outlet.imgTraceFlowName} (merge)")
                                    mergeFwdOp.fwdOp = builder.from(imageSource.source).viaFanIn(outlet.mergeObject)
                                    outlet.mergeFwdOp = mergeFwdOp
                                    numConnectionsMade++
                                } else {
                                    val fwdOp = ExperimentTraceFwdOp(imageSource.imgSourceName, outlet.imgTraceFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} via ${outlet.imgTraceFlowName}")
                                    fwdOp.fwdOp = builder.from(imageSource.source).via(outlet.flow)
                                    outlet.fwdOps.add(fwdOp)
                                    numConnectionsMade++
                                }
                            }
                            is ExperimentTraceImageFlow ->{
                                throw Exception("image source ${imageSource.imgSourceName} is connected to a trace-to-image flow. The flow input type must be the same as the source output type")
                            }
                            is ExperimentTraceTraceFlow ->{
                                throw Exception("image source ${imageSource.imgSourceName} is connected to a trace-to-trace flow. The flow input type must be the same as the source output type")
                            }
                            is ExperimentImageSink -> {
                                if (outlet.mergeObject != null) {
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} toFanIn ${outlet.imageSinkName} (merge)")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(imageSource.source).via(bufferFlow).toFanIn(outlet.mergeObject)
                                    }
                                    else{
                                        builder.from(imageSource.source).toFanIn(outlet.mergeObject)
                                    }

                                    numConnectionsMade++
                                } else {
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imageSource.imgSourceName} to ${outlet.imageSinkName}")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(imageSource.source).via(bufferFlow).to(outlet.sink)
                                    }
                                    else{
                                        builder.from(imageSource.source).to(outlet.sink)
                                    }


                                    numConnectionsMade++
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error building connections for image sources. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * Specify the graph connections for trace sources. Note that the partial connections (forward ops) are stored
     * with the outlet node (flow or sink) and not with the source
     * @param builder The graph building object
     */
    private fun buildTraceSourceParts(builder: GraphDSL.Builder<NotUsed>){
        try {
            for (traceSource in experimentTraceSources) {
                if (traceSource.bcastObject != null) {
                    builder.from(traceSource.source).viaFanOut(traceSource.bcastObject)
                    for (outlet in traceSource.outs) {
                        when (outlet) {
                            is ExperimentTraceImageFlow -> {
                                if (outlet.mergeObject != null) {
                                    val mergeFwdOp = ExperimentTraceFwdOp(traceSource.traceSourceName, outlet.traceImgFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} (broadcast) viaFanIn ${outlet.traceImgFlowName} (merge)")
                                    mergeFwdOp.fwdOp = builder.from(traceSource.bcastObject).viaFanIn(outlet.mergeObject)
                                    outlet.mergeFwdOp = mergeFwdOp
                                    numConnectionsMade++
                                } else {
                                    val mergeFwdOp = ExperimentImageFwdOp(traceSource.traceSourceName, outlet.traceImgFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} (broadcast) via ${outlet.traceImgFlowName}")
                                    mergeFwdOp.fwdOp = builder.from(traceSource.bcastObject).via(outlet.flow)
                                    outlet.fwdOps.add(mergeFwdOp)
                                    numConnectionsMade++
                                }
                            }
                            is ExperimentTraceTraceFlow -> {
                                if (outlet.mergeObject != null) {
                                    val mergeFwdOp = ExperimentTraceFwdOp(traceSource.traceSourceName, outlet.traceTraceFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} (broadcast) viaFanIn ${outlet.traceTraceFlowName} (merge)")
                                    mergeFwdOp.fwdOp = builder.from(traceSource.bcastObject).viaFanIn(outlet.mergeObject)
                                    outlet.mergeFwdOp = mergeFwdOp
                                    numConnectionsMade++
                                } else {
                                    val mergeFwdOp = ExperimentTraceFwdOp(traceSource.traceSourceName, outlet.traceTraceFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} (broadcast) via ${outlet.traceTraceFlowName}")
                                    mergeFwdOp.fwdOp = builder.from(traceSource.bcastObject).via(outlet.flow)
                                    outlet.fwdOps.add(mergeFwdOp)
                                    numConnectionsMade++
                                }
                            }
                            is ExperimentImageTraceFlow ->{
                                throw Exception("trace source ${traceSource.traceSourceName} is connected to a image-to-trace flow. The flow input type must be the same as the source output type")
                            }
                            is ExperimentImageImageFlow ->{
                                throw Exception("trace source ${traceSource.traceSourceName} is connected to a image-to-image flow. The flow input type must be the same as the source output type")
                            }
                            is ExperimentTraceSink -> {
                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} (broadcast) toFanIn ${outlet.traceSinkName} (merge)")
                                    builder.from(traceSource.bcastObject).toFanIn(outlet.mergeObject)
                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} (broadcast) to ${outlet.traceSinkName}")
                                    builder.from(traceSource.bcastObject).to(outlet.sink)
                                    numConnectionsMade++
                                }
                            }
                        }
                    }
                } else {
                    for (outlet in traceSource.outs) { //Go through each outlet of the source
                        when (outlet) {
                            is ExperimentTraceImageFlow -> {
                                if (outlet.mergeObject != null) {
                                    val mergeFwdOp = ExperimentTraceFwdOp(traceSource.traceSourceName, outlet.traceImgFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} viaFanIn ${outlet.traceImgFlowName} (merge)")
                                    mergeFwdOp.fwdOp = builder.from(traceSource.source).viaFanIn(outlet.mergeObject)
                                    outlet.mergeFwdOp = mergeFwdOp
                                    numConnectionsMade++
                                } else {
                                    val mergeFwdOp = ExperimentImageFwdOp(traceSource.traceSourceName, outlet.traceImgFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} via ${outlet.traceImgFlowName}")
                                    mergeFwdOp.fwdOp = builder.from(traceSource.source).via(outlet.flow)
                                    outlet.fwdOps.add(mergeFwdOp)
                                    numConnectionsMade++
                                }
                            }
                            is ExperimentTraceTraceFlow -> {
                                if (outlet.mergeObject != null) {
                                    val mergeFwdOp = ExperimentTraceFwdOp(traceSource.traceSourceName, outlet.traceTraceFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} viaFanIn ${outlet.traceTraceFlowName} (merge)")
                                    mergeFwdOp.fwdOp = builder.from(traceSource.source).viaFanIn(outlet.mergeObject)
                                    outlet.mergeFwdOp = mergeFwdOp
                                    numConnectionsMade++
                                } else {
                                    val mergeFwdOp = ExperimentTraceFwdOp(traceSource.traceSourceName, outlet.traceTraceFlowName)
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} via ${outlet.traceTraceFlowName}")
                                    mergeFwdOp.fwdOp = builder.from(traceSource.source).via(outlet.flow)
                                    outlet.fwdOps.add(mergeFwdOp)
                                    numConnectionsMade++
                                }
                            }
                            is ExperimentImageTraceFlow ->{
                                throw Exception("trace source ${traceSource.traceSourceName} is connected to a image-to-trace flow. The flow input type must be the same as the source output type")
                            }
                            is ExperimentImageImageFlow ->{
                                throw Exception("trace source ${traceSource.traceSourceName} is connected to a image-to-image flow. The flow input type must be the same as the source output type")
                            }
                            is ExperimentTraceSink -> {
                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} toFanIn ${outlet.traceSinkName} (merge)")
                                    builder.from(traceSource.source).toFanIn(outlet.mergeObject)
                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceSource.traceSourceName} to ${outlet.traceSinkName}")
                                    builder.from(traceSource.source).to(outlet.sink)
                                    numConnectionsMade++
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error building connections for trace sources. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * Specify the graph connections for trace-to-image flows
     * @param builder The graph building object
     */
    private fun buildTraceImageFlowParts(builder: GraphDSL.Builder<NotUsed>){
        try {
            for (traceImgFlow in experimentTraceImageFlows) {
                if (traceImgFlow.bcastObject != null) {
                    for (outlet in traceImgFlow.outs) {
                        when (outlet) {
                            is ExperimentImageTraceFlow -> {
                                createBroadcastConnection(traceImgFlow, outlet, builder)
                            }
                            is ExperimentImageImageFlow -> {
                                createBroadcastConnection(traceImgFlow, outlet, builder)
                            }
                            is ExperimentTraceImageFlow ->{
                                throw Exception("trace-to-image flow ${traceImgFlow.traceImgFlowName} is connected to a trace-to-image flow. The second flow input type must be the same as the flow output type")
                            }
                            is ExperimentTraceTraceFlow ->{
                                throw Exception("trace-to-image flow ${traceImgFlow.traceImgFlowName} is connected to a trace-to-trace flow. The second flow input type must be the same as the source output type")
                            }
                            is ExperimentImageSink -> {
                                //Add a new fwd op for the connection from the flow to the sink
                                //However a connection from a flow to a sink is terminal so we won't actually create
                                //the fwdOp object. We only create an ExperimentFwdOp object to keep a record of the
                                //connection
                                val newFwdOp = ExperimentImageFwdOp(traceImgFlow.traceImgFlowName, outlet.imageSinkName)

                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} (broadcast) toFanIn ${outlet.imageSinkName} (merge)")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(traceImgFlow.bcastObject).via(bufferFlow).toFanIn(outlet.mergeObject)
                                    }
                                    else{
                                        builder.from(traceImgFlow.bcastObject).toFanIn(outlet.mergeObject)
                                    }

                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} (broadcast) to ${outlet.imageSinkName}")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(traceImgFlow.bcastObject).via(bufferFlow).to(outlet.sink)
                                    }
                                    else{
                                        builder.from(traceImgFlow.bcastObject).to(outlet.sink)
                                    }

                                    builder.from(traceImgFlow.bcastObject).to(outlet.sink)
                                    numConnectionsMade++
                                }

                                traceImgFlow.fwdOps.add(newFwdOp)
                            }
                        }
                    }
                } else {
                    for (outlet in traceImgFlow.outs) {
                        when (outlet) {
                            is ExperimentImageTraceFlow -> {
                                createConnection(traceImgFlow, outlet, builder)
                            }
                            is ExperimentImageImageFlow -> {
                                createConnection(traceImgFlow, outlet, builder)
                            }
                            is ExperimentTraceImageFlow ->{
                                throw Exception("trace-to-image flow ${traceImgFlow.traceImgFlowName} is connected to a trace-to-image flow. The second flow input type must be the same as the flow output type")
                            }
                            is ExperimentTraceTraceFlow ->{
                                throw Exception("trace-to-image flow ${traceImgFlow.traceImgFlowName} is connected to a trace-to-trace flow. The second flow input type must be the same as the source output type")
                            }
                            is ExperimentImageSink -> {
                                //Add a new fwd op for the connection from the flow to the sink
                                //However a connection from a flow to a sink is terminal so we won't actually create
                                //the fwdOp object. We only create an ExperimentFwdOp object to keep a record of the
                                //connection
                                val newFwdOp = ExperimentImageFwdOp(traceImgFlow.traceImgFlowName, outlet.imageSinkName)

                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} toFanIn ${outlet.imageSinkName} (merge)")
                                    builder.from(traceImgFlow.flow).toFanIn(outlet.mergeObject)
                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} to ${outlet.imageSinkName}")
                                    builder.from(traceImgFlow.flow).to(outlet.sink)
                                    numConnectionsMade++
                                }

                                traceImgFlow.fwdOps.add(newFwdOp)
                            }
                        }
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error building connections for trace to image flows. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * Specify the graph connections for trace-to-trace flows
     * @param builder The graph building object
     */
    private fun buildTraceTraceFlowParts(builder: GraphDSL.Builder<NotUsed>){
        try {
            for (traceTraceFlow in experimentTraceTraceFlows) {
                if (traceTraceFlow.bcastObject != null) {
                    for (outlet in traceTraceFlow.outs) {
                        when (outlet) {
                            is ExperimentTraceImageFlow -> {
                                createBroadcastConnection(traceTraceFlow, outlet, builder)
                            }
                            is ExperimentTraceTraceFlow -> {
                                createBroadcastConnection(traceTraceFlow, outlet, builder)
                            }
                            is ExperimentImageImageFlow ->{
                                throw Exception("trace-to-trace flow ${traceTraceFlow.traceTraceFlowName} is connected to a image-to-image flow. The second flow input type must be the same as the flow output type")
                            }
                            is ExperimentImageTraceFlow ->{
                                throw Exception("trace-to-trace flow ${traceTraceFlow.traceTraceFlowName} is connected to a image-to-trace flow. The second flow input type must be the same as the source output type")
                            }
                            is ExperimentTraceSink -> {
                                //Add a new fwd op for the connection from the flow to the sink
                                //However a connection from a flow to a sink is terminal so we won't actually create
                                //the fwdOp object. We only create an ExperimentFwdOp object to keep a record of the
                                //connection
                                val newFwdOp = ExperimentTraceFwdOp(traceTraceFlow.traceTraceFlowName, outlet.traceSinkName)

                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} (broadcast) toFanIn ${outlet.traceSinkName} (merge)")
                                    builder.from(traceTraceFlow.bcastObject).toFanIn(outlet.mergeObject)
                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} (broadcast) to ${outlet.traceSinkName}")
                                    builder.from(traceTraceFlow.bcastObject).to(outlet.sink)
                                    numConnectionsMade++
                                }

                                traceTraceFlow.fwdOps.add(newFwdOp)
                            }
                        }
                    }
                } else {
                    for (outlet in traceTraceFlow.outs) {
                        when (outlet) {
                            is ExperimentTraceImageFlow -> {
                                createConnection(traceTraceFlow, outlet, builder)
                            }
                            is ExperimentTraceTraceFlow -> {
                                createConnection(traceTraceFlow, outlet, builder)
                            }
                            is ExperimentImageImageFlow ->{
                                throw Exception("trace-to-trace flow ${traceTraceFlow.traceTraceFlowName} is connected to a image-to-image flow. The second flow input type must be the same as the flow output type")
                            }
                            is ExperimentImageTraceFlow ->{
                                throw Exception("trace-to-trace flow ${traceTraceFlow.traceTraceFlowName} is connected to a image-to-trace flow. The second flow input type must be the same as the source output type")
                            }
                            is ExperimentTraceSink -> {
                                //Add a new fwd op for the connection from the flow to the sink
                                //However a connection from a flow to a sink is terminal so we won't actually create
                                //the fwdOp object. We only create an ExperimentFwdOp object to keep a record of the
                                //connection
                                val newFwdOp = ExperimentTraceFwdOp(traceTraceFlow.traceTraceFlowName, outlet.traceSinkName)

                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} toFanIn ${outlet.traceSinkName} (merge)")
                                    builder.from(traceTraceFlow.flow).toFanIn(outlet.mergeObject)
                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} to ${outlet.traceSinkName}")
                                    builder.from(traceTraceFlow.flow).to(outlet.sink)
                                    numConnectionsMade++
                                }

                                traceTraceFlow.fwdOps.add(newFwdOp)
                            }
                        }
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error building connections for trace to trace flows. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * Specify the graph connections for image-to-trace flows
     * @param builder The graph building object
     */
    private fun buildImageTraceFlowParts(builder : GraphDSL.Builder<NotUsed>){
        try {
            for (imgTraceFlow in experimentImageTraceFlows) {
                if (imgTraceFlow.bcastObject != null) {
                    for (outlet in imgTraceFlow.outs) {
                        when (outlet) {
                            is ExperimentTraceImageFlow -> {
                                createBroadcastConnection(imgTraceFlow, outlet, builder)
                            }
                            is ExperimentTraceTraceFlow -> {
                                createBroadcastConnection(imgTraceFlow, outlet, builder)
                            }
                            is ExperimentImageImageFlow ->{
                                throw Exception("image-to-trace flow ${imgTraceFlow.imgTraceFlowName} is connected to a image-to-image flow. The second flow input type must be the same as the flow output type")
                            }
                            is ExperimentImageTraceFlow ->{
                                throw Exception("image-to-trace flow ${imgTraceFlow.imgTraceFlowName} is connected to a image-to-trace flow. The second flow input type must be the same as the source output type")
                            }
                            is ExperimentTraceSink -> {
                                //Add a new fwd op for the connection from the flow to the sink
                                //However a connection from a flow to a sink is terminal so we won't actually create
                                //the fwdOp object. We only create an ExperimentFwdOp object to keep a record of the
                                //connection
                                val newFwdOp = ExperimentTraceFwdOp(imgTraceFlow.imgTraceFlowName, outlet.traceSinkName)

                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} (broadcast) toFanIn ${outlet.traceSinkName} (merge)")
                                    builder.from(imgTraceFlow.bcastObject).toFanIn(outlet.mergeObject)
                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} (broadcast) to ${outlet.traceSinkName}")
                                    builder.from(imgTraceFlow.bcastObject).to(outlet.sink)
                                    numConnectionsMade++
                                }

                                imgTraceFlow.fwdOps.add(newFwdOp)
                            }
                        }
                    }
                } else {
                    for (outlet in imgTraceFlow.outs) {
                        when (outlet) {
                            is ExperimentTraceImageFlow -> {
                                createConnection(imgTraceFlow, outlet, builder)
                            }
                            is ExperimentTraceTraceFlow -> {
                                createConnection(imgTraceFlow, outlet, builder)
                            }
                            is ExperimentImageImageFlow ->{
                                throw Exception("image-to-trace flow ${imgTraceFlow.imgTraceFlowName} is connected to a image-to-image flow. The second flow input type must be the same as the flow output type")
                            }
                            is ExperimentImageTraceFlow ->{
                                throw Exception("image-to-trace flow ${imgTraceFlow.imgTraceFlowName} is connected to a image-to-trace flow. The second flow input type must be the same as the source output type")
                            }
                            is ExperimentTraceSink -> {
                                //Add a new fwd op for the connection from the flow to the sink
                                //However a connection from a flow to a sink is terminal so we won't actually create
                                //the fwdOp object. We only create an ExperimentFwdOp object to keep a record of the
                                //connection
                                val newFwdOp = ExperimentTraceFwdOp(imgTraceFlow.imgTraceFlowName, outlet.traceSinkName)

                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} toFanIn ${outlet.traceSinkName} (merge)")
                                    builder.from(imgTraceFlow.flow).toFanIn(outlet.mergeObject)
                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} to ${outlet.traceSinkName}")
                                    builder.from(imgTraceFlow.flow).to(outlet.sink)
                                    numConnectionsMade++
                                }

                                imgTraceFlow.fwdOps.add(newFwdOp)
                            }
                        }
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error building connections for image to trace flows. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * Specify the graph connections for image-to-image flows
     * @param builder The graph building object
     */
    private fun buildImageImageFlowParts(builder : GraphDSL.Builder<NotUsed>){
        try {
            for (imgImgFlow in experimentImageImageFlows) {
                if (imgImgFlow.bcastObject != null) {
                    for (outlet in imgImgFlow.outs) {
                        when (outlet) {
                            is ExperimentImageImageFlow -> {
                                createBroadcastConnection(imgImgFlow, outlet, builder)
                            }
                            is ExperimentImageTraceFlow -> {
                                createBroadcastConnection(imgImgFlow, outlet, builder)
                            }
                            is ExperimentTraceImageFlow ->{
                                throw Exception("image-to-image flow ${imgImgFlow.imgImgFlowName} is connected to a trace-to-image flow. The second flow input type must be the same as the flow output type")
                            }
                            is ExperimentTraceTraceFlow ->{
                                throw Exception("image-to-image flow ${imgImgFlow.imgImgFlowName} is connected to a trace-to-image flow. The second flow input type must be the same as the source output type")
                            }
                            is ExperimentImageSink -> {
                                //Add a new fwd op for the connection from the flow to the sink
                                //However a connection from a flow to a sink is terminal so we won't actually create
                                //the fwdOp object. We only create an ExperimentFwdOp object to keep a record of the
                                //connection
                                val newFwdOp = ExperimentImageFwdOp(imgImgFlow.imgImgFlowName, outlet.imageSinkName)

                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} (broadcast) toFanIn ${outlet.imageSinkName} (merge)")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(imgImgFlow.bcastObject).via(bufferFlow).toFanIn(outlet.mergeObject)
                                    }
                                    else {
                                        builder.from(imgImgFlow.bcastObject).toFanIn(outlet.mergeObject)
                                    }
                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} (broadcast) to ${outlet.imageSinkName}")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(imgImgFlow.bcastObject).via(bufferFlow).to(outlet.sink)
                                    }
                                    else{
                                        builder.from(imgImgFlow.bcastObject).to(outlet.sink)
                                    }

                                    numConnectionsMade++
                                }

                                imgImgFlow.fwdOps.add(newFwdOp)
                            }
                        }
                    }
                } else {
                    for (outlet in imgImgFlow.outs) {
                        when (outlet) {
                            is ExperimentImageImageFlow -> {
                                createConnection(imgImgFlow, outlet, builder)
                            }
                            is ExperimentImageTraceFlow -> {
                                createConnection(imgImgFlow, outlet, builder)
                            }
                            is ExperimentTraceImageFlow ->{
                                throw Exception("image-to-image flow ${imgImgFlow.imgImgFlowName} is connected to a trace-to-image flow. The second flow input type must be the same as the flow output type")
                            }
                            is ExperimentTraceTraceFlow ->{
                                throw Exception("image-to-image flow ${imgImgFlow.imgImgFlowName} is connected to a trace-to-image flow. The second flow input type must be the same as the source output type")
                            }
                            is ExperimentImageSink -> {
                                //Add a new fwd op for the connection from the flow to the sink
                                //However a connection from a flow to a sink is terminal so we won't actually create
                                //the fwdOp object. We only create an ExperimentFwdOp object to keep a record of the
                                //connection
                                val newFwdOp = ExperimentImageFwdOp(imgImgFlow.imgImgFlowName, outlet.imageSinkName)

                                if(outlet.mergeObject != null){
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} toFanIn ${outlet.imageSinkName} (merge)")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(imgImgFlow.flow).via(bufferFlow).toFanIn(outlet.mergeObject)
                                    }
                                    else{
                                        builder.from(imgImgFlow.flow).toFanIn(outlet.mergeObject)
                                    }

                                    numConnectionsMade++
                                }
                                else{
                                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} to ${outlet.imageSinkName}")

                                    //Special case that only needs to be applied for image displays
                                    if(outlet.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY){
                                        val bufferFlow = builder.add(Flow.of(STRIMMImage::class.java)
                                                .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                                                .async()
                                                .named("BufferFlow"))
                                        builder.from(imgImgFlow.flow).via(bufferFlow).to(outlet.sink)
                                    }
                                    else{
                                        builder.from(imgImgFlow.flow).to(outlet.sink)
                                    }


                                    numConnectionsMade++
                                }

                                imgImgFlow.fwdOps.add(newFwdOp)
                            }
                        }
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error building connections for image to image flows. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * Create a connection from an image-to-image flow to an image-to-image flow
     * @param imgImgFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createConnection(imgImgFlow : ExperimentImageImageFlow, outlet: ExperimentImageImageFlow, builder: GraphDSL.Builder<NotUsed>){
        val fwdOpsList = imgImgFlow.fwdOps.filter { x -> x.toNodeName == imgImgFlow.name }
        if(fwdOpsList.isNotEmpty()) {
            //This is when the current flow has at least one forward op specified going to the flow
            for (fwdOp in fwdOpsList) {
                if (outlet.mergeObject != null) {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} viaFanIn ${outlet.imgImgFlowName} (merge)")
                    fwdOp.fwdOp?.viaFanIn(outlet.mergeObject)
                    numConnectionsMade++
                } else {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} via ${outlet.imgImgFlowName}")
                    fwdOp.fwdOp?.via(outlet.flow)
                    numConnectionsMade++
                }
            }
        } else {
            //Create a new forward op if none exist between flow and outlet
            val newFwdOp = ExperimentImageFwdOp(imgImgFlow.imgImgFlowName, outlet.imgImgFlowName)

            if (outlet.mergeObject != null) {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} viaFanIn ${outlet.imgImgFlowName} (merge)")
                builder.from(imgImgFlow.flow).viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} via ${outlet.imgImgFlowName}")
                newFwdOp.fwdOp = builder.from(imgImgFlow.flow).via(outlet.flow)
                numConnectionsMade++
            }

            outlet.fwdOps.add(newFwdOp)
        }
    }

    /**
     * Create a connection from an image-to-image flow to an image-to-trace flow
     * @param imgImgFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createConnection(imgImgFlow : ExperimentImageImageFlow, outlet: ExperimentImageTraceFlow, builder: GraphDSL.Builder<NotUsed>){
        val fwdOpsList = imgImgFlow.fwdOps.filter { x -> x.toNodeName == imgImgFlow.name }
        if(fwdOpsList.isNotEmpty()) {
            for (fwdOp in fwdOpsList) {
                if (outlet.mergeObject != null) {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} viaFanIn ${outlet.imgTraceFlowName} (merge)")
                    fwdOp.fwdOp?.viaFanIn(outlet.mergeObject)
                    numConnectionsMade++
                } else {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} via ${outlet.imgTraceFlowName}")
                    fwdOp.fwdOp?.via(outlet.flow)
                    numConnectionsMade++
                }
            }
        } else {
            val newFwdOp = ExperimentTraceFwdOp(imgImgFlow.imgImgFlowName, outlet.imgTraceFlowName)

            if (outlet.mergeObject != null) {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} viaFanIn ${outlet.imgTraceFlowName} (merge)")
                builder.from(imgImgFlow.flow).viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} via ${outlet.imgTraceFlowName}")
                newFwdOp.fwdOp = builder.from(imgImgFlow.flow).via(outlet.flow)
                numConnectionsMade++
            }

            outlet.fwdOps.add(newFwdOp)
        }
    }

    /**
     * Create a connection from an trace-to-image flow to an image-to-image flow
     * @param traceImgFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createConnection(traceImgFlow : ExperimentTraceImageFlow, outlet: ExperimentImageImageFlow, builder: GraphDSL.Builder<NotUsed>){
        val fwdOpsList = traceImgFlow.fwdOps.filter { x -> x.toNodeName == traceImgFlow.name }
        if(fwdOpsList.isNotEmpty()) {
            for (fwdOp in fwdOpsList) {
                if (outlet.mergeObject != null) {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} viaFanIn ${outlet.imgImgFlowName} (merge)")
                    fwdOp.fwdOp?.viaFanIn(outlet.mergeObject)
                    numConnectionsMade++
                } else {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} via ${outlet.imgImgFlowName}")
                    fwdOp.fwdOp?.via(outlet.flow)
                    numConnectionsMade++
                }
            }
        } else {
            val newFwdOp = ExperimentImageFwdOp(traceImgFlow.traceImgFlowName, outlet.imgImgFlowName)

            if (outlet.mergeObject != null) {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} viaFanIn ${outlet.imgImgFlowName} (merge)")
                builder.from(traceImgFlow.flow).viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} via ${outlet.imgImgFlowName}")
                newFwdOp.fwdOp = builder.from(traceImgFlow.flow).via(outlet.flow)
                numConnectionsMade++
            }

            outlet.fwdOps.add(newFwdOp)
        }
    }

    /**
     * Create a connection from an trace-to-image flow to an image-to-trace flow
     * @param traceImgFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createConnection(traceImgFlow : ExperimentTraceImageFlow, outlet: ExperimentImageTraceFlow, builder: GraphDSL.Builder<NotUsed>){
        val fwdOpsList = traceImgFlow.fwdOps.filter { x -> x.toNodeName == traceImgFlow.name }
        if(fwdOpsList.isNotEmpty()) {
            for (fwdOp in fwdOpsList) {
                if (outlet.mergeObject != null) {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} viaFanIn ${outlet.imgTraceFlowName}")
                    fwdOp.fwdOp?.viaFanIn(outlet.mergeObject)
                    numConnectionsMade++
                } else {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} via ${outlet.imgTraceFlowName}")
                    fwdOp.fwdOp?.via(outlet.flow)
                    numConnectionsMade++
                }
            }
        } else {
            val newFwdOp = ExperimentTraceFwdOp(traceImgFlow.traceImgFlowName, outlet.imgTraceFlowName)

            if (outlet.mergeObject != null) {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} viaFanIn ${outlet.imgTraceFlowName} (merge)")
                builder.from(traceImgFlow.flow).viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} via ${outlet.imgTraceFlowName}")
                newFwdOp.fwdOp = builder.from(traceImgFlow.flow).via(outlet.flow)
                numConnectionsMade++
            }

            outlet.fwdOps.add(newFwdOp)
        }
    }

    /**
     * Create a connection from an image-to-trace flow to an trace-to-trace flow
     * @param imgTraceFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createConnection(imgTraceFlow : ExperimentImageTraceFlow, outlet: ExperimentTraceTraceFlow, builder: GraphDSL.Builder<NotUsed>){
        val fwdOpsList = imgTraceFlow.fwdOps.filter { x -> x.toNodeName == imgTraceFlow.name }
        if(fwdOpsList.isNotEmpty()) {
            for (fwdOp in fwdOpsList) {
                if (outlet.mergeObject != null) {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} viaFanIn ${outlet.traceTraceFlowName} (merge)")
                    fwdOp.fwdOp?.viaFanIn(outlet.mergeObject)
                    numConnectionsMade++
                } else {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} via ${outlet.traceTraceFlowName}")
                    fwdOp.fwdOp?.via(outlet.flow)
                    numConnectionsMade++
                }
            }
        } else {
            val newFwdOp = ExperimentTraceFwdOp(imgTraceFlow.imgTraceFlowName, outlet.traceTraceFlowName)

            if (outlet.mergeObject != null) {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} viaFanIn ${outlet.traceTraceFlowName} (merge)")
                builder.from(imgTraceFlow.flow).viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} via ${outlet.traceTraceFlowName}")
                newFwdOp.fwdOp = builder.from(imgTraceFlow.flow).via(outlet.flow)
                numConnectionsMade++
            }

            outlet.fwdOps.add(newFwdOp)
        }
    }

    /**
     * Create a connection from an image-to-trace flow to an trace-to-image flow
     * @param imgTraceFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createConnection(imgTraceFlow : ExperimentImageTraceFlow, outlet: ExperimentTraceImageFlow, builder: GraphDSL.Builder<NotUsed>){
        val fwdOpsList = imgTraceFlow.fwdOps.filter { x -> x.toNodeName == imgTraceFlow.name }
        if(fwdOpsList.isNotEmpty()) {
            for (fwdOp in fwdOpsList) {
                if (outlet.mergeObject != null) {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} viaFanIn ${outlet.traceImgFlowName} (merge)")
                    fwdOp.fwdOp?.viaFanIn(outlet.mergeObject)
                    numConnectionsMade++
                } else {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} via ${outlet.traceImgFlowName}")
                    fwdOp.fwdOp?.via(outlet.flow)
                    numConnectionsMade++
                }
            }
        } else {
            val newFwdOp = ExperimentImageFwdOp(imgTraceFlow.imgTraceFlowName, outlet.traceImgFlowName)

            if (outlet.mergeObject != null) {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} viaFanIn ${outlet.traceImgFlowName} (merge)")
                builder.from(imgTraceFlow.flow).viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} via ${outlet.traceImgFlowName}")
                newFwdOp.fwdOp = builder.from(imgTraceFlow.flow).via(outlet.flow)
                numConnectionsMade++
            }

            outlet.fwdOps.add(newFwdOp)
        }
    }

    /**
     * Create a connection from an trace-to-trace flow to an trace-to-trace flow
     * @param traceTraceFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createConnection(traceTraceFlow : ExperimentTraceTraceFlow, outlet: ExperimentTraceTraceFlow, builder: GraphDSL.Builder<NotUsed>){
        val fwdOpsList = traceTraceFlow.fwdOps.filter { x -> x.toNodeName == traceTraceFlow.name }
        if(fwdOpsList.isNotEmpty()) {
            for (fwdOp in fwdOpsList) {
                if (outlet.mergeObject != null) {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} viaFanIn ${outlet.traceTraceFlowName} (merge)")
                    fwdOp.fwdOp?.viaFanIn(outlet.mergeObject)
                    numConnectionsMade++
                } else {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} via ${outlet.traceTraceFlowName}")
                    fwdOp.fwdOp?.via(outlet.flow)
                    numConnectionsMade++
                }
            }
        } else {
            val newFwdOp = ExperimentTraceFwdOp(traceTraceFlow.traceTraceFlowName, outlet.traceTraceFlowName)

            if (outlet.mergeObject != null) {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} viaFanIn ${outlet.traceTraceFlowName} (merge)")
                builder.from(traceTraceFlow.flow).viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} via ${outlet.traceTraceFlowName}")
                newFwdOp.fwdOp = builder.from(traceTraceFlow.flow).via(outlet.flow)
                numConnectionsMade++
            }

            outlet.fwdOps.add(newFwdOp)
        }
    }

    /**
     * Create a connection from an trace-to-trace flow to an trace-to-image flow
     * @param traceTraceFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createConnection(traceTraceFlow : ExperimentTraceTraceFlow, outlet: ExperimentTraceImageFlow, builder: GraphDSL.Builder<NotUsed>){
        val fwdOpsList = traceTraceFlow.fwdOps.filter { x -> x.toNodeName == traceTraceFlow.name }
        if(fwdOpsList.isNotEmpty()) {
            for (fwdOp in fwdOpsList) {
                if (outlet.mergeObject != null) {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} viaFanIn ${outlet.traceImgFlowName} (merge)")
                    fwdOp.fwdOp?.viaFanIn(outlet.mergeObject)
                    numConnectionsMade++
                } else {
                    GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} via ${outlet.traceImgFlowName}")
                    fwdOp.fwdOp?.via(outlet.flow)
                    numConnectionsMade++
                }
            }
        } else {
            val newFwdOp = ExperimentImageFwdOp(traceTraceFlow.traceTraceFlowName, outlet.traceImgFlowName)

            if (outlet.mergeObject != null) {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} viaFanIn ${outlet.traceImgFlowName} (merge)")
                builder.from(traceTraceFlow.flow).viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} via ${outlet.traceImgFlowName}")
                newFwdOp.fwdOp = builder.from(traceTraceFlow.flow).via(outlet.flow)
                numConnectionsMade++
            }

            outlet.fwdOps.add(newFwdOp)
        }
    }

    /**
     * Create a broadcast connection from an image-to-image flow to an image-to-image flow
     * @param imgImgFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createBroadcastConnection(imgImgFlow : ExperimentImageImageFlow, outlet: ExperimentImageImageFlow, builder: GraphDSL.Builder<NotUsed>){
        if (outlet.mergeObject != null) {
            //This condition represents a broadcast connection going to a merge connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} (broadcast) viaFanIn ${outlet.imgImgFlowName} (merge)")
            imgImgFlow.bcastFwdOp?.fwdOp?.viaFanIn(outlet.mergeObject)
            numConnectionsMade++
        } else {
            //Ths condition represents a broadcast connection going to a normal connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} (broadcast) via ${outlet.imgImgFlowName}")
            builder.from(imgImgFlow.bcastObject).via(outlet.flow)
            numConnectionsMade++
        }
    }

    /**
     * Create a broadcast connection from an image-to-image flow to an image-to-trace flow
     * @param imgImgFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createBroadcastConnection(imgImgFlow : ExperimentImageImageFlow, outlet: ExperimentImageTraceFlow, builder: GraphDSL.Builder<NotUsed>){
        if (outlet.mergeObject != null) {
            //This condition represents a broadcast connection going to a merge connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} (broadcast) viaFanIn ${outlet.imgTraceFlowName} (merge)")
            imgImgFlow.bcastFwdOp?.fwdOp?.viaFanIn(outlet.mergeObject)
            numConnectionsMade++
        } else {
            //Ths condition represents a broadcast connection going to a normal connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgImgFlow.imgImgFlowName} (broadcast) via ${outlet.imgTraceFlowName}")
            builder.from(imgImgFlow.bcastObject).via(outlet.flow)
            numConnectionsMade++
        }
    }

    /**
     * Create a broadcast connection from an trace-to-image flow to an image-to-image flow
     * @param traceImgFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createBroadcastConnection(traceImgFlow : ExperimentTraceImageFlow, outlet: ExperimentImageImageFlow, builder: GraphDSL.Builder<NotUsed>){
        if (outlet.mergeObject != null) {
            //This condition represents a broadcast connection going to a merge connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} (broadcast) viaFanIn ${outlet.imgImgFlowName} (merge)")
            traceImgFlow.bcastFwdOp?.fwdOp?.viaFanIn(outlet.mergeObject)
            numConnectionsMade++
        } else {
            //Ths condition represents a broadcast connection going to a normal connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} (broadcast) viaFanOut ${outlet.imgImgFlowName}")
            builder.from(traceImgFlow.bcastObject).via(outlet.flow)
            numConnectionsMade++
        }
    }

    /**
     * Create a broadcast connection from an trace-to-image flow to an image-to-trace flow
     * @param traceImgFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createBroadcastConnection(traceImgFlow : ExperimentTraceImageFlow, outlet: ExperimentImageTraceFlow, builder: GraphDSL.Builder<NotUsed>){
        if (outlet.mergeObject != null) {
            //This condition represents a broadcast connection going to a merge connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} (broadcast) viaFanIn ${outlet.imgTraceFlowName} (merge)")
            traceImgFlow.bcastFwdOp?.fwdOp?.viaFanIn(outlet.mergeObject)
            numConnectionsMade++
        } else {
            //Ths condition represents a broadcast connection going to a normal connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceImgFlow.traceImgFlowName} (broadcast) via ${outlet.imgTraceFlowName}")
            builder.from(traceImgFlow.bcastObject).via(outlet.flow)
            numConnectionsMade++
        }
    }

    /**
     * Create a broadcast connection from an image-to-trace flow to an trace-to-trace flow
     * @param imgTraceFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createBroadcastConnection(imgTraceFlow : ExperimentImageTraceFlow, outlet: ExperimentTraceTraceFlow, builder: GraphDSL.Builder<NotUsed>){
            if (outlet.mergeObject != null) {
                //This condition represents a broadcast connection going to a merge connection
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} (broadcast) viaFanIn ${outlet.traceTraceFlowName} (merge)")
                imgTraceFlow.bcastFwdOp?.fwdOp?.viaFanIn(outlet.mergeObject)
                numConnectionsMade++
            } else {
                //Ths condition represents a broadcast connection going to a normal connection
                GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} (broadcast) via ${outlet.traceTraceFlowName}")
                builder.from(imgTraceFlow.bcastObject).via(outlet.flow)
                numConnectionsMade++
            }
    }

    /**
     * Create a broadcast connection from an image-to-trace flow to an trace-to-image flow
     * @param imgTraceFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createBroadcastConnection(imgTraceFlow : ExperimentImageTraceFlow, outlet: ExperimentTraceImageFlow, builder: GraphDSL.Builder<NotUsed>){
        if (outlet.mergeObject != null) {
            //This condition represents a broadcast connection going to a merge connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} (broadcast) viaFanIn ${outlet.traceImgFlowName} (merge)")
            imgTraceFlow.bcastFwdOp?.fwdOp?.viaFanIn(outlet.mergeObject)
            numConnectionsMade++
        } else {
            //Ths condition represents a broadcast connection going to a normal connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${imgTraceFlow.imgTraceFlowName} (broadcast) via ${outlet.traceImgFlowName}")
            builder.from(imgTraceFlow.bcastObject).via(outlet.flow)
            numConnectionsMade++
        }
    }

    /**
     * Create a broadcast connection from an trace-to-trace flow to an trace-to-trace flow
     * @param traceTraceFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createBroadcastConnection(traceTraceFlow : ExperimentTraceTraceFlow, outlet: ExperimentTraceTraceFlow, builder: GraphDSL.Builder<NotUsed>){
        if (outlet.mergeObject != null) {
            //This condition represents a broadcast connection going to a merge connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} (broadcast) viaFanIn ${outlet.traceTraceFlowName} (merge)")
            traceTraceFlow.bcastFwdOp?.fwdOp?.viaFanIn(outlet.mergeObject)
            numConnectionsMade++
        } else {
            //Ths condition represents a broadcast connection going to a normal connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} (broadcast) via ${outlet.traceTraceFlowName}")
            builder.from(traceTraceFlow.bcastObject).via(outlet.flow)
            numConnectionsMade++
        }
    }

    /**
     * Create a broadcast connection from an trace-to-trace flow to an trace-to-image flow
     * @param traceTraceFlow The originating flow
     * @param outlet The destination flow
     * @builder The graph building object
     */
    private fun createBroadcastConnection(traceTraceFlow : ExperimentTraceTraceFlow, outlet: ExperimentTraceImageFlow, builder: GraphDSL.Builder<NotUsed>){
        if (outlet.mergeObject != null) {
            //This condition represents a broadcast connection going to a merge connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} (broadcast) viaFanIn ${outlet.traceImgFlowName} (merge)")
            traceTraceFlow.bcastFwdOp?.fwdOp?.viaFanIn(outlet.mergeObject)
            numConnectionsMade++
        } else {
            //Ths condition represents a broadcast connection going to a normal connection
            GUIMain.loggerService.log(Level.INFO, "Creating connection from ${traceTraceFlow.traceTraceFlowName} (broadcast) via ${outlet.traceImgFlowName}")
            builder.from(traceTraceFlow.bcastObject).via(outlet.flow)
            numConnectionsMade++
        }
    }

    /**
     * If a source has more than one outlet (connection going out from it) then it will need to broadcast. This method
     * will create the necessary broadcast object
     * @param builder The graph building object
     */
    private fun setBroadcastObjectForSources(builder : GraphDSL.Builder<NotUsed>){
        try {
            experimentImageSources.forEach { x ->
                if (x.outs.size > 1) {
                    if (x.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE.toLowerCase()) {
                        //Create a broadcast object. We also need to specify it in the builder here
                        x.bcastObject = builder.add(Broadcast.create<STRIMMImage>(x.outs.size))

                        //Create a forward op object but both the from and to names will be the same (the flow's) as its a broadcast fwd op
                        val bcastFwdOp = builder.from(x.source).viaFanOut(x.bcastObject)
                        val experimentFwdOp = ExperimentImageFwdOp(x.imgSourceName, x.imgSourceName)
                        experimentFwdOp.fwdOp = bcastFwdOp
                        x.bcastFwdOp = experimentFwdOp
                    }
                }
            }

            experimentTraceSources.forEach { x ->
                if (x.outs.size > 1) {
                    if (x.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE.toLowerCase()) {
                        //Create a broadcast object. We also need to specify it in the builder here
                        x.bcastObject = builder.add(Broadcast.create<List<ArrayList<TraceData>>>(x.outs.size))

                        //Create a forward op object but both the from and to names will be the same (the flow's) as its a broadcast fwd op
                        val bcastFwdOp = builder.from(x.source).viaFanOut(x.bcastObject)
                        val experimentFwdOp = ExperimentTraceFwdOp(x.traceSourceName, x.traceSourceName)
                        experimentFwdOp.fwdOp = bcastFwdOp
                        x.bcastFwdOp = experimentFwdOp
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error creating broadcast objects for sources. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * If a flow has more than one outlet (connection going out from it) then it will need to broadcast. This method
     * will create the necessary broadcast object
     * @param builder The graph building object
     */
    private fun setBroadcastObjectForFlows(builder : GraphDSL.Builder<NotUsed>){
        try {
            experimentImageImageFlows.forEach { x ->
                if (x.outs.size > 1) {
                    if (x.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE.toLowerCase()) {
                        //Create a broadcast object. We also need to specify it in the builder here
                        x.bcastObject = builder.add(Broadcast.create<STRIMMImage>(x.outs.size))

                        //Create a forward op object but both the from and to names will be the same (the flow's) as its a broadcast fwd op
                        val bcastFwdOp = builder.from(x.flow).viaFanOut(x.bcastObject)
                        val experimentFwdOp = ExperimentImageFwdOp(x.imgImgFlowName, x.imgImgFlowName)
                        experimentFwdOp.fwdOp = bcastFwdOp
                        x.bcastFwdOp = experimentFwdOp
                    }
                }
            }
            experimentImageTraceFlows.forEach { x ->
                if (x.outs.size > 1) {
                    if (x.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE.toLowerCase()) {
                        //Create a broadcast object. We also need to specify it in the builder here
                        x.bcastObject = builder.add(Broadcast.create<List<ArrayList<TraceData>>>(x.outs.size))

                        //Create a forward op object but both the from and to names will be the same (the flow's) as its a broadcast fwd op
                        val bcastFwdOp = builder.from(x.flow).viaFanOut(x.bcastObject)
                        val experimentFwdOp = ExperimentTraceFwdOp(x.imgTraceFlowName, x.imgTraceFlowName)
                        experimentFwdOp.fwdOp = bcastFwdOp
                        x.bcastFwdOp = experimentFwdOp
                    }
                }
            }
            experimentTraceImageFlows.forEach { x ->
                if (x.outs.size > 1) {
                    if (x.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE.toLowerCase()) {
                        //Create a broadcast object. We also need to specify it in the builder here
                        x.bcastObject = builder.add(Broadcast.create<STRIMMImage>(x.outs.size))

                        //Create a forward op object but both the from and to names will be the same (the flow's) as its a broadcast fwd op
                        val bcastFwdOp = builder.from(x.flow).viaFanOut(x.bcastObject)
                        val experimentFwdOp = ExperimentImageFwdOp(x.traceImgFlowName, x.traceImgFlowName)
                        experimentFwdOp.fwdOp = bcastFwdOp
                        x.bcastFwdOp = experimentFwdOp
                    }
                }
            }
            experimentTraceTraceFlows.forEach { x ->
                if (x.outs.size > 1) {
                    if (x.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE.toLowerCase()) {
                        //Create a broadcast object. We also need to specify it in the builder here
                        x.bcastObject = builder.add(Broadcast.create<List<ArrayList<TraceData>>>(x.outs.size))

                        //Create a forward op object but both the from and to names will be the same (the flow's) as its a broadcast fwd op
                        val bcastFwdOp = builder.from(x.flow).viaFanOut(x.bcastObject)
                        val experimentFwdOp = ExperimentTraceFwdOp(x.traceTraceFlowName, x.traceTraceFlowName)
                        experimentFwdOp.fwdOp = bcastFwdOp
                        x.bcastFwdOp = experimentFwdOp
                    }
                }
            }
        }
        catch (ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error creating broadcast objects for flows. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * If a flow has more than one inlet (connection going into it) then it will need to merge. THis method will create
     * the necessary merge object
     * @param builder The graph building object
     */
    private fun setMergeObjectForSinks(builder : GraphDSL.Builder<NotUsed>){
        try {
            experimentImageSinks.forEach { x ->
                if(x.ins.size > 1){
                    for(inlet in x.ins) {
                        if (x.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_INPUT_TYPE.toLowerCase() && x.mergeObject == null) {
                            when(inlet){
                                is ExperimentImageSource ->{
                                    x.mergeObject = builder.add(Merge.create<STRIMMImage>(x.ins.size))

                                    //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                                    //This won't be used later but must be done now
                                    builder.from(x.mergeObject).to(x.sink)

                                    val experimentFwdOp = ExperimentImageFwdOp(inlet.imgSourceName, x.imageSinkName)
                                    x.mergeFwdOp = experimentFwdOp
                                }
                                is ExperimentImageImageFlow -> {
                                    x.mergeObject = builder.add(Merge.create<STRIMMImage>(x.ins.size))

                                    //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                                    //This won't be used later but must be done now
                                    builder.from(x.mergeObject).to(x.sink)

                                    val experimentFwdOp = ExperimentImageFwdOp(inlet.imgImgFlowName, x.imageSinkName)
                                    x.mergeFwdOp = experimentFwdOp
                                }
                                is ExperimentTraceImageFlow -> {
                                    x.mergeObject = builder.add(Merge.create<STRIMMImage>(x.ins.size))

                                    //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                                    //This won't be used later but must be done now
                                    builder.from(x.mergeObject).to(x.sink)

                                    val experimentFwdOp = ExperimentImageFwdOp(inlet.traceImgFlowName, x.imageSinkName)
                                    x.mergeFwdOp = experimentFwdOp
                                }
                            }
                        }
                    }
                }
            }
            experimentTraceSinks.forEach { x ->
                if(x.ins.size > 1){
                    for(inlet in x.ins) {
                        if (x.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_INPUT_TYPE.toLowerCase() && x.mergeObject == null) {
                            when(inlet){
                                is ExperimentTraceSource ->{
                                    x.mergeObject = builder.add(Merge.create<List<ArrayList<TraceData>>>(x.ins.size))

                                    //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                                    //This won't be used later but must be done now
                                    builder.from(x.mergeObject).to(x.sink)

                                    val experimentFwdOp = ExperimentTraceFwdOp(inlet.traceSourceName, x.traceSinkName)
                                    x.mergeFwdOp = experimentFwdOp
                                }
                                is ExperimentImageTraceFlow -> {
                                    x.mergeObject = builder.add(Merge.create<List<ArrayList<TraceData>>>(x.ins.size))

                                    //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                                    //This won't be used later but must be done now
                                    builder.from(x.mergeObject).to(x.sink)

                                    val experimentFwdOp = ExperimentTraceFwdOp(inlet.imgTraceFlowName, x.traceSinkName)
                                    x.mergeFwdOp = experimentFwdOp
                                }
                                is ExperimentTraceTraceFlow -> {
                                    x.mergeObject = builder.add(Merge.create<List<ArrayList<TraceData>>>(x.ins.size))

                                    //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                                    //This won't be used later but must be done now
                                    builder.from(x.mergeObject).to(x.sink)

                                    val experimentFwdOp = ExperimentTraceFwdOp(inlet.traceTraceFlowName, x.traceSinkName)
                                    x.mergeFwdOp = experimentFwdOp
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error creating merge objects for sinks. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * If a flow has more than one inlet (connection going into it) then it will need to merge. THis method will create
     * the necessary merge object
     * @param builder The graph building object
     */
    private fun setMergeObjectForFlows(builder : GraphDSL.Builder<NotUsed>){
        try {
            experimentImageImageFlows.forEach { x ->
                if (x.ins.size > 1) {
                    if (x.inputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_INPUT_TYPE.toLowerCase()) {
                        //Create a merge object. We also need to specify it in the builder here
                        x.mergeObject = builder.add(Merge.create<STRIMMImage>(x.ins.size))

                        //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                        //This won't be used later but must be done now
                        builder.from(x.mergeObject).via(x.flow)

                        val experimentFwdOp = ExperimentImageFwdOp(x.imgImgFlowName, x.imgImgFlowName)
                        x.mergeFwdOp = experimentFwdOp
                    }
                }
            }
            experimentImageTraceFlows.forEach { x ->
                if (x.ins.size > 1) {
                    if (x.inputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_INPUT_TYPE.toLowerCase()) {
                        //Create a merge object. We also need to specify it in the builder here
                        x.mergeObject = builder.add(Merge.create<STRIMMImage>(x.ins.size))

                        //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                        //This won't be used later but must be done now
                        builder.from(x.mergeObject).via(x.flow)

                        val experimentFwdOp = ExperimentImageFwdOp(x.imgTraceFlowName, x.imgTraceFlowName)
                        x.mergeFwdOp = experimentFwdOp
                    }
                }
            }
            experimentTraceImageFlows.forEach { x ->
                if (x.ins.size > 1) {
                    if (x.inputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_INPUT_TYPE.toLowerCase()) {
                        //Create a merge object. We also need to specify it in the builder here
                        x.mergeObject = builder.add(Merge.create<List<ArrayList<TraceData>>>(x.ins.size))

                        //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                        //This won't be used later but must be done now
                        builder.from(x.mergeObject).via(x.flow)

                        val experimentFwdOp = ExperimentTraceFwdOp(x.traceImgFlowName, x.traceImgFlowName)
                        x.mergeFwdOp = experimentFwdOp
                    }
                }
            }
            experimentTraceTraceFlows.forEach { x ->
                if (x.ins.size > 1) {
                    if (x.inputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_INPUT_TYPE.toLowerCase()) {
                        //Create a merge object. We also need to specify it in the builder here
                        x.mergeObject = builder.add(Merge.create<List<ArrayList<TraceData>>>(x.ins.size))

                        //IMPORTANT NOTE: like broadcast, any merge object needs to have its forward op (builder.from(mergeObject).to(something)
                        //This won't be used later but must be done now
                        builder.from(x.mergeObject).via(x.flow)

                        val experimentFwdOp = ExperimentTraceFwdOp(x.traceTraceFlowName, x.traceTraceFlowName)
                        x.mergeFwdOp = experimentFwdOp
                    }
                }
            }
        }
        catch (ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error creating broadcast objects for flows. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }
    //endregion

    //region Wrapper objects for experiment config
    /**
     * Short method to call each of the populating methods
     * @param expConfig The experiment config from the JSON config file
     * @param builder The graph building object
     */
    private fun populateLists(expConfig : ExperimentConfiguration, builder : GraphDSL.Builder<NotUsed>){
        GUIMain.loggerService.log(Level.INFO, "Populating sources, flows and sinks")
        populateSources(expConfig, builder)
        populateFlows(expConfig, builder)
        populateSinks(expConfig, builder)
    }

    /**
     * Go through the experiment config (from json file) and create the appropriate source objects that correspond to the
     * config. This uses custom wrapper classes for the akka objects. This also specifies types explicitly
     * @param expConfig The experiment config from the JSON config file
     * @param builder The graph building object
     */
    private fun populateSources(expConfig: ExperimentConfiguration, builder : GraphDSL.Builder<NotUsed>){
        try {
            for (source in expConfig.sourceConfig.sources) {
                if (source.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE.toLowerCase()) {
                    val expSource = ExperimentImageSource(source.sourceName, source.deviceLabel)
                    val snapImageMethod = GUIMain.acquisitionMethodService.getAcquisitionMethod(ExperimentConstants.Acquisition.SNAP_IMAGE_METHOD_NAME) as AcquisitionMethodService.ImageMethod

                    //Use source.intervalMs.toLong() for software timing
                    val intervalTemp = 100 //TODO hardcoded
                    val akkaSource = Source.tick(Duration.ZERO, Duration.ofMillis(intervalTemp.toLong()), Unit)
                            .map { snapImageMethod.runMethod(source.deviceLabel, cameraDevices, 100) }//TODO hardcoded
                            .async()
                            .named(source.sourceName)

                    //The roi source here will allow for dynamic broadcasting of this source (i.e. publishing and subscribing)
                    //It is needed so we can specify traces from ROIs on the fly before recording. This can only be done for camera displays
                    val sourceGraph = akkaSource.toMat(BroadcastHub.of(STRIMMImage::class.java), Keep.right())
                    val fromSource = sourceGraph.run(GUIMain.actorService.materializer)
                            .async()
                    expSource.roiSource = fromSource

                    expSource.source = builder.add(fromSource)
                    expSource.outputType = ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE
                    expSource.exposureMs = source.exposureMs
                    expSource.intervalMs = source.intervalMs
                    GUIMain.experimentService.calculateNumberOfDataPointsFromInterval(source.deviceLabel, 100.0)//source.intervalMs//TODO hardcoded
                    experimentImageSources.add(expSource)
                }
                else if (source.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE.toLowerCase()) {
                    val expSource = ExperimentTraceSource(source.sourceName, source.deviceLabel)
                    val dummyOverlay = EllipseOverlay()
                    dummyOverlay.name = expSource.traceSourceName + "DummyOverlay" + Random().nextInt(1000)

                    val getTraceDataMethod = GUIMain.acquisitionMethodService.getAcquisitionMethod(ExperimentConstants.Acquisition.GET_TRACE_DATA_METHOD_NAME) as AcquisitionMethodService.TraceMethod

                    var sourceIntervalMs = 1000.0 //Default value

                    //If the source is from a channel, query the hardware post init properties to get to the proper sampling frequency
                    val channelForSource = GUIMain.experimentService.getChannelForSource(source.channel)
                    var sampFreq = 1.0
                    var blockSize = 1
                    if(channelForSource != null){
                        sampFreq = GUIMain.experimentService.getSamplingFrequencyOfChannel(channelForSource.channelName)
                        sourceIntervalMs = (1/sampFreq)*1000
                        blockSize = GUIMain.experimentService.getBlockSizeOfChannel(channelForSource.channelName)
                    }

                    //TODO hardcoded
                    val akkaSource = Source.tick(Duration.ZERO, Duration.ofNanos(100000), Unit) //Duration.ofMillis(sourceIntervalMs.toLong()
                            .map { listOf(getTraceDataMethod.runMethod(dummyOverlay,sourceIntervalMs/1000.0)) }
                            .async()
                            .named(source.sourceName)
                    expSource.source = builder.add(akkaSource)
                    expSource.outputType = ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE
                    expSource.samplingFrequencyHz = sampFreq
                    expSource.blockSize = blockSize
                    expSource.channelName = source.channel
                    GUIMain.experimentService.calculateNumberOfDataPointsFromFrequency(expSource.traceSourceName, sampFreq)
                    experimentTraceSources.add(expSource)
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error creating sources from json experiment configuration. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * Go through the experiment config (from json file) and create the appropriate flow objects that correspond to the
     * config. This uses custom wrapper classes for the akka objects. This also specifies types explicitly
     * @param expConfig The experiment config from the JSON config file
     * @param builder The graph building object
     */
    private fun populateFlows(expConfig: ExperimentConfiguration, builder : GraphDSL.Builder<NotUsed>){
        try {
            for (flow in expConfig.flowConfig.flows) {
                if (flow.inputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_INPUT_TYPE.toLowerCase() &&
                        flow.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE.toLowerCase()) {
                    val expFlow = ExperimentImageImageFlow(flow.flowName)
                    val akkaFlow = Flow.of(STRIMMImage::class.java)
//                            .buffer(ExperimentConstants.ConfigurationProperties.IMAGE_BUFFER_SIZE, OverflowStrategy.dropTail())
                            .async()
                            .named(flow.flowName)
                    expFlow.flow = builder.add(akkaFlow)
                    expFlow.inputType = flow.inputType
                    expFlow.outputType = flow.outputType
                    experimentImageImageFlows.add(expFlow)
                } else if (flow.inputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_INPUT_TYPE.toLowerCase() &&
                        flow.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE.toLowerCase()) {
                    val expFlow = ExperimentImageTraceFlow(flow.flowName)

                    val averageROIAcquisitionMethod = GUIMain.acquisitionMethodService.getAcquisitionMethod(ExperimentConstants.Acquisition.AVERAGE_ROI_METHOD_NAME) as AcquisitionMethodService.TraceMethod

                    val akkaFlow = Flow.of(STRIMMImage::class.java)
                            .map { it -> averageROIAcquisitionMethod.runMethod(it, flow.flowName) }
                            .groupedWithin(ExperimentConstants.ConfigurationProperties.TRACE_GROUPING_AMOUNT, Duration.ofMillis(ExperimentConstants.ConfigurationProperties.TRACE_GROUPING_DURATION_MS))
                            .async()
                            .named(flow.flowName)

                    expFlow.inputType = flow.inputType
                    expFlow.outputType = flow.outputType
                    expFlow.flow = builder.add(akkaFlow)
                    experimentImageTraceFlows.add(expFlow)
                } else if (flow.inputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_INPUT_TYPE.toLowerCase() &&
                        flow.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE.toLowerCase()) {
                    val expFlow = ExperimentTraceImageFlow(flow.flowName)

                    val generateImageMethod = GUIMain.acquisitionMethodService.getAcquisitionMethod(ExperimentConstants.Acquisition.GENERATE_IMAGE_METHOD_NAME) as AcquisitionMethodService.ImageMethod

                    val akkaFlow = Flow.of(List::class.java)
                            .map { it -> generateImageMethod.runMethod() }
                            .groupedWithin(ExperimentConstants.ConfigurationProperties.TRACE_GROUPING_AMOUNT, Duration.ofMillis(ExperimentConstants.ConfigurationProperties.TRACE_GROUPING_DURATION_MS))
                            .async()
                            .named(flow.flowName) as Flow<List<ArrayList<TraceData>>, STRIMMImage, NotUsed> //The "groupedWithin" call turns this into a List of ArrayLists
                    expFlow.inputType = flow.inputType
                    expFlow.outputType = flow.outputType
                    expFlow.flow = builder.add(akkaFlow)
                    experimentTraceImageFlows.add(expFlow)
                } else if (flow.inputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_INPUT_TYPE.toLowerCase() &&
                        flow.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE.toLowerCase()) {
                    val expFlow = ExperimentTraceTraceFlow(flow.flowName)
                    //TODO dummy data
                    //val dummyOverlay = EllipseOverlay()
                    //dummyOverlay.name = expFlow.traceTraceFlowName + "DummyOverlay" + Random().nextInt(1000)

                    //val getTraceDataMethod = GUIMain.acquisitionMethodService.getAcquisitionMethod(ExperimentConstants.Acquisition.GET_TRACE_DATA_METHOD_NAME) as AcquisitionMethodService.TraceMethod

                    val akkaFlow = Flow.of(List::class.java)
                            //.map { it -> dummyMethod(it)} //getTraceDataMethod.runMethod(dummyOverlay)
                            //.groupedWithin(ExperimentConstants.ConfigurationProperties.TRACE_GROUPING_AMOUNT, Duration.ofMillis(1000))//ExperimentConstants.ConfigurationProperties.TRACE_GROUPING_DURATION_MS
                            //.groupedWithin(100,Duration.ofMillis(10))//TODO is this needed?
                            .async()
                            .named(flow.flowName) as Flow<List<ArrayList<TraceData>>, List<ArrayList<TraceData>>, NotUsed> //The "groupedWithin" call turns this into a List of ArrayLists

                    expFlow.inputType = flow.inputType
                    expFlow.outputType = flow.outputType
                    expFlow.flow = builder.add(akkaFlow)
                    experimentTraceTraceFlows.add(expFlow)
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error creating flows from json experiment configuration. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    /**
     * Go through the experiment config (from json file) and create the appropriate sink objects that correspond to the
     * config. This uses custom wrapper classes for the akka objects. This also specifies types explicitly
     * @param expConfig The experiment config from the JSON config file
     * @param builder The graph building object
     */
    private fun populateSinks(expConfig: ExperimentConfiguration, builder : GraphDSL.Builder<NotUsed>){
        try {
            for (sink in expConfig.sinkConfig.sinks) {
                if (sink.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE.toLowerCase()) {
                    if (sink.displayOrStore.toLowerCase() == ExperimentConstants.ConfigurationProperties.DISPLAY.toLowerCase()) {
                        val expSink = ExperimentImageSink(sink.sinkName)

                        val plugin = createCameraPluginWithActor(sink.primaryDevice)
                        plugin?.dock(GUIMain.strimmUIService.dockableControl, GUIMain.strimmUIService.strimmFrame)

                        val cameraActor = cameraActors.filter{ x -> x.value.label.toLowerCase() == sink.primaryDevice.toLowerCase()}.keys.first()

                        val akkaSink: Sink<STRIMMImage, NotUsed> = Sink.actorRefWithAck(cameraActor, StartStreamingCamera(),
                                Acknowledgement.INSTANCE, CompleteCameraStreaming()) { ex -> FailCameraStreaming(ex) }
                        expSink.sink = builder.add(akkaSink)
                        expSink.outputType = sink.outputType
                        expSink.displayOrStore = sink.displayOrStore
                        experimentImageSinks.add(expSink)
                    } else if (sink.displayOrStore.toLowerCase() == ExperimentConstants.ConfigurationProperties.STORE.toLowerCase()) {
                        val expSink = ExperimentImageSink(sink.sinkName)
                        val cameraDataStoreActor = createCameraDataStoreActor(sink.primaryDevice)

                        val akkaSink: Sink<STRIMMImage, NotUsed> = Sink.actorRefWithAck(cameraDataStoreActor, StartCameraDataStoring(),
                                Acknowledgement.INSTANCE, CompleteCameraDataStoring()) { ex -> FailCameraDataStoring(ex) }
                        expSink.sink = builder.add(akkaSink)
                        expSink.outputType = sink.outputType
                        expSink.displayOrStore = sink.displayOrStore
                        experimentImageSinks.add(expSink)
                    }
                } else if (sink.outputType.toLowerCase() == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE.toLowerCase()) {
                    if (sink.displayOrStore.toLowerCase() == ExperimentConstants.ConfigurationProperties.DISPLAY.toLowerCase()) {
                        val expSink = ExperimentTraceSink(sink.sinkName)

                        //Because it's a display, create a dockable window plugin as well as an actor
                        val pluginCreation = createTracePluginWithActor(sink.primaryDevice)
                        pluginCreation.second?.dock(GUIMain.strimmUIService.dockableControl, GUIMain.strimmUIService.strimmFrame)

                        val traceActor = pluginCreation.first

                        if(experimentTraceSources.isNotEmpty()) {
                            //Tell trace actor sampling rate here
                            try{
                                val traceSourceToUse = experimentTraceSources.first{ x -> x.deviceLabel == sink.primaryDevice && x.channelName == sink.primaryDeviceChannel}
                                traceActor?.tell(TellDeviceSamplingRate(traceSourceToUse.samplingFrequencyHz), ActorRef.noSender())
                                traceActor?.tell(TellSetNumDataPoints(), ActorRef.noSender())
                                val analogueDataStream = GUIMain.timerService.getAnalogueDataStreamForSource(traceSourceToUse.channelName)
                                if (analogueDataStream != null) {
                                    analogueDataStream.intervalSecs = 0.0001 //TODO hardcoded
                                    traceActor?.tell(TellAnalogueDataStream(analogueDataStream), ActorRef.noSender())
                                }
                            }
                            catch(ex : Exception){
                                println("!!!")
                            }
                        }

                        if(experimentImageSources.isNotEmpty()) {
                            //Tell trace actor sampling rate here
                            try{
//                                val imageSourceToUse = experimentImageSources.first{ x -> x.deviceLabel == sink.primaryDevice}
                                traceActor?.tell(TellDeviceSamplingRate(10.0), ActorRef.noSender())//TODO hardcoded
                                traceActor?.tell(TellSetNumDataPoints(), ActorRef.noSender())
//                                val analogueDataStream = GUIMain.timerService.getAnalogueDataStreamForSource(traceSourceToUse.channelName)
//                                if (analogueDataStream != null) {
//                                    traceActor?.tell(TellAnalogueDataStream(analogueDataStream), ActorRef.noSender())
//                                }
                            }
                            catch(ex : Exception){
                                println("???")
                            }
                        }

                        val akkaSink: Sink<List<ArrayList<TraceData>>, NotUsed> = Sink.actorRefWithAck(traceActor, StartStreamingTraceROI(),
                                Acknowledgement.INSTANCE, CompleteStreamingTraceROI()) { ex -> FailStreamingTraceROI(ex) }
                        expSink.sink = builder.add(akkaSink)
                        expSink.outputType = sink.outputType
                        expSink.displayOrStore = sink.displayOrStore
                        experimentTraceSinks.add(expSink)
                    } else if (sink.displayOrStore.toLowerCase() == ExperimentConstants.ConfigurationProperties.STORE.toLowerCase()) {
                        val expSink = ExperimentTraceSink(sink.sinkName)
                        val traceDataStoreActor = createTraceDataStoreActor(sink.primaryDevice)

                        try{
                            val traceSourceToUse = experimentTraceSources.first{ x -> x.deviceLabel == sink.primaryDevice && x.channelName == sink.primaryDeviceChannel}
                            val analogueDataStream = GUIMain.timerService.getAnalogueDataStreamForSource(traceSourceToUse.channelName)
                            if (analogueDataStream != null) {
                                analogueDataStream.intervalSecs = 0.0001 //TODO hardcoded
                                traceDataStoreActor.tell(TellAnalogueDataStream(analogueDataStream), ActorRef.noSender())
                            }
                        }
                        catch(ex : Exception){
                            println("???")
                        }

                        val akkaSink: Sink<List<ArrayList<TraceData>>, NotUsed> = Sink.actorRefWithAck(traceDataStoreActor, StartTraceDataStoring(),
                                Acknowledgement.INSTANCE, CompleteTraceDataStoring()) { ex -> FailTraceDataStoring(ex) }
                        expSink.sink = builder.add(akkaSink)
                        expSink.outputType = sink.outputType
                        expSink.displayOrStore = sink.displayOrStore
                        experimentTraceSinks.add(expSink)
                    }
                }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error creating sinks from json experiment configuration. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }
    //endregion

    /**
     * Get any node that is going out of the given node
     * Note - A sink cannot have an outlet (connections cannot go out from a sink)
     * @param currentNode The experiment node that the outlets are going out of
     * @param expConfig The experiment configuration (from json)
     */
    private fun getOutlets(currentNode : ExperimentNode, expConfig: ExperimentConfiguration) : ArrayList<ExperimentNode>{
        val outlets = arrayListOf<ExperimentNode>()
        try {
            for (flow in expConfig.flowConfig.flows) {
                flow.inputNames.filter{x -> x == currentNode.name}
                    .forEach {
                        outlets.addAll(experimentImageSources.filter { x -> x.imgSourceName == flow.flowName })
                        outlets.addAll(experimentTraceSources.filter { x -> x.traceSourceName == flow.flowName })
                        outlets.addAll(experimentImageImageFlows.filter { x -> x.imgImgFlowName == flow.flowName })
                        outlets.addAll(experimentImageTraceFlows.filter { x -> x.imgTraceFlowName == flow.flowName })
                        outlets.addAll(experimentTraceImageFlows.filter { x -> x.traceImgFlowName == flow.flowName })
                        outlets.addAll(experimentTraceTraceFlows.filter { x -> x.traceTraceFlowName == flow.flowName })
                        outlets.addAll(experimentImageSinks.filter { x -> x.imageSinkName == flow.flowName })
                        outlets.addAll(experimentTraceSinks.filter { x -> x.traceSinkName == flow.flowName })
                    }
            }

            for (sink in expConfig.sinkConfig.sinks) {
                val sinksAttachedToNode = sink.inputNames.filter{x -> x == currentNode.name}
                sinksAttachedToNode.forEach{
                    outlets.addAll(experimentImageSources.filter { x -> x.imgSourceName == sink.sinkName })
                    outlets.addAll(experimentTraceSources.filter { x -> x.traceSourceName == sink.sinkName })
                    outlets.addAll(experimentImageImageFlows.filter { x -> x.imgImgFlowName == sink.sinkName })
                    outlets.addAll(experimentImageTraceFlows.filter { x -> x.imgTraceFlowName == sink.sinkName })
                    outlets.addAll(experimentTraceImageFlows.filter { x -> x.traceImgFlowName == sink.sinkName })
                    outlets.addAll(experimentTraceTraceFlows.filter { x -> x.traceTraceFlowName == sink.sinkName })
                    outlets.addAll(experimentImageSinks.filter { x -> x.imageSinkName == sink.sinkName })
                    outlets.addAll(experimentTraceSinks.filter { x -> x.traceSinkName == sink.sinkName })
                    }
            }
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error populating outlets for graph nodes from json experiment configuration. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }

        return outlets
    }

    /**
     * Get any node that is going into the given node
     * Note - A source cannot have an inlet (connections cannot go into a source)
     * @param currentNode The experiment node that the inlets are going into
     * @param expConfig The experiment configuration (from json)
     */
    private fun getInlets(currentNode : ExperimentNode, expConfig: ExperimentConfiguration) : ArrayList<ExperimentNode>{
        val inlets = arrayListOf<ExperimentNode>()
        try {
            for (flow in expConfig.flowConfig.flows) {
                if (flow.flowName == currentNode.name) { //Find the flow relating to currentNode
                    //Find any source, flows or sinks that have an inputName corresponding this the current node's input
                    inlets.addAll(experimentImageSources.filter { x -> x.imgSourceName in flow.inputNames })
                    inlets.addAll(experimentTraceSources.filter { x -> x.traceSourceName in flow.inputNames })
                    inlets.addAll(experimentImageImageFlows.filter { x -> x.imgImgFlowName in flow.inputNames })
                    inlets.addAll(experimentImageTraceFlows.filter { x -> x.imgTraceFlowName in flow.inputNames })
                    inlets.addAll(experimentTraceImageFlows.filter { x -> x.traceImgFlowName in flow.inputNames })
                    inlets.addAll(experimentTraceTraceFlows.filter { x -> x.traceTraceFlowName in flow.inputNames })
                    inlets.addAll(experimentImageSinks.filter { x -> x.imageSinkName in flow.inputNames })
                    inlets.addAll(experimentTraceSinks.filter { x -> x.traceSinkName in flow.inputNames })
                }
            }

            for (sink in expConfig.sinkConfig.sinks) {
                if (sink.sinkName == currentNode.name) { //Find the flow relating to currentNode
                    //Find any source, flows or sinks that have an inputName corresponding this the current node's input
                    inlets.addAll(experimentImageSources.filter { x -> x.imgSourceName in sink.inputNames })
                    inlets.addAll(experimentTraceSources.filter { x -> x.traceSourceName in sink.inputNames })
                    inlets.addAll(experimentImageImageFlows.filter { x -> x.imgImgFlowName in sink.inputNames })
                    inlets.addAll(experimentImageTraceFlows.filter { x -> x.imgTraceFlowName in sink.inputNames })
                    inlets.addAll(experimentTraceImageFlows.filter { x -> x.traceImgFlowName in sink.inputNames })
                    inlets.addAll(experimentTraceTraceFlows.filter { x -> x.traceTraceFlowName in sink.inputNames })
                    inlets.addAll(experimentImageSinks.filter { x -> x.imageSinkName in sink.inputNames })
                    inlets.addAll(experimentTraceSinks.filter { x -> x.traceSinkName in sink.inputNames })
                }
            }
        }
        catch (ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Error populating inlets for graph nodes from json experiment configuration. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }

        return inlets
    }

    /**
     * From the experiment config, go through the loaded camera devices. If the config has a configuration matching
     * a loaded camera device, add it to a list of camera devices
     * @param expConfig The loaded experiment configuration
     */
    private fun populateListOfCameraDevices(expConfig: ExperimentConfiguration){
        val loadedCameraDevices = GUIMain.mmService.getLoadedDevicesOfType(MMCameraDevice::class.java)
        //We go through the sinks (instead of the sources) in case there is not a 1-to-1 mapping between sinks and sources
        for(imageSource in expConfig.sinkConfig.sinks.filter{x -> x.displayOrStore == ExperimentConstants.ConfigurationProperties.DISPLAY && x.outputType == ExperimentConstants.ConfigurationProperties.IMAGE_OUTPUT_TYPE}){
            val deviceName = imageSource.primaryDevice
            val devicesWithLabel = loadedCameraDevices.filter { x -> x.label == deviceName }

            for(device in devicesWithLabel) {
                val exposureSource = expConfig.sourceConfig.sources.filter { x -> x.deviceLabel == device.label }.first()
                cameraDevices.add(CameraDeviceInfo(device, false, exposureSource.exposureMs, exposureSource.intervalMs))
            }
        }
    }

    /**
     * Initialise the camera device with any relevant info e.g. exposure
     * @param info The camera device and relevant info as a data class
     */
    fun initialiseCamera(info : CameraDeviceInfo){
        info.device.exposure = info.exposureMillis
//        MMCoreService.core.setProperty("retigaCam","Mode","Color Test Pattern")

        info.device.initialise()
        info.device.isActive = true
        noteCameraSize(info.device)
        GUIMain.loggerService.log(Level.INFO, "Camera ${info.device.label} has been initialized")
    }

    /**
     * When creating and using a camera device, make a note of its image size for future reference. It will also make
     * a note if a size different to full size has been specified
     * @param camera The camera device
     */
    fun noteCameraSize(camera : MMCameraDevice){
        val camSizes = Pair(camera.imageWidth, camera.imageHeight)
        GUIMain.strimmUIService.cameraSizeList[camera.label] = camSizes

        val camSourceConfig = expConfig.sourceConfig.sources.first { x -> x.deviceLabel == camera.label }

        if(camSourceConfig.x < 0.0 || camSourceConfig.y < 0.0){
            GUIMain.loggerService.log(Level.WARNING, "x or y coordinate for camera device ${camera.label} is less than zero, using full size coordinates instead")
        }
        else if(camSourceConfig.w <= 0.0 || camSourceConfig.h <= 0.0){
            GUIMain.loggerService.log(Level.WARNING, "w or h coordinate for camera device ${camera.label} is less than or equal to zero, using full size coordinates instead")
        }
        else if((camSourceConfig.x >= 0 && camSourceConfig.y >= 0) && camSourceConfig.w > 0.0 && camSourceConfig.h > 0.0) {
            GUIMain.strimmUIService.cameraViewSizeList[camera.label] = ResizeValues(camSourceConfig.x.toLong(), camSourceConfig.y.toLong(), camSourceConfig.w.toLong(), camSourceConfig.h.toLong())
        }
    }

    /**
     * Tell all storage based actors to start acquiring data
     */
    fun startStoringData(){
        cameraDataStoreActors.forEach { x -> x.key.tell(StartAcquiring(), ActorRef.noSender()) }
        traceDataStoreActors.forEach { x -> x.key.tell(StartTraceStore(), ActorRef.noSender()) }
    }

    /**
     * This method is called when stopping an experiment. It will send a TerminateActor() message to all store-based
     * actors
     */
    fun stopStoringData(){
        GUIMain.loggerService.log(Level.INFO, "Stopping data store actors")

        cameraDataStoreActors.forEach { x ->
            GUIMain.actorService.removeActor(x.key)
            x.key.tell(PoisonPill.getInstance(), ActorRef.noSender())
        }
        traceDataStoreActors.forEach { x ->
            GUIMain.actorService.removeActor(x.key)
            x.key.tell(PoisonPill.getInstance(), ActorRef.noSender())
        }
    }

    /**
     * This method is called when stopping an experiment. It will send a TerminateActor() message to all display-based
     * actors
     */
    fun stopDisplayingData(){
        GUIMain.loggerService.log(Level.INFO, "Stopping display actors")

        cameraActors.forEach{ x ->
            x.key.tell(TerminateActor(), ActorRef.noSender())
            GUIMain.actorService.removeActor(x.key)
            x.value.stopLive()
            x.value.isActive = false
        }

        traceActors.forEach{ x ->
            x.key.tell(TerminateActor(), ActorRef.noSender())
            GUIMain.actorService.removeActor(x.key)
        }
    }

    /**
     * This method will create both a camera display dockable window plugin and an associated camera actor to go with it
     * @param cameraDeviceLabel The label of the camera whose data the plugin will be displaying
     * @return The newly created dockable window plugin
     */
    fun createCameraPluginWithActor(cameraDeviceLabel: String) : DockableWindowPlugin?{
        //Make sure the actor name is truly unique
        val numStoreActorsForDeivce = cameraActors.count { x -> x.value.label == cameraDeviceLabel }
        val cameraActorName = if(numStoreActorsForDeivce > 0) {
            "${cameraDeviceLabel}CameraActor${numStoreActorsForDeivce + 1}"
        }
        else{
            "${cameraDeviceLabel}CameraActor1"
        }

        val cameraDeviceInfo = cameraDevices.filter{ x -> x.device.label == cameraDeviceLabel}.first()
        val plugin = GUIMain.dockableWindowPluginService.createPlugin(CameraWindowPlugin::class.java, cameraDeviceInfo.device, true, cameraActorName)
        val newCameraActor = GUIMain.actorService.getActorByName(cameraActorName)
        if(newCameraActor != null){
            cameraActors[newCameraActor] = cameraDeviceInfo.device
        }

        return plugin
    }

    /**
     * This method will create both a trace display dockable window plugin and an associated trace actor to go with it.
     * @param traceDeviceLabel The label of the trace device relating to this trace display
     * @return A pair, where the first value is the newly created trace actor, the second value is the newly created
     * dockable window plugin
     */
    fun createTracePluginWithActor(traceDeviceLabel : String) : Pair<ActorRef?, DockableWindowPlugin?> {
        //Make sure the actor name is truly unique
        val numStoreActorsForDevice = traceActors.count { x -> x.value == traceDeviceLabel }
        val traceActorName = if(numStoreActorsForDevice > 0) {
            "${traceDeviceLabel}TraceActor${numStoreActorsForDevice + 1}"
        }
        else{
            "${traceDeviceLabel}TraceActor1"
        }

        val plugin = GUIMain.dockableWindowPluginService.createPlugin(TraceWindowPlugin::class.java, "", true, traceActorName)
        val newTraceActor = GUIMain.actorService.getActorByName(traceActorName)
        if(newTraceActor != null){
            traceActors[newTraceActor] = traceDeviceLabel
        }

        return Pair(newTraceActor, plugin)
    }

    /**
     * Create a camera data store actor. There will be one camera data store actor per camera feed (providing the user
     * has chosen to store the camera feed's data)
     * @param cameraDeviceLabel The camera device's label
     * @return The newly created camera data store actor
     */
    fun createCameraDataStoreActor(cameraDeviceLabel : String) : ActorRef {
        //Make sure the actor name is truly unique
        val numStoreActorsForDeivce = cameraDataStoreActors.count { x -> x.value == cameraDeviceLabel }
        val cameraDataStoreActorName = if(numStoreActorsForDeivce > 0) {
            "${cameraDeviceLabel}CameraDataStoreActor${numStoreActorsForDeivce + 1}"
        }
        else{
            "${cameraDeviceLabel}CameraDataStoreActor1"
        }

        val uniqueActorName = GUIMain.actorService.makeActorName(cameraDataStoreActorName)

        val cameraDataStoreActor = GUIMain.actorService.createActor(CameraDataStoreActor.props(), uniqueActorName, CameraDataStoreActor::class.java)
        cameraDataStoreActors[cameraDataStoreActor] = cameraDeviceLabel
        return cameraDataStoreActor
    }

    /**
     * Create a trace data store actor. There will be one trace data store actor per trace feed (providing the user
     * has chosen to store the trace feed's data)
     * @param traceDeviceLabel The trace device's label
     * @return The newly created trace data store actor
     */
    fun createTraceDataStoreActor(traceDeviceLabel : String) : ActorRef {
        //Make sure the actor name is truly unique
        val numStoreActorsForDeivce = traceDataStoreActors.count { x -> x.value == traceDeviceLabel }
        val traceDataStoreActorName = if(numStoreActorsForDeivce > 0) {
            "${traceDeviceLabel}TraceDataStoreActor${numStoreActorsForDeivce + 1}"
        }
        else{
            "${traceDeviceLabel}TraceDataStoreActor1"
        }

        val uniqueActorName = GUIMain.actorService.makeActorName(traceDataStoreActorName)

        val traceDataStoreActor = GUIMain.actorService.createActor(TraceDataStoreActor.props(), uniqueActorName, TraceDataStoreActor::class.java)
        traceDataStoreActors[traceDataStoreActor] = traceDeviceLabel
        return traceDataStoreActor
    }
}