package uk.co.strimm.plugins

import akka.NotUsed
import akka.actor.ActorRef
import org.scijava.command.Command
import org.scijava.command.ContextCommand
import org.scijava.plugin.*
import akka.stream.ClosedShape
import akka.stream.javadsl.*
import net.imagej.display.DefaultDatasetView
import net.imagej.overlay.Overlay
import uk.co.strimm.Acknowledgement
import uk.co.strimm.ExperimentConstants
import uk.co.strimm.TraceData
import uk.co.strimm.STRIMMImage
import uk.co.strimm.actors.TraceActor
import uk.co.strimm.actors.messages.complete.CompleteStreamingTraceROI
import uk.co.strimm.actors.messages.fail.FailStreamingTraceROI
import uk.co.strimm.actors.messages.start.*
import uk.co.strimm.actors.messages.tell.TellDeviceSamplingRate
import uk.co.strimm.actors.messages.tell.TellSetNumDataPoints
import uk.co.strimm.experiment.ExperimentImageSource
import uk.co.strimm.experiment.ROIManager
import uk.co.strimm.gui.GUIMain
import uk.co.strimm.services.AcquisitionMethodService
import uk.co.strimm.streams.ExperimentStream
import java.awt.Dimension
import java.awt.FlowLayout
import java.time.Duration
import java.util.*
import java.util.logging.Level
import javax.swing.*
import javax.swing.plaf.basic.BasicArrowButton
import kotlin.collections.ArrayList

/**
 * This class will handle the "Trace from ROI" right click event from overlays on image feeds. Upon right clicking on an
 * image feed and selecting "Trace from ROI", either a new trace window will be created or the trace will be added to
 * an existing trace window. When this is run, it will generate a new akka graph who's source is the broadcast hub
 * of the main experiment graph
 */
@Plugin(type = Command::class,
        menu=[(Menu(label = "Trace from ROI"))],
        menuRoot = "context-ImageDisplay",
        headless = true,
        attrs = [Attr(name="no-legacy")])
class TraceFromROIContextDisplay : ContextCommand() {
    companion object {
        const val DIALOG_HEIGHT = 200
        const val DIALOG_WIDTH = 400
    }

    override fun run() {
        setUpDialog()
    }

    /**
     * This method will create and show a new dialog to specify the relevant settings for the new trace feed
     */
    fun setUpDialog(){
        val currentStream = GUIMain.experimentService.experimentStream
        val availableImageImageFlows = currentStream.experimentImageImageFlows.map { x -> x.imgImgFlowName }
        val availableImageTraceFlows = currentStream.experimentImageTraceFlows.map { x -> x.imgTraceFlowName }
        val availableTraceImageFlows = currentStream.experimentTraceImageFlows.map { x -> x.traceImgFlowName }
        val availableTraceTraceFlows = currentStream.experimentTraceTraceFlows.map { x -> x.traceTraceFlowName }
        val availableImageSinks = currentStream.experimentImageSinks.map { x -> x.imageSinkName }
        val availableTraceSinks = currentStream.experimentTraceSinks.map { x -> x.traceSinkName }
        val allAvailableNodes = arrayOf(availableImageImageFlows,
                availableImageTraceFlows,
                availableTraceImageFlows,
                availableTraceTraceFlows,
                availableImageSinks,
                availableTraceSinks)
                .flatMap { x -> x }.toTypedArray()

        //TODO work out how to link up and specify flows for the separate graph used for trace from ROI
        val availableFlowsModel = DefaultListModel<String>()
        val availableFlowsList = JList<String>(availableFlowsModel)
        allAvailableNodes.forEach { x -> availableFlowsModel.addElement(x) }
        availableFlowsList.layoutOrientation = JList.VERTICAL_WRAP
        availableFlowsList.selectionMode = ListSelectionModel.SINGLE_INTERVAL_SELECTION
        val availableFlowListScroller = JScrollPane(availableFlowsList)

        val selectedFlowsModel = DefaultListModel<String>()
        val selectedFlowsList = JList<String>(selectedFlowsModel)
        val selectedFlowsListScroller = JScrollPane(selectedFlowsList)

        val arrowButton = BasicArrowButton(BasicArrowButton.EAST)
        addArrowButtonListener(arrowButton, availableFlowsList, selectedFlowsModel)

        val windowModel = DefaultComboBoxModel<String>()
        windowModel.addElement(ExperimentConstants.ConfigurationProperties.NEW_WINDOW)
        val allTraceActors = GUIMain.actorService.getActorsOfType(TraceActor::class.java)
        allTraceActors.forEach { x -> windowModel.addElement(GUIMain.actorService.getActorPrettyName(x.path().name())) }

        val windowOptionList = JComboBox<String>(windowModel)

        val chkStore = JCheckBox("Store Trace from ROI data?")
        chkStore.isSelected = true

        val dialogLayout = FlowLayout()

        val panel = JPanel()
        panel.layout = dialogLayout
        panel.preferredSize = Dimension(DIALOG_WIDTH, DIALOG_HEIGHT)
        panel.add(availableFlowListScroller)
        panel.add(arrowButton)
        panel.add(selectedFlowsListScroller)
        panel.add(windowOptionList)
        panel.add(chkStore)

        val result = JOptionPane.showConfirmDialog(GUIMain.strimmUIService.strimmFrame, panel, "Trace from ROI", JOptionPane.OK_CANCEL_OPTION)
        if(result == JOptionPane.OK_OPTION){
            createTraceFeedForROI(selectedFlowsModel, windowOptionList, currentStream, chkStore.isSelected)
        }
    }

    /**
     * This method takes the selections from the user and carries out the necessary steps to display the trace from ROI
     * @param selectedFlowsModel A model containing the list of elements that correspond to the flows the user has chosen
     * @param windowOptionList The combobox showing which window to use to display
     * @param currentStream The currently loaded and running experiment stream
     * @param isStore The option of if the trace from ROI data should be stored also
     */
    fun createTraceFeedForROI(selectedFlowsModel : DefaultListModel<String>, windowOptionList : JComboBox<String>, currentStream : ExperimentStream, isStore : Boolean){
        val activeDisplays = GUIMain.imageDisplayService.activeImageDisplay
        val selectedROI = GUIMain.overlayService.getActiveOverlay(activeDisplays)
        val cameraActor = GUIMain.actorService.getPertainingCameraActorFromDisplay(activeDisplays)
        val cameraDeviceLabel = GUIMain.actorService.getPertainingCameraDeviceLabelForActor(cameraActor!!)

        selectedROI.name = cameraDeviceLabel + "TraceROI" + Random().nextInt(1000)
        var source : ExperimentImageSource? = null

        //To get the correct camera source node, get the label of the related camera device, then find it in the list of
        //experiment sources
        for(activeDisplay in activeDisplays){
            when(activeDisplay){
                is DefaultDatasetView -> {
                    if(activeDisplay.data.name.toLowerCase().contains(cameraDeviceLabel!!.toLowerCase())){
                        source = currentStream.experimentImageSources.first{ x -> cameraActor.path().name().contains(x.deviceLabel)}
                    }
                }
            }
        }

        val traceActor : ActorRef?

        if(windowOptionList.selectedItem.toString() == ExperimentConstants.ConfigurationProperties.NEW_WINDOW){
            //New window means new actor and new trace window plugin

            val pluginCreation = currentStream.createTracePluginWithActor(cameraDeviceLabel!!)
            pluginCreation.second?.dock(GUIMain.strimmUIService.dockableControl, GUIMain.strimmUIService.strimmFrame)

            traceActor = currentStream.traceActors.filter { x -> x.key.path().name() == pluginCreation.first!!.path().name()}.keys.first()
        }
        else {
            //Existing window means find the trace actor that already exists
            selectedROI.name = GUIMain.experimentService.makeUniqueROIName(cameraDeviceLabel!!)

            val selectedActorName = windowOptionList.selectedItem.toString().toLowerCase()
            val allTraceActors = GUIMain.actorService.getActorsOfType(TraceActor::class.java)
            traceActor = allTraceActors.first{ x -> GUIMain.actorService.getActorPrettyName(x.path().name()).toLowerCase() == selectedActorName }
        }

        //Due to image sources being broadcast hubs, we can dynamically create another stream from the existing
        //sinks. See populateSources() in ExperimentStream for where the broadcast hub is specified
        if(source != null){
            runTraceROIGraph(traceActor, currentStream, selectedROI, source, isStore, cameraActor, windowOptionList.selectedItem.toString())
        }
        else{
            GUIMain.loggerService.log(Level.SEVERE, "Could not find the source node pertaining to the roi display")
        }
    }

    /**
     * Once the trace from ROI settings have been specified, create a simple graph to allow for it's display. This graph
     * is not the main experiment stream graph
     * @param traceActor The new trace actor that will serve the TraceWindowPlugin
     * @param currentStream The currently loaded (and running) experiment stream
     * @param selectedROI The selected ROI (ImageJ overlay)
     * @param source The source object of the camera source
     * @param isStore The option of if the trace from ROI data should be stored also
     * @param cameraActor The camera actor pertaining to the image feed where the ROI has been drawn
     */
    private fun runTraceROIGraph(traceActor : ActorRef, currentStream: ExperimentStream, selectedROI : Overlay, source : ExperimentImageSource, isStore: Boolean, cameraActor: ActorRef, windowOption : String){
        val baseFlowName = source.imgSourceName + "TraceROIFlow"
        val flowName = GUIMain.experimentService.makeUniqueFlowName(baseFlowName)
        //Even though the experiment stream will be rebuilt once the user is done specifying ROIs, we will still use
        //the routed ROI list for reference
        GUIMain.actorService.routedRoiList[selectedROI] = Pair(cameraActor, flowName)

        val averageROIAcquisitionMethod = GUIMain.acquisitionMethodService.getAcquisitionMethod(ExperimentConstants.Acquisition.AVERAGE_ROI_METHOD_NAME) as AcquisitionMethodService.TraceMethod

        val akkaFlow = Flow.of(STRIMMImage::class.java)
            .map { image -> averageROIAcquisitionMethod.runMethod(image, flowName) }
            .groupedWithin(ExperimentConstants.ConfigurationProperties.TRACE_GROUPING_AMOUNT, Duration.ofMillis(ExperimentConstants.ConfigurationProperties.TRACE_GROUPING_DURATION_MS))
            .async()

        val akkaSink: Sink<List<ArrayList<TraceData>>, NotUsed> = Sink.actorRefWithAck(traceActor, StartStreamingTraceROI(),
                Acknowledgement.INSTANCE, CompleteStreamingTraceROI()) { ex -> FailStreamingTraceROI(ex) }

        val graph = GraphDSL.create { builder ->
            val sourceShape = builder.add(source.roiSource)
            val flowShape = builder.add(akkaFlow)
            val sinkShape = builder.add(akkaSink)
            builder.from(sourceShape).via(flowShape).to(sinkShape)

            ClosedShape.getInstance()
        }

        traceActor.tell(TellDeviceSamplingRate(10.0), ActorRef.noSender())//TODO hardcoded
        traceActor.tell(TellSetNumDataPoints(), ActorRef.noSender())
        RunnableGraph.fromGraph(graph).run(GUIMain.actorService.materializer)
//        GUIMain.experimentService.calculateNumberOfDataPointsFromInterval(selectedROI.name, source.intervalMs)
        GUIMain.experimentService.calculateNumberOfDataPointsFromFrequency(selectedROI.name, 10.0)//TODO hardcoded
        createAndAddToNewConfig(source, isStore, selectedROI, currentStream, windowOption, traceActor)
    }

    /**
     * Based on the user's specifications for the trace from ROI, store these specifications for use in creating a new
     * config
     * @param sourceNode The experiment image source that the trace ROI is reading from
     * @param isStore The flag to store or not store the trace ROI data in addition to displaying it
     * @param selectedROI The overlay (ROI) object
     * @param currentStream The currently loaded and running experiment stream
     * @param windowOption The destination trace feed of the new trace ROI (new window or existing)
     * @param traceActor The trace actor relating to the target trace feed display
     */
    fun createAndAddToNewConfig(sourceNode : ExperimentImageSource, isStore : Boolean, selectedROI : Overlay, currentStream: ExperimentStream, windowOption : String, traceActor: ActorRef){
        //Create a new config first by copying the existing one
        GUIMain.experimentService.createNewConfigFromExisting()

        //Important to do this so we keep track of any new trace ROIs that have been created and what devices they are
        //associated with
        currentStream.newTraceROIActors[traceActor] = sourceNode.deviceLabel

        //Specify new flows
        val flowToAdd = uk.co.strimm.experiment.Flow()
        val baseFlowName = sourceNode.imgSourceName + "TraceROIFlow"
        flowToAdd.flowName = GUIMain.experimentService.makeUniqueFlowName(baseFlowName)
        flowToAdd.inputNames = arrayListOf(sourceNode.imgSourceName)
        flowToAdd.inputType = ExperimentConstants.ConfigurationProperties.IMAGE_INPUT_TYPE
        flowToAdd.outputType = ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE
        GUIMain.experimentService.addFlowsToNewConfig(listOf(flowToAdd))

        //Specify new sinks
        val displaySinkToAdd = uk.co.strimm.experiment.Sink()
        val baseDisplaySinkName = sourceNode.imgSourceName + "TraceROIDisplay"
        displaySinkToAdd.sinkName = GUIMain.experimentService.makeUniqueSinkName(baseDisplaySinkName)
        displaySinkToAdd.inputNames = arrayListOf(flowToAdd.flowName)
        displaySinkToAdd.displayOrStore = ExperimentConstants.ConfigurationProperties.DISPLAY
        displaySinkToAdd.outputType = ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE
        displaySinkToAdd.primaryDevice = sourceNode.deviceLabel
        displaySinkToAdd.actorPrettyName = GUIMain.actorService.getActorPrettyName(traceActor.path().name())

        val isNewTraceFeed = isAddingToNewTraceFeed(traceActor, currentStream)

        if(isStore) {
            val storeSinkToAdd = uk.co.strimm.experiment.Sink()
            val baseStoreSinkName = sourceNode.imgSourceName + "TraceROIStore"
            storeSinkToAdd.sinkName = GUIMain.experimentService.makeUniqueSinkName(baseStoreSinkName)
            storeSinkToAdd.inputNames = arrayListOf(flowToAdd.flowName)
            storeSinkToAdd.displayOrStore = ExperimentConstants.ConfigurationProperties.STORE
            storeSinkToAdd.outputType = ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE
            storeSinkToAdd.primaryDevice = sourceNode.deviceLabel
            storeSinkToAdd.actorPrettyName = GUIMain.actorService.getActorPrettyName(traceActor.path().name())

            GUIMain.experimentService.addSinksToNewConfig(listOf(displaySinkToAdd, storeSinkToAdd), windowOption, isNewTraceFeed)
        }
        else{
            GUIMain.experimentService.addSinksToNewConfig(listOf(displaySinkToAdd), windowOption, isNewTraceFeed)
        }

        //As we are closing down and loading up a modified experiment, create an ROI object so the ROI drawn can be recreated
        val roiToAdd = ROIManager.createROIObjectFromOverlay(selectedROI, sourceNode.imgSourceName, sourceNode.deviceLabel, flowToAdd.flowName)

        GUIMain.overlayService.removeOverlay(selectedROI)
        GUIMain.experimentService.addROIsToNewConfig(listOf(roiToAdd))
    }

    /**
     * Method used to determine if the trace from ROI is going to any trace feed that has been created in this trace from ROI context
     * @param traceActor The trace actor for the target trace feed
     * @param currentStream The current experiment stream
     * @return If the target feed has been created in a trace from ROI context
     */
    fun isAddingToNewTraceFeed(traceActor : ActorRef, currentStream: ExperimentStream) : Boolean{
        return traceActor.path().name() in currentStream.newTraceROIActors.map { x -> x.key.path().name() }
    }

    /**
     * When specifying a new trace from ROI, we specify what flows the trace from ROI source can be connected to.
     * This arrow button will allow the user to select which flows to use
     * @param arrowButton The arrow button
     * @param nodeList The list of available flows
     * @param selectedFlowsModel The list
     */
    private fun addArrowButtonListener(arrowButton: BasicArrowButton, nodeList : JList<String>, selectedFlowsModel : DefaultListModel<String>){
        arrowButton.addActionListener {
            //TODO specify enable/disable logic here i.e. what nodes can go to what other nodes
            val selectedNode = nodeList.selectedValue
            selectedFlowsModel.addElement(selectedNode)
        }
    }
}

