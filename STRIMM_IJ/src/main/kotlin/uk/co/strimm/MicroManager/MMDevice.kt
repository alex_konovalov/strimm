package uk.co.strimm.MicroManager

import com.fazecast.jSerialComm.SerialPort
import mmcorej.DeviceType
import mmcorej.PropertyType
import uk.co.strimm.gui.GUIMain
import uk.co.strimm.services.MMCoreService
import java.io.Closeable
import java.util.logging.Level
import com.fazecast.jSerialComm.SerialPortEvent
import mmcorej.MMCoreJJNI
import uk.co.strimm.TimerResult
import java.util.*


private fun rn(name : String) = name + java.util.UUID.randomUUID().toString()

sealed class MMDevice(val devName : String, val library : String, val label : String, val loaded : Boolean) : Closeable {
    private val core = MMCoreService.core

    init {
        if (!loaded)
            core.loadDevice(label, library, devName)
    }

    override fun close() {
        core.unloadDevice(label)
    }

    fun getProperties() =
            core.getDevicePropertyNames(label)
                    .map { propName ->
                        val type = core.getPropertyType(label, propName)
                        when (type) {
                            PropertyType.Float -> MMFloatProperty(label, propName)
                            PropertyType.Integer -> MMIntProperty(label, propName)
                            PropertyType.String -> MMStringProperty(label, propName)
                            else -> MMUnknownProperty(label, propName)
                        }
                    }

    fun initialise() {
        try {
            core.initializeDevice(label)
        }
        catch(ex : Exception){}
    }

    fun isBusy() = core.deviceBusy(label)

    companion object {
        val deviceType : DeviceType = DeviceType.UnknownType
    }
}

class MMAutoFocusDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.AutoFocusDevice
    }
}

class MMCameraDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded){

    enum class MMBytesPerPixel(val bytesPerPixel: Long)
    {
        Bit8(1),
        Bit16(2),
        Bit32(4),
        Unknown(-1)
    }

    private fun <T>tempActivate(func : () -> T) : T =
            if (MMCoreService.core.cameraDevice == label) func()
            else MMCoreService.core.cameraDevice
                    .let { prevCam ->
                        MMCoreService.core.cameraDevice = label

                        func().apply {
                            MMCoreService.core.cameraDevice = prevCam
                        }
                    }

    private var live = false
    private var previewMode = false
    private var acquisitionCount = 0
    private var acquisitionInterval = 0.0
    private var MMMetadataTagName = "ElapsedTime-ms"

    var isActive
            get() = MMCoreService.core.cameraDevice == label
            set(value) { if (value) MMCoreService.core.cameraDevice = label else MMCoreService.core.cameraDevice = null}


    val isImageAvailable
        get() = !isBusy() && MMCoreService.core.remainingImageCount > 0

    fun startLivePreview(interval : Double){
        if (!isActive) isActive = true
        MMCoreService.core.initializeCircularBuffer()
        GUIMain.loggerService.log(Level.INFO,"Starting preview mode (continuous sequence acquisition)")
        MMCoreService.core.startContinuousSequenceAcquisition(interval)
        acquisitionInterval = interval
        live = true
        previewMode = true

        //TODO: Make work without bad functions
        if (GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.prime() == TimerResult.Error)
            println(GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.getLastError())
        if (GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.start() == TimerResult.Error)
            println(GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.getLastError())
    }

    var timeArduinoTold = 0.0.toLong()
    var timeArduinoReplied = 0.0.toLong()
    var timeAcquisitionStartedBefore = 0.0.toLong()
    var timeAcquisitionStartedAfter = 0.0.toLong()

    fun startArduino() {
        val portToUse = SerialPort.getCommPort("COM5")
        portToUse.setComPortParameters(9600, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY)
        portToUse.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0)

        if (!portToUse.isOpen) {
            println("Port is not open")
            if (portToUse.openPort()) {
                println("Opened port")
            } else {
                println("Failed to open port")
            }
        } else {
            println("Port is already open")
        }

        println("Sending byte")
        val data = 1.toByte()
        for (i in 0..10000){
            if(i==0){
                timeArduinoTold = System.currentTimeMillis()
            }
            portToUse.outputStream.write(byteArrayOf(data))
            if(portToUse.inputStream.available() > 0 && i > 0){
                timeArduinoReplied = System.currentTimeMillis()
                break
            }
        }
    }

    fun startLiveAcquisition(interval : Double) {
        if (!isActive) isActive = true

        GUIMain.loggerService.log(Level.INFO,"Starting sequence acquisition")
        try {
            val numImagesToCapture = GUIMain.experimentService.deviceDatapointNumbers[label]!!
            acquisitionCount = numImagesToCapture
//            startArduino()
            timeAcquisitionStartedBefore = System.currentTimeMillis()
            MMCoreService.core.prepareSequenceAcquisition(label)
            MMCoreService.core.startSequenceAcquisition(label, numImagesToCapture, interval, false)
            timeAcquisitionStartedAfter= System.currentTimeMillis()
//            println("Time arduino told: $timeArduinoTold")
//            println("Time arduino replied: $timeArduinoReplied")
//            println("Time acquisition started before: $timeAcquisitionStartedBefore")
//            println("Time acquisition started after: $timeAcquisitionStartedAfter")
            live = true
            previewMode = false

            //Needed to account for the delay from starting the sequence acquisition, to actually getting the first frame
            Thread.sleep(100)

            //TODO: Make work without bad functions
            if (GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.prime() == TimerResult.Error)
                println(GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.getLastError())
            if (GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.start() == TimerResult.Error)
                println(GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.getLastError())
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Failed to start acquisition. Error message: ${ex.message}")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
        }
    }

    fun stopLive() {
        GUIMain.loggerService.log(Level.INFO,"Stopping sequence acquisition")
        MMCoreService.core.stopSequenceAcquisition()

        //Make sure any previous sequence has stopped running
        while(MMCoreService.core.isSequenceRunning(label)){}

        MMCoreService.core.clearCircularBuffer()

        live = false

//        //TODO: Make not bad
        GUIMain.timerService.GET_TIMER_BAD_REMOVE_ME_PLEASE().timer.stop()
        GUIMain.timerService.resetAnalogueDataStreams()
    }

    fun snapImage() : Pair<Any, Double>{
        if(previewMode) {
            while(MMCoreService.core.remainingImageCount < 1){}

            val timeStampString = MMCoreService.core.lastTaggedImage.tags[MMMetadataTagName] as String
            return Pair(MMCoreService.core.lastTaggedImage.pix, timeStampString.toDouble())
        }
        else{
            //Wait a little so the buffer has enough images from an acquisition
            while(MMCoreService.core.remainingImageCount < 1){}

            /*
                As the buffer has been cleared before the start of sequence acquisition, we can safely assume that
                any images in the buffer are from the acquisition, we can now "pop" them from the buffer
             */
            val pop = MMCoreService.core.popNextTaggedImage()
            val timeStampString = pop.tags[MMMetadataTagName] as String

            acquisitionCount--

            if(acquisitionCount < 1){
                GUIMain.loggerService.log(Level.INFO, "Stopping sequence acquisition, starting continuous acquisition (preview mode)")
                stopLive()
                startLivePreview(acquisitionInterval)
            }

            return Pair(pop.pix, timeStampString.toDouble())
        }
    }

    val imageWidth get() = tempActivate { MMCoreService.core.imageWidth }
    val imageHeight get() = tempActivate { MMCoreService.core.imageHeight }
    val numberOfChannels get() = tempActivate { MMCoreService.core.numberOfCameraChannels }
    val bytesPerPixel get() = tempActivate {
        MMBytesPerPixel.values().find { MMCoreService.core.bytesPerPixel == it.bytesPerPixel } ?: MMBytesPerPixel.Unknown }
    val bitDepth get() = tempActivate { MMCoreService.core.imageBitDepth }

    var exposure
        get() = tempActivate { MMCoreService.core.exposure }
        set(value) { tempActivate { MMCoreService.core.exposure = value
                                    MMCoreService.core.setExposure(label, value) }
                   }

    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.CameraDevice
    }

}

class MMGalvoDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.GalvoDevice
    }
}

class MMSLMDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.SLMDevice
    }
}
class MMSerialDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.SerialDevice
    }
}
class MMShutterDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.ShutterDevice
    }
}
class MMSignalIODevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.SignalIODevice
    }
}
class MMStageDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.StageDevice
    }
}
class MMStateDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.StageDevice
    }
}
class MMXYStageDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.XYStageDevice
    }
}
class MMAnyDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.AnyType
    }
}
class MMCoreDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.CoreDevice
    }
}
class MMGenericDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.GenericDevice
    }
}
class MMHubDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.HubDevice
    }
}
class MMImageProcessorDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.ImageProcessorDevice
    }
}
class MMMagnifierDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded) {
    companion object {
        @JvmStatic
        val deviceType: DeviceType = DeviceType.MagnifierDevice
    }
}
class MMUnknownDevice(name : String, library: String, label : String = rn(name), loaded: Boolean = false)
    : MMDevice(name, library, label, loaded)
