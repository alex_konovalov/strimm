package uk.co.strimm.gui

import bibliothek.gui.dock.common.CControl
import bibliothek.gui.dock.common.CGrid
import io.scif.services.DatasetIOService
import javafx.application.Platform
import net.imagej.DatasetService
import net.imagej.display.ImageDisplayService
import org.scijava.command.Command
import org.scijava.display.DisplayService
import org.scijava.io.IOService
import org.scijava.plugin.Parameter
import org.scijava.plugin.Plugin
import org.scijava.ui.UIService
import net.imagej.ImageJService
import net.imagej.ImgPlusService
import org.scijava.command.CommandService
import org.scijava.menu.MenuService
import org.scijava.event.EventService
import org.scijava.module.ModuleService
import org.scijava.plugin.PluginInfo
import org.scijava.plugin.PluginService
import org.scijava.thread.ThreadService
import org.scijava.ui.swing.sdi.SwingSDIUI
import uk.co.strimm.plugins.*
import java.util.logging.Level
import net.imagej.app.QuitProgram
import net.imagej.display.DefaultOverlayService
import net.imagej.display.OverlayService
import net.imagej.display.WindowService
//import net.imagej.legacy.LegacyService
import net.imagej.ops.OpService
import net.imagej.roi.ROIService
import net.imagej.ui.ImageJUIService
import org.scijava.`object`.ObjectService
import org.scijava.convert.ConvertService
import org.scijava.display.DefaultDisplayService
import org.scijava.ui.swing.SwingToolBar
import uk.co.strimm.*
import uk.co.strimm.MicroManager.MMCameraDevice
import uk.co.strimm.services.*
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.Image
import java.awt.event.WindowAdapter
import java.io.File
import javax.imageio.ImageIO
import javax.swing.JFrame
import javax.swing.*

/**
 * Main GUI for the STRIMM plugin
 */
@Plugin(type = Command::class, headless = true, menuPath = "Plugins>STRIMM")
class GUIMain : Command {
    override fun run() {
        loggerService.log(Level.INFO, "Starting GUI")

        mmService.getLibraries()
        
        initAndShowGUI()
    }

    /**
     * Method to initialise and load the various GUI components of the main STRÏMM window
     */
    fun initAndShowGUI() {
        strimmUIService.dockableControl = CControl(strimmUIService.strimmFrame)
        strimmUIService.strimmFrame.add(strimmUIService.dockableControl.contentArea)
        strimmUIService.cGrid = CGrid(strimmUIService.dockableControl)
        strimmUIService.dockableControl.contentArea.deploy(strimmUIService.cGrid)
        strimmUIService.strimmFrame.layout = BorderLayout()

        val defaultUI = (uiService.defaultUI as SwingSDIUI)
        uiService.defaultUI.applicationFrame.setVisible(false)
        defaultUI.toolBar.isFloatable = false

        val buttonToolBar = defaultUI.toolBar.rootPane.contentPane.components[0] as SwingToolBar
        addAcquisitionButtons(buttonToolBar)

        strimmUIService.strimmFrame.add(defaultUI.toolBar.rootPane, BorderLayout.NORTH)
        strimmUIService.strimmFrame.setSize(Program.PROGRAM_DEFAULT_HEIGHT, Program.PROGRAM_DEFAULT_WIDTH)
        strimmUIService.strimmFrame.isVisible = true
        setOnClose(strimmUIService.strimmFrame)


        addMenuDockableWindowPlguins(
                dockableWindowPluginService.plugins.filterNot { it -> it.menuPath.size == 0 },
                (uiService.defaultUI as SwingSDIUI).applicationFrame.rootPane.jMenuBar)

        strimmUIService.imageJMenuBar = (uiService.defaultUI as SwingSDIUI).applicationFrame.rootPane.jMenuBar
        Platform.setImplicitExit(false)


        // TODO: Remove me! I am a hack because Elliot is too lazy to finish the GUI
//        timerService.REMOVE_ME()
    }

    //region Experiment buttons
    /**
     * Add custom buttons relating to experiment acquisition
     * @param imageJButtonBar The ImageJ button bar where the new buttons will be added
     */
    private fun addAcquisitionButtons(imageJButtonBar : SwingToolBar){
        val firstButton = imageJButtonBar.components[0] as JToggleButton
        imageJButtonBar.addSeparator()

        autoScaleImageButton.maximumSize = Dimension(firstButton.width+30, firstButton.height+15)
        autoScaleImageButton.toolTipText = ComponentTexts.AcquisitionButtons.SCALE_IMAGE_TOOLTIP
        autoScaleImageButton.isSelected = false
        autoScaleImageButton.text = "Auto scale image"
        setLoadExperimentConfigButtonImage(firstButton.width, firstButton.height)

        loadExperimentConfigButton.maximumSize = Dimension(firstButton.width+15,firstButton.height+15)
        loadExperimentConfigButton.toolTipText = ComponentTexts.AcquisitionButtons.LOAD_BUTTON_TOOLTIP
        setLoadExperimentConfigButtonImage(firstButton.width, firstButton.height)

        roiSpecCompleteButton.maximumSize = Dimension(firstButton.width+15,firstButton.height+15)
        roiSpecCompleteButton.toolTipText = ComponentTexts.AcquisitionButtons.ROICOMPLETE_BUTTON_TOOLTIP
        roiSpecCompleteButton.isEnabled = false
        setRoiSpecCompleteButtonImage(firstButton.width, firstButton.height)

        startExperimentButton.maximumSize = Dimension(firstButton.width+15,firstButton.height+15)
        startExperimentButton.toolTipText = ComponentTexts.AcquisitionButtons.START_BUTTON_TOOLTIP
        startExperimentButton.isEnabled = experimentService.loadedConfigurationStream != null
        setStartExperimentButtonImage(firstButton.width, firstButton.height)

        stopExperimentButton.maximumSize = Dimension(firstButton.width+15,firstButton.height+15)
        stopExperimentButton.toolTipText = ComponentTexts.AcquisitionButtons.STOP_BUTTON_TOOLTIP
        setStopExperimentButtonImage(firstButton.width, firstButton.height)

        resizeToROIButton.maximumSize = Dimension(firstButton.width+15,firstButton.height+15)
        resizeToROIButton.toolTipText = ComponentTexts.AcquisitionButtons.RESIZE_ROI_TOOLTIP
        resizeToROIButton.isEnabled = false
        setResizeROIButtonImage(firstButton.width, firstButton.height)

        fullViewButton.maximumSize = Dimension(firstButton.width+15,firstButton.height+15)
        fullViewButton.toolTipText = ComponentTexts.AcquisitionButtons.FULL_VIEW_TOOLTIP
        fullViewButton.isEnabled = false
        setFullViewButtonImage(firstButton.width, firstButton.height)

        loadPrevExperimentButton.maximumSize = Dimension(firstButton.width+15,firstButton.height+15)
        loadPrevExperimentButton.toolTipText = ComponentTexts.AcquisitionButtons.LOAD_PREV_EXPERIMENT_TOOLTIP
        setLoadPrevExperimentButtonImage(firstButton.width, firstButton.height)

//        scaleImageButton.maximumSize = Dimension(firstButton.width+15,firstButton.height+15)
//        scaleImageButton.toolTipText = ComponentTexts.AcquisitionButtons.SCALE_IMAGE_TOOLTIP
//        setLoadPrevExperimentButtonImage(firstButton.width, firstButton.height)

        imageJButtonBar.add(loadExperimentConfigButton)
        imageJButtonBar.addSeparator()
        imageJButtonBar.add(roiSpecCompleteButton)
        imageJButtonBar.addSeparator()
        imageJButtonBar.add(startExperimentButton)
        imageJButtonBar.addSeparator()
        imageJButtonBar.add(stopExperimentButton)
        imageJButtonBar.addSeparator()
        imageJButtonBar.add(resizeToROIButton)
        imageJButtonBar.addSeparator()
        imageJButtonBar.add(fullViewButton)
        imageJButtonBar.addSeparator()
        imageJButtonBar.add(loadPrevExperimentButton)
        imageJButtonBar.addSeparator()
        imageJButtonBar.add(autoScaleImageButton)

        addLoadButtonListener()
        addROISpecCompleteListener()
        addStartButtonListener()
        addStopButtonListener()
        addResizeToROIButtonListener()
        addFullViewButtonListener()
        addLoadPreviousExperimentButtonListener()
        addAutoScaleButtonListener()

    }

    /**
     * Specify the load experiment button's image
     * @param width The width of the first button in ImageJ's toolbar to use as a reference
     * @param height The width of the first button in ImageJ's toolbar to use as a reference
     */
    private fun setLoadExperimentConfigButtonImage(width : Int, height : Int){
        try {
            val img = ImageIO.read(javaClass.getResource(Paths.Icons.LOAD_ICON))
            val scaledImg = img.getScaledInstance(width, height, Image.SCALE_SMOOTH)
            loadExperimentConfigButton.icon = ImageIcon(scaledImg)
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.WARNING, "Could not load load experiment button icon")
            GUIMain.loggerService.log(Level.WARNING, ex.stackTrace)
        }
    }

    /**
     * Specify the ROI spec complete button's image
     * @param width The width of the first button in ImageJ's toolbar to use as a reference
     * @param height The width of the first button in ImageJ's toolbar to use as a reference
     */
    private fun setRoiSpecCompleteButtonImage(width : Int, height : Int){
        try {
            val img = ImageIO.read(javaClass.getResource(Paths.Icons.ROI_SPEC_COMPLETE_ICON))
            roiSpecCompleteButton.icon = ImageIcon(img.getScaledInstance(width,height, Image.SCALE_SMOOTH))
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.WARNING, "Could not load ROI specification complete button icon")
            GUIMain.loggerService.log(Level.WARNING, ex.stackTrace)
        }
    }

    /**
     * Specify the start experiment button's image
     * @param width The width of the first button in ImageJ's toolbar to use as a reference
     * @param height The width of the first button in ImageJ's toolbar to use as a reference
     */
    private fun setStartExperimentButtonImage(width : Int, height : Int){
        try {
            val img = ImageIO.read(javaClass.getResource(Paths.Icons.START_ICON))
            startExperimentButton.icon = ImageIcon(img.getScaledInstance(width, height, Image.SCALE_SMOOTH))
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.WARNING, "Could not load start button icon")
            GUIMain.loggerService.log(Level.WARNING, ex.stackTrace)
        }
    }

    /**
     * Specify the resize to ROI button's image
     * @param width The width of the first button in ImageJ's toolbar to use as a reference
     * @param height The width of the first button in ImageJ's toolbar to use as a reference
     */
    private fun setResizeROIButtonImage(width : Int, height : Int){
        try {
            val img = ImageIO.read(javaClass.getResource(Paths.Icons.RESIZE_ROI_ICON))
            resizeToROIButton.icon = ImageIcon(img.getScaledInstance(width, height, Image.SCALE_SMOOTH))
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.WARNING, "Could not load resize roi button icon")
            GUIMain.loggerService.log(Level.WARNING, ex.stackTrace)
        }
    }

    /**
     * Specify the full view button's image
     * @param width The width of the first button in ImageJ's toolbar to use as a reference
     * @param height The width of the first button in ImageJ's toolbar to use as a reference
     */
    private fun setFullViewButtonImage(width : Int, height : Int){
        try {
            val img = ImageIO.read(javaClass.getResource(Paths.Icons.FULL_VIEW_ICON))
            fullViewButton.icon = ImageIcon(img.getScaledInstance(width, height, Image.SCALE_SMOOTH))
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.WARNING, "Could not load full view button icon")
            GUIMain.loggerService.log(Level.WARNING, ex.stackTrace)
        }
    }

    /**
     * Specify the load previous experiment button's image
     * @param width The width of the first button in ImageJ's toolbar to use as a reference
     * @param height The width of the first button in ImageJ's toolbar to use as a reference
     */
    private fun setLoadPrevExperimentButtonImage(width : Int, height : Int){
        try {
            val img = ImageIO.read(javaClass.getResource(Paths.Icons.LOAD_PREV_ICON))
            loadPrevExperimentButton.icon = ImageIcon(img.getScaledInstance(width, height, Image.SCALE_SMOOTH))
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.WARNING, "Could not load load previous experiment button icon")
            GUIMain.loggerService.log(Level.WARNING, ex.stackTrace)
        }
    }

    /**
     * Specify the stop experiment button's image
     * @param width The width of the first button in ImageJ's toolbar to use as a reference
     * @param height The width of the first button in ImageJ's toolbar to use as a reference
     */
    private fun setStopExperimentButtonImage(width : Int, height : Int){
        try {
            val img = ImageIO.read(javaClass.getResource(Paths.Icons.STOP_ICON))
            stopExperimentButton.icon = ImageIcon(img.getScaledInstance(width, height, Image.SCALE_SMOOTH))
            stopExperimentButton.isEnabled = false
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.WARNING, "Could not load stop button icon")
            GUIMain.loggerService.log(Level.WARNING, ex.stackTrace)
        }
    }

    /**
     * Specify logic for loading a new experiment configuration
     */
    private fun addLoadButtonListener(){
        loadExperimentConfigButton.addActionListener{
            val folder = File(Paths.EXPERIMENT_CONFIG_FOLDER)
            val fileList = folder.listFiles({ f -> f.extension == "json" })
            if(fileList != null && fileList.isNotEmpty()) {
                val fileComboBox = JComboBox(fileList.map { f -> f.name.replace(".json", "") }.toTypedArray())
                fileComboBox.selectedIndex = 0
                JOptionPane.showMessageDialog(GUIMain.strimmUIService.strimmFrame, fileComboBox, "Select experiment configuration", JOptionPane.QUESTION_MESSAGE)

                val selectedConfigFile = fileComboBox.selectedItem as String
                val selectedFile = fileList.find { f -> f.name.replace(".json", "") == selectedConfigFile }!!
                val loadSuccess = experimentService.convertGsonToConfig(selectedFile)
                if(loadSuccess){
                    experimentService.createExperimentStream(true)
                    createStreamGraph()

                    if(experimentService.expConfig.ROIAdHoc.toLowerCase() == "true") {
                        showROISpecPrompt()
                    }
                    else{
                        roiSpecCompleteButton.isEnabled = false
                        resizeToROIButton.isEnabled = true
                        fullViewButton.isEnabled = true
                        startExperimentButton.isEnabled = true
                        stopExperimentButton.isEnabled = false
                    }
                }
                else{
                    JOptionPane.showMessageDialog(GUIMain.strimmUIService.strimmFrame, ComponentTexts.AcquisitionDialogs.ERROR_LOADING_EXPCONFIG)
                    startExperimentButton.isEnabled = false
                    stopExperimentButton.isEnabled = false
                }
            }
            else{
                JOptionPane.showMessageDialog(GUIMain.strimmUIService.strimmFrame, ComponentTexts.AcquisitionDialogs.ERROR_FINDING_CONFIGURATIONS)
                startExperimentButton.isEnabled = false
                stopExperimentButton.isEnabled = false
            }
        }
    }

    /**
     * Specify logic for the user finishing the specification of any traces from ROIs
     */
    private fun addROISpecCompleteListener(){
        roiSpecCompleteButton.addActionListener {
            var result = JOptionPane.YES_OPTION
            var noROIsSpecified = false
            if(GUIMain.experimentService.numNewTraceROIFeeds == 0){
                result = JOptionPane.showConfirmDialog(strimmUIService.strimmFrame, "No new traces from ROIs specified. Do you want to proceed?", "Trace ROIs",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                noROIsSpecified = true
            }

            if(result == JOptionPane.YES_OPTION) {
                JOptionPane.showMessageDialog(strimmUIService.strimmFrame, "Trace from ROI specification complete. Loading new experiment...")
                if(noROIsSpecified){
                    //This line would normally be called in TraceFromROIContext
                    GUIMain.experimentService.createNewConfigFromExisting()
                }
                GUIMain.experimentService.stopAndLoadNewExperiment(true)
                roiSpecCompleteButton.isEnabled = false
            }
        }
    }

    /**
     * Specify logic for when the user starts the experiment
     */
    private fun addStartButtonListener(){
        startExperimentButton.addActionListener {
            GUIMain.experimentService.stopAndLoadNewExperiment(false)
            startExperimentButton.isEnabled = false
        }
    }

    /**
     * Specify logic for when the user stops the experiment
     */
    private fun addStopButtonListener(){
        stopExperimentButton.addActionListener {
            val result = JOptionPane.showConfirmDialog(GUIMain.strimmUIService.strimmFrame, "Are you sure you want to stop acquisition?", "Stopping acquisition", JOptionPane.OK_CANCEL_OPTION)
            if(result == JOptionPane.OK_OPTION) {
                GUIMain.loggerService.log(Level.INFO, "Stopping acquisition")
                experimentService.experimentStream.stopStoringData()
                stopExperimentButton.isEnabled = false
            }
            //TODO add in a "successfully stopped" prompt
        }
    }

    /**
     * Specify logic for when the user resizes the camera feed to an ROI
     */
    private fun addResizeToROIButtonListener(){
        resizeToROIButton.addActionListener{
            val activeDisplays = GUIMain.imageDisplayService.activeImageDisplay
            strimmUIService.resizeCameraFeedToROI(activeDisplays)
        }
    }

    /**
     * Specify logic for when the user expands the camera feed to full view
     */
    private fun addFullViewButtonListener(){
        fullViewButton.addActionListener{
            strimmUIService.expandCameraFeedToFullView(GUIMain.imageDisplayService.activeImageDisplay)
        }
    }

    private fun addAutoScaleButtonListener(){
        autoScaleImageButton.addActionListener{
            strimmUIService.autoScaleCheck = autoScaleImageButton.isSelected
            GUIMain.loggerService.log(Level.INFO, "1 Autoscale is ${autoScaleImageButton.isSelected}")
        }
    }

    /**
     * Specify logic for when the user loads a previously acquired experiment
     */
    private fun addLoadPreviousExperimentButtonListener(){
        loadPrevExperimentButton.addActionListener{
            val chooser  = JFileChooser()
            chooser.currentDirectory = File(".")
            chooser.dialogTitle = "Select folder"
            chooser.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
            val result = chooser.showDialog(GUIMain.strimmUIService.strimmFrame,"Select")
            if(result == JFileChooser.APPROVE_OPTION) {
                experimentService.loadPreviousExperiment(chooser.selectedFile)
            }
        }
    }

    /**
     * Show a specify ROI prompt to the user
     */
    private fun showROISpecPrompt(){
        JOptionPane.showMessageDialog(strimmUIService.strimmFrame, ComponentTexts.AcquisitionDialogs.SPECIFY_ROI_PROMPT)
        roiSpecCompleteButton.isEnabled = true
        resizeToROIButton.isEnabled = true
        fullViewButton.isEnabled = true
        startExperimentButton.isEnabled = false
        stopExperimentButton.isEnabled = false
    }

    /**
     * This method will run an experiment stream based on the loaded specification. Note this method only loads into
     * preview mode
     */
    private fun createStreamGraph(){
        val streamCreateSuccess = experimentService.createStreamGraph()
        if (!streamCreateSuccess) {
            JOptionPane.showMessageDialog(strimmUIService.strimmFrame, ComponentTexts.AcquisitionDialogs.ERROR_CREATING_GRAPH)
            startExperimentButton.isEnabled = false
            stopExperimentButton.isEnabled = false
        } else {
            JOptionPane.showMessageDialog(strimmUIService.strimmFrame, ComponentTexts.AcquisitionDialogs.STREAM_CREATE_SUCCESS)
            experimentService.runStream(false) //This is where the fun begins
            startExperimentButton.isEnabled = true
            stopExperimentButton.isEnabled = true
        }
    }
    //endregion

    fun getMenuItemName(item: JComponent) =
            when (item) {
                is JMenu -> item.text
                is JMenuItem -> item.text
                else -> "Error"
            }

    tailrec fun addMenuItems(plugin : PluginInfo<DockableWindowPlugin>, depth : Int, currentMenu : JComponent) {
        val menuPath = plugin.menuPath

        if (menuPath.size == depth + 1) {
            val menuItem = JMenuItem(menuPath.leaf.name).apply {
                addActionListener {
                    var selectedCamera: MMCameraDevice? = null

                    //If its a camera window plugin we need to specify a camera
                    if(plugin.className == CameraWindowPlugin::class.java.name) {
                        val cameraDevices = GUIMain.mmService.getLoadedDevicesOfType(MMCameraDevice::class.java)
                        val cameraNames = cameraDevices.map{x -> x.label}.toTypedArray()
                        val optionList = JComboBox(cameraNames)
                        optionList.selectedIndex = 0
                        JOptionPane.showMessageDialog(null, optionList, "Select camera", JOptionPane.QUESTION_MESSAGE)
                        selectedCamera = cameraDevices.find { x -> x.label ==  optionList.selectedItem}!!
                    }

                    val dwPlugin = if(selectedCamera != null){
                        GUIMain.dockableWindowPluginService.createPlugin(CameraWindowPlugin::class.java,
                                                                          selectedCamera,
                                                                true,
                                                               "${selectedCamera.label}${ComponentTexts.CameraWindow.PLUGIN_TITLE_SUFFIX}")
                    } else{
                        GUIMain.dockableWindowPluginService.createPlugin(plugin, null, false)
                    }

                    dwPlugin?.dock(GUIMain.strimmUIService.dockableControl,GUIMain.strimmUIService.strimmFrame)
                        ?: GUIMain.loggerService.log(Level.WARNING, "Failed to create plugin ${plugin.pluginClass}!")
                }
            }

            currentMenu.add(menuItem)
            return
        }

        var newMenu = currentMenu.components.find { getMenuItemName(it as JComponent) == menuPath[depth].name }
        when (newMenu) {
            is JMenu -> {}
            null, is JMenuItem -> {
                newMenu = JMenu(menuPath[depth].name)
                currentMenu.add(newMenu)
            }
            else -> {}
        }
        addMenuItems(plugin, depth + 1, newMenu as JComponent)
    }

    fun addMenuDockableWindowPlguins(plugins : List<PluginInfo<DockableWindowPlugin>>, menu : JMenuBar) {
        plugins.forEach {
            addMenuItems(it, 0, menu)}
    }

    fun setOnClose(frame : JFrame){
        val exitListener = object : WindowAdapter() {
            override fun windowClosing(e: java.awt.event.WindowEvent?) {
                timerService.closeTimerService()
                GUIMain.loggerService.log(Level.INFO, "Closing STRIMM")
                GUIMain.loggerService.fh.close()
                commandService.run(QuitProgram::class.java, false)
                super.windowClosing(e)
                Platform.exit()
            }
        }
        frame.addWindowListener(exitListener)
    }

    companion object {
        val roiSpecCompleteButton = JButton()
        val loadExperimentConfigButton = JButton()
        val startExperimentButton = JButton()
        val stopExperimentButton = JButton()
        val resizeToROIButton = JButton()
        val fullViewButton = JButton()
        val loadPrevExperimentButton = JButton()
        val autoScaleImageButton = JCheckBox()

        //region Services
        @Parameter
        lateinit var actorService: ActorService

        @Parameter
        lateinit var imageJService : ImageJService

        @Parameter
        lateinit var uiService : UIService

        @Parameter
        lateinit var pipelinePluginService : PipelinePluginService

        @Parameter
        lateinit var dockableWindowPluginService : DockableWindowPluginService

        @Parameter
        lateinit var displayService : DisplayService

        @Parameter
        lateinit var imageDisplayService : ImageDisplayService

        @Parameter
        lateinit var ioService : IOService

        @Parameter
        lateinit var datasetService: DatasetService

        @Parameter
        lateinit var loggerService : LoggerService

        @Parameter
        lateinit var mmService : MMCoreService

        @Parameter
        lateinit var commandService : CommandService

        @Parameter
        lateinit var strimmUIService : StrimmUIService

        @Parameter
        lateinit var pluginService: PluginService

        @Parameter
        lateinit var threadService : ThreadService

        @Parameter
        lateinit var menuService: MenuService

        @Parameter
        lateinit var moduleService : ModuleService

        @Parameter
        lateinit var eventService : EventService

        @Parameter
        lateinit var objectService : ObjectService

        @Parameter
        lateinit var overlayService : OverlayService

        @Parameter
        lateinit var opService : OpService

        @Parameter
        lateinit var imgPlusService: ImgPlusService

        @Parameter
        lateinit var strimmSettingsService: StrimmSettingsService

        @Parameter
        lateinit var exportService : ExportService

        @Parameter
        lateinit var experimentCommandService : ExperimentCommandPluginService

        @Parameter
        lateinit var timerService: TimerService

        @Parameter
        lateinit var datasetIOService : DatasetIOService

        @Parameter
        lateinit var experimentService : ExperimentService

        @Parameter
        lateinit var softwareTimerService: SoftwareTimerService

        @Parameter
        lateinit var convertService: ConvertService

        @Parameter
        lateinit var importService : ImportService

        @Parameter
        lateinit var utilsService : UtilsService

        @Parameter
        lateinit var acquisitionMethodService: AcquisitionMethodService
        //endregion
    }
}

/**
 * Controller for the Main GUI for the STRIMM plugin
 */
class MainController {

}

