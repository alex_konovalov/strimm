package uk.co.strimm.gui

import akka.actor.ActorRef
import bibliothek.gui.dock.common.DefaultMultipleCDockable
import net.imagej.*
import net.imagej.axis.Axes
import net.imagej.display.DefaultDatasetView
import net.imagej.display.DefaultImageDisplay
import net.imglib2.type.numeric.integer.UnsignedByteType
import net.imglib2.type.numeric.integer.UnsignedShortType
import net.imglib2.type.numeric.real.FloatType
import org.scijava.display.Display
import org.scijava.plugin.Plugin
import org.scijava.ui.swing.viewer.SwingDisplayWindow
import org.scijava.ui.viewer.DisplayWindow
import uk.co.strimm.MicroManager.MMCameraDevice
import uk.co.strimm.plugins.AbstractDockableWindow
import uk.co.strimm.plugins.DockableWindowPlugin
import java.util.*
import java.awt.Robot
import java.util.logging.Level
import java.awt.event.KeyEvent
import javax.swing.*

@Plugin(type = DockableWindowPlugin::class, menuPath = "Window>Camera Feed")
class CameraWindowPlugin : AbstractDockableWindow() {
    override var title = "CameraFeed " + Random().nextInt(1000000)//This will eventually be overridden
    lateinit var cameraWindowController : CameraWindow
    var camera : MMCameraDevice? = null

    override fun setCustomData(data: Any?) {
        if(data is MMCameraDevice){
            cameraWindowController.cameraDevice = data
            dockableWindowMultiple.titleText = data.label + " camera feed"
        }
        else if(data is Dataset){
            cameraWindowController.dataset = data
        }
    }

    override var dockableWindowMultiple : DefaultMultipleCDockable = run{
        this.createDock(title).apply{
            this.titleText = title
            dockableWindowMultiple = this
            add(windowPanel)
            cameraWindowController = CameraWindow(windowPanel)
        }
    }
}

class CameraWindow constructor(val windowPanel: JPanel){
    var dataset : Dataset? = null
    var display : Display<*>? = null
    var view : DefaultDatasetView? = null
    var displayWindow : DisplayWindow? = null
    var associatedActor : ActorRef? = null
    var cameraWidth = 1376.toLong() //Dummy default TODO link to setting
    var cameraHeight = 1024.toLong() //Dummy default TODO link to setting
    var cameraDevice : MMCameraDevice? = null
    var datasetName = "Dataset" + Random().nextInt(1000000)//This will eventually be overridden

    /**
     * This method is called when a new camera display needs to be initialised, with a specific width an height however.
     * This will only ever be called after initialiseDisplay() (no size parameters) as we need something to resize in
     * the first place
     * @param width The new width of the camera feed
     * @param height The new height of the camera feed
     */
    fun initialiseDisplay(width : Long, height: Long){
        /*
         * We have to wrap this in an invokeLater() to make sure it comes after the initial creation of the display
         * in createDatasetAndAddToPanel() (which is created using the threadService). This is because this method
         * is used during resizing (you need to have something to resize in the first place)
         */
        java.awt.EventQueue.invokeLater({
            run{
                if (windowPanel.componentCount > 0) {
                    windowPanel.remove(0)
                }

                windowPanel.updateUI()

                GUIMain.loggerService.log(Level.INFO, "Initializing new camera window from resize event. New width: $width, new height: $height")
                val cam = GUIMain.experimentService.experimentStream.cameraDevices.filter { x -> x.device.label == cameraDevice?.label }.map { x -> x.device }.first()
                createDatasetAndAddToPanel(cam, width, height)
            }
        })
    }

    /**
     * This is called when the camera feed is first created.
     */
    fun initialiseDisplay(){
        if(windowPanel.componentCount > 0) {
            windowPanel.remove(0)
        }

        windowPanel.updateUI()

        if(dataset == null) {
            val cam = GUIMain.experimentService.experimentStream.cameraDevices.filter { x -> x.device.label == cameraDevice?.label }.map { x -> x.device }.first()
            cameraWidth = cam.imageWidth
            cameraHeight = cam.imageHeight
            GUIMain.loggerService.log(Level.INFO, "Initializing camera window display. Width: $cameraWidth, Height: $cameraHeight")
            createDatasetAndAddToPanel(cam, cameraWidth, cameraHeight)
        }
        else{
            createDisplayAndAddToPanel()
        }
    }

    /**
     * This method is similar to createDatasetAndAddToPanel() however, it is used when the Camera Window's dataset
     * already exists. This is therefore only used when loading a previous experiment. This also contains a bug fix
     * where we need to force a keypress
     */
    private fun createDisplayAndAddToPanel(){
        display = GUIMain.displayService.createDisplayQuietly(dataset)

        view = ((display as DefaultImageDisplay).activeView as DefaultDatasetView)

        //Create a display window to actually display the image
        displayWindow = GUIMain.uiService.defaultUI.createDisplayWindow(display)

        GUIMain.uiService.viewerPlugins
            .map { GUIMain.pluginService.createInstance(it) }
            .find { it != null && it.canView(display) && it.isCompatible(GUIMain.uiService.defaultUI) }
            ?.let {
                GUIMain.threadService.queue {
                    (displayWindow as SwingDisplayWindow).apply {
                        GUIMain.uiService.addDisplayViewer(it)
                        it.view(this, display)
                        pack()
                        val rootPane = this.rootPane
                        windowPanel.add(rootPane)
                    }
                }
            }
    }

    /**
     * Common method to create a dataset and associated ImageJ components to allow for image display. Note - as this is
     * only a display (not a store) the dataset is one planar image that is overwritten with every new image
     * @param cam The camera device for this feed
     * @param width The width of the camera feed
     * @param height The height of the camera feed
     */
    private fun createDatasetAndAddToPanel(cam : MMCameraDevice, width: Long, height: Long){
        datasetName = "Camera-" + cam.label + "-Dataset"
        dataset = when (cam.bytesPerPixel)
        {
            MMCameraDevice.MMBytesPerPixel.Bit8 ->
                GUIMain.datasetService.create(UnsignedByteType(), longArrayOf(width, height), datasetName, arrayOf(Axes.X, Axes.Y))
            MMCameraDevice.MMBytesPerPixel.Bit16 ->
                GUIMain.datasetService.create(UnsignedShortType(), longArrayOf(width, height), datasetName, arrayOf(Axes.X, Axes.Y))
            MMCameraDevice.MMBytesPerPixel.Bit32 ->
                GUIMain.datasetService.create(FloatType(), longArrayOf(width, height), datasetName, arrayOf(Axes.X, Axes.Y))
            MMCameraDevice.MMBytesPerPixel.Unknown -> {
                Thread.sleep(1000)
                throw Exception("Unknown Bit Depth!")
            }
        }

        //Create a display in the background
        display = GUIMain.displayService.createDisplayQuietly(dataset)

        view = ((display as DefaultImageDisplay).activeView as DefaultDatasetView)

        //Create a display window to actually display the image
        displayWindow = GUIMain.uiService.defaultUI.createDisplayWindow(display)

        GUIMain.uiService.viewerPlugins
            .map {
                GUIMain.pluginService.createInstance(it) }
            .find {
                it != null && it.canView(display) && it.isCompatible(GUIMain.uiService.defaultUI) }
            ?.let {
                GUIMain.threadService.queue {
                    (displayWindow as SwingDisplayWindow).apply {
                        GUIMain.uiService.addDisplayViewer(it)
                        it.view(this, display)
                        pack()
                        val rootPane = this.rootPane
                        windowPanel.add(rootPane)
                    }
                }
            }
    }

    fun pressMinusButton(){
        /*
        * We have to force a minus sign keypress due to a Swing SDI UI bug otherwise the display
        * will just be white
        * Mentioned here: https://imagej.net/2012-08-01_-_Loading_and_displaying_a_dataset_with_the_ImageJ2_API
        */
        windowPanel.requestFocus()
        val robot = Robot()
        robot.keyPress(KeyEvent.VK_MINUS)
    }

    fun addSliderListener(){
        //TODO
//        Thread.sleep(5000)
//        JOptionPane.showMessageDialog(GUIMain.strimmUIService.strimmFrame, "Loaded successfully")
//        println("Now adding listener")

//        val window = displayWindow as SwingDisplayWindow
//        val scrollPanel = window.contentPane.components[2] as JPanel
//        val scrollBar = scrollPanel.components[1] as JScrollBar
//        scrollBar.addAdjustmentListener {
//            println(it.value)
//        }
//        val fwdButton = scrollBar.components[0] as JButton
//        val backwardButton = scrollBar.components[1] as JButton
    }
}