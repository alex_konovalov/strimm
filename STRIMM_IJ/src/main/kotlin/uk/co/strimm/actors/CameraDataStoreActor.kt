package uk.co.strimm.actors

import akka.actor.AbstractActor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Props
//import ij.IJ
import net.imagej.axis.Axes
import net.imagej.axis.CalibratedAxis
import net.imglib2.img.array.ArrayImg
import net.imglib2.img.array.ArrayImgs
//import net.imglib2.img.display.imagej.ImageJFunctions
//import net.imglib2.img.display.imagej.ImageJFunctions
import net.imglib2.type.numeric.integer.UnsignedByteType
import net.imglib2.type.numeric.integer.UnsignedShortType
import net.imglib2.type.numeric.real.FloatType
import net.imglib2.view.StackView
import uk.co.strimm.Acknowledgement
import uk.co.strimm.CameraMetaDataStore
import uk.co.strimm.Paths
import uk.co.strimm.STRIMMImage
import uk.co.strimm.actors.messages.Message
import uk.co.strimm.actors.messages.complete.CompleteCameraDataStoring
import uk.co.strimm.actors.messages.fail.FailCameraDataStoring
import uk.co.strimm.actors.messages.start.StartAcquiring
import uk.co.strimm.actors.messages.start.StartCameraDataStoring
import uk.co.strimm.gui.GUIMain
import java.util.*
import java.util.logging.Level

data class ByteImg(var stack : ArrayImg<UnsignedByteType,net.imglib2.img.basictypeaccess.array.ByteArray>)
data class ShortImg(var stack : ArrayImg<UnsignedShortType,net.imglib2.img.basictypeaccess.array.ShortArray>)
data class FloatImg(var stack : ArrayImg<FloatType,net.imglib2.img.basictypeaccess.array.FloatArray>)

class ArrayImgStore{
    var byteStack = arrayListOf<ByteImg>()
    var shortStack = arrayListOf<ShortImg>()
    var floatStack = arrayListOf<FloatImg>()
}

class CameraDataStoreActor : AbstractActor() {
    companion object {
        fun props(): Props {
            return Props.create<CameraDataStoreActor>(CameraDataStoreActor::class.java) { CameraDataStoreActor() }
        }
    }

    var imageCounter = 0
    val imgStack = ArrayImgStore()
    val imgStackInfo = arrayListOf<CameraMetaDataStore>()
    var startAcquiring = false
    val numDimensions = 3

    /**
     * Note: data flow will be terminated if the actor doesn't acknowledge data messages from the sender
     */
    override fun createReceive(): Receive {
        return receiveBuilder()
                .match<Message>(Message::class.java) {
                    GUIMain.loggerService.log(Level.INFO, "Camera data store actor receiving message")
                    sender().tell(Acknowledgement.INSTANCE, self())
                }
                .match<StartCameraDataStoring>(StartCameraDataStoring::class.java) {
                    GUIMain.loggerService.log(Level.INFO, "Camera data store actor starting")
                    sender().tell(Acknowledgement.INSTANCE, self())
                }
                .match<CompleteCameraDataStoring>(CompleteCameraDataStoring::class.java) {
                    GUIMain.loggerService.log(Level.INFO, "Camera data store actor completing")
                    sender().tell(Acknowledgement.INSTANCE, self())
                }
                .match<FailCameraDataStoring>(FailCameraDataStoring::class.java) { failCameraDataStoring ->
                    GUIMain.loggerService.log(Level.SEVERE, "Camera data store stream failed. Error message: ${failCameraDataStoring.ex.message}")
                    GUIMain.loggerService.log(Level.SEVERE, failCameraDataStoring.ex.stackTrace)
                    sender().tell(Acknowledgement.INSTANCE, self())
                }
                .match<StartAcquiring>(StartAcquiring::class.java) {
                    GUIMain.loggerService.log(Level.INFO, "Starting camera data storing")
                    startAcquiring = true
                }
                .match<STRIMMImage>(STRIMMImage::class.java) { image ->
                    if (startAcquiring) {
                        when (image.pix) {
                            is ByteArray -> {
                                val shouldStop = checkIfShouldStop(image.sourceCamera)
                                if (!shouldStop) {
                                    val arrayImg : ArrayImg<UnsignedByteType, net.imglib2.img.basictypeaccess.array.ByteArray>
                                    arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                        val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                        val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetByteArray(image, resizeVals)
                                        ArrayImgs.unsignedBytes(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                    } else{
                                        val imgSizeFull = GUIMain.strimmUIService.cameraSizeList[image.sourceCamera]!!
                                        ArrayImgs.unsignedBytes(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                    }

                                    imgStack.byteStack.add(ByteImg(arrayImg))
                                    imgStackInfo.add(CameraMetaDataStore(image.timeAcquired, image.sourceCamera, imageCounter))
                                    imageCounter++
                                } else {
                                    sendPoisonPill()
                                }
                            }
                            is ShortArray -> {
                                val shouldStop = checkIfShouldStop(image.sourceCamera)
                                if (!shouldStop) {
                                    val arrayImg : ArrayImg<UnsignedShortType, net.imglib2.img.basictypeaccess.array.ShortArray>
                                    arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                        val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                        val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetShortArray(image, resizeVals)
                                        ArrayImgs.unsignedShorts(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                    }else{
                                        val imgSizeFull = GUIMain.strimmUIService.cameraSizeList[image.sourceCamera]!!
                                        ArrayImgs.unsignedShorts(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                    }

                                    imgStack.shortStack.add(ShortImg(arrayImg))
                                    imgStackInfo.add(CameraMetaDataStore(image.timeAcquired, image.sourceCamera, imageCounter))
                                    imageCounter++
                                } else {
                                    sendPoisonPill()
                                }
                            }
                            is FloatArray -> {
                                val shouldStop = checkIfShouldStop(image.sourceCamera)
                                if (!shouldStop) {
                                    val arrayImg : ArrayImg<FloatType, net.imglib2.img.basictypeaccess.array.FloatArray>
                                    arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                        val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                        val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetFloatArray(image, resizeVals)
                                        ArrayImgs.floats(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                    }else{
                                        val imgSizeFull = GUIMain.strimmUIService.cameraSizeList[image.sourceCamera]!!
                                        ArrayImgs.floats(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                    }

                                    imgStack.floatStack.add(FloatImg(arrayImg))
                                    imgStackInfo.add(CameraMetaDataStore(image.timeAcquired, image.sourceCamera, imageCounter))
                                    imageCounter++
                                } else {
                                    sendPoisonPill()
                                }
                            }
                        }

                        if (imageCounter % 100 == 0) {
                            GUIMain.loggerService.log(Level.INFO, "$imageCounter frames processed")
                            val totalMem = (((Runtime.getRuntime().totalMemory()/1024)/1024)/1024).toDouble()
                            val maxMem = (((Runtime.getRuntime().maxMemory()/1024)/1024)/1024).toDouble()
                            val freeMem = (((Runtime.getRuntime().freeMemory()/1024)/1024)/1024).toDouble()
                            GUIMain.loggerService.log(Level.INFO, "Total memory (GB): $totalMem, Max memory (GB): $maxMem, Free memory (GB): $freeMem")
                        }
                    }
                    sender().tell(Acknowledgement.INSTANCE, self())
                }
                .matchAny {
                    sender().tell(Acknowledgement.INSTANCE, self())
                }
                .build()
    }


//    var emptyDataset : Dataset? = null
//    private fun writeEmptyTiff(){
//        val imageStack = imgStack.byteStack.map { x -> x.stack }
//        val datasetName = "Test" + Random().nextInt(1000)
//
//        emptyDataset = GUIMain.datasetService.create(StackView(imageStack)).apply {
//            val ax = Array<CalibratedAxis?>(numDimensions) { null }
//            axes(ax)
//            ax[0]?.setType(Axes.X)
//            ax[1]?.setType(Axes.Y)
//            ax[2]?.setType(Axes.TIME)
//            setAxes(ax)
//            name = datasetName //TODO name to come from the experiment builder
//        }
//
//        GUIMain.datasetIOService.save(emptyDataset, "${emptyDataset!!.name}_empty.tif")
//    }

    private fun checkIfShouldStop(deviceLabel : String) : Boolean{
        return imageCounter >= GUIMain.experimentService.deviceDatapointNumbers[deviceLabel]!!
    }

    private fun sendPoisonPill(){
        self.tell(PoisonPill.getInstance(), ActorRef.noSender())
    }

    override fun postStop() {
        //TODO is postStop() the most appropriate method to write data?
        GUIMain.loggerService.log(Level.INFO, "Writing camera data")
        writeData()
        super.postStop()
    }

    private fun writeData() {
        if(imgStackInfo.size > 0) {
            GUIMain.exportService.writeCameraMetaDataToFile(imgStackInfo)
        }
        else{
            GUIMain.loggerService.log(Level.INFO, "No camera meta data to write")
        }

        when {
            imgStack.byteStack.size > 0 -> {
                val imageStack = imgStack.byteStack.map { x -> x.stack }

                val camFeedName = imgStackInfo[0].cameraFeedName
                val datasetName = GUIMain.actorService.getPertainingDatasetForCameraDevice(camFeedName)

                val half = Math.floor(imageStack.size.toDouble()/2.toDouble()).toInt()
                val quarter = Math.floor(imageStack.size.toDouble()/4.toDouble()).toInt()

                //TODO splitting up the dataset into 4 parts is a temporary solution until we fix the issue with slow saving times
                val dataset1 = GUIMain.datasetService.create(StackView(imageStack.subList(0, quarter))).apply {
                    val ax = Array<CalibratedAxis?>(numDimensions) { null }
                    axes(ax)
                    ax[0]?.setType(Axes.X)
                    ax[1]?.setType(Axes.Y)
                    ax[2]?.setType(Axes.TIME)
                    setAxes(ax)
                    name = datasetName
                }
                val dataset2 = GUIMain.datasetService.create(StackView(imageStack.subList(quarter, half))).apply {
                    val ax = Array<CalibratedAxis?>(numDimensions) { null }
                    axes(ax)
                    ax[0]?.setType(Axes.X)
                    ax[1]?.setType(Axes.Y)
                    ax[2]?.setType(Axes.TIME)
                    setAxes(ax)
                    name = datasetName
                }
                val dataset3 = GUIMain.datasetService.create(StackView(imageStack.subList(half, half+quarter))).apply {
                    val ax = Array<CalibratedAxis?>(numDimensions) { null }
                    axes(ax)
                    ax[0]?.setType(Axes.X)
                    ax[1]?.setType(Axes.Y)
                    ax[2]?.setType(Axes.TIME)
                    setAxes(ax)
                    name = datasetName
                }
                val dataset4 = GUIMain.datasetService.create(StackView(imageStack.subList(half+quarter, imageStack.size))).apply {
                    val ax = Array<CalibratedAxis?>(numDimensions) { null }
                    axes(ax)
                    ax[0]?.setType(Axes.X)
                    ax[1]?.setType(Axes.Y)
                    ax[2]?.setType(Axes.TIME)
                    setAxes(ax)
                    name = datasetName
                }

                Thread({
                    val beforeTime = System.currentTimeMillis()
                    GUIMain.datasetIOService.save(dataset1, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${dataset1.name}_part1.tif")
                    println("Part 1 save took: ${System.currentTimeMillis()-beforeTime}ms")
                }).start()
                Thread({
                    val beforeTime = System.currentTimeMillis()
                    GUIMain.datasetIOService.save(dataset2, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${dataset2.name}_part2.tif")
                    println("Part 2 save took: ${System.currentTimeMillis()-beforeTime}ms")
                }).start()
                Thread({
                    val beforeTime = System.currentTimeMillis()
                    GUIMain.datasetIOService.save(dataset3, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${dataset3.name}_part3.tif")
                    println("Part 3 save took: ${System.currentTimeMillis()-beforeTime}ms")
                }).start()
                Thread({
                    val beforeTime = System.currentTimeMillis()
                    GUIMain.datasetIOService.save(dataset4, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${dataset4.name}_part4.tif")
                    println("Part 4 save took: ${System.currentTimeMillis()-beforeTime}ms")
                }).start()

//                GUIMain.ioService.save(dataset, "${dataset.name}.tif")
//                ImageJ().io().save(dataset,"./" + dataset.name + ".tif")
            }
            imgStack.shortStack.size > 0 -> {
                val imageStack = imgStack.shortStack.map { x -> x.stack }
                val camFeedName = imgStackInfo[0].cameraFeedName
                val datasetName = "Test" + Random().nextInt(1000)//GUIMain.actorService.getPertainingDatasetForCameraDevice(camFeedName)

                //TODO splitting up the dataset into 4 parts is a temporary solution until we fix the issue with slow saving times
                if(imageStack.size >= 4) {
                    val half = Math.floor(imageStack.size.toDouble() / 2.toDouble()).toInt()
                    val quarter = Math.floor(imageStack.size.toDouble() / 4.toDouble()).toInt()

                    val dataset1 = GUIMain.datasetService.create(StackView(imageStack.subList(0, quarter))).apply {
                        val ax = Array<CalibratedAxis?>(numDimensions) { null }
                        axes(ax)
                        ax[0]?.setType(Axes.X)
                        ax[1]?.setType(Axes.Y)
                        ax[2]?.setType(Axes.TIME)
                        setAxes(ax)
                        name = datasetName
                    }
                    val dataset2 = GUIMain.datasetService.create(StackView(imageStack.subList(quarter, half))).apply {
                        val ax = Array<CalibratedAxis?>(numDimensions) { null }
                        axes(ax)
                        ax[0]?.setType(Axes.X)
                        ax[1]?.setType(Axes.Y)
                        ax[2]?.setType(Axes.TIME)
                        setAxes(ax)
                        name = datasetName
                    }
                    val dataset3 = GUIMain.datasetService.create(StackView(imageStack.subList(half, half + quarter))).apply {
                        val ax = Array<CalibratedAxis?>(numDimensions) { null }
                        axes(ax)
                        ax[0]?.setType(Axes.X)
                        ax[1]?.setType(Axes.Y)
                        ax[2]?.setType(Axes.TIME)
                        setAxes(ax)
                        name = datasetName
                    }
                    val dataset4 = GUIMain.datasetService.create(StackView(imageStack.subList(half + quarter, imageStack.size))).apply {
                        val ax = Array<CalibratedAxis?>(numDimensions) { null }
                        axes(ax)
                        ax[0]?.setType(Axes.X)
                        ax[1]?.setType(Axes.Y)
                        ax[2]?.setType(Axes.TIME)
                        setAxes(ax)
                        name = datasetName
                    }

                    Thread({
                        val beforeTime = System.currentTimeMillis()
                        GUIMain.datasetIOService.save(dataset1, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${dataset1.name}_part1.tif")
                        println("Part 1 save took: ${System.currentTimeMillis() - beforeTime}ms")
                    }).start()
                    Thread({
                        val beforeTime = System.currentTimeMillis()
                        GUIMain.datasetIOService.save(dataset2, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${dataset2.name}_part2.tif")
                        println("Part 2 save took: ${System.currentTimeMillis() - beforeTime}ms")
                    }).start()
                    Thread({
                        val beforeTime = System.currentTimeMillis()
                        GUIMain.datasetIOService.save(dataset3, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${dataset3.name}_part3.tif")
                        println("Part 3 save took: ${System.currentTimeMillis() - beforeTime}ms")
                    }).start()
                    Thread({
                        val beforeTime = System.currentTimeMillis()
                        GUIMain.datasetIOService.save(dataset4, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${dataset4.name}_part4.tif")
                        println("Part 4 save took: ${System.currentTimeMillis() - beforeTime}ms")
                    }).start()
                }
                else{
                    val ds = GUIMain.datasetService.create(StackView(imageStack)).apply {
                        val ax = Array<CalibratedAxis?>(numDimensions) { null }
                        axes(ax)
                        ax[0]?.setType(Axes.X)
                        ax[1]?.setType(Axes.Y)
                        ax[2]?.setType(Axes.TIME)
                        setAxes(ax)
                        name = datasetName //TODO name to come from the experiment builder
                    }
                    GUIMain.datasetIOService.save(ds, "${Paths.EXPERIMENT_OUTPUT_FOLDER}/${ds.name}.tif")
                }
            }
            imgStack.floatStack.size > 0 -> {
                val imageStack = imgStack.floatStack.map { x -> x.stack }
                val dataset = GUIMain.datasetService.create(StackView(imageStack)).apply {
                    val ax = Array<CalibratedAxis?>(numDimensions) { null }
                    axes(ax)
                    ax[0]?.setType(Axes.X)
                    ax[1]?.setType(Axes.Y)
                    ax[2]?.setType(Axes.TIME)
                    setAxes(ax)
                    name = "Test" + Random().nextInt(1000) //TODO name to come from the experiment builder
                }

                GUIMain.datasetIOService.save(dataset, "${dataset.name}.tif")
            }
        }
    }
}