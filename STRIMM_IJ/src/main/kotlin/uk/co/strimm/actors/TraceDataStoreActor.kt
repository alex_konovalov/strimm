package uk.co.strimm.actors

import akka.actor.*
import uk.co.strimm.Acknowledgement
import uk.co.strimm.actors.messages.Message
import uk.co.strimm.actors.messages.complete.CompleteTraceDataStoring
import uk.co.strimm.actors.messages.fail.FailTraceDataStoring
import uk.co.strimm.actors.messages.start.StartTraceDataStoring
import uk.co.strimm.gui.GUIMain
import uk.co.strimm.TraceData
import uk.co.strimm.actors.messages.start.StartTraceStore
import java.util.logging.Level
import uk.co.strimm.TraceDataStore
import uk.co.strimm.actors.messages.tell.TellAnalogueDataStream
import uk.co.strimm.services.AnalogueDataStream
import java.util.*

class TraceDataStoreActor : AbstractActor() {
    companion object {
        fun props(): Props {
            return Props.create<TraceDataStoreActor>(TraceDataStoreActor::class.java) { TraceDataStoreActor() }
        }
    }

    val traceData = ArrayList<TraceDataStore>()
    var traceStore = false
    var dataPointCounters = hashMapOf<String,Int>()
    var associatedAnalogueDataStream : AnalogueDataStream? = null

    /**
     * Note: data flow will be terminated if the actor doesn't acknowledge data messages from the sender
     */
    override fun createReceive(): Receive {
        return receiveBuilder()
            .match<Message>(Message::class.java) { message ->
                GUIMain.loggerService.log(Level.INFO, "Trace data store actor receiving message")
                sender().tell(Acknowledgement.INSTANCE, self())
            }
            .match<StartTraceDataStoring>(StartTraceDataStoring::class.java){ startTraceDataStoring ->
                GUIMain.loggerService.log(Level.INFO, "Trace data store actor starting data storing")
                sender().tell(Acknowledgement.INSTANCE, self())
            }
            .match<CompleteTraceDataStoring>(CompleteTraceDataStoring::class.java){ completeTraceDataStoring ->
                GUIMain.loggerService.log(Level.INFO, "Trace data store actor completing data storing")
                sender().tell(Acknowledgement.INSTANCE, self())
            }
            .match<FailTraceDataStoring>(FailTraceDataStoring::class.java){ failTraceDataStoring ->
                GUIMain.loggerService.log(Level.SEVERE, "Trace data store stream failed. Error message: ${failTraceDataStoring.ex.message}")
                GUIMain.loggerService.log(Level.SEVERE, failTraceDataStoring.ex.stackTrace)
                sender().tell(Acknowledgement.INSTANCE, self())
            }
            .match<StartTraceStore>(StartTraceStore::class.java){
                traceStore = true
            }
            .match<TellAnalogueDataStream>(TellAnalogueDataStream::class.java){
                associatedAnalogueDataStream = it.analogueDataStream
                it.analogueDataStream.associatedTraceDataStoreActor = this.self()
            }
            .matchAny {
                val incomingDataList = it as List<List<TraceData>>
                val incomingData = incomingDataList.flatten()
                if(incomingData.isNotEmpty() && traceStore) {
                    for(traceData in incomingData) {
                        if(traceData.data.first!!.name !in dataPointCounters.keys){
                            dataPointCounters[traceData.data.first!!.name] = 0
                        }

                        val shouldStop = checkIfShouldStop(traceData.data.first!!.name)
                        if(!shouldStop) {
                            this.traceData.add(TraceDataStore(traceData.timeAcquired, traceData.data.first, traceData.data.second, dataPointCounters[traceData.data.first!!.name]!!))
                            dataPointCounters[traceData.data.first!!.name] = dataPointCounters[traceData.data.first!!.name]!!+1
                        }
                        else{
                            sendPoisonPill()
                        }
                    }
                }

                sender().tell(Acknowledgement.INSTANCE, self())
            }
            .build()
    }

    private fun checkIfShouldStop(deviceLabel : String) : Boolean{
        for(dataPointNumberPair in GUIMain.experimentService.deviceDatapointNumbers){
            if(deviceLabel.contains(dataPointNumberPair.key)){
                if(dataPointCounters.all{ x -> x.value % 10000 == 0}){
                    GUIMain.loggerService.log(Level.INFO, "Trace stored ${dataPointCounters.values.first()} points so far")
                }

                if(dataPointCounters.all { x -> x.value >= dataPointNumberPair.value}){
                    return true
                }
            }
        }
        return false
//        return dataPointCounters.values.all { x -> x > GUIMain.experimentService.deviceDatapointNumbers[deviceLabel]!! }
    }

    private fun sendPoisonPill(){
        self.tell(PoisonPill.getInstance(), ActorRef.noSender())
    }

    override fun postStop() {
        //TODO is this the most appropriate method to write data?
        GUIMain.loggerService.log(Level.INFO, "Writing trace data")
        writeData()
        super.postStop()
    }

    private fun writeData(){
        //TODO export to format based on setting
        if(traceData.size > 0) {
            GUIMain.exportService.writeTracesToFile(traceData)
        }
        else{
            GUIMain.loggerService.log(Level.INFO, "No trace data to write")
        }
    }
}