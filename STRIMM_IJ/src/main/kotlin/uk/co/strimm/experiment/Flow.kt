package uk.co.strimm.experiment

class Flow{
    var flowName = ""
    var inputNames = arrayListOf<String>()
    var inputType = ""
    var outputType = ""
}