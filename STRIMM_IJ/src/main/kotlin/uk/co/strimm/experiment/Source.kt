package uk.co.strimm.experiment

/**
 * This class represents any source in the experiment. It facilitates both image based and trace based sources
 */
class Source{
    var sourceName = ""
    var deviceLabel = ""
    var exposureMs = 0.0
    var intervalMs = 0.0
    var outputType = ""
    var samplingFrequencyHz = 0.0
    var x = 0.0
    var y = 0.0
    var w = 0.0
    var h = 0.0
    var channel = ""
}