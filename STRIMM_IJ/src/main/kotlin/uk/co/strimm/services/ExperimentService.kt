package uk.co.strimm.services

import akka.NotUsed
import akka.stream.javadsl.RunnableGraph
import com.google.gson.GsonBuilder
import net.imagej.ImageJService
import org.apache.commons.io.FileUtils
import org.scijava.plugin.Plugin
import org.scijava.service.AbstractService
import org.scijava.service.Service
import uk.co.strimm.ExperimentConstants
import uk.co.strimm.Paths
import uk.co.strimm.experiment.*
import uk.co.strimm.gui.CameraWindowPlugin
import uk.co.strimm.gui.GUIMain
import uk.co.strimm.gui.TraceWindowPlugin
import uk.co.strimm.streams.ExperimentStream
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.logging.Level
import javax.swing.JOptionPane

@Plugin(type = Service::class)
class ExperimentService  : AbstractService(), ImageJService {
    private val gson = GsonBuilder().setPrettyPrinting().create()
    var expConfig = ExperimentConfiguration()
    var loadedConfigurationStream : RunnableGraph<NotUsed>? = null
    lateinit var experimentStream : ExperimentStream
    var configWithTraceROIs : ExperimentConfiguration? = null
    lateinit var loadedConfigFile : File
    private var flowsForTraceROIs = arrayListOf<Flow>()
    private var sinksForTraceROIs = arrayListOf<Sink>()
    private var roisForTraceROIs = arrayListOf<ROI>()
    var numNewTraceROIFeeds = 0
    var deviceDatapointNumbers = hashMapOf<String, Int>()

    fun createExperimentStream(loadCameraConfig : Boolean){
        experimentStream = ExperimentStream(expConfig, loadCameraConfig)
    }

    fun convertGsonToConfig(configFile : File) : Boolean{
        return try {
            expConfig = gson.fromJson(FileReader(configFile), ExperimentConfiguration::class.java)
            loadedConfigFile = configFile
            true
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Failed to load experiment configuration ${configFile.absolutePath}, check file is present and syntax is correct")
            GUIMain.loggerService.log(Level.SEVERE, "Error message: ${ex.message}")
            false
        }
    }

    fun getChannelForSource(sourceChannelName : String) : Channel? {
        for(channel in expConfig.hardwareDevices.channelConfig.channels){
            if(sourceChannelName == channel.channelName){
                return channel
            }
        }

        return null
    }

    fun getSamplingFrequencyOfChannel(sourceChannelName : String) : Double{
        var sampFreq = 1.0
        for(channel in expConfig.hardwareDevices.channelConfig.channels){
            if(channel.channelName == sourceChannelName){
                val fullSampFreq = getSampleRateForTypeOfChannel(channel.type)
                sampFreq = fullSampFreq/channel.clockDiv //TODO consider rounding just in case?
                break
            }
        }

        return sampFreq
    }

    fun getSampleRateForTypeOfChannel(channelType : Int) : Double{
        return when(channelType){
            ExperimentConstants.ConfigurationProperties.ANALOGUE_IN_TYPE -> { return expConfig.hardwareDevices.postInitProperties.analogueInputSampleRate }
            ExperimentConstants.ConfigurationProperties.ANALOGUE_OUT_TYPE -> { return expConfig.hardwareDevices.postInitProperties.analogueOutputSampleRate }
            ExperimentConstants.ConfigurationProperties.DIGITAL_IN_TYPE -> { return expConfig.hardwareDevices.postInitProperties.digitalInputSampleRate }
            ExperimentConstants.ConfigurationProperties.DIGITAL_OUT_TYPE -> { return expConfig.hardwareDevices.postInitProperties.digitalOutputSampleRate }
            else -> 1.0
        }
    }

    fun getBlockSizeOfChannel(sourceChannelName : String) : Int{
        var blockSize = 1
        for(channel in expConfig.hardwareDevices.channelConfig.channels){
            if(channel.channelName == sourceChannelName){
                when(channel.type){
                    ExperimentConstants.ConfigurationProperties.ANALOGUE_IN_TYPE -> { blockSize = expConfig.hardwareDevices.postInitProperties.analogueInputBlockSize }
                    ExperimentConstants.ConfigurationProperties.ANALOGUE_OUT_TYPE -> { blockSize = expConfig.hardwareDevices.postInitProperties.analogueOutputBlockSize }
                    ExperimentConstants.ConfigurationProperties.DIGITAL_IN_TYPE -> { blockSize = expConfig.hardwareDevices.postInitProperties.digitalInputBlockSize }
                    ExperimentConstants.ConfigurationProperties.DIGITAL_OUT_TYPE -> { blockSize = expConfig.hardwareDevices.postInitProperties.digitalOutputBlockSize }
                }
                break
            }
        }

        return blockSize
    }

    fun createStreamGraph() : Boolean{
        val streamGraph = experimentStream.createStream(expConfig)
        return if(streamGraph != null) {
            loadedConfigurationStream = streamGraph
            true
        }
        else{
            GUIMain.loggerService.log(Level.SEVERE, "Failed to create Experiment stream")
            false
        }
    }

    /**
     * Begin streaming of all data based on specifications
     */
    fun runStream(liveAcquisition : Boolean){
        experimentStream.runStream(liveAcquisition)
    }

    /**
     * Create a new experiment config representing an experiment but with any trace ROIs. This does so by coping the
     * previous config first
     */
    fun createNewConfigFromExisting(){
        if(configWithTraceROIs == null) {
            configWithTraceROIs = expConfig
        }
    }

    /**
     * Add flow objects to list for use in creating new experiment config
     * @param flowList Any new flow objects to add
     */
    fun addFlowsToNewConfig(flowList : List<Flow>){
        flowsForTraceROIs.addAll(flowList)
        GUIMain.loggerService.log(Level.INFO, "Adding trace from ROI flows to new experiment config")
    }

    /**
     * Add sink objects to list for use in creating new experiment config
     * @param sinkList Any new sink objects to add
     */
    fun addSinksToNewConfig(sinkList : List<Sink>, windowOption : String, isNewTraceFeed: Boolean){
        if(windowOption == ExperimentConstants.ConfigurationProperties.NEW_WINDOW){
            sinksForTraceROIs.addAll(sinkList)
        }
        else{
            addInputNameToExistingSink(sinkList, windowOption, isNewTraceFeed)
        }
        GUIMain.loggerService.log(Level.INFO, "Adding trace from ROI sinks to new experiment config")
    }

    /**
     * This method will add information for newly added sinks to the new config (the new config will be used after the
     * user has finished specifying ROIs). It will determine what sink's input names to add to.
     * @param sinkList The list of newly created sinks
     * @param traceActorName The trace actor related to the new sinks
     * @param isNewTraceFeed Flag to say if the trace ROI is going to an existing trace feed sink or a newly created
     * trace feed sink
     */
    fun addInputNameToExistingSink(sinkList: List<Sink>, traceActorName : String, isNewTraceFeed : Boolean){
        if(isNewTraceFeed) {
            //This block will be used if the sink (trace from roi) is going to an existing window that has been created
            //from trace from ROI specification
            for(newTraceActor in experimentStream.newTraceROIActors) {
                val actorPrettyName = GUIMain.actorService.getActorPrettyName(newTraceActor.key.path().name())
                if(actorPrettyName.toLowerCase() == traceActorName.toLowerCase()) {
                    val sinksForActor = sinksForTraceROIs.filter { x ->
                        x.primaryDevice.toLowerCase() == newTraceActor.value.toLowerCase() &&
                        x.outputType == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE &&
                        x.actorPrettyName == actorPrettyName}

                    sinksForActor.forEach {
                        val additionalInputsForSink = sinkList.map { x -> x.inputNames }.flatten().distinct()
                        it.inputNames.addAll(additionalInputsForSink)
                    }
                }
            }
        }
        else{
            //This block will be used if the sink (trace from roi) is going to an existing trace window that existed
            //before any traces from ROIs were specified
            for (traceActor in experimentStream.traceActors) {
                val actorPrettyName = GUIMain.actorService.getActorPrettyName(traceActor.key.path().name())
                if (actorPrettyName.toLowerCase() == traceActorName.toLowerCase()) {
                    val sinksForActor = configWithTraceROIs!!.sinkConfig.sinks.filter { x ->
                        x.primaryDevice.toLowerCase() == traceActor.value.toLowerCase() &&
                        x.outputType == ExperimentConstants.ConfigurationProperties.TRACE_OUTPUT_TYPE &&
                        x.actorPrettyName == actorPrettyName}

                    sinksForActor.forEach {
                        val additionalInputsForSink = sinkList.map { x -> x.inputNames }.flatten().distinct()
                        it.inputNames.addAll(additionalInputsForSink)
                    }
                }
            }
        }
    }

    /**
     * Add ROI objects to list for use in creating new experiment config
     * @param roiList Any new ROI objects to add
     */
    fun addROIsToNewConfig(roiList : List<ROI>){
        roisForTraceROIs.addAll(roiList)
        GUIMain.loggerService.log(Level.INFO, "Adding trace ROIs to new experiment config")
        numNewTraceROIFeeds++
    }

    fun addSizesToNewConfig(){
        for(resizeSet in GUIMain.strimmUIService.cameraViewSizeList){
            val deviceSources = configWithTraceROIs!!.sourceConfig.sources.filter { x -> x.deviceLabel == resizeSet.key }
            for(deviceSource in deviceSources){
                deviceSource.x = resizeSet.value.x!!.toDouble()
                deviceSource.y = resizeSet.value.y!!.toDouble()
                deviceSource.w = resizeSet.value.w!!.toDouble()
                deviceSource.h = resizeSet.value.h!!.toDouble()
            }
        }
    }

    /**
     * From the new stream components that have been specified by the user, create a new experiment JSON config and
     * write this to file.
     * @return The newly created experiment config file
     */
    fun writeNewConfig() : File {
        configWithTraceROIs!!.ROIAdHoc = "False"
        configWithTraceROIs!!.experimentConfigurationName += "_WithTraceROIs"

        //Populate new flows
        if(configWithTraceROIs!!.flowConfig.flows.isEmpty()) {
            configWithTraceROIs!!.flowConfig.flows = flowsForTraceROIs
        }
        else{
            configWithTraceROIs!!.flowConfig.flows.addAll(flowsForTraceROIs)
        }

        //Populate new sinks
        if(configWithTraceROIs!!.sinkConfig.sinks.isEmpty()) {
            configWithTraceROIs!!.sinkConfig.sinks = sinksForTraceROIs
        }
        else{
            configWithTraceROIs!!.sinkConfig.sinks.addAll(sinksForTraceROIs)
        }

        //Populate new ROIs
        if(configWithTraceROIs!!.roiConfig.rois.isEmpty()) {
            configWithTraceROIs!!.roiConfig.rois = roisForTraceROIs
        }
        else{
            configWithTraceROIs!!.roiConfig.rois.addAll(roisForTraceROIs)
        }


        //Account for any resizes that have been done
        addSizesToNewConfig()

        val pathAndName = loadedConfigFile.canonicalPath.replace(loadedConfigFile.name, loadedConfigFile.name.replace(".json", "") + "_WithTraceROIs.json")
        val newFile = File(pathAndName)

        if(configWithTraceROIs != null) {
            try {
                val writer = FileWriter(newFile)
                GUIMain.loggerService.log(Level.INFO, "Writing experiment config with trace ROI info")
                gson.toJson(configWithTraceROIs!!, writer)
                writer.flush()
                writer.close()
            }
            catch(ex : Exception){
                GUIMain.loggerService.log(Level.SEVERE, "Error in writing new experiment config to file. Error message: ${ex.message}")
                GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
            }
        }

        return newFile
    }

    /**
     * Method to make sure each ROI has a unique name
     * @param sourceName The name of the source the ROI is related to
     * @return A unique trace ROI name
     */
    fun makeUniqueROIName(sourceName : String) : String{
        val roisForSource = roisForTraceROIs.filter { x -> x.ROIName.toLowerCase().contains(sourceName.toLowerCase()) }
        return sourceName + "TraceROI" + (roisForSource.size+1).toString()
    }

    /**
     * Method to make sure each flow created from a trace from ROI has a unique name
     * @param flowName The name of the flow the ROI is related to
     * @return A unique flow name
     */
    fun makeUniqueFlowName(flowName : String) : String{
        val flows = flowsForTraceROIs.filter { x -> x.flowName.toLowerCase().contains(flowName.toLowerCase()) }
        return flowName + (flows.size+1).toString()
    }

    /**
     * Method to make sure each sink relating to a trace from ROI has a unique name
     * @param sinkName The name of the sink the ROI is related to
     * @return A unique sink name
     */
    fun makeUniqueSinkName(sinkName : String) : String{
        val sinks = sinksForTraceROIs.filter { x -> x.sinkName.toLowerCase().contains(sinkName.toLowerCase()) }
        return sinkName + (sinks.size+1).toString()
    }

    /**
     * This method is run when the trace from ROI specification is complete. This will write the newly modified config
     * (which includes any trace from ROI components), stop the current stream (stream, actors and windows), then go
     * through the experiment loading and running process again starting at the loading of the JSON config
     */
    fun stopAndLoadNewExperiment(writeNewConfig : Boolean){
        GUIMain.loggerService.log(Level.INFO, "Loading new experiment with trace roi modifications")

        if(writeNewConfig) {
            //Write the new config with the new trace from ROI changes
            val newFile = GUIMain.experimentService.writeNewConfig()
            //Load the new config
            convertGsonToConfig(newFile)
        }

        //Stop the current stream's actors
        //This will send a kill message to each actor, thus effectively stopping the whole experiment stream
        experimentStream.stopDisplayingData()
        experimentStream.stopStoringData()

        //Close and remove the dockable window plugins
        val cameraWindowPlugins = GUIMain.dockableWindowPluginService.getPluginsOfType(CameraWindowPlugin::class.java)
        cameraWindowPlugins.forEach { x ->
            x.value.close()
            GUIMain.dockableWindowPluginService.removeDockableWindowPlugin(x.key) }

        val traceWindowPlugins = GUIMain.dockableWindowPluginService.getPluginsOfType(TraceWindowPlugin::class.java)
        traceWindowPlugins.forEach { x ->
            x.value.close()
            GUIMain.dockableWindowPluginService.removeDockableWindowPlugin(x.key)
        }

        //Create the stream graph
        createExperimentStream(false)
        val createSuccess = createStreamGraph()

        //Run the stream graph
        runStream(true)

        GUIMain.roiSpecCompleteButton.isEnabled = false
        GUIMain.startExperimentButton.isEnabled = false
        GUIMain.stopExperimentButton.isEnabled = true
    }

    fun calculateNumberOfDataPointsFromInterval(deviceName : String, interval : Double){
        val numDataPoints = BigDecimal(experimentStream.durationMs/interval).setScale(1, RoundingMode.FLOOR).toInt()
        deviceDatapointNumbers[deviceName] = numDataPoints
        GUIMain.loggerService.log(Level.INFO, "Device $deviceName should have $numDataPoints data points")
    }

    fun calculateNumberOfDataPointsFromFrequency(deviceName : String, samplingFrequencyHz : Double){
        val durationInSeconds = experimentStream.durationMs.toDouble()/1000.0
        val numDataPoints = BigDecimal(durationInSeconds*samplingFrequencyHz).setScale(1, RoundingMode.FLOOR).toInt()
        deviceDatapointNumbers[deviceName] = numDataPoints
        GUIMain.loggerService.log(Level.INFO, "Device $deviceName should have $numDataPoints data points")
    }

    fun loadPreviousExperiment(selectedDirectory : File){
        val tifFiles = FileUtils.listFiles(selectedDirectory,arrayOf("tif"),true)
        var tifFile : File? = null
        if(tifFiles.isNotEmpty()){
            if(tifFiles.size > 1){
                JOptionPane.showMessageDialog(GUIMain.strimmUIService.strimmFrame, "Found multiple .tif files, choosing first one alphabetically", "Multiple .tif files", JOptionPane.WARNING_MESSAGE)
            }

            tifFile = tifFiles.first()
        }

        var cameraMetadataFile : File? = null
        var traceDataFile : File? = null

        val csvFiles = FileUtils.listFiles(selectedDirectory,arrayOf("csv"),true)
        for(csvFile in csvFiles){
            if(csvFile.name.contains(Paths.TRACE_DATA_PREFIX)){
                traceDataFile = csvFile
            }

            if(csvFile.name.contains(Paths.CAMERA_METADATA_PREFIX)){
                cameraMetadataFile = csvFile
            }
        }

        var missingFile = false
        var missingFilesMessage = "Could not find the following files:\n"
        var loadTif = true
        var loadCameraMetadata = true
        var loadTraceData = true
        if(tifFile == null){
            missingFilesMessage += "- Image file (.tif).\n"
            missingFile = true
            loadTif = false
        }
        if(cameraMetadataFile == null){
            missingFilesMessage += "- Camera metadata file (csv file with name containing \"${Paths.CAMERA_METADATA_PREFIX}\").\n"
            missingFile = true
            loadCameraMetadata = false
        }
        if(traceDataFile == null){
            missingFilesMessage += "- Trace data file (csv file with name containing \"${Paths.TRACE_DATA_PREFIX}\").\n"
            missingFile = true
            loadTraceData = false
        }

        if(missingFile) {
            missingFilesMessage += "If this is expected please ignore this message."
            JOptionPane.showMessageDialog(GUIMain.strimmUIService.strimmFrame, missingFilesMessage, "Missing files", JOptionPane.WARNING_MESSAGE)
        }

        var newCameraWindowPlugin : CameraWindowPlugin? = null
        if(loadTif) {
            newCameraWindowPlugin = GUIMain.importService.importTif(tifFile!!)
        }

        if(loadTraceData) {
            GUIMain.importService.importTraceData(traceDataFile!!)
        }

        if(loadCameraMetadata) {
            GUIMain.importService.importCameraMetadata(cameraMetadataFile!!)
        }

        if(newCameraWindowPlugin != null){
            java.awt.EventQueue.invokeLater {
                Thread(Runnable {
                    newCameraWindowPlugin.cameraWindowController.pressMinusButton()
                    GUIMain.strimmUIService.redrawROIs(newCameraWindowPlugin, true)
                    newCameraWindowPlugin.cameraWindowController.addSliderListener()
                }).start()
            }
        }
    }
}