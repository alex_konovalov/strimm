package uk.co.strimm.services

import net.imagej.ImageJService
import net.imagej.overlay.EllipseOverlay
import net.imagej.overlay.Overlay
import net.imagej.overlay.PolygonOverlay
import net.imagej.overlay.RectangleOverlay
import net.imglib2.Cursor
import net.imglib2.img.array.ArrayImg
import net.imglib2.img.array.ArrayImgs
import net.imglib2.roi.Masks
import net.imglib2.roi.Regions
import net.imglib2.roi.geom.GeomMasks
import net.imglib2.type.numeric.complex.AbstractComplexType
import net.imglib2.type.numeric.integer.UnsignedByteType
import net.imglib2.type.numeric.integer.UnsignedShortType
import net.imglib2.type.numeric.real.FloatType
import net.imglib2.util.Intervals
import net.imglib2.view.Views
import org.scijava.plugin.Plugin
import org.scijava.service.AbstractService
import org.scijava.service.Service
import uk.co.strimm.*
import uk.co.strimm.MicroManager.MMCameraDevice
import uk.co.strimm.gui.GUIMain
import java.util.*
import java.util.logging.Level
import kotlin.NoSuchElementException

@Plugin(type = Service::class)
class AcquisitionMethodService : AbstractService(), ImageJService {
    interface AcquisitionMethod{
        var name : String
        var description : String
        fun runMethod(vararg inputs : Any) : Any
    }

    abstract class TraceMethod : AcquisitionMethod{
        override fun runMethod(vararg inputs: Any): java.util.ArrayList<TraceData> {
            return arrayListOf(TraceData(Pair(null,0.0),12345))//TODO dummy
        }
    }

    abstract class ImageMethod : AcquisitionMethod{
        override fun runMethod(vararg inputs: Any): STRIMMImage {
            return STRIMMImage("",null,12345) //TODO dummy
        }
    }

    /**
     * Calculate the average pixel intensity for any combination of ROIs (overlays) on an image
     * @param name The name used to identify this method. Must be unique
     * @param description A short description of what the method does
     */
    private class AverageROI(override var name: String, override var description: String) : TraceMethod(){
        /**
         * The top level method that is actually run for this acquisition method
         * @param inputs inputs[0] is the STRIMMImage object. Inputs [1] is the name of the Akka flow (@see ExperimentStream)
         */
        override fun runMethod(vararg inputs: Any): java.util.ArrayList<TraceData> {
            val image = inputs[0] as STRIMMImage
            val flowName = inputs[1] as String

            //Match the device name
            val overlaysForDevice = GUIMain.actorService.routedRoiList.filter { x -> x.value.second.toLowerCase() == flowName.toLowerCase() }

            val arrayListToReturn = arrayListOf<TraceData>()
            if(overlaysForDevice.isNotEmpty()) {
                for (deviceOverlay in overlaysForDevice) {
                    arrayListToReturn.add(averageROI(deviceOverlay.key, image))
                }
            }
            return arrayListToReturn
        }

        /**
         * Take an ImageJ overlay and iterate through every pixel in this overlay. Take all the pixel values and then
         * calculate the average.
         * @param roi The ImageJ overlay
         * @param image The snapped image from the camera feed
         * @return A TraceData object containing the averaged value and the ROI object
         */
        private fun averageROI(roi : Overlay, image : STRIMMImage) : TraceData {
            val x: Double
            val y: Double
            val w: Double
            val h: Double
            var viewCursor : Cursor<out AbstractComplexType<*>>? = null

            when (roi) {
                is EllipseOverlay -> {
                    try {
                        x = roi.getOrigin(0)
                        y = roi.getOrigin(1)
                        w = roi.getRadius(0) //remember its radius, not width
                        h = roi.getRadius(1) //remember its radius, not width
                        val imgSizeFull= GUIMain.strimmUIService.cameraSizeList[image.sourceCamera]!!

                        //Masks and interval lines below taken from https://forum.image.sc/t/examples-of-usage-of-imglib2-roi/22855/14
                        when(image.pix){
                            is ByteArray ->{
                                //The image provided in this method will come in full size, check if a resize is needed before beginning the ROI calculation
                                val arrayImg : ArrayImg<UnsignedByteType, net.imglib2.img.basictypeaccess.array.ByteArray>
                                arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                    val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                    val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetByteArray(image, resizeVals)
                                    ArrayImgs.unsignedBytes(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                } else{
                                    ArrayImgs.unsignedBytes(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                }

                                //Proceed with iterating over a cursor for the ROI
                                val ellipsoidMask = GeomMasks.closedEllipsoid(doubleArrayOf(x,y),doubleArrayOf(w,h))
                                val ellipsoidInterval = Intervals.largestContainedInterval(ellipsoidMask)
                                val maskRRA = Masks.toRealRandomAccessible(ellipsoidMask)
                                val interval = Views.interval(Views.raster(maskRRA), ellipsoidInterval)
                                val iterableRegion = Regions.iterable(interval)
                                viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                            }
                            is ShortArray ->{
                                //The image provided in this method will come in full size, check if a resize is needed before beginning the ROI calculation
                                val arrayImg : ArrayImg<UnsignedShortType, net.imglib2.img.basictypeaccess.array.ShortArray>
                                arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                    val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                    val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetShortArray(image, resizeVals)
                                    ArrayImgs.unsignedShorts(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                } else{
                                    ArrayImgs.unsignedShorts(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                }

                                //Proceed with iterating over a cursor for the ROI
                                val ellipsoidMask = GeomMasks.closedEllipsoid(doubleArrayOf(x,y),doubleArrayOf(w,h))
                                val ellipsoidInterval = Intervals.largestContainedInterval(ellipsoidMask)
                                val maskRRA = Masks.toRealRandomAccessible(ellipsoidMask)
                                val interval = Views.interval(Views.raster(maskRRA), ellipsoidInterval)
                                val iterableRegion = Regions.iterable(interval)
                                viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                            }
                            is FloatArray ->{
                                //The image provided in this method will come in full size, check if a resize is needed before beginning the ROI calculation
                                val arrayImg : ArrayImg<FloatType, net.imglib2.img.basictypeaccess.array.FloatArray>
                                arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                    val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                    val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetFloatArray(image, resizeVals)
                                    ArrayImgs.floats(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                } else{
                                    ArrayImgs.floats(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                }

                                //Proceed with iterating over a cursor for the ROI
                                val ellipsoidMask = GeomMasks.closedEllipsoid(doubleArrayOf(x,y),doubleArrayOf(w,h))
                                val ellipsoidInterval = Intervals.largestContainedInterval(ellipsoidMask)
                                val maskRRA = Masks.toRealRandomAccessible(ellipsoidMask)
                                val interval = Views.interval(Views.raster(maskRRA), ellipsoidInterval)
                                val iterableRegion = Regions.iterable(interval)
                                viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                            }
                        }

                    } catch (ex: Exception) {
                        GUIMain.loggerService.log(java.util.logging.Level.SEVERE, "Could not read ellipse overlay data. Error: " + ex.message)
                    }
                }
                is RectangleOverlay -> {
                    try {
                        x = roi.getOrigin(0)
                        y = roi.getOrigin(1)
                        w = roi.getExtent(0)
                        h = roi.getExtent(1)

                        val imgSizeFull= GUIMain.strimmUIService.cameraSizeList[image.sourceCamera]!!

                        //Masks and interval lines below taken from https://forum.image.sc/t/examples-of-usage-of-imglib2-roi/22855/14
                        when(image.pix) {
                            is ByteArray -> {
                                //The image provided in this method will come in full size, check if a resize is needed before beginning the ROI calculation
                                val arrayImg : ArrayImg<UnsignedByteType, net.imglib2.img.basictypeaccess.array.ByteArray>
                                arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                    val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                    val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetByteArray(image, resizeVals)
                                    ArrayImgs.unsignedBytes(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                } else{
                                    ArrayImgs.unsignedBytes(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                }

                                //Proceed with iterating over a cursor for the ROI
                                val maskExtentX = (x+w)-1
                                val maskExtentY = (h+y)-1
                                val boxMask = GeomMasks.closedBox(doubleArrayOf(x,y),doubleArrayOf(maskExtentX,maskExtentY))
                                val boxInterval = Intervals.largestContainedInterval(boxMask)
                                val maskRRA = Masks.toRealRandomAccessible(boxMask)
                                val interval = Views.interval(Views.raster(maskRRA), boxInterval)
                                val iterableRegion = Regions.iterable(interval)
                                viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                            }
                            is ShortArray -> {
                                //The image provided in this method will come in full size, check if a resize is needed before beginning the ROI calculation
                                val arrayImg : ArrayImg<UnsignedShortType, net.imglib2.img.basictypeaccess.array.ShortArray>
                                arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                    val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                    val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetShortArray(image, resizeVals)
                                    ArrayImgs.unsignedShorts(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                } else{
                                    ArrayImgs.unsignedShorts(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                }

                                //Proceed with iterating over a cursor for the ROI
                                val maskExtentX = (x+w)-1
                                val maskExtentY = (h+y)-1
                                val boxMask = GeomMasks.closedBox(doubleArrayOf(x,y),doubleArrayOf(maskExtentX,maskExtentY))
                                val boxInterval = Intervals.largestContainedInterval(boxMask)
                                val maskRRA = Masks.toRealRandomAccessible(boxMask)
                                val interval = Views.interval(Views.raster(maskRRA), boxInterval)
                                val iterableRegion = Regions.iterable(interval)
                                viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                            }
                            is FloatArray -> {
                                //The image provided in this method will come in full size, check if a resize is needed before beginning the ROI calculation
                                val arrayImg : ArrayImg<FloatType, net.imglib2.img.basictypeaccess.array.FloatArray>
                                arrayImg = if(image.sourceCamera in GUIMain.strimmUIService.cameraViewSizeList.keys){
                                    val resizeVals = GUIMain.strimmUIService.cameraViewSizeList[image.sourceCamera]!!
                                    val resizedImage = GUIMain.acquisitionMethodService.getImageSubsetFloatArray(image, resizeVals)
                                    ArrayImgs.floats(resizedImage,resizeVals.w!!.toLong(), resizeVals.h!!.toLong())
                                } else{
                                    ArrayImgs.floats(image.pix, imgSizeFull.first!!.toLong(), imgSizeFull.second!!.toLong())
                                }

                                //Proceed with iterating over a cursor for the ROI
                                val maskExtentX = (x+w)-1
                                val maskExtentY = (h+y)-1
                                val boxMask = GeomMasks.closedBox(doubleArrayOf(x,y),doubleArrayOf(maskExtentX,maskExtentY))
                                val boxInterval = Intervals.largestContainedInterval(boxMask)
                                val maskRRA = Masks.toRealRandomAccessible(boxMask)
                                val interval = Views.interval(Views.raster(maskRRA), boxInterval)
                                val iterableRegion = Regions.iterable(interval)
                                viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                            }
                        }
                    } catch (ex: Exception) {
                        GUIMain.loggerService.log(java.util.logging.Level.SEVERE, "Could not read rectangle overlay data. Error: " + ex.message)
                    }

                }
                is PolygonOverlay -> {
                    //TODO this is a bit more complicated
                }
            }

            val average = calculateAverage(viewCursor)
            return TraceData(Pair(roi, average), image.timeAcquired)
        }

        /**
         * Calculate the average of all the pixels in an ROI.
         * @param viewCursor The object representing a cursor that will iterate over all the pixels within the ROI
         * @return The average pixel intensity over the ROI. If pixel intensity is -999 then something has gone wrong
         */
        private fun calculateAverage(viewCursor : Cursor<out AbstractComplexType<*>>?) : Double{
            var average = -999.0

            try {
                var total = 0.0
                var totalPixels = 0

                while (viewCursor!!.hasNext()) {
                    //.next() does a .fwd() and a .get()
                    val value = viewCursor.next()
                    val pixelValue = value.getRealDouble()
                    total += pixelValue
                    totalPixels++
                }

                average = (total / totalPixels)

            } catch (ex: Exception) {
                GUIMain.loggerService.log(Level.SEVERE, "Could not iterate through overlay data. Error: " + ex.message)
                GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
            }

            return average
        }
    }

    /**
     * Get the next image from a camera device
     * @param name The name used to identify this method. Must be unique
     * @param description A short description of what the method does
     */
    private class SnapImage(override var name: String, override var description: String) : ImageMethod(){
        private var firstRun = true
        private var isTriggered = false
        private var clockCounter = 0.0

        /**
         * The top level method that is actually run for this acquisition method
         * @param inputs inputs[0] the camera device label. inputs[1] is a list of all camera devices with basic
         * information
         */
        override fun runMethod(vararg inputs: Any): STRIMMImage {
            val cameraLabel = inputs[0] as String
            val cameraDevices = inputs[1] as ArrayList<CameraDeviceInfo>
            val intervalMs = inputs[2] as Int
            val cameraEntry = cameraDevices.filter { x -> x.device.label == cameraLabel }
            val camToUse = cameraEntry.map { x -> x.device }.first()

            if(firstRun){
                setSnapMode(camToUse)
                firstRun = false
            }

            while (!camToUse.isImageAvailable) { }
            val image = camToUse.snapImage()

            val timeSnapped: Double
            if(isTriggered){
                timeSnapped = clockCounter
                clockCounter += intervalMs
            }
            else{
                timeSnapped = image.second
            }

            return STRIMMImage(cameraLabel, image.first, timeSnapped)
        }

        private fun setSnapMode(cam : MMCameraDevice){
            try {
                val triggerModeProperty = cam.getProperties().first { x -> x.name == "TriggerMode" }//TODO hardcoded
                if(triggerModeProperty.getValue() == "Strobed"){//TODO hardcoded
                    isTriggered = true
                }
            }
            catch(ex : NoSuchElementException){
                GUIMain.loggerService.log(Level.WARNING, "Camera has no \"TriggerMode\" property")
                isTriggered = false
            }
        }
    }

    /**
     * Generate an image not necessarily from a camera device
     * @param name The name used to identify this method. Must be unique
     * @param description A short description of what the method does
     */
    private class GenerateImage(override var name: String, override var description: String) : ImageMethod(){
        override fun runMethod(vararg inputs: Any): STRIMMImage {
            //TODO Dummy for now. Whem might we use such a method?
            return STRIMMImage("","", 0.0)
        }
    }

    /**
     * Get trace data from a trace device
     * @param name The name used to identify this method. Must be unique
     * @param description A short description of what the method does
     */
    private class GetTraceData(override var name: String, override var description: String) : TraceMethod(){
        private var clockCounter = 0.0
        override fun runMethod(vararg inputs: Any): java.util.ArrayList<TraceData> {
            val dummyOverlay = inputs[0] as Overlay
            val intervalSecs = inputs[1] as Double
            val traceDataList = arrayListOf<TraceData>()

//            for(i in 0 until 100) {
                try {
//                    val traceVal = GUIMain.experimentService.analogueBuffer.removeFirst()
//                    traceDataList.add(TraceData(Pair(dummyOverlay, traceVal), clockCounter*1000))
                } catch (ex: Exception) {
                    //println("Error. buffer size is ${GUIMain.experimentService.analogueBuffer.size}")
                }

                clockCounter += intervalSecs
//            }

//            println("Got some data, clock counter: $clockCounter")
            return traceDataList
        }
    }

    var acquisitionMethods : ArrayList<AcquisitionMethod> = arrayListOf()

    init {
        registerDefaultMethods()
        registerCustomMethods()
    }

    private fun registerDefaultMethods(){
        val averageROIMethod = AverageROI(ExperimentConstants.Acquisition.AVERAGE_ROI_METHOD_NAME,"Average the pixel intensity any ROIs (overlays) on a given image")
        val snapImageMethod = SnapImage(ExperimentConstants.Acquisition.SNAP_IMAGE_METHOD_NAME,"Get the next available image from a camera device")
        val generateImageMethod = GenerateImage(ExperimentConstants.Acquisition.GENERATE_IMAGE_METHOD_NAME,"Generate an image")
        val getTraceDataMethod = GetTraceData(ExperimentConstants.Acquisition.GET_TRACE_DATA_METHOD_NAME,"Get data from a trace source or flow")
        registerAcquisitionMethod(averageROIMethod)
        registerAcquisitionMethod(snapImageMethod)
        registerAcquisitionMethod(generateImageMethod)
        registerAcquisitionMethod(getTraceDataMethod)
    }

    private fun registerCustomMethods(){
        //TODO load classes for custom methods (as plugins) from a specified folder
    }

    fun registerAcquisitionMethod(acquisitionMethod: AcquisitionMethod){
        if(isAcquisitionMethodNameUnique(acquisitionMethod.name)) {
            acquisitionMethods.add(acquisitionMethod)
            println("Registered acquisition method ${acquisitionMethod.name}")
        }
        else{
            println("Acquisition method ${acquisitionMethod.name} is not unique and has not been registered")
        }
    }

    private fun isAcquisitionMethodNameUnique(acquisitionMethodName : String):Boolean{
        return !acquisitionMethods.any { x -> x.name ==  acquisitionMethodName}
    }

    fun getAcquisitionMethod(acquisitionMethodName : String) : AcquisitionMethod?{
        return try {
            val method = acquisitionMethods.first { x -> x.name == acquisitionMethodName }
            val inst = method.javaClass.getConstructor(String::class.java, String::class.java)
            inst.newInstance(method.name, method.description)
        }
        catch(ex : Exception){
            GUIMain.loggerService.log(Level.SEVERE, "Could not find acquisition method called $acquisitionMethodName")
            GUIMain.loggerService.log(Level.SEVERE, ex.stackTrace)
            null
        }
    }

    fun getImageSubsetByteArray(fullSizeImage : STRIMMImage, resizeValues : ResizeValues) : kotlin.ByteArray?{
        val imgSize = GUIMain.strimmUIService.cameraSizeList[fullSizeImage.sourceCamera]
        when(fullSizeImage.pix){
            is ByteArray ->{
                val arrayImg = ArrayImgs.unsignedBytes(fullSizeImage.pix, imgSize!!.first!!.toLong(), imgSize.second!!.toLong())
                val resizedArray = arrayListOf<Byte>()
                val maskExtentX = (resizeValues.w!!.toDouble()+resizeValues.x!!.toDouble())-1
                val maskExtentY = (resizeValues.h!!.toDouble()+resizeValues.y!!.toDouble())-1
                val boxMask = GeomMasks.closedBox(doubleArrayOf(resizeValues.x.toDouble(),resizeValues.y.toDouble()),doubleArrayOf(maskExtentX,maskExtentY))
                val boxInterval = Intervals.largestContainedInterval(boxMask)
                val maskRRA = Masks.toRealRandomAccessible(boxMask)
                val interval = Views.interval(Views.raster(maskRRA), boxInterval)
                val iterableRegion = Regions.iterable(interval)
                val viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                var pixelCounter = 0
                while(viewCursor.hasNext()){
                    val value = viewCursor.next()
                    resizedArray.add(value.realDouble.toByte())
                    pixelCounter++
                }
                return resizedArray.toByteArray()
            }
            else -> return null
        }
    }

    fun getImageSubsetShortArray(fullSizeImage : STRIMMImage, resizeValues : ResizeValues) : kotlin.ShortArray?{
        val imgSize = GUIMain.strimmUIService.cameraSizeList[fullSizeImage.sourceCamera]
        when(fullSizeImage.pix){
            is ShortArray ->{
                val arrayImg = ArrayImgs.unsignedShorts(fullSizeImage.pix, imgSize!!.first!!.toLong(), imgSize.second!!.toLong())
                val resizedArray = arrayListOf<Short>()
                val maskExtentX = (resizeValues.w!!.toDouble()+resizeValues.x!!.toDouble())-1
                val maskExtentY = (resizeValues.h!!.toDouble()+resizeValues.y!!.toDouble())-1
                val boxMask = GeomMasks.closedBox(doubleArrayOf(resizeValues.x.toDouble(),resizeValues.y.toDouble()),doubleArrayOf(maskExtentX,maskExtentY))
                val boxInterval = Intervals.largestContainedInterval(boxMask)
                val maskRRA = Masks.toRealRandomAccessible(boxMask)
                val interval = Views.interval(Views.raster(maskRRA), boxInterval)
                val iterableRegion = Regions.iterable(interval)
                val viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                var pixelCounter = 0
                while(viewCursor.hasNext()){
                    val value = viewCursor.next()
                    resizedArray.add(value.realDouble.toShort())
                    pixelCounter++
                }
                return resizedArray.toShortArray()
            }
            else -> return null
        }
    }

    fun getImageSubsetFloatArray(fullSizeImage : STRIMMImage, resizeValues : ResizeValues) : kotlin.FloatArray?{
        val imgSize = GUIMain.strimmUIService.cameraSizeList[fullSizeImage.sourceCamera]
        when(fullSizeImage.pix){
            is FloatArray ->{
                val arrayImg = ArrayImgs.floats(fullSizeImage.pix, imgSize!!.first!!.toLong(), imgSize.second!!.toLong())
                val resizedArray = arrayListOf<Float>()
                val maskExtentX = (resizeValues.w!!.toDouble()+resizeValues.x!!.toDouble())-1
                val maskExtentY = (resizeValues.h!!.toDouble()+resizeValues.y!!.toDouble())-1
                val boxMask = GeomMasks.closedBox(doubleArrayOf(resizeValues.x.toDouble(),resizeValues.y.toDouble()),doubleArrayOf(maskExtentX,maskExtentY))
                val boxInterval = Intervals.largestContainedInterval(boxMask)
                val maskRRA = Masks.toRealRandomAccessible(boxMask)
                val interval = Views.interval(Views.raster(maskRRA), boxInterval)
                val iterableRegion = Regions.iterable(interval)
                val viewCursor = Regions.sample(iterableRegion, arrayImg).cursor()
                var pixelCounter = 0
                while(viewCursor.hasNext()){
                    val value = viewCursor.next()
                    resizedArray.add(value.realDouble.toFloat())
                    pixelCounter++
                }
                return resizedArray.toFloatArray()
            }
            else -> return null
        }
    }
}