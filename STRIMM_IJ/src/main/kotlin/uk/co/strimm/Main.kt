package uk.co.strimm

import net.imagej.ImageJ
import uk.co.strimm.gui.GUIMain
import java.net.URLClassLoader
import uk.co.strimm.Timer
import uk.co.strimm.TimerResult

/**
 * Main class. Everything starts here
 */
open class Main{
    companion object {
        /**
         * Main method.
         *
         * @param args the array of arguments
         */
        @JvmStatic
        fun main(args: Array<String>) {
            /*val tmp = (System.getProperty("user.dir") + "/DAQs/STRIMM_KotlinWrap").apply(::println)

            val runtime = STRIMM_JNI(tmp)
            if (!runtime.valid) return

            println("\n*************** Test Init ***************")
            var res = runtime.initialise()
            if (res == TimerResult.Error)
                println(runtime.getLastError())
            else
                println("Success!")

            println("\n*************** Test Get Installed Devices ***************")
            runtime.addInstallDirectory(System.getProperty("user.dir") + "/DAQs/")
            val devices = runtime.getInstalledDevices()
            devices?.forEach(::println)

            println("\n*************** Test De-init ***************")
            res = runtime.deinitialise()
            if (res == TimerResult.Error)
                println(runtime.getLastError())
            else
                println("Success!")*/



            val ij = ImageJ()
            ij.launch(".")
            ij.command().run(GUIMain::class.java, true)
        }
    }
}